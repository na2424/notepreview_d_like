﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace EmmyTypeGenerator
{
	public static class Generator
    {
        /// <summary>
        /// 该文件只用来给ide进行lua类型提示的,不要在运行时require该文件或者打包到版本中.
        /// </summary>
        private static string TypeDefineFilePath
        {
            get { return Directory.GetCurrentDirectory() + "/_unity_types.lua"; }
        }

        private static string[] supportNameSpaceList = new string[]{
                "Unity",
                "CS",
                //"DC",
                //"DG",
                "DankagLikeLuaAPI",
			};

        private static HashSet<Type> luaNumberTypeSet = new HashSet<Type>
        {
            typeof(byte),
            typeof(sbyte),
            typeof(short),
            typeof(ushort),
            typeof(int),
            typeof(uint),
            typeof(long),
            typeof(ulong),
            typeof(float),
            typeof(double)
        };

        private static HashSet<string> luaKeywordSet = new HashSet<string>
        {
            "and",
            "break",
            "do",
            "else",
            "elseif",
            "end",
            "false",
            "for",
            "function",
            "if",
            "in",
            "local",
            "nil",
            "not",
            "or",
            "repeat",
            "return",
            "then",
            "true",
            "until",
            "while"
        };

        public static StringBuilder sb = new StringBuilder(1024);
        private static StringBuilder tempSb = new StringBuilder(1024);
        private static List<Type> exportTypeList = new List<Type>();

        private static Dictionary<Type, List<MethodInfo>>
            extensionMethodsDic = new Dictionary<Type, List<MethodInfo>>();

        [MenuItem("DC/Lua/EmmyTypeGenerate")]
        public static void GenerateEmmyTypeFiles()
        {
            var set = CollectAllExportType();
            exportTypeList.AddRange(set);

            HandleExtensionMethods();

            GenerateTypeDefines();

            AssetDatabase.Refresh();
            Debug.Log("Generate lau snippet Complete!");
        }

        [MenuItem("DC/Lua/TestGene")]
        public static void Test()
        {
            var v3Type = typeof(Vector3);
            var assembly = v3Type.Assembly;
            HashSet<Type> set = CollectAllExportType();
            var array = set.ToArray();
            var hasV3 = set.Contains(v3Type);
            var export = IsExportType(v3Type);
        }

        private static HashSet<Type> CollectAllExportType()
        {
            //收集要导出的类型
            var allAssembly = AppDomain.CurrentDomain.GetAssemblies();
            //去重复
            var set = new HashSet<Type>();
            foreach (var assemblyInst in allAssembly)
            {
                Type[] collection = CollectType(assemblyInst);
                foreach (var typeInst in collection)
                {
                    if (!set.Contains(typeInst))
                    {
                        set.Add(typeInst);
                    }
                }
            }
            return set;
        }

        public static bool IsExportType(Type item)
        {
            for (int i = 0; i < supportNameSpaceList.Length; i++)
            {
                string itemNamespace = item.Namespace;
                
                if (string.IsNullOrEmpty(itemNamespace))
                {
                    if (item.FullName.Contains("Interop"))
                    {
                        return false;
                    }
                    if (item.FullName.Contains("PrivateImpl"))
                    {
                        return false;
                    }
                    if (item.FullName.Contains("__"))
                    {
                        return false;
                    }
                    if (item.FullName.Contains("CriAtom"))
                    {
						return false;
					}
					if (item.FullName.Contains("CriWare"))
					{
						return false;
					}
					if (item.FullName.Contains("CriProfiler"))
					{
						return false;
					}

					if (item.FullName.Contains("CS.Cri"))
					{
						return false;
					}

					if (item.FullName.Contains("Consts"))
					{
						return false;
					}

					if (item.FullName.Contains("TextEditor"))
					{
						return false;
					}

					if (item.FullName.Contains("Assembly"))
					{
						return false;
					}

					if (item.FullName.Contains("SRMath"))
					{
						return false;
					}

					if (item.FullName.Contains("Android"))
					{
						return false;
					}

					if (item.FullName.Contains("iOS"))
					{
						return false;
					}

					if (item.FullName.Contains("Locale"))
					{
						return false;
					}

					if (item.FullName.CustomEndsWith(".c"))
					{
						return false;
					}

                    

					//很多i,j,p,o这样的类占用空间浪费时间，类名少于3直接干掉
					return item.Name.Length > 2;
                }
                else
                {
                    return true;
                    if (itemNamespace.Contains("PrivateImplementationDetails"))
                    {
                        return false;
                    }
                    //不要编辑器
                    if (itemNamespace.CustomStartsWith("UnityEditor"))
                    {
                        return false;
                    }
                    if (itemNamespace.CustomStartsWith("System"))
                    {
                        return false;
                    }
                    if (itemNamespace.CustomStartsWith("AI"))
					{
						return false;
					}
					if (itemNamespace.Contains("Diagnostics"))
					{
						return false;
					}
					if (itemNamespace.Contains("Device"))
                    {
						return false;
					}
					if (itemNamespace.Contains("CrashReportHandler"))
					{
						return false;
					}
					if (itemNamespace.Contains("UnityEngine.IO"))
					{
						return false;
					}
					if (itemNamespace.Contains("UnityEngine.AI"))
					{
						return false;
					}
					if (itemNamespace.Contains("Baselib"))
					{
						return false;
					}
					if (itemNamespace.Contains("LowLevel"))
					{
						return false;
					}
					if (itemNamespace.Contains("UIElements"))
					{
						return false;
					}
					if (itemNamespace.Contains("Assertions"))
					{
						return false;
					}
					if (itemNamespace.Contains("UnityEngineInternal.Video"))
					{
						return false;
					}
					if (itemNamespace.Contains("UnityEngineInternal.Input"))
					{
						return false;
					}
					if (itemNamespace.Contains("UnityEngineInternal.GIDebug"))
					{
						return false;
					}

					if (itemNamespace.Contains("SocialPlatforms"))
					{
						return false;
					}
					if (itemNamespace.Contains("SubsystemsImplementation"))
					{
						return false;
					}
					if (itemNamespace.Contains("PlayerLoop"))
					{
						return false;
					}
                    if (itemNamespace.Contains("CoreModule"))
                    {
                        return false;
                    }
					//if (itemNamespace.CustomStartsWith("UnityEngineInternal")) ////////////////////UnityEngineInternal 必要？
					//{
					//	return false;
					//}
					if (itemNamespace.Contains("CriAtom"))
					{
                        return false;
                    }
					if (itemNamespace.Contains("CriProfiler"))
					{
						return false;
					}
					if (itemNamespace.Contains("DOTweenEditor"))
					{
						return false;
					}
					if (itemNamespace.Contains(".UnityEditor"))
					{
						return false;
					}
					if (itemNamespace.Contains("Analytics"))
					{
						return false;
					}
					if (itemNamespace.Contains(".Editor."))
					{
						return false;
					}
					if (itemNamespace.Contains("Networking"))
					{
						return false;
					}
					if (itemNamespace.Contains(".Rendering"))
					{
						return false;
					}
					if (itemNamespace.Contains(".EventSystems"))
					{
						return false;
					}
					if (itemNamespace.Contains("NVIDIA"))
					{
						return false;
					}
					if (itemNamespace.Contains("Apple"))
					{
						return false;
					}
					if (itemNamespace.Contains(".VFX"))
					{
						return false;
					}
					if (itemNamespace.Contains(".WSA"))
					{
						return false;
					}
					if (itemNamespace.Contains(".U2D"))
					{
						return false;
					}
					if (itemNamespace.Contains(".TLS"))
					{
						return false;
					}
					if (itemNamespace.Contains(".tvOS"))
					{
						return false;
					}
					if (itemNamespace.Contains("Scripting"))
					{
						return false;
					}
					if (itemNamespace.Contains(".Audio"))
					{
						return false;
					}
					if (itemNamespace.Contains("Terrain"))
					{
						return false;
					}
					if (itemNamespace.Contains("Plugins"))
					{
						return false;
					}
					if (itemNamespace.Contains(".Profiling"))
					{
						return false;
					}
                    if (itemNamespace.Contains(".Core"))
                    {
                        return false;
                    }
                    if (itemNamespace.Contains(".Android"))
					{
						return false;
					}
					if (item.FullName.Contains("iPhone"))
					{
						return false;
					}
					if (itemNamespace.Contains(".iOS"))
					{
						return false;
					}
					if (itemNamespace.Contains("CodeEditor"))
					{
						return false;
					}
					if (itemNamespace.Contains("TextCore"))
					{
						return false;
					}
					if (itemNamespace.Contains("CSObjectWrapEditor"))
					{
						return false;
					}
					if (itemNamespace.Contains("XLuaTest"))
					{
						return false;
					}
					if (itemNamespace.Contains("VirtualTexturing"))
					{
						return false;
					}
					
					if (itemNamespace.Contains("Accessibility"))
					{
						return false;
					}
					if (itemNamespace.Contains(".Yoga"))
					{
						return false;
					}
					if (itemNamespace.Contains(".TestTools"))
					{
						return false;
					}
					if (itemNamespace.Contains(".c__"))
					{
						return false;
					}
					if (item.FullName.CustomEndsWith(".c"))
					{
						return false;
					}
					if (itemNamespace.Contains(".Playables"))
					{
						return false;
					}
					if (itemNamespace.Contains(".Bindings"))
					{
						return false;
					}
					if (itemNamespace.Contains("Animations"))
					{
						return false;
					}
					if (itemNamespace.Contains(".Events"))
					{
						return false;
					}
					if (itemNamespace.Contains("SceneManagement"))
					{
						return false;
					}
					if (itemNamespace.Contains("TinyProfiling"))
					{
						return false;
					}
					if (itemNamespace.Contains("Advertisements"))
					{
						return false;
					}
					
					if (itemNamespace.Contains("Collections"))
					{
						return false;
					}
					if (itemNamespace.Contains("ParticleSystemJobs"))
					{
						return false;
					}
					//if (itemNamespace.Contains("Internal"))
					//{
					//	return false;
					//}
					

					if (itemNamespace.Contains("IL2CPP"))
					{
						return false;
					}
					if (itemNamespace.Contains(".Search"))
					{
						return false;
					}

					if (itemNamespace.Contains("Bee.Core"))
					{
						return false;
					}
					if (itemNamespace.Contains("TreeView"))
					{
						return false;
					}
					if (itemNamespace.Contains("NUnit"))
					{
						return false;
					}
					if (itemNamespace.Contains("SRDebugger"))
					{
						return false;
					}
					if (itemNamespace.Contains("TestRunner"))
					{
						return false;
					}
					if (itemNamespace.Contains(".Jobs"))
					{
						return false;
					}
					if (itemNamespace.Contains("Tilemaps"))
					{
						return false;
					}
					if (itemNamespace.Contains("CoroutineTween"))
					{
						return false;
					}
					if (itemNamespace.Contains("Serialization"))
					{
						return false;
					}
					if (itemNamespace.Contains("Windows"))
					{
						return false;
					}
					if (itemNamespace.Contains("Gradle"))
					{
						return false;
					}
					if (itemNamespace.Contains("Builder"))
					{
						return false;
					}
					if (itemNamespace.Contains("Curl"))
					{
						return false;
					}
					if (itemNamespace.Contains("Subsystems"))
					{
						return false;
					}
					if (itemNamespace.Contains("Connect"))
					{
						return false;
					}
					
					if (itemNamespace.Contains(".Pool"))
					{
						return false;
					}
					if (itemNamespace.Contains(".XR"))
					{
						return false;
					}
                    if (itemNamespace.Contains("Experimental"))
                    {
                        return false;
                    }
                    if (itemNamespace.CustomEndsWith(".c"))
					{
						return false;
					}

					if (item.FullName.Contains("__"))
					{
						return false;
					}

					if (item.FullName.Contains("Assembly"))
					{
						return false;
					}

					if (item.FullName.Contains("SRMath"))
					{
						return false;
					}

					if (item.FullName.Contains("AndroidJNI"))
					{
						return false;
					}

					if (item.FullName.Contains("CS.Consts"))
					{
						return false;
					}

					if (item.FullName.Contains("CS.Locale"))
					{
						return false;
					}

					if (item.FullName.Contains("SROptions"))
					{
						return false;
					}

					if (item.FullName.Contains("SRDebugger"))
					{
						return false;
					}

					if (item.FullName.Contains("CS.Cri"))
					{
						return false;
					}

					if (item.FullName.Contains("TextEditor"))
					{
						return false;
					}

					if (item.FullName.Contains("Android"))
					{
						return false;
					}


					//lua里用不到
					if (itemNamespace.Contains("Burst"))
                    {
                        return false;
                    }
					if (itemNamespace.Contains("InputSystem"))
					{
						return false;
					}
					if (itemNamespace.CustomStartsWith(supportNameSpaceList[i]))
                    {
						Debug.Log(itemNamespace);
						return true;
                    }
                }
            }
            return false;
        }

        private static Type[] CollectType(Assembly assembly)
        {
            var types = assembly.GetTypes();
            var retTypes = new HashSet<Type>();
            foreach (var item in types)
            {
                if (IsExportType(item) && !retTypes.Contains(item))
                {
                    retTypes.Add(item);
                }
            }
            return retTypes.ToArray();
        }

        private static void HandleExtensionMethods()
        {
            for (var i = 0; i < exportTypeList.Count; i++)
            {
                Type type = exportTypeList[i];

                MethodInfo[] publicStaticMethodInfos =
                    type.GetMethods(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);
                for (var j = 0; j < publicStaticMethodInfos.Length; j++)
                {
                    MethodInfo methodInfo = publicStaticMethodInfos[j];
                    if (methodInfo.IsDefined(typeof(ExtensionAttribute), false))
                    {
                        Type extensionType = methodInfo.GetParameters()[0].ParameterType;
                        if (extensionMethodsDic.TryGetValue(extensionType, out List<MethodInfo> extensionMethodList))
                        {
                            extensionMethodList.Add(methodInfo);
                        }
                        else
                        {
                            List<MethodInfo> methodList = new List<MethodInfo> { methodInfo };
                            extensionMethodsDic.Add(extensionType, methodList);
                        }
                    }
                }
            }
        }

        private static void GenerateTypeDefines()
        {
            sb.Clear();

            sb.AppendLine(string.Format("---@class {0}", "CS"));
            sb.AppendLine("CS = {}");
            // sb.AppendLine(string.Format("---@class {0}", "Unity"));
            // sb.AppendLine("Unity = {}");
            // sb.AppendLine(string.Format("---@class {0}", "UnityEditor"));
            // sb.AppendLine("UnityEditor = {}");

            // var nameSpaceSet = new HashSet<string>();
            // for (int i = 0; i < exportTypeList.Count; i++)
            // {
            //     Type type = exportTypeList[i];
            //     if (!nameSpaceSet.Contains(type.Namespace))
            //     {
            //         nameSpaceSet.Add(type.Namespace);
            //         sb.AppendLine(string.Format("---@class {0}", type.Namespace));
            //         sb.AppendLine(string.Format("{0} = {{}}", type.Namespace));
            //     }
            // }
            // sb.AppendLine();

            for (int i = 0; i < exportTypeList.Count; i++)
            {
                Type typeInst = exportTypeList[i];

                keepStringTypeName = typeInst == typeof(string);

                WriteClassDefine(typeInst);
                WriteClassFieldDefine(typeInst);
                sb.AppendLine(string.Format("{0} = {{}}", typeInst.ToLuaTypeName().ReplaceDotOrPlusWithUnderscore()));

                WriteClassConstructorDefine(typeInst);
                WriteClassMethodDefine(typeInst);

                sb.AppendLine("");
            }
            string st = sb.ToString();
           // GUIUtility.systemCopyBuffer = st;

			File.WriteAllText(TypeDefineFilePath, st);

			
			Debug.Log(TypeDefineFilePath);
			Application.OpenURL("file://" + Path.GetDirectoryName(TypeDefineFilePath));
		}

        #region TypeDefineFileGenerator

        public static void WriteClassDefine(Type type)
        {
            if (type.BaseType != null && !type.IsEnum)
            {
                sb.AppendLine(string.Format("---@class {0} : {1}", type.ToLuaTypeName(),
                    type.BaseType.ToLuaTypeName()));
            }
            else
            {
                sb.AppendLine(string.Format("---@class {0}", type.ToLuaTypeName()));
            }
        }

        public static void WriteClassFieldDefine(Type type)
        {
            FieldInfo[] publicInstanceFieldInfos =
                type.GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            FieldInfo[] publicStaticFieldInfos =
                type.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);
            List<FieldInfo> fieldInfoList = new List<FieldInfo>();
            fieldInfoList.AddRange(publicStaticFieldInfos);
            if (!type.IsEnum)
            {
                fieldInfoList.AddRange(publicInstanceFieldInfos);
            }

            for (int i = 0; i < fieldInfoList.Count; i++)
            {
                FieldInfo fieldInfo = fieldInfoList[i];
                if (fieldInfo.IsMemberObsolete(type))
                {
                    continue;
                }

                Type fieldType = fieldInfo.FieldType;
                sb.AppendLine(string.Format("---@field {0} {1}", fieldInfo.Name, fieldType.ToLuaTypeName()));
            }

            PropertyInfo[] publicInstancePropertyInfo =
                type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            PropertyInfo[] publicStaticPropertyInfo =
                type.GetProperties(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);
            List<PropertyInfo> propertyInfoList = new List<PropertyInfo>();
            propertyInfoList.AddRange(publicStaticPropertyInfo);
            if (!type.IsEnum)
            {
                propertyInfoList.AddRange(publicInstancePropertyInfo);
            }

            for (int i = 0; i < propertyInfoList.Count; i++)
            {
                PropertyInfo propertyInfo = propertyInfoList[i];
                if (propertyInfo.IsMemberObsolete(type))
                {
                    continue;
                }

                Type propertyType = propertyInfo.PropertyType;
                sb.AppendLine(string.Format("---@field {0} {1}", propertyInfo.Name, propertyType.ToLuaTypeName()));
            }
        }

        public static void WriteClassConstructorDefine(Type type)
        {
            if (type == typeof(MonoBehaviour) || type.IsSubclassOf(typeof(MonoBehaviour)))
            {
                return;
            }

            string className = type.ToLuaTypeName().ReplaceDotOrPlusWithUnderscore();
            ConstructorInfo[] constructorInfos = type.GetConstructors();
            if (constructorInfos.Length == 0)
            {
                return;
            }

            for (int i = 0; i < constructorInfos.Length - 1; i++)
            {
                ConstructorInfo ctorInfo = constructorInfos[i];
                if (ctorInfo.IsStatic || ctorInfo.IsGenericMethod)
                {
                    continue;
                }

                WriteOverloadMethodCommentDecalre(ctorInfo.GetParameters(), type);
            }

            ConstructorInfo lastCtorInfo = constructorInfos[constructorInfos.Length - 1];
            WriteMethodFunctionDeclare(lastCtorInfo.GetParameters(), type, "New", className, true);
        }

        public static void WriteClassMethodDefine(Type type)
        {
            // string classNameWithNameSpace = type.ToLuaTypeName().Replace(".", "_");
            string classNameWithNameSpace = type.ToLuaTypeName();

            Dictionary<string, List<MethodInfo>> methodGroup = new Dictionary<string, List<MethodInfo>>();
            MethodInfo[] publicInstanceMethodInfos =
                type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            MethodInfo[] publicStaticMethodInfos =
                type.GetMethods(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);

            Action<MethodInfo> recordMethodGroup = methodInfo =>
            {
                string methodName = methodInfo.Name;

                if (methodInfo.IsGenericMethod)
                {
                    return;
                }

                if (methodName.StartsWith("get_") || methodName.StartsWith("set_") || methodName.StartsWith("op_"))
                {
                    return;
                }

                if (methodName.StartsWith("add_") || methodName.StartsWith("remove_"))
                {
                    return;
                }

                if (methodGroup.ContainsKey(methodName))
                {
                    List<MethodInfo> methodInfoList = methodGroup[methodName];
                    if (methodInfoList == null)
                    {
                        methodInfoList = new List<MethodInfo>();
                    }

                    methodInfoList.Add(methodInfo);
                    methodGroup[methodName] = methodInfoList;
                }
                else
                {
                    methodGroup.Add(methodName, new List<MethodInfo> { methodInfo });
                }
            };

            for (int i = 0; i < publicStaticMethodInfos.Length; i++)
            {
                MethodInfo methodInfo = publicStaticMethodInfos[i];
                if (methodInfo.IsMemberObsolete(type))
                {
                    continue;
                }

                recordMethodGroup(methodInfo);
            }

            for (int i = 0; i < publicInstanceMethodInfos.Length; i++)
            {
                MethodInfo methodInfo = publicInstanceMethodInfos[i];
                if (methodInfo.IsMemberObsolete(type))
                {
                    continue;
                }

                recordMethodGroup(methodInfo);
            }

            foreach (var oneGroup in methodGroup)
            {
                List<MethodInfo> methodInfoList = oneGroup.Value;
                //前面的方法都是overload
                for (int i = 0; i < methodInfoList.Count - 1; i++)
                {
                    WriteOverloadMethodCommentDecalre(methodInfoList[i].GetParameters(), methodInfoList[i].ReturnType);
                }

                MethodInfo lastMethodInfo = methodInfoList[methodInfoList.Count - 1];
                WriteMethodFunctionDeclare(lastMethodInfo.GetParameters(), lastMethodInfo.ReturnType,
                    lastMethodInfo.Name,
                    classNameWithNameSpace, lastMethodInfo.IsStatic);
            }

            WriteExtensionMethodFunctionDecalre(type);
        }

        public static void WriteOverloadMethodCommentDecalre(ParameterInfo[] parameterInfos, Type returnType)
        {
            List<ParameterInfo> outOrRefParameterInfoList = new List<ParameterInfo>();

            tempSb.Clear();
            for (int i = 0; i < parameterInfos.Length; i++)
            {
                ParameterInfo parameterInfo = parameterInfos[i];
                string parameterName = parameterInfo.Name;
                string parameterTypeName = parameterInfo.ParameterType.ToLuaTypeName();
                if (parameterInfo.IsOut)
                {
                    parameterName = "out_" + parameterName;
                    outOrRefParameterInfoList.Add(parameterInfo);

                    parameterTypeName = parameterInfo.ParameterType.GetElementType().ToLuaTypeName();
                }
                else if (parameterInfo.ParameterType.IsByRef)
                {
                    parameterName = "ref_" + parameterName;
                    outOrRefParameterInfoList.Add(parameterInfo);

                    parameterTypeName = parameterInfo.ParameterType.GetElementType().ToLuaTypeName();
                }

                parameterName = EscapeLuaKeyword(parameterName);
                if (i == parameterInfos.Length - 1)
                {
                    tempSb.Append(string.Format("{0} : {1}", parameterName, parameterTypeName));
                }
                else
                {
                    tempSb.Append(string.Format("{0} : {1}, ", parameterName, parameterTypeName));
                }
            }

            //return
            List<Type> returnTypeList = new List<Type>();
            if (returnType != null && returnType != typeof(void))
            {
                returnTypeList.Add(returnType);
            }

            for (int i = 0; i < outOrRefParameterInfoList.Count; i++)
            {
                returnTypeList.Add(outOrRefParameterInfoList[i].ParameterType.GetElementType());
            }

            string returnTypeString = "";
            for (int i = 0; i < returnTypeList.Count; i++)
            {
                if (i == returnTypeList.Count - 1)
                {
                    returnTypeString += returnTypeList[i].ToLuaTypeName();
                }
                else
                {
                    returnTypeString += returnTypeList[i].ToLuaTypeName() + ", ";
                }
            }

            if (returnTypeList.Count > 0)
            {
                sb.AppendLine(string.Format("---@overload fun({0}) : {1}", tempSb, returnTypeString));
            }
            else
            {
                sb.AppendLine(string.Format("---@overload fun({0})", tempSb));
            }
        }

        public static void WriteMethodFunctionDeclare(ParameterInfo[] parameterInfos, Type returnType,
            string methodName,
            string className, bool isStaticMethod)
        {
            List<ParameterInfo> outOrRefParameterInfoList = new List<ParameterInfo>();

            tempSb.Clear();
            for (int i = 0; i < parameterInfos.Length; i++)
            {
                ParameterInfo parameterInfo = parameterInfos[i];
                string parameterName = parameterInfo.Name;
                string parameterTypeName = parameterInfo.ParameterType.ToLuaTypeName();
                if (parameterInfo.IsOut)
                {
                    parameterName = "out_" + parameterName;
                    outOrRefParameterInfoList.Add(parameterInfo);

                    parameterTypeName = parameterInfo.ParameterType.GetElementType().ToLuaTypeName();
                }
                else if (parameterInfo.ParameterType.IsByRef)
                {
                    parameterName = "ref_" + parameterName;
                    outOrRefParameterInfoList.Add(parameterInfo);

                    parameterTypeName = parameterInfo.ParameterType.GetElementType().ToLuaTypeName();
                }

                parameterName = EscapeLuaKeyword(parameterName);

                if (i == parameterInfos.Length - 1)
                {
                    tempSb.Append(parameterName);
                }
                else
                {
                    tempSb.Append(string.Format("{0}, ", parameterName));
                }

                sb.AppendLine(string.Format("---@param {0} {1}", parameterName, parameterTypeName));
            }

            //return
            bool haveReturen = returnType != null && returnType != typeof(void) || outOrRefParameterInfoList.Count > 0;

            if (haveReturen)
            {
                sb.Append("---@return ");
            }

            if (returnType != null && returnType != typeof(void))
            {
                sb.Append(returnType.ToLuaTypeName());
            }

            for (int i = 0; i < outOrRefParameterInfoList.Count; i++)
            {
                sb.Append(string.Format(",{0}",
                    outOrRefParameterInfoList[i].ParameterType.GetElementType().ToLuaTypeName()));
            }

            if (haveReturen)
            {
                sb.AppendLine("");
            }

            if(methodName == "New")
            {
				sb.AppendLine(string.Format("function {0}({1}) end", className, tempSb));
                return;
			}

            if (isStaticMethod)
            {
                sb.AppendLine(string.Format("function {0}.{1}({2}) end", className, methodName, tempSb));
            }
            else
            {
                sb.AppendLine(string.Format("function {0}:{1}({2}) end", className, methodName, tempSb));
            }
        }

        private static void WriteExtensionMethodFunctionDecalre(Type type)
        {
            if (extensionMethodsDic.TryGetValue(type, out List<MethodInfo> extensionMethodList))
            {
                for (var i = 0; i < extensionMethodList.Count; i++)
                {
                    MethodInfo methodInfo = extensionMethodList[i];
                    ParameterInfo[] parameterInfos = methodInfo.GetParameters();
                    if (parameterInfos.Length > 0)
                    {
                        //第一个param是拓展类型，去掉
                        parameterInfos = parameterInfos.ToList().GetRange(1, parameterInfos.Length - 1).ToArray();
                    }

                    Type returnType = methodInfo.ReturnType;
                    string methodName = methodInfo.Name;
                    string classNameWithNameSpace = type.ToLuaTypeName();

                    WriteMethodFunctionDeclare(parameterInfos, returnType, methodName, classNameWithNameSpace, false);
                }
            }
        }

        #endregion

        private static bool TypeIsExport(Type type)
        {
            return exportTypeList.Contains(type) || type == typeof(string) ||
                               luaNumberTypeSet.Contains(type) || type == typeof(bool);
        }

        private static bool keepStringTypeName;

        public static string ToLuaTypeName(this Type type)
        {
            if (type == null)
            {
                return "NullType";
            }

            // if (!TypeIsExport(type))
            // {
            //     if (type.IsEnum)
            //     {
            //         return "NotExportEnum";
            //     }

            //     return "NotExportType";
            // }

            if (luaNumberTypeSet.Contains(type))
            {
                return "number";
            }

            if (type == typeof(string))
            {
                return keepStringTypeName ? "System.String" : "string";
            }

            if (type == typeof(bool))
            {
                return "boolean";
            }

            string typeName = type.FullName;
            if (typeName == null)
            {
                return "CS." + type.ToString().EscapeGenericTypeSuffix();
            }

            if (type.IsEnum)
            {
                return "CS." + type.FullName.EscapeGenericTypeSuffix().Replace("+", ".");
            }

            //去除泛型后缀
            typeName = typeName.EscapeGenericTypeSuffix();

            int bracketIndex = typeName.IndexOf("[[");
            if (bracketIndex > 0)
            {
                typeName = typeName.Substring(0, bracketIndex);
                Type[] genericTypes = type.GetGenericArguments();
                for (int i = 0; i < genericTypes.Length; i++)
                {
                    Type genericArgumentType = genericTypes[i];
                    string genericArgumentTypeName;
                    if (CSharpTypeNameDic.ContainsKey(genericArgumentType))
                    {
                        genericArgumentTypeName = CSharpTypeNameDic[genericArgumentType];
                    }
                    else
                    {
                        genericArgumentTypeName = genericArgumentType.ToLuaTypeName();
                    }

                    typeName = typeName + "_" + genericArgumentTypeName.ReplaceDotOrPlusWithUnderscore();
                }
            }

            return "CS." + typeName;
        }

        private static Dictionary<Type, string> CSharpTypeNameDic = new Dictionary<Type, string>
        {
            {typeof(byte), "byte"},
            {typeof(sbyte), "sbyte"},
            {typeof(short), "short"},
            {typeof(ushort), "ushort"},
            {typeof(int), "int"},
            {typeof(uint), "uint"},
            {typeof(long), "long"},
            {typeof(ulong), "ulong"},
            {typeof(float), "float"},
            {typeof(double), "double"},
            {typeof(bool), "bool"},
            {typeof(string), "string"},
        };


        public static string EscapeLuaKeyword(string s)
        {
            if (luaKeywordSet.Contains(s))
            {
                return "_" + s;
            }

            return s;
        }

        public static string ReplaceDotOrPlusWithUnderscore(this string s)
        {
            // return s.Replace(".", "_").Replace("+", "_");
            return s.Replace("+", "_");
        }

        public static string EscapeGenericTypeSuffix(this string s)
        {
            var reg = "[^a-zA-Z0-9\\._+]";
            string result = Regex.Replace(s, reg, "").Replace("+", ".");
            return result;
        }

        public static bool IsMemberObsolete(this MemberInfo memberInfo, Type type)
        {
            return memberInfo.GetCustomAttributes(typeof(ObsoleteAttribute), false).Length > 0 ||
                   IsMemberFilter(memberInfo, type);
        }

        public static bool IsMemberFilter(MemberInfo mi, Type type)
        {
            if (type.IsGenericType)
            {
            }
            return false;
        }
    }
}