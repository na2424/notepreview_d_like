﻿Shader "Custom/Attack"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {} // Unity側でテクスチャをセット
		_Alpha("Alpha",  Float) = 1.0
	}

	SubShader
	{

		// 描画タイプ。 Opaque=不透明 Transparent=半透明 TransparentCutout=抜き
		Tags { "RenderType"="TransparentCutout"  "Queue" = "Transparent+5"}
		LOD 0
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		AlphaToMask On

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"
			// Unityからシェーダーに渡される構造体
			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed _Alpha;

			v2f vert(appdata v)
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				col.a *= clamp(_Alpha, 0, 1);
				return col;
			}
			ENDCG
		}
	}
}