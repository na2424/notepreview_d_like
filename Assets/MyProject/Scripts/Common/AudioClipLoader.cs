﻿using Cysharp.Threading.Tasks;
using System;
using System.IO;
using System.Threading;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// 非同期な音楽ファイル読み込みを行なうクラス
/// ・対応フォーマットはOgg Vorbisまたはmp3
/// ・AudioClipを生成する
/// </summary>
public sealed class AudioClipLoader
{
	/// <summary>
	/// ファイルから音声を読み込み
	/// </summary>
	/// <param name="ct">キャンセルトークン</param>
	/// <param name="path">ファイルパス(.ogg)</param>
	/// <returns>AudioClipを持つUniTask</returns>
	public static async UniTask<AudioClip> LoadAsync(CancellationToken ct, string path, bool isStreaming)
	{
		var extension = Path.GetExtension(path);

		// .oggと.mp3形式に対応(.wavは非推奨)
		AudioType audioType =
			extension.Equals(".ogg") ? AudioType.OGGVORBIS :
			extension.Equals(".mp3") ? AudioType.MPEG : AudioType.WAV;

		AudioClip clip = null; 

		using (UnityWebRequest webRequest = UnityWebRequestMultimedia.GetAudioClip(path, audioType))
		{
			try
			{
				((DownloadHandlerAudioClip)webRequest.downloadHandler).streamAudio = isStreaming;

				await webRequest.SendWebRequest().WithCancellation(ct);

				if (webRequest.result == UnityWebRequest.Result.Success)
				{
					clip = DownloadHandlerAudioClip.GetContent(webRequest);
				}
				else
				{
					throw new Exception(webRequest.error);
				}
			}
			catch (OperationCanceledException e)
			{
				Debug.LogWarning("楽曲読み込みがキャンセルされました:" + e.Message);
				return null;
			}
			catch(Exception e)
			{
				string errorMessage = "音声ファイルが読み込めませんでした:" + e.Message;
				Debug.LogWarning(errorMessage);
				throw new Exception(errorMessage);
			}
		}

		return clip;
	}
}
