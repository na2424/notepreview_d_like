﻿using Cysharp.Text;
using Cysharp.Threading.Tasks;
using Dialog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEngine;
using UnityEngine.Networking;

public sealed class AssetBundleManager : MonoBehaviour
{
	Dictionary<int, AssetBundle> _assetBundleDict;

	CancellationTokenSource _ctSource;
	Action _onError;

	public void Init(Action onError)
	{
		_onError = onError;
		_assetBundleDict = new Dictionary<int, AssetBundle>();
	}

	/// <summary>
	/// アセットバンドルをロードします。
	/// アセットバンドルのハッシュ値を返します。
	/// </summary>
	/// <param name="name">ファイル名(譜面フォルダからの相対パス)</param>
	/// <returns>アセットバンドルのハッシュ値</returns>
	public int LoadAssetBundle(string name)
	{
		int hash = 0;

		var path = Application.platform == RuntimePlatform.Android ?
			ZString.Concat("file://", Path.Combine(LuaManager.FolderPath, name)) :
			Path.Combine(LuaManager.FolderPath, name);

		try
		{
			var assetBandle = AssetBundle.LoadFromFile(path);
			hash = assetBandle.GetHashCode();
			_assetBundleDict.Add(hash, assetBandle);
		}
		catch (Exception e)
		{
			throw new Exception("AssetBandleの読み込みに失敗しました。path:" + path + " エラー内容:" + e.Message);
		}

		return hash;
	}

	/// <summary>
	/// 指定した型のアセットをアセットバンドルのハッシュ値とアセット名からロードします。
	/// </summary>
	/// <param name="hash">ハッシュ値</param>
	/// <returns></returns>
	public T LoadAsset<T>(int hash, string name) where T : UnityEngine.Object
	{
		if (_assetBundleDict.TryGetValue(hash, out var assetBundle))
		{
			return assetBundle.LoadAsset<T>(name);
		}

		return null;
	}

	/// <summary>
	/// 
	/// </summary>
	/// <param name="name"></param>
	/// <param name="assetName"></param>
	public void InstantiateAssetBundle(string assetBandleName, string assetName)
	{
		_ctSource = new CancellationTokenSource();
		InstantiateAssetBundleAsync(assetBandleName, assetName, _ctSource.Token).Forget();
	}

	async UniTask InstantiateAssetBundleAsync(string name, string assetName, CancellationToken ct)
	{
		var path =
#if UNITY_ANDROID
			ZString.Concat("file://", Path.Combine(LuaManager.FolderPath, name));
#else
			Path.Combine(LuaManager.FolderPath, name);
#endif

		try
		{
			using (UnityWebRequest webRequest = UnityWebRequestAssetBundle.GetAssetBundle(path))
			{
				await webRequest.SendWebRequest().WithCancellation(ct);

				if (webRequest.result == UnityWebRequest.Result.Success)
				{
					var assetbundle = DownloadHandlerAssetBundle.GetContent(webRequest);
					var prefab = assetbundle.LoadAsset<GameObject>(assetName);

					Instantiate(prefab);
				}
				else
				{
					throw new Exception(webRequest.error);
				}
			}

		}
		catch (Exception e)
		{
			var builder = new DialogParametor.Builder("Luaエラー", $"AssetBandleの読み込みに失敗しました。path:" + path + " エラー内容:" + e.Message);
			builder.AddDefaultAction("選曲画面に戻る", () => GameManager.Instance.ChangeScene(SceneName.SelectMusic));
			builder.AddCallbackOnAutoClosed(() => GameManager.Instance.ChangeScene(SceneName.SelectMusic));
			DialogManager.Instance.Open(builder.Build());
			_onError();
		}
	}

	public void OnDestroy()
	{
		if (_ctSource != null)
		{
			_ctSource.Cancel();
			_ctSource = null;
		}
		
		if (_assetBundleDict != null)
		{
			foreach (var asset in _assetBundleDict.Values)
			{
				asset.Unload(true);
			}

			_assetBundleDict.Clear();
			_assetBundleDict = null;
		}
	}
}
