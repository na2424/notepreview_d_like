﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public sealed class AspectRatioImage : MonoBehaviour
{
    [SerializeField] RectTransform _rectTransform;
    [SerializeField] Image _image = default;

    private void Reset()
    {
        _rectTransform = transform.parent.GetComponent<RectTransform>();
        _image = transform.GetComponent<Image>();
    }

    public void SetTexture(Texture2D tex)
    {
        if (tex == null)
        {
            return;
        }

        SetSize(tex.width, tex.height);

        _image.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100f);
    }

    public void SetSize(int width, int height)
    {
        float baseSizeRatio = _rectTransform.sizeDelta.x / _rectTransform.sizeDelta.y;
        float sizeRatio = (float)width / height;

        if (sizeRatio < baseSizeRatio)
        {
            float fitWidth = width * _rectTransform.sizeDelta.y / height;
            _image.rectTransform.sizeDelta = new Vector2(fitWidth, _rectTransform.sizeDelta.y);
        }
        else
        {
            float fitHeight = height * _rectTransform.sizeDelta.x / width;
            _image.rectTransform.sizeDelta = new Vector2(_rectTransform.sizeDelta.x, fitHeight);
        }
    }

    void OnDestroy()
    {
        if (_image.sprite != null)
        {
            Destroy(_image.sprite);
            _image.sprite = null;
        }
    }
}
