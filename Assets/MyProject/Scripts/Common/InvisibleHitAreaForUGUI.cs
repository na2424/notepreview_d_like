﻿using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// 見えないGraphic
/// </summary>
public class InvisibleHitAreaForUGUI : Graphic
{
	// 頂点を生成する必要があるときのコールバック関数
	protected override void OnPopulateMesh( VertexHelper vh )
	{
		base.OnPopulateMesh( vh );
		//頂点を全てクリアし、何も表示されないように
		vh.Clear();
	}

#if UNITY_EDITOR
	protected virtual void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.green;
		var rt = GetComponent<RectTransform>();
		var rect = rt.rect;
		var offset = Vector3.Scale( new Vector3( rect.x + rect.width * 0.5f, rt.rect.y + rect.height * 0.5f ), transform.lossyScale );
		var pos = transform.position + offset;
		var size = Vector3.Scale( rect.size, transform.lossyScale );
		Gizmos.DrawWireCube( pos, size );
	}

	//表示する必要がないので、インスペクターに何も表示しないように
	[CustomEditor( typeof( InvisibleHitAreaForUGUI ) )]
	class GraphicCastEditor : Editor
	{
		public override void OnInspectorGUI() { }
	}
#endif

}
