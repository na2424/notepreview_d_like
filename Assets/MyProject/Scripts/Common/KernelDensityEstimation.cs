﻿using System;

public static class KernelDensityEstimation
{
    static readonly double DIV_SQR_PI2 = 1 / Math.Sqrt(2 * Math.PI);
    public static double GaussianKernel(double x)
    {
        return DIV_SQR_PI2 * Math.Exp(-0.5f * x * x);
    }

    public static float KernelDensityEstimate(in float[] data, in float x, double bandWidth)
    {
        double sum = 0;

        for (int i = 0; i < data.Length; i++)
        {
            float dataPoint = data[i];
            double diff = (x - dataPoint) / bandWidth;
            sum += GaussianKernel(diff);
        }

        return (float)(sum / (data.Length * bandWidth));
    }
}
