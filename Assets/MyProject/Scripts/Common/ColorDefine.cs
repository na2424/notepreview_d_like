﻿using UnityEngine;

[CreateAssetMenu( menuName = "MyScriptable/Create ColorDefine", fileName = "ColorDefine")]
public class ColorDefine : ScriptableObject
{
	[SerializeField]
	Color _black = default;
	public Color Black => _black;

	[SerializeField]
	Color _gray = default;
	public Color Gray => _gray;

	[SerializeField]
	Color _red = default;
	public Color Red => _red;

	[SerializeField]
	Color _green = default;
	public Color Green => _green;

	[SerializeField]
	Color _blue = default;
	public Color Blue => _blue;

	[SerializeField]
	Color _yellow = default;
	public Color Yellow => _yellow;

	[SerializeField]
	Color _cyan = default;
	public Color Cyan => _cyan;

	[SerializeField]
	Color _white = default;
	public Color White => _white;

	[SerializeField]
	Color _darkGray = default;
	public Color DarkGray => _darkGray;

	[Space]

	[SerializeField]
	Color _easy = default;
	public Color Easy => _easy;

	[SerializeField]
	Color _normal = default;
	public Color Normal => _normal;

	[SerializeField]
	Color _hard = default;
	public Color Hard => _hard;

	[SerializeField]
	Color _extra = default;
	public Color Extra => _extra;

	[SerializeField]
	Color _lunatic = default;
	public Color Lunatic => _lunatic;

	public Color GetDifficultyColor(DifficultyType type) => type switch
	{
		DifficultyType.Easy => Easy,
		DifficultyType.Normal => Normal,
		DifficultyType.Hard => Hard,
		DifficultyType.Extra => Extra,
		DifficultyType.Lunatic => Lunatic,
		_ => Color.black
	};
}
