using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public sealed class SizeChangeToggle : Toggle
{
    RectTransform _rt = null;
    Vector3 _size;

    const float SIZE_RATIO = 0.96f;

    RectTransform Rt
    {
        get
        {
            if (_rt is not null)
            {
                return _rt;
            }
            else
            {
                _rt = transform.GetChild(0).GetComponent<RectTransform>();
                _size = _rt.localScale;
                return _rt;
            }
        }
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        if (currentSelectionState == SelectionState.Disabled)
        {
            return;
        }

        base.OnPointerEnter(eventData);
        Rt.localScale = _size * SIZE_RATIO;
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        if (currentSelectionState == SelectionState.Disabled)
        {
            return;
        }
        base.OnPointerExit(eventData);
        Rt.localScale = _size;
    }
}
