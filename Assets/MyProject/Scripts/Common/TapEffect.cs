﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// タップのエフェクトを管理するクラス
/// </summary>
public sealed class TapEffect : MonoBehaviour, ICallUpdate
{
    [SerializeField] TapEffectPool _tapEffectPool;
    [SerializeField] Camera _defaultCamera;

    Camera _camera = null;

    public void Init()
    {
        _tapEffectPool.Init();
        _camera = _defaultCamera;
    }

    public void SetCamera(Camera camera)
    {
        _camera = camera;
        _defaultCamera.enabled = false;
    }

    public void SetDefaultCamera()
    {
        _camera = _defaultCamera;
        _defaultCamera.enabled = true;
    }

    public void CallUpdate()
    {
        if (Input.GetMouseButtonDown(0) && _camera is not null)
        {
            // マウスのワールド座標までパーティクルを移動し、パーティクルエフェクトを生成する
            var pos = _camera.ScreenToWorldPoint(Input.mousePosition + _camera.transform.forward * 10);
            _tapEffectPool.Play(pos);
        }
    }
}
