﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Cysharp.Text;
using ELEBEAT;

public enum DensityType : int
{
	Total = 0,
	Yellow = 1,
	Blue = 2,
	Green = 3,
}

public class DensityDrawer : MonoBehaviour
{
	public struct DensityPlot
	{
		public float Time;
		public float Density;

		public DensityPlot(float time, float density)
		{
			Time = time;
			Density = density;
		}
	}

	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] TextMeshProUGUI _peakNpsText;
	[SerializeField] TextMeshProUGUI _medianNpsText;
	[SerializeField] TextMeshProUGUI _timeText;
	[SerializeField] RawImage[] _densityGraphRawImage;

	//------------------
	// キャッシュ.
	//------------------
	List<DensityPlot> _densityPlotList = new List<DensityPlot>();

	Color[] _buffer;
	Texture2D[] _drawTexture;

	float _offset;
	bool[] _isDensityDrawn = new bool[4];
	float _maxDensity = 1f;
	float _medianDensity = 1f;
	float _lastTime = 1f;
	float _totalPeakNps = 1f;
	float _totalMedianNps = 1f;

	//------------------
	// 定数.
	//------------------
	const string PEAK_NPS_FORMAT = "Peak NPS : {0}";
	const string MEDIAN_NPS_FORMAT = "Median NPS : {0}";
	const string TIME_FORMAT = "曲の長さ {0:D2}:{1:D2}";

	readonly Color BG_COLOR = new Color(24 / 255f, 33 / 255f, 40 / 255f);
	readonly Color MEDIAN_LINE_COLOR = new Color(0.5f, 0f, 0.5f, 0.5f);

	readonly Color TYPE_0_LOW_TARGET_COLOR = new Color(0, 0.678f, 0.753f);
	readonly Color TYPE_0_HIGH_TARGET_COLOR = new Color(0.51f, 0f, 0.631f);
	readonly Color TYPE_1_LOW_TARGET_COLOR = Color.yellow; //new Color(0,957f, 0.898f, 0.067f);
	readonly Color TYPE_1_HIGH_TARGET_COLOR = new Color(1f, 0.784f, 0f);
	readonly Color TYPE_2_LOW_TARGET_COLOR = new Color(0 / 255f, 91 / 255f, 255 / 255f);
	readonly Color TYPE_2_HIGH_TARGET_COLOR = new Color(0 / 255f, 170 / 255f, 255 / 255f);
	readonly Color TYPE_3_LOW_TARGET_COLOR = Color.green; //new Color(0.404f, 0.753f, 0f);
	readonly Color TYPE_3_HIGH_TARGET_COLOR = new Color(0.216f, 0.753f, 0f);

	public void Init()
	{
		_drawTexture = new Texture2D[4];
	}

	public void Reset(float offset)
	{
		_offset = offset;

		Array.Fill(_isDensityDrawn, false);
	}

	/// <summary>
	/// 密度の計算を行う
	/// (結果は_densityPlotListに入る。
	/// 他にdensityTypeがTotalの場合には_lastTime、_maxDensityをキャッシュする)
	/// </summary>
	/// <param name="sequence"></param>
	/// <param name="bpmHelper"></param>
	/// <param name="densityType"></param>
	public int Calculate(Sequence sequence, BpmHelper bpmHelper, DensityType densityType)
	{
		if (sequence.BeatPositions.Count <= 1)
		{
			return sequence.BeatPositions.Count;
		}

		_densityPlotList.Clear();

		float[] times = null;

		if (densityType == DensityType.Total)
		{
			times = sequence.BeatPositions
				.Select(beatPos => (float)bpmHelper.BeatToTime(beatPos))
				.ToArray();

			_lastTime = times[times.Length - 1];
		}
		else
		{
			List<float> timeList = new List<float>();
			int count = sequence.BeatPositions.Count;
			for (int i = 0; i < count; i++)
			{
				var noteType = sequence.NoteTypes[i];
				bool enable =
					// 黄色
					densityType == DensityType.Yellow ?
						noteType == NoteType.Normal :
					// 青色
					densityType == DensityType.Blue ?
						noteType == NoteType.LongStart ||
						noteType == NoteType.LongRelay ||
						noteType == NoteType.LongEnd :
					// 緑色
					densityType == DensityType.Green ?
						noteType == NoteType.Fuzzy ||
						noteType == NoteType.FuzzyLongStart ||
						noteType == NoteType.FuzzyLongRelay ||
						noteType == NoteType.FuzzyLongEnd :
					false;

				if (enable)
				{
					timeList.Add((float)bpmHelper.BeatToTime(sequence.BeatPositions[i]));
				}
			}
			times = timeList.ToArray();
		}

		if (times.Length <= 1)
		{
			return times.Length;
		}

		// カーネル密度推定の計算とグラフへの追加
		float interval = (_lastTime - _offset) / _lastTime; // 横軸の間隔を調整するための値

		for (float t = _offset; t <= _lastTime; t += interval)
		{
			// カーネル密度推定
			// バンド幅は適宜調整
			float density = KernelDensityEstimation.KernelDensityEstimate(in times, t, 0.42);
			_densityPlotList.Add(new DensityPlot(t, density));
		}

		if (densityType == DensityType.Total)
		{
			DrawPeekNPS(times.Length, _densityPlotList);
			DrawMedianNPS(times.Length, _densityPlotList);
			DrawTime(_offset, times.NonAllocLast());
			_maxDensity = _densityPlotList.Max(v => v.Density);
		}

		return times.Length;
	}

	float GetPeakDensity(List<DensityPlot> densityPlots)
	{
		float peakDensity = 0f;

		foreach (var dp in densityPlots)
		{
			if (peakDensity < dp.Density)
			{
				peakDensity = dp.Density;
			}
		}

		return peakDensity;
	}

	float GetMedianDensity(List<DensityPlot> densityPlots)
	{
		var densities = densityPlots.Select(d => d.Density).ToArray();
		Array.Sort(densities);
		int half = densities.Length / 2;

		// 偶数なら２値の平均を取る
		_medianDensity = densities.Length % 2 == 0 ?
			(densities[half] + densities[half + 1]) / 2f :
			densities[half + 1];

		return _medianDensity;
	}


	void DrawBg(DensityType densityType)
	{
		int typeIndex = (int)densityType;
		for (int i = 0; i < _densityGraphRawImage.Length; i++)
		{
			_densityGraphRawImage[i].gameObject.SetActive(typeIndex == i);
		}

		if (_isDensityDrawn[typeIndex])
		{
			return;
		}

		RawImage rawImage = _densityGraphRawImage[typeIndex];
		var drawTexture = _drawTexture[typeIndex];

		if (drawTexture == null)
		{
			drawTexture = CreateTexture(rawImage);
			SetTextureBuffer(drawTexture);
		}

		int width = drawTexture.width;
		int height = drawTexture.height;

		// 画面を塗りつぶす
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				_buffer[x + width * y] = BG_COLOR;
			}
		}

		drawTexture.SetPixels(_buffer);
		drawTexture.Apply();

		rawImage.texture = drawTexture;

		_isDensityDrawn[typeIndex] = true;
	}

	public void DrawGraph(DensityType densityType, int length)
	{
		if (length <= 1)
		{
			DrawNPS(length);
			DrawTime(0, 0);
			DrawBg(densityType);

			return;
		}

		int densityIndex = (int)densityType;

		for (int i = 0; i < _densityGraphRawImage.Length; i++)
		{
			_densityGraphRawImage[i].gameObject.SetActive(densityIndex == i);
		}

		// 一度描画していたら以降の処理を行わない
		if (_isDensityDrawn[densityIndex])
		{
			return;
		}

		RawImage rawImage = _densityGraphRawImage[densityIndex];
		var drawTexture = _drawTexture[densityIndex];

		if (drawTexture == null)
		{
			drawTexture = CreateTexture(rawImage);
			SetTextureBuffer(drawTexture);
		}

		int width = drawTexture.width;
		int height = drawTexture.height;

		Color lowTargetColor = GetGraphColor(densityType, false);
		Color highTargetColor = GetGraphColor(densityType, true);

		// 画面を塗りつぶす
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				_buffer[x + width * y] = BG_COLOR;
			}
		}

		// Median線描画
		int medianHeight = (int)(_totalMedianNps / _totalPeakNps * height);
		for (int x = 0; x < width; x++)
		{
			_buffer[x + width * medianHeight] = MEDIAN_LINE_COLOR;

			if (x + width * (medianHeight + 1) < _buffer.Length)
			{
				_buffer[x + width * (medianHeight + 1)] = MEDIAN_LINE_COLOR;
			}
		}

		var densityPlotList = _densityPlotList;

		// DensityGraph描画
		if (densityPlotList != null)
		{
			float lastTime = _lastTime;

			float maxDensity = densityPlotList.Max(v => v.Density);

			float rate = 1f;

			if (densityType != DensityType.Total)
			{
				float peakDensity = GetPeakDensity(densityPlotList);
				float peakNps = peakDensity * length;
				rate = _totalPeakNps / peakNps;
			}

			float yScale = maxDensity * rate;
			int plotCount = densityPlotList.Count;

			Span<int> pX = stackalloc int[plotCount];
			Span<int> pY = stackalloc int[plotCount];

			for (int i = 0; i < plotCount; i++)
			{
				pX[i] = (int)(densityPlotList[i].Time / lastTime * width);
				pY[i] = (int)(Mathf.Clamp(densityPlotList[i].Density / yScale * height, 0f, height - 1));
			}

			int index = 0;
			for (int i = 0; i < width; i++)
			{
				if (index + 1 < pX.Length)
				{
					int y = (int)Mathf.Lerp(pY[index], pY[index + 1], Mathf.Clamp01((float)(i - pX[index]) / (float)(pX[index + 1] - pX[index])));

					if (y > 0)
					{
						for (int j = 0; j <= y; j++)
						{
							_buffer[i + j * width] = Color.Lerp(lowTargetColor, highTargetColor, (float)j / height);
						}
					}
				}
				else if (index < pX.Length)
				{
					_buffer[i + pY[index] * width] = Color.Lerp(lowTargetColor, highTargetColor, (float)1 / height);
				}

				if (pX[index + 1] < i && index < pX.Length - 1)
				{
					index++;

					if (pX.Length <= index + 1)
					{
						break;
					}
				}
			}
		}

		drawTexture.SetPixels(_buffer);
		drawTexture.Apply();

		rawImage.texture = drawTexture;

		_isDensityDrawn[densityIndex] = true;
	}

	/// <summary>
	/// _bufferにテクスチャの色情報の配列を設定する
	/// </summary>
	/// <param name="drawTexture"></param>
	/// <param name="rawImage"></param>
	Texture2D CreateTexture(RawImage rawImage)
	{
		return new Texture2D((int)rawImage.rectTransform.sizeDelta.x, (int)rawImage.rectTransform.sizeDelta.y, TextureFormat.RGBA32, false)
		{
			filterMode = FilterMode.Point,
			wrapMode = TextureWrapMode.Clamp
		};
	}

	void SetTextureBuffer(Texture2D tex)
	{
		var pixels = tex.GetPixels();
		_buffer = new Color[pixels.Length];
	}

	void DrawNPS(float nps)
	{
		_peakNpsText.SetTextFormat(PEAK_NPS_FORMAT, nps.ToString("F2"));
		_medianNpsText.SetTextFormat(MEDIAN_NPS_FORMAT, nps.ToString("F2"));
	}

	void DrawPeekNPS(int noteLength, List<DensityPlot> densityPlots)
	{
		float peakDensity = GetPeakDensity(densityPlots);
		float peakNps = peakDensity * noteLength;
		_peakNpsText.SetTextFormat(PEAK_NPS_FORMAT, peakNps.ToString("F2"));

		_totalPeakNps = peakNps;
	}

	void DrawMedianNPS(int noteLength, List<DensityPlot> densityPlots)
	{
		float medianDensity = GetMedianDensity(densityPlots);
		float medianNps = medianDensity * noteLength;
		_medianNpsText.SetTextFormat(MEDIAN_NPS_FORMAT, medianNps.ToString("F2"));

		_totalMedianNps = medianNps;
	}

	void DrawTime(float offset, float lastTime)
	{
		if(offset == 0 && lastTime == 0)
		{
			_timeText.text = string.Empty;
			return;
		}

		float time = lastTime - offset;
		int minute = (int)(time / 60);
		int second = (int)time % 60;
		
		_timeText.SetTextFormat(TIME_FORMAT, minute, second);
	}

	Color GetGraphColor(DensityType type, bool isHigh) => type switch
	{
		DensityType.Total => isHigh ? TYPE_0_HIGH_TARGET_COLOR : TYPE_0_LOW_TARGET_COLOR,
		DensityType.Yellow => isHigh ? TYPE_1_HIGH_TARGET_COLOR : TYPE_1_LOW_TARGET_COLOR,
		DensityType.Blue => isHigh ? TYPE_2_HIGH_TARGET_COLOR : TYPE_2_LOW_TARGET_COLOR,
		DensityType.Green => isHigh ? TYPE_3_HIGH_TARGET_COLOR : TYPE_3_LOW_TARGET_COLOR,
		_ => Color.black
	};

	public void Dispose()
	{
		for (int i = 0; i < _drawTexture.Length; i++)
		{
			Destroy(_drawTexture[i]);
			_drawTexture[i] = null;
		}

		_drawTexture = null;
	}
}
