﻿using Cysharp.Text;
using Cysharp.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEngine;

public sealed class ExternalDirectory
{
	public static string SongsPath =>
		ZString.Concat(Directory.GetCurrentDirectory(), "/Songs");

	public static string NoteSkinsPath =>
		ZString.Concat(Directory.GetCurrentDirectory(), "/NoteSkins");

	public static string NoteSkinsStreamingAssetsPath =>
		ZString.Concat(Application.streamingAssetsPath, "/NoteSkins");

	public static string SoundEffectsPath =>
		ZString.Concat(Directory.GetCurrentDirectory(), "/SoundEffects");

	public static string GlobalLuaPath =>
		ZString.Concat(Directory.GetCurrentDirectory(), "/GlobalLua");


	public static async UniTask<List<NoteSkinInfo>> NoteSkinsCopyFromStreamingToPersistentAsync(CancellationToken ct)
	{
		NoteSkinsLoader noteSkinsLoader = new NoteSkinsLoader();
		var noteSkinsList = await noteSkinsLoader.ReadAsync(ct);
		return noteSkinsList;
	}

	public static List<TouchSeInfo> LoadTouchSeInfos()
	{
		return new TouchSeLoader().LoadTouchSeInfos();
	}

	public static List<GlobalLuaInfo> LoadGlobalLua()
	{
		return new GlobalLuaLoader().LoadGlobalLuaInfos();
	}
}
