﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StringReplaceSpriteAsset
{
	public static string ReplaceUra(string originalText)
	{
		string result = originalText;

		if (originalText.Contains("Ⓤ"))
		{
			result = originalText.Replace("Ⓤ", "<sprite name=\"ura\">");
		}

		if (originalText.CustomStartsWith("(Ura)"))
		{
			result = originalText.Replace("(Ura)", "<sprite name=\"ura\">");
		}

		if (originalText.Contains("Ⓓ"))
		{
			result = originalText.Replace("Ⓓ", "<sprite name=\"danmaku\">");
		}

		if (originalText.CustomStartsWith("(Danmaku)"))
		{
			result = originalText.Replace("(Danmaku)", "<sprite name=\"danmaku\">");
		}

		if (originalText.Contains("®"))
		{
			result = originalText.Replace("®", "<sprite name=\"rescore\">");
		}

		if (originalText.Contains("Ⓡ"))
		{
			result = originalText.Replace("Ⓡ", "<sprite name=\"rescore\">");
		}

		if (originalText.CustomStartsWith("(Rescore)"))
		{
			result = originalText.Replace("(Rescore)", "<sprite name=\"rescore\">");
		}

		if (originalText.Contains("Ⓕ"))
		{
			result = originalText.Replace("Ⓕ", "<sprite name=\"full\">");
		}

		if (originalText.CustomStartsWith("(Full)"))
		{
			result = originalText.Replace("(Full)", "<sprite name=\"full\">");
		}

		return result;
	}

}
