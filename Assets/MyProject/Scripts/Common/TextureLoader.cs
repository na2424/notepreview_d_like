﻿using System.IO;
using UnityEngine;

public sealed class TextureLoader : MonoBehaviour
{
	public static Texture2D Load(string path)
	{
		Texture2D texture = new Texture2D(1, 1, TextureFormat.RGB24, false);

		byte[] bytes = null;

		try
		{
			bytes = File.ReadAllBytes(path);
		}
		catch
		{
			return texture;
		}
		
		texture.filterMode = FilterMode.Bilinear;
		texture.LoadImage(bytes, true);

		return texture;
	}
}
