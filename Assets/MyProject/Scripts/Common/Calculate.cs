﻿using UnityEngine;

public static class Calculate
{
	public static bool FastApproximately(float a, float b, float threshold)
	{
		return ((a - b) < 0 ? ((a - b) * -1) : (a - b)) <= threshold;
	}

	public static bool FastApproximately(double a, double b, double threshold)
	{
		return ((a - b) < 0 ? ((a - b) * -1) : (a - b)) <= threshold;
	}

	public static int Digit(int num)
	{
		// Mathf.Log10(0)はNegativeInfinityを返すため、別途処理する。
		return (num == 0) ? 1 : ((int)Mathf.Log10(num) + 1);
	}

	/// <summary>
	/// 点Pから直線ABに下ろした垂線の足の座標を返す
	/// </summary>
	public static Vector2 PerpendicularFootPoint(Vector2 a, Vector2 b, Vector2 p)
	{
		Vector2 ab = (b - a).normalized;
		return a + Vector2.Dot(p - a, ab) * ab;
	}
}
