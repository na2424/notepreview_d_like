﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public sealed class TapEffectController : MonoBehaviour
{
	[SerializeField] ParticleSystem[] _ps;

	ObjectPool<TapEffectController> _pool;

	GameObject _gameObject;
	bool _isPlay = false;

	public void Init(ObjectPool<TapEffectController> pool)
	{
		_pool = pool;
		_gameObject = gameObject;
	}

	public void SetActive(bool isActive)
	{
		_gameObject.SetActive(isActive);
	}

	public void Play()
	{
		_isPlay = true;

		foreach (var p in _ps)
		{
			p.Play();
		}
	}

	void Update()
	{
		if (_isPlay && !_ps[0].isPlaying)
		{
			OnFinish();
		}
	}

	void OnFinish()
	{
		_isPlay = false;
		_pool.Release(this);
	}
}
