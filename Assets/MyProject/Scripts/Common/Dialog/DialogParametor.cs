﻿using System;
using UnityEngine;

namespace Dialog
{
	/// <summary>
	/// ダイアログを開くために必要なパラメータ.
	/// </summary>
	public class DialogParametor
	{
		/// <summary>
		/// 選択肢の種類.
		/// </summary>
		public enum OptionsType
		{
			Invalid,
			Single,
			DefaultCancel,
			DestructiveCancel,
		}

		public class ActionButton
		{
			/// <summary>
			/// ボタンに表示する(選択肢の)文字列.
			/// </summary>
			public string Label { get; private set; }
			/// <summary>
			/// ボタンを押下したときのコールバック.
			/// </summary>
			public Action Callback { get; private set; }

			public ActionButton(string label, Action callback)
			{
				Label = label;
				Callback = callback;
			}
		}

		/// <summary>
		/// タイトルに表示する文字列.
		/// </summary>
		public string Title { get; protected set; }
		/// <summary>
		/// メッセージ欄に表示する文字列.
		/// </summary>
		public string Message { get; protected set; }

		/// <summary>
		/// 通常の選択肢のボタン. 「OK」など.
		/// </summary>
		ActionButton _defaultButton = null;
		public ActionButton DefaultButton => _defaultButton;

		/// <summary>
		/// 破壊的決定な選択肢のボタン. 「破棄」など.
		/// </summary>
		ActionButton _destructiveButton = null;
		public ActionButton DestructiveButton => _destructiveButton;

		/// <summary>
		/// キャンセル選択肢のボタン.
		/// </summary>
		ActionButton _cancelButton = null;
		public ActionButton CancelButton => _cancelButton;

		/// <summary>
		/// ダイアログが勝手に閉じられたときのコールバック.
		/// </summary>
		public Action CallbackOnAutoClosed { get; private set; }

		/// <summary>
		/// ダイアログの選択肢の出し方.
		/// </summary>
		public OptionsType Options { get; private set; } = OptionsType.Invalid;

		/// <summary>
		/// プライオリティ.
		/// </summary>
		public DialogPriority Priority = DialogPriority.Normal;

		/// <summary>
		/// アナリティクスのイベント名.
		/// </summary>
		public string AnalyticsEventName = null;

		DialogParametor() { }

		/// <summary>
		/// DialogManager に渡すためのデータ DialogData を作るためのビルダークラス.
		/// </summary>
		public class Builder
		{
			readonly DialogParametor _param;

			/// <summary>
			/// コンストラクタ.
			/// </summary>
			/// <param name="title">タイトルに表示する文字列.</param>
			/// <param name="message">メッセージ欄に表示する文字列.</param>
			public Builder(string title, string message, DialogPriority priority = DialogPriority.Normal)
			{
				_param = new DialogParametor
				{
					Title = title,
					Message = message,
					Priority = priority,
				};
			}

			/// <summary>
			/// コンストラクタ.
			/// </summary>
			/// <param name="title">タイトルに表示する文字列.</param>
			/// <param name="message">メッセージ欄に表示する文字列.</param>
			public Builder(string title, string message, string analyticsEventName) : this(title, message, DialogPriority.Normal)
			{
				_param.AnalyticsEventName = analyticsEventName;
			}

			bool addAction(ref ActionButton dst, ActionButton src)
			{
				if (src == null || dst != null)
				{
					Debug.Assert(false);
					return false;
				}
				dst = src;
				return true;
			}

			/// <summary>
			/// 通常選択肢のボタンを加える.
			/// </summary>
			/// <param name="label">ボタンに表示する(選択肢の)文字列.</param>
			/// <param name="callback">ボタンを押下したときのコールバック.</param>
			/// <returns>Builder</returns>
			public Builder AddDefaultAction(string label, Action callback)
			{
				if (_param.AnalyticsEventName != null)
				{
					//callback += () => getAnalyticsEventData(   ).SendEvent();
				}

				if (addAction(ref _param._defaultButton, new ActionButton(label, callback)))
					return this;
				return null;
			}

			/// <summary>
			/// 破壊的選択肢のボタンを加える.
			/// </summary>
			/// <param name="label">ボタンに表示する(選択肢の)文字列.</param>
			/// <param name="callback">ボタンを押下したときのコールバック.</param>
			/// <returns>Builder</returns>
			public Builder AddDestructiveAction(string label, Action callback)
			{
				if (addAction(ref _param._destructiveButton, new ActionButton(label, callback)))
					return this;
				return null;
			}

			/// <summary>
			/// キャンセル選択肢のボタンを加える.
			/// </summary>
			/// <param name="label">ボタンに表示する(選択肢の)文字列.</param>
			/// <param name="callback">ボタンを押下したときのコールバック.</param>
			/// <returns>Builder</returns>
			public Builder AddCancelAction(string label, Action callback)
			{
				if (_param.AnalyticsEventName != null)
				{
					//callback += () => getAnalyticsEventData(  ).SendEvent();
				}

				if (addAction(ref _param._cancelButton, new ActionButton(label, callback)))
					return this;
				return null;
			}

			public Builder AddCallbackOnAutoClosed(Action callback)
			{
				_param.CallbackOnAutoClosed += callback;
				return this;
			}

			/// <summary>
			/// 生成したデータを返す.
			/// </summary>
			/// <returns>生成したデータ.</returns>
			public DialogParametor Build()
			{
				if (_param.DefaultButton != null && _param.DestructiveButton == null && _param.CancelButton == null)
					_param.Options = OptionsType.Single;
				else if (_param.DefaultButton != null && _param.DestructiveButton == null && _param.CancelButton != null)
					_param.Options = OptionsType.DefaultCancel;
				else if (_param.DefaultButton == null && _param.DestructiveButton != null && _param.CancelButton != null)
					_param.Options = OptionsType.DestructiveCancel;
				else
				{
					_param.Options = OptionsType.Invalid;
					return null;
				}

				return _param;
			}
		}
	}
}
