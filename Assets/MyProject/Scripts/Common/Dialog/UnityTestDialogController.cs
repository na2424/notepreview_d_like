﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Dialog
{
	/// <summary>
	/// テスト用ダイアログのためのコンポーネント.
	/// </summary>
	public class UnityTestDialogController : UnityDialogController
	{
		public static bool IsTest = false;
		public static Action<Tuple<string, string>> TestCallBack = null;
		readonly WaitForSeconds WaitTime = new WaitForSeconds( 1f );

		public new static UnityTestDialogController Create()
		{
			var unityDialogPrefab = Resources.Load<GameObject>("Dialog/Dialog_UnityTestEditor");
			if( unityDialogPrefab )
			{
				// UnityAPI の関数はメインスレッドからしか呼んではいけないが、
				// DialogManager の Start() でしか呼ばれない
				var obj = Instantiate( unityDialogPrefab );
				if( obj )
					return obj.GetComponent<UnityTestDialogController>();
			}
			return null;
		}

		public override bool Open( DialogParametor data )
		{
			base.Open( data );
			StartCoroutine( DelayClickDecideButton() );
			return true;
		}

		IEnumerator DelayClickDecideButton()
		{
			yield return WaitTime;
			var e = new PointerEventData( FindObjectOfType<EventSystem>() );
			_decideButton.OnPointerClick( e );
		}

		protected override bool close()
		{
			TestCallBack?.Invoke( new Tuple<string, string>( _titleText.text, _messageText.text ) );
			return base.close();
		}

		protected override void OnDisable()
		{
			base.OnDisable();
			TestCallBack = null;
		}
	}
}
