﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Dialog
{
	/// Dialog のプライオリティ.
	/// DialogManager のプライオリティと比較し、
	/// DialogManagerPriority < DialogPriority のときはダイアログを表示しない.
	/// （「プライオリティの高いダイアログが、プライオリティの低いダイアログを上書きする」ような運用は想定していない.）
	public enum DialogPriority
	{
		Normal = 0,
		High = 1,
	}

	/// DialogManager のプライオリティ.
	/// Dialog のプライオリティと比較し、
	/// DialogManagerPriority < DialogPriority のときはダイアログを表示しない.
	/// 連番ではない（複数の識別子が同じ値を持ってもよい）.
	/// もっと細かく制御しなけれなならなくなったら、Mask にした方がいいかも.
	public enum DialogManagerPriority
	{
		Normal = 0,
		Player = 1,
	}

	/// <summary>
	/// ダイアログマネージャ.
	/// </summary>
	[Prefab("SingletonPrefabs/Dialog/DialogManager", true)]
	public class DialogManager : SingletonMonoBehaviour<DialogManager>
	{
		public const string GameObjectName = "DialogManager";

		// ダイアログコントロールインタフェイス.
		IDialogController _dialogController = null;

		class RequestedDialogParams
		{
			public DialogParametor DialogParam = null;
			public Action SucceededCallback = null;
			public Action FailedCallback = null;
			public string File = "";
			public int Line = 0;
			public string Member = "";
		}
		RequestedDialogParams _requestedDialogParams = null;

		DialogManagerPriority _priority = DialogManagerPriority.Normal;
		public void SetPriority(DialogManagerPriority priority)
		{
			Debug.Log($"Set Priority of DialogManager => [{priority}]");
			_priority = priority;
		}

		void Start()
		{
#if UNITY_EDITOR
			_dialogController = UnityTestDialogController.IsTest ?
				UnityTestDialogController.Create() :
				UnityDialogController.Create();
#else
			_dialogController = UnityDialogController.Create();
#endif
		}

		void Update()
		{
			if (_requestedDialogParams != null)
			{
				//Debug.Assert(_dialogController != null);

				var p = _requestedDialogParams;

				if ((int)p.DialogParam.Priority < (int)_priority)
				{
					// プライオリティ比較によるダイアログオープン失敗.
					FailedToOpenDialog(
						$"Failed to Open Dialog (DialogPriolity[{p.DialogParam.Priority}], ManagerPriolity[{_priority}]) [ {p.DialogParam.Title} | {p.DialogParam.Message} ]",
						p
					);
				}
				else if (!_dialogController.Open(p.DialogParam))
				{
					// ダイアログオープン失敗.
					FailedToOpenDialog(
						$"Failed to Open Dialog [ {p.DialogParam.Title} | {p.DialogParam.Message} ]",
						p
					);
				}
				else
				{
					// 成功.
					p.SucceededCallback?.Invoke();
				}

				_requestedDialogParams = null;
			}
		}
		void FailedToOpenDialog(string mes, RequestedDialogParams p)
		{
			// ダイアログを呼び出そうとしたコードの ファイル名/行番号/関数名 を渡す.
			Debug.LogError($"{mes}, {p.File}, {p.Line}, {p.Member}");

			// 失敗時のコールバックを発火.
			p.FailedCallback?.Invoke();
		}

		/// <summary>
		/// ダイアログを開く.
		/// </summary>
		/// <param name="param">ダイアログ情報. DialogData.Builder で生成する.</param>
		/// <param name="succeededCallback">ダイアログを正しく開けたときのコールバック.</param>
		/// <param name="failedCallback">ダイアログを開けなかったときのコールバック.</param>
		public void Open(
			DialogParametor param,
			Action succeededCallback = null,
			Action failedCallback = null,
			[CallerFilePath] string file = "",
			[CallerLineNumber] int line = 0,
			[CallerMemberName] string member = "")
		{
			if (_requestedDialogParams != null)
				failedCallback?.Invoke();

			_requestedDialogParams = new RequestedDialogParams
			{
				DialogParam = param,
				SucceededCallback = succeededCallback,
				FailedCallback = failedCallback,
				File = file,
				Line = line,
				Member = member,
			};
		}

		/// <summary>
		/// ダイアログを閉じる.
		/// ScreenOrientationManager から呼ぶためのもの.
		/// </summary>
		public void Close(
			[CallerFilePath] string file = "",
			[CallerLineNumber] int line = 0,
			[CallerMemberName] string member = "")
		{
			Debug.Log($"Close Dialog | From [{file}:{line}|{member}]");
			_dialogController?.Close();
		}

		/// <summary>
		/// アプリケーションのフォーカス状態が変化した時のコールバック.
		/// （アプリケーションのフォーカスが外れた時（≒スリープした時）、ダイアログは閉じる.）
		/// </summary>
		/// <param name="hasFocus">フォーカス状態.</param>
		void OnApplicationFocus(bool hasFocus)
		{
			if (!hasFocus)
			{
#if UNITY_EDITOR
				Debug.Log($"Close Dialog OnApplicationFocus -> {hasFocus}");
#endif
				_dialogController?.Close();
			}
		}
	}
}
