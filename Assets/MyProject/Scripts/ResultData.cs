﻿[System.Serializable]
public class ResultData
{
    public int Score;
    public Judge Judge;
    public bool IsDead;
    public bool IsFullCombo;
    public bool IsAllBrilliant;
    public int MaxCombo;
    public TimingHistory TimingHistory;
    public LifeHistory LifeHistory;
    public NoFuzzyTimingHistory NoFuzzyTimingHistory;
    public EarlyLate EarlyLate;
}