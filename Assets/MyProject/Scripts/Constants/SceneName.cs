﻿/// <summary>
/// シーン名を定数で管理するクラス
/// </summary>
public static class SceneName
{
	public const string SelectMusic = "SelectMusic";
	public const string Game = "Game";
	public const string Result = "Result";
}
