﻿/// <summary>
/// AddressableAssetのAddressを管理する定数クラス
/// </summary>
public static class AddressableAssetAddress{

	  public const string EDITOR_SCENE_LIST          = "EditorSceneList";
	  public const string PREFABS_JUDGE_EFFECT       = "Prefabs/JudgeEffect";
	  public const string PREFABS_JUDGE_EFFECT_TYPE2 = "Prefabs/JudgeEffect_Type2";
	  public const string PREFABS_LIGHT_ENCHANT      = "Prefabs/LightEnchant";
	  public const string PREFABS_NOTE               = "Prefabs/Note";
	  public const string PREFABS_NOTE_LEGACY        = "Prefabs/Note_Legacy";
	  public const string RESOURCES                  = "Resources";

}
