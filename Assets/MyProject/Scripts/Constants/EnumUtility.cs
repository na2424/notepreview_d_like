﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EnumUtility
{
    public static string DifficultyToString(DifficultyType type) => type switch
	{
		DifficultyType.Easy => "Easy",
		DifficultyType.Normal => "Normal",
		DifficultyType.Hard => "Hard",
		DifficultyType.Extra => "Extra",
		DifficultyType.Lunatic => "Lunatic",
		_ => ""
	};
}
