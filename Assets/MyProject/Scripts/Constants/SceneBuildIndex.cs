﻿/// <summary>
/// シーンのビルドインデックスを定数で管理するクラス
/// </summary>
public static class SceneBuildIndex
{
	public const int SelectMusic = 0;
	public const int Game = 1;
	public const int Result = 2;
}
