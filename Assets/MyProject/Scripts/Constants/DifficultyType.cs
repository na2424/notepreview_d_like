﻿/// <summary>
/// 難易度の種類
/// </summary>
public enum DifficultyType : int
{
	Easy = 0,
	Normal = 1,
	Hard = 2,
	Extra = 3,
	Lunatic = 4,
}
