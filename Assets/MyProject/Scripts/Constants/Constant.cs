﻿using UnityEngine;

public static class Constant
{
	public static class ShaderProperty
	{
		public readonly static int MainTexId = Shader.PropertyToID("_MainTex");
		public readonly static int MaskTexId = Shader.PropertyToID("_MaskTex");
		public readonly static int RangeId = Shader.PropertyToID("_Range");
		public readonly static int StencilId = Shader.PropertyToID("_Stencil");
		public readonly static int BottomColor = Shader.PropertyToID("_BottomColor");
		public readonly static int TopColor = Shader.PropertyToID("_TopColor");
		public readonly static int Color = Shader.PropertyToID("_Color");
		public readonly static int Alpha = Shader.PropertyToID("_Alpha");
		public readonly static int OutlineColor = Shader.PropertyToID("_OutlineColor");
		public readonly static int FaceDilate = Shader.PropertyToID("_FaceDilate");
		public readonly static int OutlineWidth = Shader.PropertyToID("_OutlineWidth");
	}

	public static class AudioMixer
	{
		public readonly static string PITCH = "Pitch";
	}

	public static class Note
	{
		public const float BEAT_DISTANCE = 1.5f;
		public const float TIME_DISTANCE = 3f;
		public const float LANE_DISTANCE = 0.5f;
		public const float OFFSET_X = 1.5f;
		public const float OFFSET_Z = 0f;
		public const float LANE_LENGTH = 25f;
		public const float FIXED_BPM = 120f;
		public readonly static string DEFAULT_NOTESKINS_NAME = "Default";
		public readonly static string COLORB_NOTESKINS_NAME = "色覚対応";
		public readonly static string DEFAULT_TOUCH_SE_NAME = "ダンカグライク";
		public readonly static string DEFAULT_TOUCH_SE_ACB = "CueSheet_0.acb";
		public readonly static Color DefaultSeparateColor = new Color(255 / 255f, 206 / 255f, 0 / 255f, 155 / 255f);
		public readonly static Color DefaultBeamColor = new Color(236 / 255f, 226 / 255f, 0, 128 / 255f);
		public readonly static Color DefaultJudgePlateColor = new Color(93 / 255f, 83 / 255f, 50 /255f, 188 / 255f);
		public const int LANE_COUNT = 7;
	}

	public static class PlayerOption
	{
		public static class SoundVolume
		{
			public const float DEFAULT_MUSIC_VOLUME = 0.8f;
			public const float DEFAULT_SYSTEM_SE_VOLUME = 0.5f;
			public const float DEFAULT_TOUCH_SE_VOLUME = 0.5f;
		}

		public static class Other
		{
			public readonly static string DEFAULT_GLOBAL_LUA_NAME = "None";
			public readonly static string DEFAULT_GLOBAL_LUA_FILENAME = "";
		}
	}

	public static class RankBoundary
	{
		public const int S_PLUS = 990000;
		public const int S = 900000;
		public const int A = 800000;
		public const int B = 700000;
		public const int C = 600000;
	}

	public static class JudgeTime
	{
		public const float BRILLIANT_TIME = 0.050f;
		public const float GREAT_TIME = 0.100f;
		public const float FAST_TIME = 0.120f;
		public const float BAD_TIME = 0.150f;
		public const float LONG_REVISION_TIME = 0.033f;
		public const float LONG_REVISION_DISTANCE = 0.5f;
		public const float JUDGE_DISTANCE = 0.55f;
		public const float FUZZY_START_TIME = -0.016f;
	}

	public static class SpecialNumber
	{
		public const int DIFFICULT_X = 12345678;
	}

	public static class PlayerPrefsName
	{
		public const string CACHE_NOTICE_ID_PREFSNAME = "CacheNoticeId";
	}

	public static class App
	{
		public const string VersionName = "v1.59-2";
	}
}
