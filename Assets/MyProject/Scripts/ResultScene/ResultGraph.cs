﻿using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ResultGraph : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] CanvasGroup _graphCanvasGroup = default;
	[SerializeField] RawImage _graphImage = default;
	[SerializeField] ColorDefine _colorDefine;

	//------------------
	// キャッシュ.
	//------------------
	Color[] _buffer;
	Texture2D _drawTexture = null;
	float _musicRate = 1f;

	//------------------
	// 定数.
	//------------------

	//------------------
	// プロパティ.
	//------------------
	double BrilliantTime => GameManager.Instance.JudgeTimeOption.BrilliantTime * _musicRate;
	double GreatTime => GameManager.Instance.JudgeTimeOption.GreatTime * _musicRate;
	double FastTime => GameManager.Instance.JudgeTimeOption.FastTime * _musicRate;
	double BadTime => GameManager.Instance.JudgeTimeOption.BadTime * _musicRate;
	double HitTime => GameManager.Instance.JudgeTimeOption.GetLongEndHitTime() * _musicRate;
	public CanvasGroup CanvasGroup => _graphCanvasGroup;

	public void Init()
	{
		CanvasGroup.alpha = 0f;
	}

	public async UniTask DrawGraphAsync()
	{
		_musicRate = GameManager.Instance.JudgeTimeOption.MusicRate;
		_drawTexture = new Texture2D((int)_graphImage.rectTransform.sizeDelta.x, (int)_graphImage.rectTransform.sizeDelta.y, TextureFormat.RGBA32, false);
		_drawTexture.filterMode = FilterMode.Point;
		_drawTexture.wrapMode = TextureWrapMode.Clamp;

		int width = _drawTexture.width;
		int height = _drawTexture.height;
		Color[] pixels = _drawTexture.GetPixels();

		await UniTask.SwitchToThreadPool();

		_buffer = new Color[pixels.Length];
		pixels.CopyTo(_buffer, 0);

		float perfectScaleY = (float)BrilliantTime / (float)HitTime;
		int parfectFastY = (int)(-perfectScaleY * (height / 2) + (height / 2));
		int parfectSlowY = (int)(perfectScaleY * (height / 2) + (height / 2));

		// 画面を塗りつぶす
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				if (y == height / 2)
				{
					_buffer[x + width * y] = Color.gray;
				}
				else if (y == parfectFastY)
				{
					_buffer[x + width * y] = Color.gray;
				}
				else if (y == parfectSlowY)
				{
					_buffer[x + width * y] = Color.gray;
				}
				else
				{
					_buffer[x + width * y] = Color.black;
				}
			}
		}

		// ミス描画
		var timingHistory = GameManager.Instance.ResultData.TimingHistory;
		if (timingHistory != null)
		{
			float lastTime = timingHistory.MusicTime.Last();
			float yScale = (float)HitTime;

			int count = timingHistory.MusicTime.Count;
			for (int i = 0; i < count; i++)
			{
				int y = (int)(timingHistory.Timing[i] / yScale * (height / 2) + (height / 2));

				if (y > height)
				{
					int x = (int)(timingHistory.MusicTime[i] / lastTime * width);
					Color color = JudgeToColor(timingHistory.JudgeTypes[i]);
					for (int j = 0; j < height; j++)
					{
						//x = (int)(timingHistory.MusicTime[i] / lastTime * width);
						var index = x + j * width;

						if (0 <= index && index < _buffer.Length && x < width)
						{
							// Error:
							// IndexOutOfRangeException: Index was outside the bounds of the array.
							_buffer[x + j * width] = color;
						}
					}
				}
			}
		}

		// ライフ描画
		var lifeHistory = GameManager.Instance.ResultData.LifeHistory;
		if (lifeHistory != null)
		{
			float lastTime = lifeHistory.MusicTime.Last();
			float yScale = 500f;

			List<int> pX = new List<int>();
			List<int> pY = new List<int>();

			for (int i = 0; i < lifeHistory.MusicTime.Count; i++)
			{
				pX.Add((int)(lifeHistory.MusicTime[i] / lastTime * width));
				pY.Add((int)(Mathf.Clamp(lifeHistory.Life[i] / yScale * height, 0f, height - 1)));
			}

			int index = 0;
			for (int i = 0; i < width; i++)
			{
				if (index + 1 < pX.Count)
				{
					int y = (int)Mathf.Lerp(pY[index], pY[index + 1], Mathf.Clamp01((float)(i - pX[index]) / (float)(pX[index + 1] - pX[index])));

					_buffer[i + y * width] = Color.white;

					//可能なら下にもう2px分描画
					if (y - 1 >= 0)
					{
						_buffer[i + (y - 1) * width] = Color.white;
					}

					//if (y - 2 >= 0)
					//{
					//	_buffer[i + (y - 2) * width] = Color.white;
					//}
				}
				else if (index < pX.Count)
				{
					_buffer[i + pY[index] * width] = Color.white;
				}

				if (pX[index + 1] < i && index < pX.Count - 1)
				{
					index++;
				}
			}
		}

		// タイミング描画
		if (timingHistory != null)
		{
			float lastTime = timingHistory.MusicTime.Last();
			float yScale = (float)HitTime;

			int count = timingHistory.MusicTime.Count;
			for (int i = 0; i < count; i++)
			{
				int y = (int)(timingHistory.Timing[i] / yScale * (height / 2) + (height / 2));

				if (y <= height)
				{
					int x = (int)(timingHistory.MusicTime[i] / lastTime * width);
					Color color = JudgeToColor(timingHistory.JudgeTypes[i]);
					int index = x + y * width;
					if (0 <= index && index < _buffer.Length)
					{
						_buffer[index] = color;

						//可能なら2px分描画
						if (x + 1 < width && y + 1 < height)
						{
							_buffer[(x + 1) + y * width] = color;
							_buffer[x + (y + 1) * width] = color;
							_buffer[(x + 1) + (y + 1) * width] = color;
						}
					}
				}

			}
		}

		await UniTask.SwitchToMainThread();

		_drawTexture.SetPixels(_buffer);
		_drawTexture.Apply();

		_graphImage.texture = _drawTexture;

		CanvasGroup.DOFade(1f, 1f);
	}

	Color JudgeToColor(JudgeType type) => type switch
	{
		JudgeType.Brilliant => _colorDefine.Cyan,
		JudgeType.Great => _colorDefine.Yellow,
		JudgeType.Fast => _colorDefine.Green,
		JudgeType.Slow => _colorDefine.Green,
		JudgeType.Bad => _colorDefine.Blue,
		JudgeType.Missed => _colorDefine.Red,
		JudgeType.None => Color.white,
		_ => Color.white
	};

	public void Draw(Color color, int pX, int pY)
	{
		for (int x = 0; x < _drawTexture.width; x++)
		{
			for (int y = 0; y < _drawTexture.height; y++)
			{
				if (x == pX && y == pY)
				{
					int index = x + _drawTexture.width * y;
					if (0 <= index && index < _buffer.Length)
					{
						_buffer[index] = color;
					}
					return;
				}
			}
		}
	}
}
