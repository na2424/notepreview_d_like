﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public sealed class ResultPlayModePanel : MonoBehaviour
{
    [SerializeField] Image _playModeImage;
    [SerializeField] List<PlayModePair> _playStates;

    public void SetPlayMode(PlayMode playMode)
	{
		if (playMode == PlayMode.Normal || playMode == PlayMode.InputCheck)
		{
			_playModeImage.gameObject.SetActive(false);
		}
		else
		{
			_playModeImage.sprite = _playStates.Find(p => p.PlayMode == playMode).Sprite;
			_playModeImage.gameObject.SetActive(true);
		}
	}
}
