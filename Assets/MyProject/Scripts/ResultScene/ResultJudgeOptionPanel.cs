﻿using Cysharp.Text;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ResultJudgeOptionPanel : MonoBehaviour
{
	[SerializeField] GameObject _judgeOptionPanelObj;
	[SerializeField] TextMeshProUGUI _brilliantText = default;
	[SerializeField] TextMeshProUGUI _greatText = default;
	[SerializeField] TextMeshProUGUI _fastText = default;
	[SerializeField] TextMeshProUGUI _badText = default;
	[SerializeField] TextMeshProUGUI _judgeDistanceText = default;
	[SerializeField] TextMeshProUGUI _longRevisionTimeText = default;
	[SerializeField] TextMeshProUGUI _longRevisionDistanceText = default;
	[SerializeField] TextMeshProUGUI _fuzzyStartTimeText = default;

	public void SetActive(bool isActive)
	{
		_judgeOptionPanelObj.SetActive(isActive);
	}

	public void SetJudgeOptionText()
	{
		_judgeOptionPanelObj.SetActive(false);
		var judgeTimeOption = GameManager.Instance.JudgeTimeOption;
		_brilliantText.text = ZString.Concat("Brilliant : ", ((int)((decimal)judgeTimeOption.BrilliantTime * 1000)).ToString(), "ms");
		_greatText.text = ZString.Concat("Great : ", ((int)((decimal)judgeTimeOption.GreatTime * 1000)).ToString(), "ms");
		_fastText.text = ZString.Concat("Fast : ", ((int)((decimal)judgeTimeOption.FastTime * 1000)).ToString(), "ms");
		_badText.text = ZString.Concat("Bad : ", ((int)((decimal)judgeTimeOption.BadTime * 1000)).ToString(), "ms");

		//var localize = LocalizeManager.Instance;

		_judgeDistanceText.SetTextFormat("判定の横幅 : {0}", (decimal)judgeTimeOption.JudgeDistance);
		_longRevisionTimeText.SetTextFormat("ロング時間補正 : {0}{1}", ((int)((decimal)judgeTimeOption.LongRevisionTime * 1000)).ToString(), "ms");
		_longRevisionDistanceText.SetTextFormat("ロング横幅補正 : {0}", ((decimal)judgeTimeOption.LongRevisionDistance).ToString());
		_fuzzyStartTimeText.SetTextFormat("ファジー判定開始時間 : {0}{1}", ((int)((decimal)judgeTimeOption.FuzzyStartTime * 1000)).ToString(), "ms");

		//_fuzzyStartTimeText.SetTextFormat(localize.GetStringValue(LocalizationConstant.FuzzyStartTime), ((int)((decimal)judgeTimeOption.FuzzyStartTime * 1000)).ToString(), "ms");
	}
}

// 多言語対応メモ
// JudgeDistanceText,判定の横幅 : {0},Judge distance: {0}
// LongRevisionTime,ロング時間補正 : {0}{1},Long time correction: {0}{1}
// LongRevisionDistance,ロング横幅補正 : {0},Long distance correction: {0}
// FuzzyStartTime,ファジー判定開始時間 : {0}{1},Fuzzy judge start time: {0}{1}