﻿using Cysharp.Text;
using TMPro;
using UnityEngine;

public sealed class ResultMusicRatePanel : MonoBehaviour
{
	[SerializeField] GameObject MusicRatePanelObj;
	[SerializeField] TextMeshProUGUI _musicRateText = default;

	public void Init()
	{
		//if (GameManager.Instance.PlayModeOption.PlayMode == PlayMode.Online)
		//{
		//	MusicRatePanelObj.SetActive(false);
		//	return;
		//}

		float musicRate = GameManager.Instance.JudgeTimeOption.MusicRate;

		if (Calculate.FastApproximately(musicRate, 1f, 0.0001f))
		{
			MusicRatePanelObj.SetActive(false);
			return;
		}

		MusicRatePanelObj.SetActive(true);
		_musicRateText.SetTextFormat("x{0:F2}", musicRate);
	}
}
