using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultBgmController : MonoBehaviour
{
    [SerializeField] AudioSource _audioSource;

    public void Play()
    {
        _audioSource.volume = GameManager.Instance.VolumeOption.MusicVolume;
        _audioSource.loop = true;
        _audioSource.Play();
    }
}
