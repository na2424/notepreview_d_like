﻿using Cysharp.Text;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ELEBEAT;

public sealed class ResultSceneView : ViewBase
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------

	[Header("Panel")]
	[SerializeField] CanvasGroup _mainPanel = default;
	[SerializeField] CanvasGroup _scorePanel = default;
	[SerializeField] CanvasGroup _judgePanel = default;
	[SerializeField] CanvasGroup _underJudgePanel = default;
	[SerializeField] CanvasGroup _comboPanel = default;

	[Header("Panel (Controller)")]
	[SerializeField] ClearStateViewController _clearStateViewController;
	[SerializeField] ResultMusicRatePanel _musicRatePanel;

	[Header("Main")]
	[SerializeField] Button _backSelectMusicButton = default;
	[SerializeField] Button _judgeOptionPanelButton = default;
	[SerializeField] AspectRatioImage _jacketImage = default;
	[SerializeField] Canvas _jacketCanvas = default;

	[Header("RightTop")]
	[SerializeField] TextScroll _titleTextScroll = default;
	[SerializeField] TextScroll _artistTextScroll = default;
	[SerializeField] TextMeshProUGUI _difficultyText = default;
	[SerializeField] Image _difficultyBgImage = default;
	[SerializeField] ResultPlayModePanel _playModePanel;

	[Header("RightMiddle")]
	[SerializeField] TextMeshProUGUI[] _judgeText;
	[SerializeField] TextMeshProUGUI _maxComboText = default;
	[SerializeField] TextMeshProUGUI _scoreText = default;
	[SerializeField] TextMeshProUGUI _highScoreText = default;
	[SerializeField] Image _newRecodeScoreImage = default;
	[SerializeField] Image _newRecodeComboImage = default;
	[SerializeField] Image _rankImage = default;
	[SerializeField] RefSpriteScriptableObject _rankSprite;
	[SerializeField] GameObject _rankUIParticle;
	[SerializeField] TextMeshProUGUI[] _earlyLateText;
	[SerializeField] GameObject _earlyLateObj;
	[SerializeField] LayoutElement _earlyLateLayoutElement;

	[Header("JacketArea")]
	[SerializeField] ResultJudgeOptionPanel _judgeOptionPanel;

	[Header("Others")]
	[SerializeField] GameObject _nextPanel;
	[SerializeField] GameObject _customJudgement;
	[SerializeField] TapEffect _tapEffect;
	[SerializeField] ColorDefine _colorDefine;

	//------------------
	// キャッシュ.
	//------------------

	//------------------
	// 定数.
	//------------------
	const int EARLY = 0;
	const int LATE = 1;

	//------------------
	// プロパティ.
	//------------------

	public Button.ButtonClickedEvent OnClickBackSelectMusicButton { get { return _backSelectMusicButton.onClick; } }
	public Button.ButtonClickedEvent OnClickJudgeOptionPanelButton { get { return _judgeOptionPanelButton.onClick; } }
	public GameObject NextPanelObj => _nextPanel;
	public GameObject CustomJudgementObj => _customJudgement;

	public void SetInteractable_BackSelectMusicButton(bool interactable)
	{
		_backSelectMusicButton.interactable = interactable;
	}

	public void SetInteractable_JudgeOptionPanelButton(bool interactable)
	{
		_judgeOptionPanelButton.interactable = interactable;
	}

	public override void Init()
	{
		NextPanelObj.SetActive(false);
		FadeManager.Instance.SetBlack();
		_jacketCanvas.pixelPerfect = false;

		_mainPanel.alpha = 0f;
		_scorePanel.alpha = 0f;
		_judgePanel.alpha = 0f;
		_underJudgePanel.alpha = 0f;
		_comboPanel.alpha = 0f;
		_rankImage.color = Color.clear;
		_rankImage.sprite = null;
		_backSelectMusicButton.interactable = false;

		_scoreText.NonAllocateSetText(0, 7);

		_musicRatePanel.Init();
		_clearStateViewController.Init();
		_tapEffect.Init();

		// 判定幅オプションのテキスト初期化
		_judgeOptionPanel.SetJudgeOptionText();

		// 背景
		BgManager.Instance.UseDimmer(false);
	}

	public void SetJacketTexture(SongInfo songInfo)
	{
		if (songInfo.JacketTexture != null)
		{
			_jacketImage.SetTexture(songInfo.JacketTexture);
		}
	}

	public void SetPlayMode(PlayMode playMode)
	{
		_playModePanel.SetPlayMode(playMode);
	}

	public void SetMusicInfo(SongInfo songInfo, DifficultyType difficultyType)
	{
		_titleTextScroll.SetText(ZString.Concat(StringReplaceSpriteAsset.ReplaceUra(songInfo.Title), " ", songInfo.SubTitle));
		_artistTextScroll.SetText(songInfo.Artist);
		if (songInfo.Difficulty != null && songInfo.Difficulty.Count > 0)
		{

			_difficultyText.text = ZString.Concat(EnumUtility.DifficultyToString(GameManager.Instance.SelectDifficulty), "★", ConvertDifficultyString(GetDifficultyValue(songInfo)));
			_difficultyBgImage.color = _colorDefine.GetDifficultyColor(difficultyType);
		}
	}

	public void SetMaxComboText(int maxCombo)
	{
		_maxComboText.SetText(maxCombo);
	}

	int GetDifficultyValue(SongInfo songInfo) => GameManager.Instance.SelectDifficulty switch
	{
		DifficultyType.Easy => songInfo.Difficulty[0],
		DifficultyType.Normal => songInfo.Difficulty[1],
		DifficultyType.Hard => songInfo.Difficulty[2],
		DifficultyType.Extra => songInfo.Difficulty[3],
		DifficultyType.Lunatic => songInfo.Difficulty[4],
		_ => 0
	};

	string ConvertDifficultyString(int difficulty)
	{
		return difficulty == Constant.SpecialNumber.DIFFICULT_X ?
				"X" :
				difficulty.ToString();
	}

	public void ShowMainPanel(Action onComplete)
	{
		_mainPanel.DOFade(1f, 1f).OnComplete(() =>
		{
			_jacketCanvas.pixelPerfect = true;
			onComplete();
		});
	}

	public void ShowClearState(ClearStateType type)
	{
		_clearStateViewController.ShowState(type, false, true);
	}

	//public void ShowAnimOnlinePanel()
	//{
	//	_resultOnlinePanel.ShowAnimOnlinePanel();
	//}

	public void ShowHighScore(ResultData resultData, ScoreData beforeData, bool canSaveCustomJudgent)
	{
		bool isRecordPlayMode = GameManager.Instance.PlayModeOption.PlayMode == PlayMode.Normal /*|| GameManager.Instance.PlayModeOption.PlayMode == PlayMode.Online*/;
		bool isNewRecordScore = isRecordPlayMode && canSaveCustomJudgent && !resultData.IsDead && (beforeData.Id == "" || beforeData.Score < resultData.Score);
		bool isNewRecordCombo = isRecordPlayMode && canSaveCustomJudgent && !resultData.IsDead && (beforeData.Id == "" || beforeData.Combo < resultData.MaxCombo);
		_newRecodeScoreImage.gameObject.SetActive(isNewRecordScore);
		_newRecodeComboImage.gameObject.SetActive(isNewRecordCombo);

		int highScore = isNewRecordScore ? resultData.Score : beforeData.Score;
		_highScoreText.NonAllocateSetText(highScore, 7);
	}

	static string GetZeroFillStringWithAlpha(int value, int zeroFillLength)
	{
		using (var sb = ZString.CreateUtf8StringBuilder())
		{
			var digit = Calculate.Digit(value);
			int d = digit;

			if (digit > zeroFillLength)
			{
				zeroFillLength = digit;
			}

			if (digit < zeroFillLength)
			{
				sb.Append("<alpha=#66>");
				var zeroDigit = zeroFillLength - digit;
				for (int i = 0; i < zeroDigit; i++)
				{
					sb.Append('0');
				}

				sb.Append("<alpha=#FF>");
				sb.Append(value);
			}
			else
			{
				sb.Append(value);
			}

			return sb.ToString();
		}

	}

	public void ShowJudge(int[] judges, int maxDigit)
	{
		for (int i = 0; i < _judgeText.Length; i++)
		{
			_judgeText[i].text = GetZeroFillStringWithAlpha(judges[i], maxDigit);
		}
	}

	public void ShowEarlyLate(bool isShow, EarlyLate earlyLate)
	{
		_earlyLateObj.SetActive(isShow);

		if (isShow)
		{
			//Locale locale = LocalizationSettings.SelectedLocale;
			//bool isEnglish = locale.Identifier.Code == "en";
			//bool isCustom = GameManager.Instance.JudgeTimeOption.IsCustom;
			//bool isRate = !Calculate.FastApproximately(GameManager.Instance.JudgeTimeOption.MusicRate, 1f, 0.0001f);

			//if (isEnglish && isCustom && isRate)
			//{
			//	_earlyLateLayoutElement.ignoreLayout = true;
			//	_earlyLateObj.GetComponent<RectTransform>().anchoredPosition = new Vector2(630, 23);
			//	var comboPanelRt = _comboPanel.GetComponent<RectTransform>();
			//	comboPanelRt.anchoredPosition = comboPanelRt.anchoredPosition + new Vector2(0, 36);
			//}

			_earlyLateText[EARLY].SetText(earlyLate.Early);
			_earlyLateText[LATE].SetText(earlyLate.Late);
		}
	}

	public void ShowRank(int rank)
	{
		if (rank == 0 || rank == 1)
		{
			_rankUIParticle.SetActive(true);
		}

		_rankImage.transform.localScale = Vector3.one * 3f;
		_rankImage.transform.localRotation = Quaternion.Euler(Vector3.forward * 90f);
		_rankImage.transform.DOScale(Vector3.one, 1f);
		_rankImage.transform.DORotate(Vector3.zero, 1f);

		_rankImage.sprite = _rankSprite.GetSprite(rank);
		_rankImage.enabled = true;
		_rankImage.DOColor(Color.white, 1f);
	}

	public void PlayScoreAnimation(int score, Action onComplete)
	{
		Vector3 move = new Vector3(100f, 0f, 0f);
		float scorePanelPosX = _scorePanel.transform.localPosition.x;
		_scorePanel.transform.localPosition += move;
		float judgePanelPosX = _judgePanel.transform.localPosition.x;
		_judgePanel.transform.localPosition += move;
		float underJudgePanelPosX = _underJudgePanel.transform.localPosition.x;
		_underJudgePanel.transform.localPosition += move;
		float comboPanelPosX = _comboPanel.transform.localPosition.x;
		_comboPanel.transform.localPosition += move;

		DOTween.Sequence()
			.AppendCallback(() => { CountUpScore(score).Forget(); })
			.Append(_scorePanel.DOFade(1f, 1f))
			.Join(_scorePanel.transform.DOLocalMoveX(scorePanelPosX, 0.5f))
			.Append(_judgePanel.DOFade(1f, 1f))
			.Join(_judgePanel.transform.DOLocalMoveX(judgePanelPosX, 0.5f))
			.Join(_underJudgePanel.DOFade(1f, 1f))
			.Join(_underJudgePanel.transform.DOLocalMoveX(underJudgePanelPosX, 0.5f))
			.Append(_comboPanel.DOFade(1f, 1f))
			.Join(_comboPanel.transform.DOLocalMoveX(comboPanelPosX, 0.5f))
			.OnComplete(() => {
				onComplete();
		 	});

		async UniTask CountUpScore(int score)
		{
			//var resultData = GameManager.Instance.ResultData;
			float time = 0;
			float waitTime = 1f;
			while (time < waitTime)
			{
				_scoreText.NonAllocateSetText((int)(score * time / waitTime), 7);
				time += Time.deltaTime;
				await UniTask.Yield();
			}
			_scoreText.NonAllocateSetText(score, 7);
		}
	}

	int _jacketAreaIndex = 0;
	const int MAX_JACKET_AREA_PANEL_COUNT = 1;
	bool _hasScoreHistory = false;

	public void ChangeJacketAreaPanel()
	{
		_jacketAreaIndex++;

		// if (_jacketAreaIndex == 2 && !_hasScoreHistory)
		// {
		// 	_jacketAreaIndex++;
		// }

		if (_jacketAreaIndex > MAX_JACKET_AREA_PANEL_COUNT)
		{
			_jacketAreaIndex = 0;
		}

		_judgeOptionPanel.SetActive(_jacketAreaIndex == 1);
		///_scoreHistoryPanel.SetActive(_jacketAreaIndex == 2);
	}

	private void OnDestroy()
	{
		//_resultOnlinePanel.OnFinishScene();
		DOTween.Clear();
	}

	public override void CallUpdate()
	{
		_tapEffect.CallUpdate();
	}
}
