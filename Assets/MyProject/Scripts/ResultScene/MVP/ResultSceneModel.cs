﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Dialog;

[Serializable]
public class ScoreData
{
	public string Id;
	public int Score;
	public int Combo;
	public int ClearState;
	public int Rank;
}

public sealed class ResultSceneModel
{
	//------------------
	// キャッシュ.
	//------------------
	ResultData _resultData = null;
	ScoreData _beforeRecord = null;
	SongInfo _selectSongInfo;
	PlayMode _playMode;
	int _rank;

	bool _canSaveCustomJudgent = false;

	//------------------
	// 定数.
	//------------------
	readonly int[] RANK_BOUNDARY = new int[]
	{
		Constant.RankBoundary.S_PLUS,
		Constant.RankBoundary.S,
		Constant.RankBoundary.A,
		Constant.RankBoundary.B,
		Constant.RankBoundary.C
	};

	//------------------
	// プロパティ.
	//------------------
	public ResultData ResultData => _resultData;
	public ScoreData BeforeRecord => _beforeRecord;

	public PlayMode PlayMode => _playMode;

	public SongInfo SongInfo => _selectSongInfo;
	public bool IsCustomJudge => GameManager.Instance.JudgeTimeOption.IsCustom;
	public bool IsShowEarlyLate => GameManager.Instance.DisplayOption.IsShowEarlyLateDisplay;
	public int Rank => _rank;

	public bool CanSaveCustomJudgent => _canSaveCustomJudgent;

	/// <summary>
	/// 初期化処理
	/// </summary>
	public void Init()
	{
		_selectSongInfo = GameManager.Instance.SelectSongInfo;
		_playMode = GameManager.Instance.PlayModeOption.PlayMode;
		_resultData = GameManager.Instance.ResultData;

		// Rank
		_rank = JudgeRank(_resultData.Score, _resultData.IsDead);

		_beforeRecord = GetBeforeRecord();
	}

	/// <summary>
	/// 各判定を取得
	/// </summary>
	public int[] GetJudges()
	{
		var judges = new int[]
		{
			_resultData.Judge.Brilliant,
			_resultData.Judge.Great,
			_resultData.Judge.Fast + _resultData.Judge.Slow,
			_resultData.Judge.Bad,
			_resultData.Judge.Missed
		};
		return judges;
	}

	public EarlyLate GetEarlyLate()
	{
		return _resultData.EarlyLate;
	}

	/// <summary>
	/// 前の自己ベストを取得する
	/// </summary>
	/// <returns>自己ベストのScoreData</returns>
	ScoreData GetBeforeRecord()
	{
		ScoreData scoreData = new ScoreData()
		{
			Id = "",
			Score = 0,
			Combo = 0,
			ClearState = 0,
			Rank = int.MaxValue,
		};

		//var folderName = GameManager.Instance.SelectSongInfo.FolderName;
		//int scoreDataIndex = ScoreDataPrefas.instance.FindIndexScore(folderName, GameManager.Instance.SelectDifficulty);

		//if (scoreDataIndex != -1)
		//{
		//	var beforeScoreData = ScoreDataPrefas.instance.ScoreDataList[scoreDataIndex];
		//	scoreData.Id = beforeScoreData.Id;
		//	scoreData.Score = beforeScoreData.Score;
		//	scoreData.Combo = beforeScoreData.Combo;
		//	scoreData.ClearState = beforeScoreData.ClearState;
		//	scoreData.Rank = beforeScoreData.Rank;
		//}

		return scoreData;
	}

	/// <summary>
	/// スコアを保存
	/// </summary>
	public void SaveScore()
	{
		//if (!CanSave(_playMode))
		//{
		//	return;
		//}

		//// ClearState
		//int clearState = 0;

		//if (_resultData.IsAllBrilliant)
		//{
		//	clearState = Constant.SpecialNumber.AB_THRESHOLD;
		//}
		//else if (_resultData.IsFullCombo)
		//{
		//	clearState = Constant.SpecialNumber.FC_THRESHOLD;
		//}
		//else if (!_resultData.IsDead)
		//{
		//	clearState = Constant.SpecialNumber.CLEAR_THRESHOLD;
		//}

		//var folderName = GameManager.Instance.SelectSongInfo.FolderName;
		//var difficulty = GameManager.Instance.SelectDifficulty;
		//string id = ScoreDataPrefas.GetId(folderName, difficulty);

		//ScoreData scoreData = new ScoreData()
		//{
		//	Id = id,
		//	Score = _resultData.Score,
		//	Combo = _resultData.MaxCombo,
		//	ClearState = clearState,
		//	Rank = _rank
		//};

		//int scoreDataIndex = ScoreDataPrefas.instance.ScoreDataList.FindIndex(data => data.Id == id);

		//if (scoreDataIndex == -1)
		//{
		//	ScoreDataPrefas.instance.ScoreDataList.Add(scoreData);
		//}
		//else
		//{
		//	// スコア更新があれば上書きする.
		//	var hiScoreData = ScoreDataPrefas.instance.ScoreDataList[scoreDataIndex];

		//	if (hiScoreData.Score < scoreData.Score)
		//	{
		//		hiScoreData.Score = scoreData.Score;
		//	}

		//	if (hiScoreData.Combo < scoreData.Combo)
		//	{
		//		hiScoreData.Combo = scoreData.Combo;
		//	}

		//	if (hiScoreData.ClearState < scoreData.ClearState)
		//	{
		//		hiScoreData.ClearState = scoreData.ClearState;
		//	}

		//	if (hiScoreData.Rank > scoreData.Rank)
		//	{
		//		hiScoreData.Rank = scoreData.Rank;
		//	}

		//	ScoreDataPrefas.instance.ScoreDataList[scoreDataIndex] = hiScoreData;
		//}

		//ScoreDataPrefas.Save();
	}

	/// <summary>
	/// セーブできるか
	/// </summary>
	/// <param name="playMode"></param>
	/// <returns>bool</returns>
	//bool CanSave(PlayMode playMode)
	//{
	//	_canSaveCustomJudgent =
	//		(
	//		!GameManager.Instance.JudgeTimeOption.IsCustom ||
	//		GameManager.Instance.JudgeTimeOption.CanSave
	//		) &&
	//		GameManager.Instance.JudgeTimeOption.MusicRate > 0.99999f;

	//	return (playMode == PlayMode.Normal || playMode == PlayMode.Online) &&
	//		!GameManager.Instance.ResultData.IsDead &&
	//		_canSaveCustomJudgent;
	//}

	/// <summary>
	/// ランクを求める
	/// </summary>
	/// <param name="score"></param>
	/// <param name="isDead"></param>
	/// <returns></returns>
	int JudgeRank(int score, bool isDead)
	{
		if (isDead)
		{
			return RANK_BOUNDARY.Length;
		}

		for (int i = 0; i < RANK_BOUNDARY.Length; i++)
		{
			if (score >= RANK_BOUNDARY[i])
			{
				return i;
			}
		}

		return RANK_BOUNDARY.Length;
	}

	/// <summary>
	/// 判定の最大桁数を取得
	/// </summary>
	/// <returns></returns>
	public int GetMaxDigitJudge()
	{
		int maxDigit = 4;
		var judge = _resultData.Judge;

		if (maxDigit < Calculate.Digit(judge.Brilliant))
		{
			maxDigit = Calculate.Digit(judge.Brilliant);
		}
		if (maxDigit < Calculate.Digit(judge.Great))
		{
			maxDigit = Calculate.Digit(judge.Great);
		}
		if (maxDigit < Calculate.Digit(judge.Fast + judge.Slow))
		{
			maxDigit = Calculate.Digit(judge.Fast + judge.Slow);
		}
		if (maxDigit < Calculate.Digit(judge.Bad))
		{
			maxDigit = Calculate.Digit(judge.Bad);
		}
		if (maxDigit < Calculate.Digit(judge.Missed))
		{
			maxDigit = Calculate.Digit(judge.Missed);
		}

		return maxDigit;
	}

}
