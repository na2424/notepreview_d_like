﻿using Cysharp.Threading.Tasks;
using System;
using UnityEngine;

public sealed class ResultScenePresenter : PresenterBase<ResultSceneView, ResultSceneModel>
{
	readonly ResultGraph _resultGraph;
	readonly ResultBgmController _resultBgmController;

	/// <summary>
	/// DIを使用しないコンストラクタインジェクションとして振る舞う
	/// </summary>
	public ResultScenePresenter(ResultSceneView view,
		ResultGraph resultGraph,
		ResultBgmController resultBgmController) : base(view)
	{
		_resultGraph = resultGraph;
		_resultBgmController = resultBgmController;
	}

	#region Entry point
	public override void Start()
	{
		InitAsync().Forget();
	}

	public override void Tick()
	{
		_view.CallUpdate();
	}

	public override void Dispose()
	{
	}
	#endregion

	async UniTask InitAsync()
	{
		// Model初期化
		_model.Init();

		// スコア保存
		_model.SaveScore();

		// View初期化
		_view.Init();
		_view.CustomJudgementObj.SetActive(false /*_model.IsCustomJudge && !_view.HasOnlineResult*/);

		//var scoreHistoryRivalType = GameManager.Instance.DisplayOption.ScoreHistoryRivalType;
		//if (scoreHistoryRivalType == ScoreHistoryRivalType.Type1)
		//{
		//	_view.SetPlayerScore(_model.GetPlayerScores());
		//}
		//else if (scoreHistoryRivalType == ScoreHistoryRivalType.Type2)
		//{
		//	_view.SetRivalScore(_model.GetRivalScores());
		//}
		//else
		//{
		//	_view.SetPlayerScore(null);
		//}

		// 各コントローラー初期化
		_resultGraph.Init();

		// ジャケット
		_view.SetJacketTexture(_model.SongInfo);
		// 通常｜オート｜リハーサル｜オンライン状態の表示
		_view.SetPlayMode(_model.PlayMode);
		// クリア状態
		_view.ShowClearState(GetGameStateType());
		// 楽曲情報
		_view.SetMusicInfo(_model.SongInfo, GameManager.Instance.SelectDifficulty);

		// UIイベント設定
		SetEvent();

		// 表示開始
		await FadeManager.Instance.FadeInAsync();

		_resultBgmController.Play();

		_view.ShowMainPanel(onComplete: () =>
		{
			ShowScore(onComplete: async () =>
			{
				await _resultGraph.DrawGraphAsync();
				//_resultAutoTimingPanel.Open();
				await UniTask.Yield();
				OnDrawFinish();
			});
		});

		//_view.ShowAnimOnlinePanel();
	}

	ClearStateType GetGameStateType()
	{
		var resultData = GameManager.Instance.ResultData;

		if (resultData.IsAllBrilliant)
		{
			return ClearStateType.AllBrilliant;
		}
		else if (resultData.IsFullCombo)
		{
			return ClearStateType.FullCombo;
		}
		else if (!resultData.IsDead)
		{
			return ClearStateType.Cleared;
		}

		return ClearStateType.GameOver;
	}

	void SetEvent()
	{
		_view.OnClickBackSelectMusicButton.AddListener(() =>
		{
			_view.SetInteractable_BackSelectMusicButton(false);
			FinishScene().Forget();
		});

		_view.OnClickJudgeOptionPanelButton.AddListener(() =>
		{
			_view.ChangeJacketAreaPanel();
		});
	}

	void OnDrawFinish()
	{
		_view.NextPanelObj.SetActive(true);
		_view.SetInteractable_BackSelectMusicButton(true);
	}

	void ShowScore(Action onComplete)
	{
		// ハイスコア
		_view.ShowHighScore(_model.ResultData, _model.BeforeRecord, _model.CanSaveCustomJudgent);

		// 判定
		_view.ShowJudge(_model.GetJudges(), _model.GetMaxDigitJudge());

		// Early / Late
		_view.ShowEarlyLate(_model.IsShowEarlyLate, _model.GetEarlyLate());

		// ランク
		_view.ShowRank(_model.Rank);

		// 最大コンボ
		_view.SetMaxComboText(_model.ResultData.MaxCombo);

		// スコアアニメーション
		_view.PlayScoreAnimation(_model.ResultData.Score, onComplete);
	}

	async UniTask FinishScene()
	{
		await FadeManager.Instance.FadeOutAsync(SceneBuildIndex.SelectMusic);
	}
}
