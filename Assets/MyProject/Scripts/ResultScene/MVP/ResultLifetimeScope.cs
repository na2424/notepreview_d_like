﻿using UnityEngine;

/// <summary>
/// ResultSceneで最初に実行されるクラス
/// シーンのMVPクラスを構築する
/// </summary>
public sealed class ResultLifetimeScope : LifetimeScope
{
	[SerializeField] ResultSceneView _view;
	[SerializeField] ResultGraph _resultGraph;
	[SerializeField] ResultBgmController _resultBgmController;

	void Awake()
    {
		_presenter = new ResultScenePresenter(_view,
			_resultGraph,
			_resultBgmController
		);
	}
}
