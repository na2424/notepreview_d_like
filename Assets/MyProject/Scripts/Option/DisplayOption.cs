﻿using System;

[Serializable]
public class DisplayOption
{
	public float Dimmer = 0.25f;
	public float VisualOffset = 0f;
	public NotesEffectType NotesEffectType = NotesEffectType.Type1;
	public bool BeatBar = true;
	public bool RelayFrontDisplay = false;
	public bool IsShowEarlyLateDisplay = false;
	public bool VSync = true;
	public bool IsShowFps = false;
	public int TargetFrameRate = 120;
}
