﻿using System;
using UnityEngine;

[Serializable]
public class NotesOption
{
	public float HiSpeed = 5.0f;
	public float Size = 1f;
	public float Timing = 0f;
	public bool AutoTiming = false;
	public float SameTimeWidthRate = 1f;
	public float FrameRevision = 0f;
	public int PollingRate = 300;
	public JudgeAreaType JudgeAreaType = JudgeAreaType.Screen;
	public NotesScrollType NotesScrollType = NotesScrollType.Decelerate;
	public string NoteSkinsName = Constant.Note.DEFAULT_NOTESKINS_NAME;
	public string TouchSeName = Constant.Note.DEFAULT_NOTESKINS_NAME;
	public int LaneAlpha = 50;
	public int LaneCameraOffsetZ = 0;
	public bool IsCustomSeparator = false;
	public Color SeparatorColor = Constant.Note.DefaultSeparateColor;
	public bool IsCustomBeam = false;
	public Color BeamColor = Constant.Note.DefaultBeamColor;
}
