﻿using UnityEngine;

public enum MusicLoadType : int
{
	Decompress = 0,
	Compress = 1,
	Streaming = 2,
}

[System.Serializable]
public class JudgeTimeOption
{
	public float BrilliantTime = Constant.JudgeTime.BRILLIANT_TIME;
	public float GreatTime = Constant.JudgeTime.GREAT_TIME;
	public float FastTime = Constant.JudgeTime.FAST_TIME;
	public float BadTime = Constant.JudgeTime.BAD_TIME;
	public float JudgeDistance = Constant.JudgeTime.JUDGE_DISTANCE;
	public float LongRevisionTime = Constant.JudgeTime.LONG_REVISION_TIME;
	public float LongRevisionDistance = Constant.JudgeTime.LONG_REVISION_DISTANCE;
	public float FuzzyStartTime = Constant.JudgeTime.FUZZY_START_TIME;
	public bool CanSave = false;
	public float MusicRate = 1.0f;
	public bool FuzzyJudgeMitigation = true;
	public bool Mirror = false;
	public bool Vibration = false;
	public string GlobalLuaName = Constant.PlayerOption.Other.DEFAULT_GLOBAL_LUA_NAME;
	public float NullTapNoSoundTime = 0.2f;
	public MusicLoadType MusicLoadType = MusicLoadType.Decompress;
	public int DspBufferSize = 256;
	public bool EnableResultScene = false;

	public bool IsCustom =>
		BrilliantTime != Constant.JudgeTime.BRILLIANT_TIME ||
		GreatTime != Constant.JudgeTime.GREAT_TIME ||
		FastTime != Constant.JudgeTime.FAST_TIME ||
		BadTime != Constant.JudgeTime.BAD_TIME ||
		JudgeDistance != Constant.JudgeTime.JUDGE_DISTANCE ||
		LongRevisionTime != Constant.JudgeTime.LONG_REVISION_TIME ||
		LongRevisionDistance != Constant.JudgeTime.LONG_REVISION_DISTANCE ||
		FuzzyStartTime != Constant.JudgeTime.FUZZY_START_TIME;

	/// <summary>
	/// 判定時間で一番長い時間を取得する
	/// </summary>
	/// <returns>判定が行なわれる時間</returns>
	public float GetHitTime()
	{
		float hitTime = BadTime;

		if (hitTime < FastTime)
		{
			hitTime = FastTime;
		}

		if (hitTime < GreatTime)
		{
			hitTime = GreatTime;
		}

		if (hitTime < BrilliantTime)
		{
			hitTime = BrilliantTime;
		}

		return hitTime;
	}

	/// <summary>
	/// ロング終端時間補正を加えた判定時間で一番長い時間を取得する
	/// </summary>
	/// <returns>判定が行なわれる時間</returns>
	public float GetLongEndHitTime()
	{
		float hitTime = BadTime;

		if (hitTime < FastTime)
		{
			hitTime = FastTime;
		}

		if (hitTime < GreatTime + LongRevisionTime)
		{
			hitTime = GreatTime + LongRevisionTime;
		}

		if (hitTime < BrilliantTime + LongRevisionTime)
		{
			hitTime = BrilliantTime + LongRevisionTime;
		}

		return hitTime;
	}

	/// <summary>
	/// コンボが繋がる判定時間で一番長い時間を取得する
	/// </summary>
	/// <returns>判定が行なわれる時間</returns>
	public float GetComboHitTime()
	{
		float hitTime = GreatTime;

		if (hitTime < GreatTime)
		{
			hitTime = GreatTime;
		}

		if (hitTime < BrilliantTime)
		{
			hitTime = BrilliantTime;
		}

		return hitTime;
	}
}
