﻿/// <summary>
/// プレイモードのオプション情報
/// </summary>
[System.Serializable]
public class PlayModeOption
{
	public PlayMode PlayMode = PlayMode.Auto;
}
