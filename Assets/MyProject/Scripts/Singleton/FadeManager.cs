﻿using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// シーン移動時のフェードイン・アウトを担当するクラス
/// </summary>
[Prefab("SingletonPrefabs/FadeManager", true)]
public sealed class FadeManager : SingletonMonoBehaviour<FadeManager>
{
    //--------------------------------------
    // インスペクタまたは外部から設定.
    //--------------------------------------
    [SerializeField] Canvas _fadeCanvas;
    [SerializeField] Image _fadeImage;
    [SerializeField] Texture _maskTexture = null;
	[SerializeField] TextMeshProUGUI _dotsText;

	//------------------
	// キャッシュ.
	//------------------
	// フェード用Imageの透明度
	float _alpha = 0.0f;

    // フェードインアウトのフラグ
    bool _isFadeIn = false;
    bool _isFadeOut = false;

    // フェードしたい時間（単位は秒）
    float _fadeTime = 0.2f;

    // 遷移先のシーン番号
    int _nextScene = 1;

	// Dot表示用
	byte _dotCount = 0;
	float _elapseTime = 0.0f;
	char[] _dots = new char[3];
	bool _isShowDot = false;

    //------------------
	// 定数.
	//------------------
    const int WAIT_MS = 240;

	// フェード用のCanvasとImage生成
	protected override void Awake()
	{
		base.Awake();
        _fadeImage.material.SetTexture(Constant.ShaderProperty.MaskTexId, _maskTexture);
    }

    public void SetBlack()
	{
        _fadeImage.color = Color.black;
    }

    /// <summary>
    /// フェードイン開始
    /// </summary>
    /// <returns></returns>
    public async UniTask FadeInAsync()
    {
        //UpdateMaskTexture();
        _fadeImage.color = Color.black;
        _isFadeIn = true;
        await UpdateAsync();
    }

    /// <summary>
    /// フェードアウト開始
    /// </summary>
    /// <param name="n">次のシーンのインデックス</param>
    /// <returns></returns>
    public async UniTask FadeOutAsync(int n)
    {
        //UpdateMaskTexture();
        _nextScene = n;
        _fadeImage.color = Color.clear;
        _fadeCanvas.enabled = true;
        _isFadeOut = true;

		EnableDot();
		UpdateDotsAsync().Forget();
		await UpdateAsync();
		DisnableDot();
	}

    void UpdateMaskTexture()
    {
        _fadeImage.material.SetTexture(Constant.ShaderProperty.MaskTexId, _maskTexture);
    }

    void UpdateMaskCutout(float range)
    {
        enabled = true;
        _fadeImage.material.SetFloat(Constant.ShaderProperty.RangeId, 1 - range);
    }

    async UniTask UpdateAsync()
    {
        while(true)
		{
            //フラグ有効なら毎フレームフェードイン/アウト処理
            if (_isFadeIn)
            {
                //経過時間から透明度計算
                _alpha -= Time.deltaTime / _fadeTime;
                UpdateMaskCutout(_alpha);
                //フェード用Imageの色・透明度設定
                _fadeImage.color = new Color(0.0f, 0.0f, 0.0f, _alpha);

                //フェードイン終了判定
                if (_alpha <= 0.0f)
                {
                    _isFadeIn = false;
                    _alpha = 0.0f;

                    await UniTask.Delay(WAIT_MS);

                    _fadeCanvas.enabled = false;
                    return;
                }
            }
            else if (_isFadeOut)
            {
                //経過時間から透明度計算
                _alpha += Time.deltaTime / _fadeTime;
                UpdateMaskCutout(_alpha);
                //フェード用Imageの色・透明度設定
                _fadeImage.color = new Color(0.0f, 0.0f, 0.0f, _alpha);

                //フェードアウト終了判定
                if (_alpha >= 1.0f)
                {
                    await UniTask.Delay(WAIT_MS);
                    _isFadeOut = false;
                    _alpha = 1.0f;

					//次のシーンへ遷移
					await SceneManager.LoadSceneAsync(_nextScene);
					await Resources.UnloadUnusedAssets();
					System.GC.Collect();
					return;
                }
            }

            await UniTask.Yield();
        }
        
    }

	void EnableDot()
	{
		for (int i = 0; i < _dots.Length; i++)
		{
			_dots[i] = ' ';
		}
		_dotsText.SetCharArray(_dots);
		_dotCount = 0;
		_elapseTime = 0.03f;

		_isShowDot = true;
	}

	void DisnableDot()
	{
		_isShowDot = false;
	}

	async UniTask UpdateDotsAsync()
	{
		while (true)
		{
			if (!_isShowDot)
			{
				return;
			}

			_elapseTime += Time.unscaledDeltaTime;

			if (_elapseTime > 0.22f)
			{
				_elapseTime = 0f;

				for (int i = 0; i < _dots.Length; i++)
				{
					_dots[i] = i < _dotCount ? '.' : ' ';
				}

				_dotsText.SetCharArray(_dots);

				_dotCount++;
				if (_dotCount > _dots.Length)
				{
					_dotCount = 0;
				}
			}

			await UniTask.Yield();
		}
	}
}