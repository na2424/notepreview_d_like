﻿using UnityEngine;

public enum SystemSEType : int
{
	Clear = 0,
	Failed,
	ChangeSelectMusic,
	ChangeDifficulty,
	TitleStart,
	Pon,
	ResumeCount,
	CutIn,
	DanmakuStageEnd,
}

/// <summary>
/// システムSEの音声とSystemSETypeを紐づけて再生するクラス
/// </summary>
[Prefab("SingletonPrefabs/SystemSEManager", true)]
public sealed class SystemSEManager : SingletonMonoBehaviour<SystemSEManager>
{
	[SerializeField] AudioSource _audioSource;
	// MEMO: SETypeの順番に合わせておく.
	[SerializeField] AudioClip[] _clips = new AudioClip[1];

	protected override void Awake()
	{
		base.Awake();

		SetMute(GameManager.Instance.VolumeOption.SystemSEMute);
		SetVolume(GameManager.Instance.VolumeOption.SystemSEVolume);
	}

	/// <summary>
	/// SEを再生する
	/// </summary>
	/// <param name="seType">SystemSEの種類</param>
	public void Play(SystemSEType seType)
	{
		if(_audioSource != null)
		{
			_audioSource.PlayOneShot(_clips[(int)seType]);
		}
	}

	/// <summary>
	/// ミュートの設定
	/// </summary>
	/// <param name="isMute">ミュートにするか</param>
	public void SetMute(bool isMute)
	{
		if(_audioSource != null)
		{
			_audioSource.mute = isMute;
		}
	}

	/// <summary>
	/// 音量の設定
	/// </summary>
	/// <param name="value">音量(0～1)</param>
	public void SetVolume(float value)
	{
		if(_audioSource!= null)
		{
			_audioSource.volume = value;
		}
	}
}
