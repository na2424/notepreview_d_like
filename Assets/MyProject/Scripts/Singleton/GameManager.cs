﻿using SRDebugger.UI.Other;
using UnityEngine;
using UnityEngine.SceneManagement;

[Prefab("SingletonPrefabs/GameManager", true)]
public sealed class GameManager : SingletonMonoBehaviour<GameManager>
{
	public SongInfo SelectSongInfo;
	public ResultData ResultData;
	public TouchSeInfo TouchSeInfo;
	public NotesOption NotesOption = new NotesOption();
	public DisplayOption DisplayOption = new DisplayOption();
	public VolumeOption VolumeOption = new VolumeOption();
	public JudgeTimeOption JudgeTimeOption = new JudgeTimeOption();
	public PlayModeOption PlayModeOption = new PlayModeOption();

	public int SelectDataIndex = 0;
	public DifficultyType SelectDifficulty = DifficultyType.Easy;
	public bool IsCMod = false;
	public float LastSecondHint = 0f;
	public AnimationCurve NoteScrollAnimationCurve;
	public AnimationCurve InverseAnimationCurve;
	public bool EnableResultScene { get; set; } = false;

	protected override void Awake()
	{
		base.Awake();

		SetOptionFromSaveData();
		SetAudioDspBufferSize();

		QualitySettings.vSyncCount = DisplayOption.VSync ? 1 : 0;
		Application.targetFrameRate = DisplayOption.TargetFrameRate;
		Physics.autoSimulation = false;
		InverseAnimationCurve = NoteScrollAnimationCurve.CreateInverseCurve();
	}

	/// <summary>
	/// オプションのセーブデータを読み込む
	/// </summary>
	void SetOptionFromSaveData()
	{
		if (GameSettingsPrefas.instance.NotesOption != null)
		{
			NotesOption = GameSettingsPrefas.instance.NotesOption;
			NotesOption.NotesScrollType = NotesScrollType.Decelerate;
		}
		if (GameSettingsPrefas.instance.DisplayOption != null)
		{
			DisplayOption = GameSettingsPrefas.instance.DisplayOption;
		}
		if (GameSettingsPrefas.instance.VolumeOption != null)
		{
			VolumeOption = GameSettingsPrefas.instance.VolumeOption;
		}
		if (GameSettingsPrefas.instance.JudgeTimeOption != null)
		{
			JudgeTimeOption = GameSettingsPrefas.instance.JudgeTimeOption;
		}
	}

	/// <summary>
	/// 音の遅延対策
	/// </summary>
	void SetAudioDspBufferSize()
	{
		AudioConfiguration ac = AudioSettings.GetConfiguration();
		ac.dspBufferSize = Application.platform == RuntimePlatform.WindowsEditor ? 1024 : JudgeTimeOption.DspBufferSize;
		AudioSettings.Reset(ac);
	}

	/// <summary>
	/// シーンを切り替える
	/// </summary>
	/// <param name="sceneName"></param>
	public void ChangeScene(string sceneName)
	{
		SceneManager.LoadScene(sceneName);
	}

	/// <summary>
	/// アプリを終了する
	/// </summary>
	public void Quit()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.runInBackground = false;
		Application.Quit();
#endif
	}
}
