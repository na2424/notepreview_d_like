﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UBL;
using System.IO;
using System.Linq;
using System;

/// <summary>
/// 背景を設定するクラス
/// </summary>
[Prefab("SingletonPrefabs/BgManager", true)]
public sealed class BgManager : SingletonMonoBehaviour<BgManager>
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] RawImage _bgImage;
	[SerializeField] CanvasScaler _canvasScaler;

	//------------------
	// キャッシュ.
	//------------------
	Dictionary<string, Texture> _bgChangeTextureCache = new Dictionary<string, Texture>(StringComparer.Ordinal);
	[SerializeField] Material _defaultMaterial;
	float _screenRatio = 1f;
	string _defaultImagePath = "";
	int _defaultMaterialHash;
	int _defaultImageHash = -1;
	int _currentImageHash = -1;
	int _defaultShaderHash = -1;
	bool _isAnimate = false;

	//------------------
	// 定数.
	//------------------
	const int DEFAULT_DIMMER = 33;

	//------------------
	// プロパティ.
	//------------------
	public Texture CurrentBgTexture => _bgImage.texture;

	protected override void Awake()
	{
		base.Awake();

		// Canvasの解像度
		_canvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);

		// スクリーンのサイズ比
		_screenRatio = (float)Screen.width / (float)Screen.height;

		// デフォルトマテリアルをキャッシュ
		CacheDefault();
	}

	void CacheDefault()
	{
		_defaultMaterial = _bgImage.material;
		_defaultMaterialHash = _defaultMaterial.GetHashCode();
		_defaultShaderHash = _defaultMaterial.shader.GetHashCode();
	}


	/// <summary>
	/// 解像度を初期化
	/// </summary>
	public void ResetResolution()
	{
		// Canvasの解像度
		_canvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);

		// スクリーンのサイズ比
		_screenRatio = (float)Screen.width / (float)Screen.height;

		if (_bgImage.texture != null)
		{
			SetSize((Texture2D)_bgImage.texture);
		}
	}

	/// <summary>
	/// ファイルパスから背景をロードする
	/// </summary>
	/// <param name="path">ファイルパス</param>
	public void LoadFromPath(string path = null, bool isDefaultImage = false)
	{
		if (path == null)
		{
			path = string.Empty;
		}

		Texture2D texture = null;

		if (path.HasValue())
		{
			texture = TextureLoader.Load(path);
		}
		else
		{
			texture = new Texture2D(128, 128, TextureFormat.RGB24, false);
		}

		_bgImage.texture = texture;

		ResetResolution();

		int hash = texture.GetHashCode();

		_defaultImageHash = hash;

		if (isDefaultImage)
		{
			_defaultImageHash = hash;
			_defaultImagePath = path;
		}
	}

	/// <summary>
	/// 画面サイズとテクスチャのサイズを比較して、
	/// 背景の大きさを設定する(外側に合わせる)
	/// </summary>
	/// <param name="texture"></param>
	void SetSize(Texture2D texture)
	{
		float width = texture.width;
		float height = texture.height;
		float ratio = width / height;

		_bgImage.rectTransform.sizeDelta = ratio >= _screenRatio ?
			new Vector2(_bgImage.texture.width * Screen.height / (float)_bgImage.texture.height, Screen.height) :
			new Vector2(Screen.width, _bgImage.texture.height * Screen.width / (float)_bgImage.texture.width);
	}

	/// <summary>
	/// 背景ディマーを設定する.
	/// オプションを使用しない時はデフォルト値が使用される.
	/// </summary>
	/// <param name="shouldUse">オプションの背景ディマーを使用するか</param>
	public void UseDimmer(bool shouldUse)
	{
		Color color;

		if (shouldUse)
		{
			float dark = 1f - GameManager.Instance.DisplayOption.Dimmer / 100f;
			color = new Color(dark, dark, dark, 1f);
		}
		else
		{
			float dark = 1f - DEFAULT_DIMMER / 100f;
			color = new Color(dark, dark, dark, 1f);
		}

		_isAnimate = true;
		_bgImage.DOColor(color, 1f).OnComplete(() => { _isAnimate = false; });
	}

	/// <summary>
	/// 背景ディマーを直接変更する(0～1)
	/// </summary>
	/// <param name="dimmer"></param>
	public void SetDimmer(float dimmer)
	{
		if (_isAnimate)
		{
			_bgImage.DOKill();
			_isAnimate = false;
		}

		float dark = 1f - Mathf.Clamp01(dimmer);
		_bgImage.color = new Color(dark, dark, dark, 1f);
	}


	/// <summary>
	/// ファイルパスからBgChange画像を事前ロードする
	/// </summary>
	/// <param name="path">ファイルパス</param>
	public void LoadBgChangeImages(string directoryPath, List<string> bgChangeImageName)
	{
		if (bgChangeImageName.IsNullOrEmpty())
		{
			return;
		}

		foreach (var name in bgChangeImageName)
		{
			string path = Path.Combine(directoryPath, name);

			if (_bgChangeTextureCache.ContainsKey(name))
			{
				continue;
			}

			Texture2D texture = path.HasValue() ?
				TextureLoader.Load(path) :
				new Texture2D(128, 128, TextureFormat.RGB24, false);

			_bgChangeTextureCache[name] = texture;
		}
	}

	/// <summary>
	/// 指定のBgChange画像に変える
	/// </summary>
	/// <param name="name"></param>
	public void ChangeBgImage(string name = null)
	{
		if (_bgChangeTextureCache.ContainsKey(name))
		{
			Texture2D texture = (Texture2D)_bgChangeTextureCache[name];
			_bgImage.texture = texture;
			SetSize(texture);
			_currentImageHash = _bgImage.texture.GetHashCode();
		}
	}

	/// <summary>
	/// 指定のBgChange画像に変える
	/// </summary>
	/// <param name="name"></param>
	public void SetBgImage(Texture2D texture)
	{
		_bgImage.texture = texture;
		SetSize(texture);
	}

	public bool IsDefaultImage()
	{
		return _defaultImageHash == _currentImageHash;
	}

	public void SetDefaultImage()
	{
		LoadFromPath(_defaultImagePath);
	}

	/// <summary>
	/// BgChange画像のキャッシュを破棄する
	/// </summary>
	public void ClearBgChangeImageCache()
	{
		_bgChangeTextureCache.Clear();
	}

	/// <summary>
	/// Materialを設定します
	/// </summary>
	/// <param name="material"></param>
	public void SetMaterial(Material material)
	{
		_bgImage.material = material;
	}

	/// <summary>
	/// マテリアルを元に戻す
	/// </summary>
	public void ResetMaterial()
	{
		if (_bgImage.material == null)
		{
			_bgImage.material = _defaultMaterial;
			return;
		}

		if (_bgImage.material.GetHashCode() != _defaultMaterialHash)
		{
			_bgImage.material = _defaultMaterial;
		}
	}

	/// <summary>
	/// BgImageに付けたMaterialを破棄する.
	/// テクスチャが付いていればそれも破棄する
	/// </summary>
	public void ClearMaterial()
	{
		if (_bgImage.material != null && 
			_bgImage.material.GetHashCode() != _defaultMaterialHash &&
			_bgImage.material.shader.GetHashCode() != _defaultShaderHash)
		{
			if (_bgImage.material.mainTexture != null)
			{
				if (_bgImage.material.mainTexture.GetHashCode() != _defaultImageHash)
				{
					Destroy(_bgImage.material.mainTexture);
				}
			}

			Destroy(_bgImage.material);
			_bgImage.material = null;

			_bgImage.material = _defaultMaterial;
		}
	}
}
