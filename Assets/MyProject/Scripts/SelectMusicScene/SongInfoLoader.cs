﻿using Cysharp.Text;
using Cysharp.Threading.Tasks;
using Dialog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using UnityEngine;

public sealed class SongInfoLoader
{
	[System.Flags]
	public enum TagFlag
	{
		VERSION = 1 << 0,
		TITLE = 1 << 1,
		SUBTITLE = 1 << 2,
		ARTIST = 1 << 3,
		MUSIC = 1 << 4,
		BANNER = 1 << 5,
		BACKGROUND = 1 << 6,
		CREDIT = 1 << 7,
		ILLUST = 1 << 8,
		DESCRIPTION = 1 << 9,
		OFFSET = 1 << 10,
		BPMS = 1 << 11,
		BASEBPM = 1 << 12,
		SPEEDS = 1 << 13,
		SCROLLS = 1 << 14,
		BGCHANGES = 1 << 15,
		CMOD = 1 << 16,
		LUA = 1 << 17,
		DIFFICULTIES = 1 << 18,
		SAMPLESTART = 1 << 19,
		SAMPLELENGTH = 1 << 20,
		LASTSECONDHINT = 1 << 21,

		// 全フラグ
		ALL = (1 << 22) - 1,
	}

	//------------------
	// 定数.
	//------------------
	const string ERROR_SEARCH_TITLE = "譜面検索エラー";
	const string ERROR_SEARCH_TEXT = "ダンカグライクの譜面を探している途中で問題が発生しました。";
	const string ERROR_NOSONGS_TITLE = "譜面がありません";
	const string ERROR_NOSONGS_TEXT = "ダンカグライクの譜面データ(.dl)があるか確認してください。";
	const string ERROR_TAG_VERSION_TEXT = "#VERSIONタグの記述に不備があります。";
	const string ERROR_TAG_BPMS_TEXT = "#BPMSタグの記述に不備があります。";
	const string ERROR_TAG_SPEEDS_TEXT = "#SPEEDSタグの記述に不備があります。";
	const string ERROR_TAG_SCROLLS_TEXT = "#SCROLLSタグの記述に不備があります。";
	const string ERROR_TAG_BGCHANGES_TEXT = "#BGCHANGESタグの記述に不備があります。";
	const string CANCEL_MESSAGE = "読み込みをキャンセルしました";

	/// <summary>
	/// 楽曲情報の読み込み
	/// (譜面の中身はここでは読まない)
	/// </summary>
	/// <returns></returns>
	public async UniTask<SongInfo> ReadAsync(CancellationToken ct, string directory)
	{
		string text = ReadFileText(directory);

		if (string.IsNullOrEmpty(text))
		{
			var builder = new DialogParametor.Builder(ERROR_NOSONGS_TITLE, ERROR_NOSONGS_TEXT);
			builder.AddDefaultAction("はい", () => { });
			builder.AddCallbackOnAutoClosed(() => { });
			DialogManager.Instance.Open(builder.Build());
			return null;
		}

		if (ct.IsCancellationRequested)
		{
			return null;
		}

		SongInfo songInfo = null;

		try
		{
			await UniTask.SwitchToThreadPool();

			songInfo = LoadSongInfoAsync(text);
			songInfo.DirectoryPath = directory;

			await UniTask.Yield();

			songInfo.JacketTexture = CreateJacketTexture(songInfo.DirectoryPath, songInfo.JacketFileName);
		}
		catch (Exception ex)
		{
			var builder = new DialogParametor.Builder("エラー", ex.Message);
			builder.AddDefaultAction("閉じる", () => { });
			builder.AddCallbackOnAutoClosed(() => { });
			DialogManager.Instance.Open(builder.Build());
		}

		return songInfo;
	}

	/// <summary>
	/// ファイルからテキストを取得.
	/// </summary>
	string ReadFileText(string directory)
	{
		string result;

		var extensions = new string[] { "*.dl", "*.txt" };

		string[] dlfiles = null;

		try
		{
			// .dlまたは.txtのファイルを検索
			foreach (string ext in extensions)
			{
				dlfiles = Directory.GetFiles(directory, ext, SearchOption.TopDirectoryOnly);

				if (dlfiles.Length > 0)
				{
					break;
				}
			}

			if (dlfiles.Length == 0)
			{
				return null;
			}

			var path = Path.Combine(directory, dlfiles[0]);
			result = File.ReadAllText(path, Encoding.UTF8);
		}
		catch
		{
			return null;
		}

		return result;
	}

	/// <summary>
	/// テキストから楽曲情報を読み取りつつ、SongInfoクラスに情報を入れて返す
	/// </summary>
	/// <param name="text">テキスト</param>
	/// <returns>SonInfo</returns>
	SongInfo LoadSongInfoAsync(string text)
	{
		SongInfo songInfo = new SongInfo();

		StringReader rs = new StringReader(text);
		TagFlag tagFlag = 0;

		bool hasBaseBpm = false;
		CultureInfo invC = CultureInfo.InvariantCulture;

		try
		{
			//ストリームの末端まで繰り返す
			while (rs.Peek() > -1)
			{
				//一行読み込む
				var lineText = rs.ReadLine().Trim();

				if (!HasFlag(tagFlag, TagFlag.VERSION) && lineText.CustomStartsWith("#VERSION:"))
				{
					tagFlag |= TagFlag.VERSION;
					var versionText = lineText.GetBetweenChars(':', ';');

					if (int.TryParse(versionText, NumberStyles.Integer, CultureInfo.InvariantCulture.NumberFormat, out var v))
					{
						songInfo.Version = (byte)v;
					}
					else
					{
						throw CreateErrorWithValue(ERROR_TAG_VERSION_TEXT, versionText);
					}
					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.TITLE) && lineText.CustomStartsWith("#TITLE:"))
				{
					tagFlag |= TagFlag.TITLE;
					songInfo.Title = lineText.GetBetweenChars(':', ';');
					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.SUBTITLE) && lineText.CustomStartsWith("#SUBTITLE:"))
				{
					tagFlag |= TagFlag.SUBTITLE;
					songInfo.SubTitle = lineText.GetBetweenChars(':', ';');
					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.ARTIST) && lineText.CustomStartsWith("#ARTIST:"))
				{
					tagFlag |= TagFlag.ARTIST;
					songInfo.Artist = lineText.GetBetweenChars(':', ';');
					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.MUSIC) && lineText.CustomStartsWith("#MUSIC:"))
				{
					tagFlag |= TagFlag.MUSIC;
					songInfo.MusicFileName = lineText.GetBetweenChars(':', ';');
					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.BANNER) && (lineText.CustomStartsWith("#JACKET:") || lineText.CustomStartsWith("#BANNER:")))
				{
					tagFlag |= TagFlag.BANNER;
					songInfo.JacketFileName = lineText.GetBetweenChars(':', ';');
					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.BACKGROUND) && lineText.CustomStartsWith("#BACKGROUND:"))
				{
					tagFlag |= TagFlag.BACKGROUND;
					songInfo.BgFileName = lineText.GetBetweenChars(':', ';');
					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.CREDIT) && lineText.CustomStartsWith("#CREDIT:"))
				{
					tagFlag |= TagFlag.CREDIT;
					songInfo.ChartArtist = lineText.GetBetweenChars(':', ';');
					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.ILLUST) && lineText.CustomStartsWith("#ILLUST:"))
				{
					tagFlag |= TagFlag.ILLUST;
					songInfo.Illust = lineText.GetBetweenChars(':', ';');
					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.DESCRIPTION) && lineText.CustomStartsWith("#DESCRIPTION:"))
				{
					tagFlag |= TagFlag.DESCRIPTION;

					if (lineText.Contains(";"))
					{
						songInfo.Description.Add(lineText.GetBetweenChars(':', ';'));
					}
					else
					{
						lineText = rs.ReadLine().Trim();

						while (!lineText.Contains(";"))
						{
							songInfo.Description.Add(lineText);
							lineText = rs.ReadLine().Trim();
						}
					}

					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.OFFSET) && lineText.StartsWith("#OFFSET:"))
				{
					tagFlag |= TagFlag.OFFSET;

					if (float.TryParse(lineText.GetBetweenChars(':', ';'), NumberStyles.Float, CultureInfo.InvariantCulture, out var value))
					{
						songInfo.Offset = value;
					}
					else
					{
						songInfo.Offset = 0f;
					}
					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.BPMS) && lineText.CustomStartsWith("#BPMS:"))
				{
					tagFlag |= TagFlag.BPMS;

					while (!lineText.Contains(";"))
					{
						lineText += rs.ReadLine().Trim();
					}

					var bpmData = lineText.GetBetweenChars(':', ';').Split(new char[] { '=', ',' }, StringSplitOptions.RemoveEmptyEntries);

					if (!hasBaseBpm)
					{
						if (float.TryParse(bpmData[1], NumberStyles.Float, CultureInfo.InvariantCulture, out var baseBpm))
						{
							songInfo.BaseBpm = baseBpm;
						}
						else
						{
							throw CreateErrorWithValue(ERROR_TAG_BPMS_TEXT, bpmData[1]);
						}
					}

					if (bpmData.Length % 2 != 0)
					{
						throw new Exception(ERROR_TAG_BPMS_TEXT);
					}

					for (int i = 0; i < bpmData.Length; i++)
					{
						if (i % 2 == 0)
						{
							if (float.TryParse(bpmData[i], NumberStyles.Float, CultureInfo.InvariantCulture, out var bpmPosition))
							{
								songInfo.BpmPositions.Add(bpmPosition);
							}
							else
							{
								throw CreateErrorWithValue(ERROR_TAG_BPMS_TEXT, bpmData[i]);
							}
						}
						else
						{
							if (float.TryParse(bpmData[i], NumberStyles.Float, CultureInfo.InvariantCulture, out var bpm))
							{
								songInfo.Bpms.Add(bpm);
							}
							else
							{
								throw CreateErrorWithValue(ERROR_TAG_BPMS_TEXT, bpmData[i]);
							}
						}
					}

					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.BASEBPM) && lineText.CustomStartsWith("#BASEBPM:"))
				{
					tagFlag |= TagFlag.BASEBPM;

					if (float.TryParse(lineText.GetBetweenChars(':', ';'), NumberStyles.Float, CultureInfo.InvariantCulture, out var value))
					{
						songInfo.BaseBpm = value;
						hasBaseBpm = true;
					}
					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.SPEEDS) && lineText.CustomStartsWith("#SPEEDS:"))
				{
					tagFlag |= TagFlag.SPEEDS;

					while (!lineText.Contains(";"))
					{
						lineText += rs.ReadLine().Trim();
					}

					var speedData = lineText.GetBetweenChars(':', ';').Split(new char[] { '=', ',' }, StringSplitOptions.RemoveEmptyEntries);

					if (speedData.Length % 3 != 0)
					{
						throw new Exception(ERROR_TAG_SPEEDS_TEXT);
					}

					for (int i = 0; i < speedData.Length; i++)
					{
						if (i % 3 == 0)
						{
							if (float.TryParse(speedData[i], NumberStyles.Float, CultureInfo.InvariantCulture, out var speedPosition))
							{
								songInfo.SpeedPositions.Add(speedPosition);
							}
							else
							{
								throw CreateErrorWithValue(ERROR_TAG_SPEEDS_TEXT, speedData[i]);
							}
						}
						else if (i % 3 == 1)
						{
							if (float.TryParse(speedData[i], NumberStyles.Float, CultureInfo.InvariantCulture, out var speedStretchRatio))
							{
								songInfo.SpeedStretchRatios.Add(speedStretchRatio);
							}
							else
							{
								throw CreateErrorWithValue(ERROR_TAG_SPEEDS_TEXT, speedData[i]);
							}
						}
						else if (i % 3 == 2)
						{
							if (float.TryParse(speedData[i], NumberStyles.Float, CultureInfo.InvariantCulture, out var speedDelayBeat))
							{
								songInfo.SpeedDelayBeats.Add(speedDelayBeat);
							}
							else
							{
								throw CreateErrorWithValue(ERROR_TAG_SPEEDS_TEXT, speedData[i]);
							}
						}
					}

					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.SCROLLS) && lineText.CustomStartsWith("#SCROLLS:"))
				{
					tagFlag |= TagFlag.SCROLLS;

					while (!lineText.Contains(";"))
					{
						lineText += rs.ReadLine().Trim();
					}

					var scrollData = lineText.GetBetweenChars(':', ';').Split(new char[] { '=', ',' }, StringSplitOptions.RemoveEmptyEntries);

					if (scrollData.Length % 2 != 0)
					{
						throw new Exception(ERROR_TAG_SCROLLS_TEXT);
					}

					for (int i = 0; i < scrollData.Length; i++)
					{
						if (i % 2 == 0)
						{
							if (float.TryParse(scrollData[i], NumberStyles.Float, CultureInfo.InvariantCulture, out var scrollPosition))
							{
								songInfo.ScrollPositions.Add(scrollPosition);
							}
							else
							{
								throw CreateErrorWithValue(ERROR_TAG_SCROLLS_TEXT, scrollData[i]);
							}
						}
						else
						{
							if (float.TryParse(scrollData[i], NumberStyles.Float, CultureInfo.InvariantCulture, out var scroll))
							{
								songInfo.Scrolls.Add(scroll);
							}
							else
							{
								throw CreateErrorWithValue(ERROR_TAG_SCROLLS_TEXT, scrollData[i]);
							}
						}
					}

					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.BGCHANGES) && lineText.CustomStartsWith("#BGCHANGES:"))
				{
					tagFlag |= TagFlag.BGCHANGES;

					while (!lineText.Contains(";"))
					{
						lineText += rs.ReadLine().Trim();
					}

					var bgChangeData = lineText.GetBetweenChars(':', ';').Split(new char[] { '=', ',' }, StringSplitOptions.RemoveEmptyEntries);

					if (bgChangeData.Length % 2 != 0)
					{
						throw CreateErrorWithValue(ERROR_TAG_BGCHANGES_TEXT, ",=");
					}

					for (int i = 0; i < bgChangeData.Length; i++)
					{
						if (i % 2 == 0)
						{
							if (float.TryParse(bgChangeData[i], NumberStyles.Float, CultureInfo.InvariantCulture, out var bgChangePosition))
							{
								songInfo.BgChangePositions.Add(bgChangePosition);
							}
							else
							{
								throw CreateErrorWithValue(ERROR_TAG_BGCHANGES_TEXT, bgChangeData[i]);
							}
						}
						else if (i % 2 == 1)
						{
							songInfo.BgChangeImageName.Add(bgChangeData[i].Trim());
						}
					}

					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.CMOD) && lineText.CustomStartsWith("#CMOD:"))
				{
					tagFlag |= TagFlag.CMOD;
					if (int.TryParse(lineText.GetBetweenChars(':', ';'), NumberStyles.Integer, CultureInfo.InvariantCulture, out var value))
					{
						songInfo.IsCMod = (value == 1);
					}
					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.LUA) && lineText.CustomStartsWith("#LUA:"))
				{
					tagFlag |= TagFlag.LUA;
					songInfo.LuaScript = lineText.GetBetweenChars(':', ';');
					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.DIFFICULTIES) && lineText.CustomStartsWith("#DIFFICULTIES:"))
				{
					tagFlag |= TagFlag.DIFFICULTIES;
					var diff = lineText.GetBetweenChars(':', ';').Split(',');

					int count = 0;
					songInfo.Difficulty = new List<int>(5) { 0, 0, 0, 0, 0 };

					foreach (var d in diff)
					{
						if (d.IsNullOrEmpty() || count >= 5)
						{
							continue;
						}

						if (int.TryParse(d, NumberStyles.Integer, CultureInfo.InvariantCulture, out var value))
						{
							songInfo.Difficulty[count] = value;
						}
						else
						{
							songInfo.Difficulty[count] = IsDifficultyX(d) ?
								Constant.SpecialNumber.DIFFICULT_X:
								songInfo.Difficulty[count] = 0;
						}
						count++;
					}

					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.SAMPLESTART) && lineText.CustomStartsWith("#SAMPLESTART:"))
				{
					tagFlag |= TagFlag.SAMPLESTART;

					if (float.TryParse(lineText.GetBetweenChars(':', ';'), NumberStyles.Float, CultureInfo.InvariantCulture, out var value))
					{
						songInfo.SampleStart = value;
					}
					else
					{
						songInfo.SampleStart = 0f;
					}
					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.SAMPLELENGTH) && lineText.CustomStartsWith("#SAMPLELENGTH:"))
				{
					tagFlag |= TagFlag.SAMPLELENGTH;

					if (float.TryParse(lineText.GetBetweenChars(':', ';'), NumberStyles.Float, CultureInfo.InvariantCulture, out var value))
					{
						songInfo.SampleLength = value;
					}
					else
					{
						songInfo.SampleLength = 12f;
					}
					continue;
				}

				if (!HasFlag(tagFlag, TagFlag.LASTSECONDHINT) && lineText.CustomStartsWith("#LASTSECONDHINT:"))
				{
					tagFlag |= TagFlag.LASTSECONDHINT;

					if (float.TryParse(lineText.GetBetweenChars(':', ';'), NumberStyles.Float, CultureInfo.InvariantCulture, out var value))
					{
						songInfo.LastSecondHint = value;
					}
					else
					{
						songInfo.LastSecondHint = 0f;
					}
					continue;
				}

				if (lineText.CustomStartsWith("#NOTES:"))
				{
					break;
				}

				bool isLoaded = HasFlag(tagFlag, TagFlag.ALL);

				if (isLoaded)
				{
					break;
				}
			}
		}
		catch (Exception ex)
		{
			throw new Exception(ex.Message);
		}

		rs.Close();
		return songInfo;

		static bool HasFlag(TagFlag flag, TagFlag tag)
		{
			return (flag & tag) == tag;
		}

		static bool IsDifficultyX(string difficulty)
		{
			return difficulty == "X" || difficulty == "x";
		}

	}

	// <summary>
	/// 値付きのエラーを返す
	/// </summary>
	Exception CreateErrorWithValue(string message, string value)
	{
		return new Exception(ZString.Concat(message, '"', value, '"'));
	}

	/// <summary>
	/// Jacket画像のTextureを生成
	/// </summary>
	/// <param name="path">ファイルパス</param>
	/// <returns>Texture</returns>
	Texture2D CreateJacketTexture(string directoryPath, string jacketFileName)
	{
		Texture2D texture = null;
		if (jacketFileName.HasValue())
		{
			var path = Path.Combine(directoryPath, jacketFileName);
			texture = TextureLoader.Load(path);
		}
		else
		{
			texture = new Texture2D(1, 1, TextureFormat.RGB24, false);
		}

		return texture;
	}
}
