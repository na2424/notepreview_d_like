﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SFB;
using System.IO;
using TMPro;
using System.Threading;
using System;
using Dialog;
using Cysharp.Threading.Tasks;
using Cysharp.Text;

#if UNITY_STANDALONE_WIN
using B83.Win32;
#elif UNITY_STANDALONE_OSX
using Shibuya24.Utility;
#endif

/// <summary>
/// 楽曲選択画面を管理するクラス
/// 
/// </summary>
public sealed class SelectMusicSceneController : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[Header("UI")]
	[SerializeField] Button _playmodeButton = default;
	[SerializeField] Button _settingButton = default;
	[SerializeField] Button _folderDialogButton = default;
	[SerializeField] Button _loadButton = default;
	[SerializeField] Button _sequenceInfoButton = default;
	[SerializeField] Button _previewMusicButton = default;
	[SerializeField] Button _startButton = default;

	[SerializeField] Canvas _luaCanvas = default;

	[SerializeField] TextMeshProUGUI _messageText = default;

	[SerializeField] TMP_InputField _folderInputField = default;

	[SerializeField] TextMeshProUGUI _versionText = default;

	[Header("Controller")]
	[SerializeField] MusicInfoPanel _musicInfoPanel;
	[SerializeField] PlayModePanelController _playModePanelController;
	[SerializeField] GameSettingsPanel _gameSettingsPanel;
	[SerializeField] PreviewMusic _previewMusic;
	[SerializeField] PreviewMusicIconController _previewMusicIconController;
	[SerializeField] SequenceInfoPanel _sequenceInfoPanel;
	[SerializeField] KeyConfigPanel _keyConfigPanel;
	[SerializeField] TapEffect _tapEffect;

	//------------------
	// キャッシュ.
	//------------------
	SongInfoLoader _songInfoLoader = new SongInfoLoader();
	CancellationTokenSource _ctSource;
	bool _isPlayPreviewMusic = false;

	//------------------
	// 定数.
	//------------------
	const string SUCCESS_MESSAGE = "タグの読み込みに成功しました";
	const string FAILDE_MESSAGE = "譜面ファイルが読み込めませんでした";
	const string DLPATH = "dlpath:";


	void Start()
	{
		_versionText.text = Constant.App.VersionName;

#if UNITY_STANDALONE_WIN
		TitleBarSample.ChangeBarName(ZString.Concat("DankagLikeNotePreview " , Constant.App.VersionName));
#endif

		try
		{
			CreateNoteSkinsDirectory();
			CreateTouchSoundEffectsDirectory();
			CreateGlobalLuaDirectory();
		}
		catch (Exception e)
		{
			var builder = new DialogParametor.Builder("フォルダの作成に失敗しました", e.Message);
			builder.AddDefaultAction("閉じる", () => { });
			builder.AddCallbackOnAutoClosed(() => { });
			DialogManager.Instance.Open(builder.Build());
		}

		if(!SRDebug.IsInitialized)
		{
			SRDebug.Init();
		}

		_previewMusic.Init();
		_keyConfigPanel.Init();
		_tapEffect.Init();

		// 譜面フォルダのテキストボックスの初期文字を設定
		SetFolderInputField();

		// イベント設定
		SetEvent();

		// 各コントローラーの初期化
		Init();

		// フェードイン
		FadeManager.Instance.FadeInAsync().Forget();
	}

#if UNITY_STANDALONE_WIN
	private void OnEnable()
	{
		UnityDragAndDropHook.InstallHook();
		UnityDragAndDropHook.OnDroppedFiles += OnDroppedFiles;
	}

	private void OnDroppedFiles(List<string> aPathNames, POINT aDropPoint)
	{
		if (File.Exists(aPathNames[0]))
		{
			_folderInputField.text = Path.GetDirectoryName(aPathNames[0]);
		}
		else if (Directory.Exists(aPathNames[0]))
		{
			_folderInputField.text = aPathNames[0];
		}
	}

	private void OnDisable()
	{
		UnityDragAndDropHook.OnDroppedFiles -= OnDroppedFiles;
		UnityDragAndDropHook.UninstallHook();
	}

#elif UNITY_STANDALONE_OSX
	private void OnEnable()
	{
		UniDragAndDrop.Initialize();

		UniDragAndDrop.onDragAndDropFilePath = x =>
		{
			OnDroppedFiles(x);
		};
	}

	private void OnDroppedFiles(string path)
	{
		if (File.Exists(path))
		{
			_folderInputField.text = Path.GetDirectoryName(path);
		}
		else if (Directory.Exists(path))
		{
			_folderInputField.text = path;
		}
	}

	private void OnDisable()
	{
	}
#endif

	/// <summary>
	/// FolderInputFieldの初期文字を設定する (3つの場合がある)
	/// ・アプリ単体で起動した → 空(Empty)にする
	/// ・譜面エディタアプリから起動した → 譜面エディタの「譜面フォルダ」を反映
	/// ・プレビューシーンから戻ってきた → キャッシュを反映
	/// </summary>
	void SetFolderInputField()
	{
		if (GameManager.Instance.SelectSongInfo.DirectoryPath.IsNullOrEmpty())
		{
			string path = ReceiveArgs();

			if (!path.IsNullOrEmpty())
			{
				_folderInputField.text = path;
			}
		}
		else
		{
			_folderInputField.text = GameManager.Instance.SelectSongInfo.DirectoryPath;
		}
	}

	/// <summary>
	/// 外部アプリから起動した時のコマンドライン引数を受け取る、無ければnullを返す。
	/// (先頭に"dlpath:"文字があるものを抜き出す)
	/// </summary>
	string ReceiveArgs()
	{
		string[] args = System.Environment.GetCommandLineArgs();
		for (var i = 0; i < args.Length; i++)
		{
			if (args[i].CustomStartsWith(DLPATH))
			{
				int length = args[i].Length - DLPATH.Length;
				return args[i].Substring(DLPATH.Length, length);
			}
		}

		return null;
	}

	/// <summary>
	/// NoteSkinsフォルダを生成する
	/// </summary>
	void CreateNoteSkinsDirectory()
	{
		string noteSkinsDirectory = ExternalDirectory.NoteSkinsPath;

		if (!Directory.Exists(noteSkinsDirectory))
		{
			Directory.CreateDirectory(noteSkinsDirectory);
		}
	}

	/// <summary>
	/// SoundEffectsフォルダを生成する
	/// </summary>
	void CreateTouchSoundEffectsDirectory()
	{
		string soundEffectsDirectory = ExternalDirectory.SoundEffectsPath;

		if (!Directory.Exists(soundEffectsDirectory))
		{
			Directory.CreateDirectory(soundEffectsDirectory);
		}
	}

	/// <summary>
	/// GlobalLuaフォルダを生成する
	/// </summary>
	void CreateGlobalLuaDirectory()
	{
		string globalLuaDirectory = ExternalDirectory.GlobalLuaPath;

		if (!Directory.Exists(globalLuaDirectory))
		{
			Directory.CreateDirectory(globalLuaDirectory);
		}
	}

	void SetEvent()
	{
		// プレイモード
		_playmodeButton.onClick.AddListener(() =>
		{
			_playModePanelController.Open();
		});

		// ゲーム設定
		_settingButton.onClick.AddListener(() =>
		{
			_gameSettingsPanel.SetActive(true);
		});

		// フォルダ選択ボタン
		_folderDialogButton.onClick.AddListener(() =>
		{
			_isPlayPreviewMusic = false;
			_previewMusicIconController.ChangeIcon(_isPlayPreviewMusic);
			_previewMusic.Pause();

			string inputText = _folderInputField.text;
			string openDirectory = Path.IsPathFullyQualified(inputText) ? inputText : Directory.GetCurrentDirectory();
			var paths = StandaloneFileBrowser.OpenFolderPanel("譜面フォルダ", openDirectory, true);

			if (paths != null && paths.Length > 0)
			{
				_folderInputField.text = paths[0];
			}
		});

		// 譜面ファイルロードボタン
		_loadButton.onClick.AddListener(async () =>
		{
			_previewMusicButton.interactable = false;
			string folderPath = _folderInputField.text;
			_ctSource = new CancellationTokenSource();

			if (!Path.IsPathFullyQualified(folderPath))
			{
				_messageText.text = FAILDE_MESSAGE;
				return;
			}

			try
			{
				GameManager.Instance.SelectSongInfo = await _songInfoLoader.ReadAsync(_ctSource.Token, folderPath);
				SongInfo songInfo = GameManager.Instance.SelectSongInfo;
				GameManager.Instance.IsCMod = songInfo.IsCMod;
				GameManager.Instance.LastSecondHint = songInfo.LastSecondHint;

				string path = "";

				if (songInfo.BgFileName.HasValue())
				{
					path = Path.Combine(songInfo.DirectoryPath, songInfo.BgFileName);
				}

				if (songInfo.MusicFileName.HasValue())
				{
					if (!File.Exists(Path.Combine(songInfo.DirectoryPath, songInfo.MusicFileName)))
					{
						_messageText.text = "楽曲ファイルが見つかりませんでした";
						return;
					}
				}
				else
				{
					_messageText.text = "楽曲ファイルタグに記述がありません";
					return;
				}

				_musicInfoPanel.UpdateUIPanel(songInfo, true);
				BgManager.Instance.LoadFromPath(path, true);
				_musicInfoPanel.UpdateSelectButton(true);
				_luaCanvas.enabled = songInfo.LuaScript.HasValue();

				// 楽曲プレビューのリセット
				_isPlayPreviewMusic = false;
				_previewMusicButton.interactable = true;
				_previewMusicIconController.ChangeIcon(false);

				_previewMusic.SetPreview(songInfo, 
					onError: () => _previewMusicButton.interactable = false
				);

				_messageText.text = SUCCESS_MESSAGE;
			}
			catch (Exception ex)
			{
				Debug.LogError(ex.Message);
				_messageText.text = FAILDE_MESSAGE;
			}
		});

		// 譜面詳細ボタンのイベントを設定
		_sequenceInfoButton.onClick.AddListener(() =>
		{
			if (_startButton.interactable)
			{
				_sequenceInfoPanel.Open(GameManager.Instance.SelectSongInfo);
			}
		});

		// 楽曲プレビューボタン
		_previewMusicButton.onClick.AddListener(() => 
		{
			_isPlayPreviewMusic = !_isPlayPreviewMusic;
			_previewMusicIconController.ChangeIcon(_isPlayPreviewMusic);

			if(_isPlayPreviewMusic)
			{
				_previewMusic.Play();
			}
			else
			{
				_previewMusic.Pause();
			}
		});

		// 開始ボタン
		_startButton.onClick.AddListener(async () =>
		{
			await FadeManager.Instance.FadeOutAsync(SceneBuildIndex.Game);
		});

		_previewMusic.OnStop = () =>
		{
			_isPlayPreviewMusic = false;
			_previewMusicIconController.ChangeIcon(false);
		};
	}

	void Init()
	{
		// 背景のディマー
		BgManager.Instance.UseDimmer(false);
		BgManager.Instance.LoadFromPath(null);

		// NoteSkinsフォルダを確認
		CheckNoteSkinsDirectory();

		// 各コントローラーの初期化
		_musicInfoPanel.Init();
		_playModePanelController.Init();
		_gameSettingsPanel.Init();
		_sequenceInfoPanel.Init();

		// 楽曲プレビュー
		_previewMusicButton.interactable = false;
	}

	/// <summary>
	/// NoteSkinsフォルダがあるか確認し、無ければフォルダを作成する
	/// </summary>
	void CheckNoteSkinsDirectory()
	{
		string noteSkinsDirectory = ExternalDirectory.NoteSkinsPath;

		if (!Directory.Exists(noteSkinsDirectory))
		{
			Directory.CreateDirectory(noteSkinsDirectory);
		}
	}

	void Update()
	{
		if (!_keyConfigPanel.IsOpen && Input.GetKeyDown(KeyCode.Escape))
		{
			GameManager.Instance.Quit();
		}

		_previewMusic.CallUpdate();
		_gameSettingsPanel.CallUpdate();
		_tapEffect.CallUpdate();
	}
}
