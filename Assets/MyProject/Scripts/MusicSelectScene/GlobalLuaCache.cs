﻿using System.Collections.Generic;
using UnityEngine;

public sealed class GlobalLuaCache
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	List<GlobalLuaInfo> _globalLuaInfoCache = new List<GlobalLuaInfo>();

	//------------------
	// キャッシュ.
	//------------------
	static GlobalLuaCache _instance;

	//------------------
	// プロパティ.
	//------------------
	public bool HasCache => _globalLuaInfoCache.Count > 0;

	public List<GlobalLuaInfo> Data => _globalLuaInfoCache;

	public static GlobalLuaCache Instance => _instance ??= new GlobalLuaCache();

	/// <summary>
	/// GlobalLuaInfoのキャッシュを保持する
	/// メモリ上に残り続ける
	/// </summary>
	/// <param name="globalLuaInfos"></param>
	public void Cache(List<GlobalLuaInfo> globalLuaInfos)
	{
		_globalLuaInfoCache = globalLuaInfos.DeepCopy();
	}

	public void Clear()
	{
		int count = _globalLuaInfoCache.Count;

		for (var i = 0; i < count; ++i)
		{
			_globalLuaInfoCache[i] = null;
		}

		_globalLuaInfoCache.Clear();
	}
}