﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

public sealed class GlobalLuaLoader
{
	/// <summary>
	/// GlobalLua情報の読み込み
	/// </summary>
	/// <returns>List<GlobalLuaInfo></returns>
	public List<GlobalLuaInfo> LoadGlobalLuaInfos()
	{
		List<GlobalLuaInfo> globalLuaInfos = new List<GlobalLuaInfo>();

		var directories = Directory.GetDirectories(ExternalDirectory.GlobalLuaPath);

		foreach (var directory in directories)
		{
			var luafiles = Directory.GetFiles(directory, "default.lua", System.IO.SearchOption.TopDirectoryOnly);

			if (luafiles.Length == 0)
			{
				luafiles = Directory.GetFiles(directory, "*.lua", System.IO.SearchOption.TopDirectoryOnly);
			}

			if (luafiles.Length == 0)
			{
				continue;
			}

			globalLuaInfos.Add(new GlobalLuaInfo()
			{
				GlobalLuaName = directory.GetFolderNameFromPath(),
				FileName = Path.GetFileName(luafiles[0])
			});
		}

		return globalLuaInfos;
	}
}
