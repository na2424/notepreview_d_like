﻿using Cysharp.Threading.Tasks;
using Dialog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using UnityEngine;

public sealed class NoteSkinsLoader
{
	/// <summary>
	/// ノーツスキン情報の読み込み
	/// </summary>
	/// <returns></returns>
	public async UniTask<List<NoteSkinInfo>> ReadAsync(CancellationToken ct)
	{
		List<NoteSkinInfo> noteSkinInfos = new List<NoteSkinInfo>();

		try
		{
			var jsonList = ReadJson();

			if (jsonList.Count == 0)
			{
				return noteSkinInfos;
			}

			if (ct.IsCancellationRequested)
			{
				return noteSkinInfos;
			}

			await UniTask.SwitchToThreadPool();

			foreach (var json in jsonList)
			{
				noteSkinInfos.Add(JsonUtility.FromJson<NoteSkinInfo>(json.Item2));
				noteSkinInfos[noteSkinInfos.Count - 1].DirectoryPath = json.Item1;
			}

			await UniTask.Yield();

			foreach (NoteSkinInfo info in noteSkinInfos)
			{
				info.NotesTapTexture = CreateNoteSkinTexture(info.DirectoryPath, info.notes_tap);
				info.NotesSlideTexture = CreateNoteSkinTexture(info.DirectoryPath, info.notes_slide);
				info.NotesFuzzyTexture = CreateNoteSkinTexture(info.DirectoryPath, info.notes_fuzzy);
				info.RelaySlideTexture = CreateNoteSkinTexture(info.DirectoryPath, info.relay_slide);
				info.RelayFuzzyTexture = CreateNoteSkinTexture(info.DirectoryPath, info.relay_fuzzy);
				info.LineSlideTexture = CreateNoteSkinTexture(info.DirectoryPath, info.line_slide);
				info.LineFuzzyTexture = CreateNoteSkinTexture(info.DirectoryPath, info.line_fuzzy);
			}
		}
		catch (Exception ex)
		{
			var builder = new DialogParametor.Builder("ノーツスキン読み込みエラー", ex.Message);
			builder.AddDefaultAction("閉じる", () => { });
			builder.AddCallbackOnAutoClosed(() => { });
			DialogManager.Instance.Open(builder.Build());
		}

		return noteSkinInfos;
	}

	List<(string, string)> ReadJson()
	{
		var directories = Directory.GetDirectories(ExternalDirectory.NoteSkinsPath);
		List<(string, string)> result = new List<(string, string)>();

		try
		{
			foreach (var directory in directories)
			{
				// .jsonファイルを検索
				var jsonFiles = Directory.GetFiles(directory, "*.json", System.IO.SearchOption.TopDirectoryOnly);

				if (jsonFiles.Length == 0)
				{
					continue;
				}

				var path = Path.Combine(directory, jsonFiles[0]);
				var text = File.ReadAllText(path, Encoding.UTF8);
				result.Add((directory, text));
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return result;
	}

	/// <summary>
	/// ノーツスキン画像のTextureを生成
	/// </summary>
	/// <param name="path">ファイルパス</param>
	/// <returns>Texture</returns>
	Texture CreateNoteSkinTexture(string directoryPath, string fileName)
	{
		Texture2D texture = null;
		if (fileName.HasValue())
		{
			var path = Path.Combine(directoryPath, fileName);
			texture = TextureLoader.Load(path);
		}
		else
		{
			texture = new Texture2D(1, 1, TextureFormat.RGB24, false);
		}

		return texture;
	}
}
