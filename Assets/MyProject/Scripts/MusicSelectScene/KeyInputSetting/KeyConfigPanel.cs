﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyConfigPanel : MonoBehaviour
{
    [SerializeField] RebindSaveManager _rebindSaveManager;

    [SerializeField] Button _openButton = default;
    [SerializeField] Button _closeButton = default;
    public bool IsOpen { get; private set; }

    public void Init()
    {
        _rebindSaveManager.Load();
        _openButton.onClick.AddListener(() => OnOpen());
        _closeButton.onClick.AddListener(() => OnClose());
    }

    public void OnOpen()
    {
        _rebindSaveManager.Load();
        gameObject.SetActive(true);
		IsOpen = true;
	}

    public void OnClose()
    {
        _rebindSaveManager.Save();
        gameObject.SetActive(false);
		IsOpen = false;
	}

}
