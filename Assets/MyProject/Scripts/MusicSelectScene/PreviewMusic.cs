﻿using Cysharp.Text;
using Cysharp.Threading.Tasks;
using Dialog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEngine;
using UnityEngine.Audio;

/// <summary>
/// ステージ選択の曲のプレビュー再生を担当するクラス
/// </summary>
[RequireComponent(typeof(AudioSource))]
public sealed class PreviewMusic : MonoBehaviour, ICallUpdate
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] AudioSource _audioSource;
	[SerializeField, Range(0f, 1f)] float _maxVolume;

	[SerializeField] AudioMixerGroup _pitchAudioMixer;

	public Action OnStop = null;

	//------------------
	// キャッシュ.
	//------------------
	Dictionary<int, AudioClip> _clipCacheDict = new Dictionary<int, AudioClip>();
	Queue<int> _hashCache = new Queue<int>();

	float _sampleStart = 0f;
	float _sampleLength = 0f;
	float _currentVolumeRate = 1f;
	bool _isMusicRate = false;

	SongInfo _songInfo;

	CancellationTokenSource _ctSource = null;

	//------------------
	// 定数.
	//------------------
	const float FADEOUT_TIME = 2f;
	const int CACHE_MAX = 30;

	//------------------
	// プロパティ.
	//------------------
	public static PreviewMusic Instance { get; private set; }
	public SongInfo SongInfo => _songInfo;
	public bool IsPlaying => _audioSource.isPlaying;

	/// <summary>
	/// 初期化
	/// </summary>
	public void Init()
	{
		Instance = this;
		SetMute(GameManager.Instance.VolumeOption.MusicMute);
		SetVolume(GameManager.Instance.VolumeOption.MusicVolume);
		SetMusicRate(GameManager.Instance.JudgeTimeOption.MusicRate, true);
	}

	/// <summary>
	/// ミュートを有効にするか
	/// </summary>
	/// <param name="isMute">有効か</param>
	public void SetMute(bool isMute)
	{
		_audioSource.mute = isMute;
	}

	/// <summary>
	/// 音量の設定
	/// </summary>
	/// <param name="volume">音量(0～1)</param>
	public void SetVolume(float volume)
	{
		_maxVolume = volume;
		_audioSource.volume = _maxVolume * _currentVolumeRate;
	}

	/// <summary>
	/// 楽曲速度の設定
	/// </summary>
	/// <param name="musicRate">速度(0.75～1.5)</param>
	public void SetMusicRate(float musicRate, bool isInit = false)
	{
		//Debug.Log(musicRate);
		bool isMusicRate = !Calculate.FastApproximately(musicRate, 1f, 0.001f);

		if (isMusicRate != _isMusicRate || isInit)
		{
			_isMusicRate = isMusicRate;
			_audioSource.outputAudioMixerGroup = isMusicRate ? _pitchAudioMixer : null;
		}

		if (isMusicRate)
		{
			_pitchAudioMixer.audioMixer.SetFloat(Constant.AudioMixer.PITCH, 1f / musicRate);
		}

		_audioSource.pitch = musicRate;
	}

	/// <summary>
	/// 一時停止
	/// </summary>
	public void Pause()
	{
		if(_audioSource.isPlaying)
		{
			_audioSource.Pause();
		}
	}

	public void Stop()
	{
		if (_audioSource.clip != null)
		{
			_audioSource.Stop();
			OnStop();
		}
	}

	/// <summary>
	/// 再生
	/// </summary>
	public void Play()
	{
		if(_audioSource.clip != null)
		{
			_audioSource.Play();
		}
	}

	/// <summary>
	/// 指定パスの音源をプレビュー再生する.
	/// </summary>
	/// <param name="directoryPath">ディレクトリパス</param>
	/// <param name="fileName">ファイル名</param>
	/// <param name="sampleStart">プレビュー開始時間</param>
	/// <param name="sampleLength">プレビューの長さ</param>
	public void SetPreview(SongInfo songInfo, Action onError = null, Action onComplete = null)
	{
		// 既にキャンセルトークンがある時は前の処理をキャンセルする
		if (_ctSource != null)
		{
			_ctSource.Cancel();
			_ctSource = null;
		}

		_songInfo = songInfo;

		// 新しくキャンセルトークンを発行する
		_ctSource = new CancellationTokenSource();

		SetPreviewAsync(_ctSource.Token, songInfo.DirectoryPath, songInfo.MusicFileName, songInfo.SampleStart, songInfo.SampleLength, onError, onComplete).Forget();
	}

	/// <summary>
	/// 指定したパスのファイルからAudioClipを生成して、楽曲のプレビュー再生を行なう.
	/// </summary>
	/// <param name="ct">キャンセルトークン</param>
	/// <param name="directoryPath">ディレクトリのパス</param>
	/// <param name="fileName">ファイル名</param>
	/// <param name="sampleStart">プレビューの開始位置(秒)</param>
	/// <param name="sampleLength">プレビューの長さ(秒)</param>
	/// <returns></returns>
	async UniTask SetPreviewAsync(CancellationToken ct, string directoryPath, string fileName, float sampleStart, float sampleLength, Action onError, Action onComplete = null)
	{
		if (_audioSource.isPlaying)
		{
			_audioSource.Stop();
		}

		_sampleStart = sampleStart;
		_sampleLength = sampleLength;

		AudioClip clip = null;

		// フォルダとファイル名からハッシュ値を生成する (Dictionaryのキーとして使う)
		int hash = ZString.Concat(directoryPath, fileName).GetHashCode();

		// clipのキャッシュが既にあれば
		if (_clipCacheDict.ContainsKey(hash))
		{
			clip = _clipCacheDict[hash];
		}
		else
		{
			var path = ZString.Concat("file://", Path.Combine(directoryPath, fileName));

			try
			{
				clip = await AudioClipLoader.LoadAsync(ct, path, true);
			}
			catch (Exception ex)
			{
				Debug.LogError(ex.Message);
				onError?.Invoke();

				int index = directoryPath.IndexOf("Songs");
				string relativePath = directoryPath.Substring(index, directoryPath.Length - index);
				var builder = new DialogParametor.Builder(
					"楽曲読み込みエラー",
					ZString.Concat("楽曲を読み込めませんでした。ファイルの形式、フォルダやファイル名に読み込めない文字がないか確認してください。\n楽曲ファイルパス : ", Path.Combine(relativePath, fileName)));

				builder.AddDefaultAction("選曲を続ける", () => { });
				builder.AddCallbackOnAutoClosed(() => { });
				DialogManager.Instance.Open(builder.Build());
				return;
			}
			_clipCacheDict[hash] = clip;

			// キャッシュの上限を超えた時は先に入れたものから削除する
			if (_hashCache.Count >= CACHE_MAX)
			{
				int dequeueHash = _hashCache.Dequeue();
				bool isUnloaded = _clipCacheDict[dequeueHash].UnloadAudioData();
				Destroy(_clipCacheDict[dequeueHash]);

				if (!isUnloaded)
				{
					var builder = new DialogParametor.Builder(
					"楽曲アンロードエラー",
					"楽曲キャッシュのアンロードに失敗しました。");

					builder.AddDefaultAction("選曲を続ける", () => { });
					builder.AddCallbackOnAutoClosed(() => { });
					DialogManager.Instance.Open(builder.Build());
				}

				_clipCacheDict.Remove(dequeueHash);
			}

			_hashCache.Enqueue(hash);
		}

		if (ct.IsCancellationRequested)
		{
			return;
		}

		SetAudioClip(clip);
		onComplete?.Invoke();
	}

	/// <summary>
	/// audioClipをAudioSourceに設定して再生
	/// その他設定をAudioSouceに反映する
	/// </summary>
	/// <param name="audioClip"></param>
	void SetAudioClip(AudioClip audioClip)
	{
		_audioSource.clip = audioClip;
		_audioSource.volume = _maxVolume;
		_audioSource.loop = false;
		_audioSource.time = _sampleStart;
	}

	public void CallUpdate()
	{
		// プレビューが再生されていないときはスキップ
		if (!_audioSource.isPlaying)
		{
			return;
		}

		// プレビューの終了時間になったらプレビューの最初から再生する
		if (_audioSource.time > _sampleStart + _sampleLength)
		{
			_currentVolumeRate = 1f;
			_audioSource.time = _sampleStart;
			_audioSource.volume = _maxVolume;
		}
		// プレビューの終了時間よりFADEOUT_TIME秒前になったら徐々にフェードアウトさせる
		else if (_audioSource.time > _sampleStart + _sampleLength - FADEOUT_TIME)
		{
			_currentVolumeRate = Mathf.Clamp01((_sampleStart + _sampleLength - _audioSource.time) / FADEOUT_TIME);
			_audioSource.volume = _maxVolume * _currentVolumeRate;
		}
	}

	public void ClearClipCache()
	{
		_clipCacheDict.Clear();
	}

	/// <summary>
	/// オブジェクトが破棄されるときに呼ばれる
	/// 解放処理
	/// </summary>
	void OnDestroy()
	{
		Instance = null;
		_clipCacheDict.Clear();
		_hashCache.Clear();
		_clipCacheDict = null;
		_hashCache = null;
	}
}
