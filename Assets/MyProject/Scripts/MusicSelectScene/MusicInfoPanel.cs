﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public sealed class MusicInfoPanel : MonoBehaviour
{
	[Serializable]
	public class DifficultyPanel
	{
		public bool Exist = false;
		public Toggle Toggle = default;
		public Image BgImage = default;
		public TextMeshProUGUI DifficultyLabel = default;
		public TextMeshProUGUI DifficultyText = default;
	}

	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] Button _selectButton;
	[SerializeField] AspectRatioRawImage _jacketRawImage;
	[SerializeField] TextScroll _titleTextScroll;
	[SerializeField] TextScroll _subtitleTextScroll;
	[SerializeField] TextScroll _artistTextScroll;
	[SerializeField] DifficultyPanel[] _difficultyPanel;

	[SerializeField] ColorDefine _colorDefine;

	//------------------
	// キャッシュ.
	//------------------

	//------------------
	// 定数.
	//------------------
	readonly Color32 DISABLE_DIFFICULTY_COLOR = new Color32(90, 81, 76, 255);
	readonly Color32 ENABLE_DIFFICULTY_COLOR = new Color32(255, 255, 255, 255);
	readonly Color32 DISABLE_DIFFICULTY_BG_COLOR = new Color32(228, 220, 213, 255);
	const string NONE_DIFFICULTY_TEXT = "-";

	//------------------
	// プロパティ.
	//------------------

	public void Init()
	{
		for (int i = 0; i < _difficultyPanel.Length; i++)
		{
			_difficultyPanel[i].Toggle.isOn = false;
			SetToggleEvent(_difficultyPanel[i].Toggle, i);

			_difficultyPanel[i].Exist = false;
			_difficultyPanel[i].DifficultyText.SetText(NONE_DIFFICULTY_TEXT);
			_difficultyPanel[i].Toggle.interactable = false;
		}

		int selectToggleIndex = (int)GameManager.Instance.SelectDifficulty;

		if (!_difficultyPanel[selectToggleIndex].Toggle.isOn)
		{
			_difficultyPanel[selectToggleIndex].Toggle.isOn = true;
		}
		else
		{
			// Lunaticの時に色が付かない問題の対応.
			SetEnableColorDifficultyPanel(selectToggleIndex);
		}

		_selectButton.interactable = false;
	}

	public void Show()
	{
		gameObject.SetActive(true);
	}

	public void Hide()
	{
		gameObject.SetActive(false);
	}

	/// <summary>
	/// ドグルが変更されたときのイベントを付ける
	/// </summary>
	/// <param name="toggle">トグル</param>
	/// <param name="index">要素番号</param>
	void SetToggleEvent(Toggle toggle, int index)
	{
		toggle.onValueChanged.AddListener(isOn =>
		{
			if (isOn)
			{
				_selectButton.interactable = true;
				GameManager.Instance.SelectDifficulty = (DifficultyType)index;
				SetEnableColorDifficultyPanel(index);
			}
			else
			{
				SetDisableColorDifficultyPanel(index);
			}
		});
	}

	/// <summary>
	/// 難易度を選んだ時の色を付ける
	/// </summary>
	/// <param name="index">難易度パネルの要素番号</param>
	void SetEnableColorDifficultyPanel(int index)
	{
		_selectButton.interactable = true;
		_difficultyPanel[index].BgImage.color = GetDifficultyColor((DifficultyType)index);
		_difficultyPanel[index].DifficultyLabel.color = ENABLE_DIFFICULTY_COLOR;
		_difficultyPanel[index].DifficultyText.color = ENABLE_DIFFICULTY_COLOR;

		Color GetDifficultyColor(DifficultyType type) => type switch
		{
			DifficultyType.Easy => _colorDefine.Easy,
			DifficultyType.Normal => _colorDefine.Normal,
			DifficultyType.Hard => _colorDefine.Hard,
			DifficultyType.Extra => _colorDefine.Extra,
			DifficultyType.Lunatic => _colorDefine.Lunatic,
			_ => Color.black
		};
	}

	/// <summary>
	/// 難易度が非選択になった時の色を付ける
	/// </summary>
	/// <param name="index">難易度パネルの要素番号</param>
	void SetDisableColorDifficultyPanel(int index)
	{
		_difficultyPanel[index].BgImage.color = DISABLE_DIFFICULTY_BG_COLOR;
		_difficultyPanel[index].DifficultyLabel.color = DISABLE_DIFFICULTY_COLOR;
		_difficultyPanel[index].DifficultyText.color = DISABLE_DIFFICULTY_COLOR;
	}

	/// <summary>
	/// SongInfoの情報をパネルに反映させる.
	/// </summary>
	/// <param name="songInfo">楽曲データ</param>
	public void UpdateUIPanel(SongInfo songInfo, bool isCheckSelectButton = false)
	{
		_titleTextScroll.SetText(StringReplaceSpriteAsset.ReplaceUra(songInfo.Title));
		_subtitleTextScroll.SetText(songInfo.SubTitle);
		_artistTextScroll.SetText(songInfo.Artist);
		_jacketRawImage.SetTexture(songInfo.JacketTexture);

		for (int i = 0; i < _difficultyPanel.Length; i++)
		{
			if (songInfo.Difficulty.Count <= i || songInfo.Difficulty[i] == 0)
			{
				_difficultyPanel[i].Exist = false;
				_difficultyPanel[i].DifficultyText.SetText(NONE_DIFFICULTY_TEXT);
				_difficultyPanel[i].Toggle.interactable = false;
			}
			else
			{
				_difficultyPanel[i].Exist = true;
				_difficultyPanel[i].DifficultyText.text = ConvertDifficultyString(songInfo.Difficulty[i]);


				_difficultyPanel[i].Toggle.interactable = true;
			}
		}

		if (!_difficultyPanel[(int)GameManager.Instance.SelectDifficulty].Exist)
			_selectButton.interactable = false;
	}

	public void UpdateSelectButton(bool isCheckSelectButton = false)
	{
		if (!_difficultyPanel[(int)GameManager.Instance.SelectDifficulty].Exist)
			_selectButton.interactable = false;

		if (isCheckSelectButton)
		{
			_selectButton.interactable = _difficultyPanel[(int)GameManager.Instance.SelectDifficulty].Exist;
		}
	}

	string ConvertDifficultyString(int difficulty)
	{
		return difficulty == Constant.SpecialNumber.DIFFICULT_X ?
				"X" :
				difficulty.ToString();
	}
}
