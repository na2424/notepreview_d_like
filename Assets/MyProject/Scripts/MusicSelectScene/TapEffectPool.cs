﻿using UnityEngine;
using UnityEngine.Pool;

public sealed class TapEffectPool : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] TapEffectController _tapEffectControllerPrefab;

	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<TapEffectController> _pool = null;
	Transform _transform;

	//------------------
	// 定数.
	//------------------
	// 初期のプールサイズ
	const int DEFAULT_CAPACITY = 8;
	// プールサイズを最大どれだけ大きくするか
	const int MAX_SIZE = 16;

	public void Init()
	{
		_transform = transform;
	}

	public ObjectPool<TapEffectController> Pool
	{
		get
		{
			if (_pool is null)
			{
				_pool = new ObjectPool<TapEffectController>(OnCreatePoolObject, OnTakeFromPool, OnReturnedToPool, OnDestroyPoolObject, false, DEFAULT_CAPACITY, MAX_SIZE);
			}

			return _pool;
		}
	}

	public void Play(Vector3 pos)
	{
		var controller = Pool.Get();
		controller.transform.localPosition = pos;
		controller.Play();
	}

	TapEffectController OnCreatePoolObject()
	{
		var controller = Instantiate(_tapEffectControllerPrefab, _transform);
		controller.Init(Pool);
		return controller;
	}

	void OnTakeFromPool(TapEffectController controller)
	{
		controller.gameObject.SetActive(true);
		controller.Play();
	}

	void OnReturnedToPool(TapEffectController controller)
	{
		controller.gameObject.SetActive(false);
	}

	void OnDestroyPoolObject(TapEffectController controller)
	{
		Destroy(controller.gameObject);
	}
}
