﻿using Cysharp.Text;
using Cysharp.Threading.Tasks;
using ELEBEAT;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 譜面情報のパネル
/// </summary>
public sealed class SequenceInfoPanel : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] Button _closeButton = default;
	[SerializeField] ScrollRect _scrollRect = default;
	[SerializeField] RouletteManager _rouletteManager;
	[SerializeField] DensityDrawer _densityDrawer;
	[SerializeField] TextScroll _sequenceArtistText;
	[SerializeField] GameObject _sequenceInfoPanelObj;
	[SerializeField] TextMeshProUGUI _loadStateText;
	[SerializeField] TextMeshProUGUI _totalNotesText;
	[SerializeField] TextMeshProUGUI[] _noteCountText;
	[SerializeField] TextMeshProUGUI _longCount;
	[SerializeField] TextMeshProUGUI _fuzzyLongCount;
	[SerializeField] TextMeshProUGUI _laneNotesCount;

	[SerializeField] Toggle[] _densityToggles;

	//------------------
	// キャッシュ.
	//------------------
	CancellationTokenSource _ctSource;
	SequenceReader _sequenceReader = new SequenceReader();
	Sequence _sequence;
	BpmHelper _bpmHelper;

	//------------------
	// 定数.
	//------------------
	const string WAIT_TEXT = "読み込み中...";
	const string ERROR_TEXT = "譜面が読み込めませんでした";

	//------------------
	// プロパティ.
	//------------------

	public void Init()
	{
		SetEvent();
		gameObject.SetActive(false);
		_densityDrawer.Init();
	}

	void SetEvent()
	{
		_closeButton.onClick.AddListener(() => Close());

		for (int i = 0; i < _densityToggles.Length; i++)
		{
			SetDensityToggleEvent(_densityToggles[i], i);
		}
	}

	void SetDensityToggleEvent(Toggle toggle, int index)
	{
		toggle.onValueChanged.AddListener(isOn =>
		{
			if (isOn)
			{
				var type = (DensityType)index;
				int noteLength = _densityDrawer.Calculate(_sequence, _bpmHelper, type);
				_densityDrawer.DrawGraph(type, noteLength);
			}
		});
	}

	void Close()
	{
		_sequence = null;
		_bpmHelper = null;

		if (_ctSource != null)
		{
			_ctSource.Cancel();
			_ctSource = null;
		}

		// Open時では変化しなかった為、Closeに入れている
		_densityToggles[(int)DensityType.Total].SetIsOnWithoutNotify(true);

		gameObject.SetActive(false);
	}

	public void Open(SongInfo songInfo)
	{
		_loadStateText.text = WAIT_TEXT;
		_loadStateText.gameObject.SetActive(true);
		_sequenceInfoPanelObj.SetActive(false);
		_sequenceArtistText.SetText(songInfo.ChartArtist);
		_scrollRect.verticalNormalizedPosition = 1.0f;
		_densityToggles[(int)DensityType.Total].SetIsOnWithoutNotify(true);
		_densityDrawer.Reset(songInfo.Offset);

		gameObject.SetActive(true);

		if (_ctSource != null)
		{
			_ctSource.Cancel();
			_ctSource = null;
		}

		_ctSource = new CancellationTokenSource();

		LoadAsync(_ctSource.Token, songInfo).Forget();
	}

	async UniTask<bool> LoadAsync(CancellationToken ct, SongInfo songInfo)
	{
		Sequence sequence = null;

		try
		{
			sequence = await _sequenceReader.ReadAsync(ct, songInfo);
		}
		catch (Exception ex)
		{
			_loadStateText.SetTextFormat("{0}\n({1})", ERROR_TEXT, ex.Message);
			return false;
		}

		if (ct.IsCancellationRequested)
		{
			return false;
		}

		_loadStateText.gameObject.SetActive(false);
		SetSequenceDetail(sequence);

		if (sequence.Credit.HasValue())
		{
			_sequenceArtistText.SetText(sequence.Credit);
		}

		await UniTask.Yield();

		BpmHelper bpmHelper = new BpmHelper();
		bpmHelper.Init(songInfo);
		_sequence = sequence;
		_bpmHelper = bpmHelper;

		int noteLength = _densityDrawer.Calculate(sequence, bpmHelper, DensityType.Total);
		_densityDrawer.DrawGraph(DensityType.Total, noteLength);

		return true;
	}

	void SetSequenceDetail(Sequence sequence)
	{
		_totalNotesText.text = sequence.BeatPositions.Count.ToString();

		int yellowCount = sequence.NoteTypes.Count(noteType => noteType == NoteType.Normal);

		int blueCount = sequence.NoteTypes.Count(noteType =>
			 noteType == NoteType.LongStart ||
			 noteType == NoteType.LongRelay ||
			 noteType == NoteType.LongEnd);

		int greenCount = sequence.NoteTypes.Count(noteType =>
			noteType == NoteType.Fuzzy ||
			noteType == NoteType.FuzzyLongStart ||
			noteType == NoteType.FuzzyLongRelay ||
			noteType == NoteType.FuzzyLongEnd);

		_noteCountText[0].text = yellowCount.ToString();
		_noteCountText[1].text = blueCount.ToString();
		_noteCountText[2].text = greenCount.ToString();

		_rouletteManager.Init(new int[3] { yellowCount, blueCount, greenCount });

		_longCount.text = sequence.LongInfo.Count(longInfo => longInfo.LongType == LongType.Long).ToString();
		_fuzzyLongCount.text = sequence.LongInfo.Count(longInfo => longInfo.LongType == LongType.FuzzyLong).ToString();

		Span<int> laneCounts = stackalloc int[7] { 0, 0, 0, 0, 0, 0, 0 };

		for (int i = 0; i < sequence.Lanes.Count; i++)
		{
			laneCounts[sequence.Lanes[i]]++;
		}

		using (var sb = ZString.CreateUtf8StringBuilder())
		{
			for (int i = 0; i < laneCounts.Length; i++)
			{
				sb.Append(laneCounts[i]);

				if (i < laneCounts.Length - 1)
				{
					sb.Append(" | ");
				}
			}
			_laneNotesCount.text = sb.ToString();
		}

		_sequenceInfoPanelObj.SetActive(true);
	}


	private void OnDestroy()
	{
		_densityDrawer.Dispose();
	}

}
