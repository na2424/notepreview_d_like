﻿using System.Collections.Generic;
using UnityEngine;

public sealed class TouchSeCache
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	List<TouchSeInfo> _touchSeInfoCache = new List<TouchSeInfo>();

	//------------------
	// キャッシュ.
	//------------------
	static TouchSeCache _instance;

	//------------------
	// プロパティ.
	//------------------
	public bool HasCache => _touchSeInfoCache.Count > 0;

	public List<TouchSeInfo> Data => _touchSeInfoCache;

	public static TouchSeCache Instance => _instance ??= new TouchSeCache();

	/// <summary>
	/// TouchSeのキャッシュを保持する
	/// メモリ上に残り続ける
	/// </summary>
	/// <param name="touchSeInfos"></param>
	public void Cache(List<TouchSeInfo> touchSeInfos)
	{
		_touchSeInfoCache = touchSeInfos.DeepCopy();
	}

	public void Clear()
	{
		int count = _touchSeInfoCache.Count;

		for (var i = 0; i < count; ++i)
		{
			_touchSeInfoCache[i] = null;
		}

		_touchSeInfoCache.Clear();
	}
}