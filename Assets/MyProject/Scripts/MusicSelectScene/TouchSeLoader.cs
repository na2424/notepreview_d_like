﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

public sealed class TouchSeLoader
{
	/// <summary>
	/// タッチSE情報の読み込み
	/// </summary>
	/// <returns></returns>
	public List<TouchSeInfo> LoadTouchSeInfos()
	{
		List<TouchSeInfo> touchSeInfos = new List<TouchSeInfo>();

		var directories = Directory.GetDirectories(ExternalDirectory.SoundEffectsPath);

		foreach (var directory in directories)
		{
			var acbfiles = Directory.GetFiles(directory, "*.acb", System.IO.SearchOption.TopDirectoryOnly);

			if (acbfiles.Length == 0)
			{
				continue;
			}

			touchSeInfos.Add(new TouchSeInfo()
			{
				TouchSeName = directory.GetFolderNameFromPath(),
				AcbFile = Path.GetFileName(acbfiles[0])
			});
		}

		return touchSeInfos;
	}
}
