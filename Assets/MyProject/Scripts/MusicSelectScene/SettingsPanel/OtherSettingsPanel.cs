﻿using Cysharp.Text;
using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OtherSettingsPanel : MonoBehaviour
{
	/// <summary>
	/// スライダー値を設定するクラス
	/// </summary>
	[Serializable]
	public class SliderPanel
	{
		[SerializeField] Button _downButton = default;
		[SerializeField] Button _upButton = default;
		[SerializeField] Slider _slider = default;
		[SerializeField] TextMeshProUGUI _valueText = default;

		public Action<float> OnChangeValue = null;

		// 現在の値
		float _currentValue = 0f;
		// スライダーの1移動分
		float _deltaValue = 1f;
		// 値の単位文字
		string _unit;

		public void Init(float value, float delta, string unit, float maxValue)
		{
			SetEvent();
			_currentValue = value;
			_slider.maxValue = maxValue;
			_slider.value = (int)(value);
			_deltaValue = delta;
			_unit = unit;
		}

		void SetEvent()
		{
			_downButton.onClick.AddListener(() =>
			{
				_slider.value = Mathf.Clamp(_currentValue - _deltaValue, _slider.minValue, _slider.maxValue);
			});

			_upButton.onClick.AddListener(() =>
			{
				_slider.value = Mathf.Clamp(_currentValue + _deltaValue, _slider.minValue, _slider.maxValue);
			});

			_slider.onValueChanged.AddListener(value =>
			{
				_currentValue = value;
				_valueText.SetText(ZString.Concat(value, _unit));
				OnChangeValue(value);
			});
		}

		public void SetValue(float value)
		{
			_currentValue = value;
			_slider.value = (int)(value);
		}
	}

	[Serializable]
	public class MusicRatePanel
	{
		[SerializeField] Button _resetButton = default;
		[SerializeField] Button _downButton = default;
		[SerializeField] Button _upButton = default;
		[SerializeField] Slider _rateSilder = default;
		[SerializeField] TextMeshProUGUI _musicRateText = default;

		public Action<int> OnChangeValue = null;

		public void Init(float musicRate)
		{
			SetEvent();

			_rateSilder.value = (int)((decimal)musicRate * 100);
			_musicRateText.SetTextFormat("x{0:F2}", musicRate);
		}

		void SetEvent()
		{
			_resetButton.onClick.AddListener(() =>
			{
				_rateSilder.value = 100;
			});

			_downButton.onClick.AddListener(() =>
			{
				_rateSilder.value = Mathf.Clamp(_rateSilder.value - 1, _rateSilder.minValue, _rateSilder.maxValue);
			});

			_upButton.onClick.AddListener(() =>
			{
				_rateSilder.value = Mathf.Clamp(_rateSilder.value + 1, _rateSilder.minValue, _rateSilder.maxValue);
			});

			_rateSilder.onValueChanged.AddListener(value =>
			{
				_musicRateText.SetTextFormat("x{0:F2}", value / 100f);
				OnChangeValue((int)value);
			});
		}

		public void Reset()
		{
			_rateSilder.value = 100;
		}
	}

	[Serializable]
	public class MirrorPanel
	{
		[SerializeField] Toggle _noToggle = default;
		[SerializeField] Toggle _yesToggle = default;

		public Action<bool> OnChangeValue = null;

		public void Init(bool isEnable)
		{
			_noToggle.isOn = !isEnable;
			_yesToggle.isOn = isEnable;

			_yesToggle.onValueChanged.AddListener(isOn =>
			{
				OnChangeValue(isOn);
			});
		}

		public void Reset()
		{
			_noToggle.isOn = true;
			_yesToggle.isOn = false;
		}
	}

	[Serializable]
	public sealed class GlobalLuaPanel
	{
		//--------------------------------------
		// インスペクタまたは外部から設定.
		//--------------------------------------
		[SerializeField] Button _downButton = default;
		[SerializeField] Button _upButton = default;
		[SerializeField] TextMeshProUGUI _globalLuaNameText = default;

		public Action<string> OnChangeValue = null;

		//------------------
		// キャッシュ.
		//------------------
		List<GlobalLuaInfo> _globalLuaInfoList = new List<GlobalLuaInfo>();
		int _currentIndex = 0;

		//------------------
		// プロパティ.
		//------------------
		public List<GlobalLuaInfo> GlobalLuaInfoList => _globalLuaInfoList;

		public void Init(string globalLuaName)
		{
			// ◀ボタンを押したとき
			_downButton.onClick.AddListener(() =>
			{
				_currentIndex--;

				if (_currentIndex < 0)
				{
					_currentIndex = _globalLuaInfoList.Count - 1;
				}

				_globalLuaNameText.text = _globalLuaInfoList[_currentIndex].GlobalLuaName;

				// セーブデータに反映
				OnChangeValue?.Invoke(_globalLuaInfoList[_currentIndex].GlobalLuaName);
			});

			// ▶ボタンを押したとき
			_upButton.onClick.AddListener(() =>
			{
				_currentIndex++;

				if (_currentIndex >= _globalLuaInfoList.Count)
				{
					_currentIndex = 0;
				}

				_globalLuaNameText.text = _globalLuaInfoList[_currentIndex].GlobalLuaName;

				// セーブデータに反映
				OnChangeValue?.Invoke(_globalLuaInfoList[_currentIndex].GlobalLuaName);
			});

			CancellationTokenSource ctSource = new CancellationTokenSource();
			InitGlobalLua(globalLuaName);
		}

		void InitGlobalLua(string globalLuaName)
		{
			if (!GlobalLuaCache.Instance.HasCache)
			{
				// デフォルトGlobalLuaをリストに追加 (インデックス番号0がデフォルトになる)
				_globalLuaInfoList.Add(new GlobalLuaInfo()
				{
					GlobalLuaName = Constant.PlayerOption.Other.DEFAULT_GLOBAL_LUA_NAME,
					FileName = Constant.PlayerOption.Other.DEFAULT_GLOBAL_LUA_FILENAME
				});

				// カスタムGlobalLuaをリストに追加
				_globalLuaInfoList.AddRange(ExternalDirectory.LoadGlobalLua());

				GlobalLuaCache.Instance.Cache(_globalLuaInfoList);
			}

			_globalLuaInfoList = GlobalLuaCache.Instance.Data;

			if (_globalLuaInfoList.Count <= 1)
			{
				_upButton.gameObject.SetActive(false);
				_downButton.gameObject.SetActive(false);
			}

			// セーブデータのGlobalLuaの名前から初期値を設定
			_currentIndex = _globalLuaInfoList.FindIndex(t => t.GlobalLuaName == globalLuaName);

			// ない時はデフォルトにする
			if (_currentIndex < 0)
			{
				_currentIndex = 0;
			}

			// 表示名更新
			_globalLuaNameText.SetText(_globalLuaInfoList[_currentIndex].GlobalLuaName);
		}

		public void Reset()
		{
			_currentIndex = 0;

			_globalLuaNameText.text = _globalLuaInfoList[_currentIndex].GlobalLuaName;

			// セーブデータに反映
			OnChangeValue?.Invoke(_globalLuaInfoList[_currentIndex].GlobalLuaName);
		}
	}

	[Serializable]
	public class YesNoTogglePanel
	{
		[SerializeField] Toggle _noToggle = default;
		[SerializeField] Toggle _yesToggle = default;

		public Action<bool> OnChangeValue = null;

		public void Init(bool isEnable)
		{
			_noToggle.isOn = !isEnable;
			_yesToggle.isOn = isEnable;

			_yesToggle.onValueChanged.AddListener(isOn =>
			{
				OnChangeValue(isOn);
			});
		}

		public void Reset()
		{
			_noToggle.isOn = true;
			_yesToggle.isOn = false;
		}
	}

	/// <summary>
	/// DSP Buffer Size
	/// </summary>
	[Serializable]
	public class DspBufferSizePanel
	{
		[SerializeField] Toggle _256Toggle = default;
		[SerializeField] Toggle _512Toggle = default;

		public Action<bool> OnChangeValue = null;

		public void Init(bool is512)
		{
			_256Toggle.isOn = !is512;
			_512Toggle.isOn = is512;

			_512Toggle.onValueChanged.AddListener(is512 =>
			{
				OnChangeValue(is512);
			});
		}

		public void Reset()
		{
			_256Toggle.isOn = true;
			_512Toggle.isOn = false;
		}
	}


	[Header("画面サイズ")]
	[SerializeField] SliderPanel _widthSizePanel;
	[SerializeField] SliderPanel _heightSizePanel;
	[SerializeField] Button _resetButton = default;
	[SerializeField] Button _excuteButton = default;

	[Header("特殊オプション")]
	[SerializeField] MusicRatePanel _musicRatePanel;
	[SerializeField] MirrorPanel _mirrorPanel;
	[SerializeField] GlobalLuaPanel _globalLuaPanel;
	[SerializeField] YesNoTogglePanel _hitDistancePanel;
	[SerializeField] YesNoTogglePanel _resultScenePanel;

	[Header("オーディオ設定")]
	[SerializeField] DspBufferSizePanel _dspBufferSizePanel;

	float _width = 1280f;
	float _height = 720f;

	public void Init()
	{
		var judgeTimeOption = GameManager.Instance.JudgeTimeOption;

		_width = Screen.width;
		_height = Screen.height;

		_widthSizePanel.OnChangeValue = value =>
		{
			_width = value;
		};

		_heightSizePanel.OnChangeValue = value =>
		{
			_height = value;
		};

		_resetButton.onClick.AddListener(() =>
		{
			_width = 1280f;
			_height = 720f;
			_widthSizePanel.SetValue(_width);
			_heightSizePanel.SetValue(_height);
		});

		_excuteButton.onClick.AddListener(() =>
		{
			Screen.SetResolution((int)((decimal)_width), (int)((decimal)_height), false);
			BgManager.Instance.ResetResolution();
		});


		//
		// 特殊オプション
		//

		_musicRatePanel.OnChangeValue = value =>
		{
			float musicRate = (float)((decimal)value / 100);
			judgeTimeOption.MusicRate = musicRate;
			PreviewMusic.Instance.SetMusicRate(musicRate);
		};

		_mirrorPanel.OnChangeValue = value =>
		{
			judgeTimeOption.Mirror = value;
		};

		_globalLuaPanel.OnChangeValue = value =>
		{
			judgeTimeOption.GlobalLuaName = value;
		};

		//
		// オーディオ設定
		//

		_dspBufferSizePanel.OnChangeValue = async value =>
		{
			SongInfo songInfo = PreviewMusic.Instance.SongInfo;
			bool isPlaying = false;
			if (PreviewMusic.Instance.IsPlaying)
			{
				isPlaying = true;
				PreviewMusic.Instance.Pause();
			}

			judgeTimeOption.DspBufferSize = value ? 512 : 256;

			AudioConfiguration ac = AudioSettings.GetConfiguration();
			ac.dspBufferSize = judgeTimeOption.DspBufferSize;
			//ac.sampleRate = 44100;
			AudioSettings.Reset(ac);

			await UniTask.Yield();

			PreviewMusic.Instance.ClearClipCache();
			PreviewMusic.Instance.SetMusicRate(judgeTimeOption.MusicRate);

			if (songInfo != null)
			{
				//songInfo = songInfo.CopySongInfo();
				PreviewMusic.Instance.SetPreview(songInfo, () =>
				{
					Debug.LogError("再生に失敗しました");
				}, () => 
				{
					if (isPlaying)
					{
						PreviewMusic.Instance.Play();
					}
				});
			}

		};

		_hitDistancePanel.OnChangeValue = value =>
		{
			if (value)
			{
				judgeTimeOption.LongRevisionDistance = 0f;
				judgeTimeOption.JudgeDistance = 0.25f;
			}
			else
			{
				judgeTimeOption.LongRevisionDistance = Constant.JudgeTime.LONG_REVISION_DISTANCE;
				judgeTimeOption.JudgeDistance = Constant.JudgeTime.JUDGE_DISTANCE;
			}
		};

		_resultScenePanel.OnChangeValue = value =>
		{
			judgeTimeOption.EnableResultScene = value;
		};

		_widthSizePanel.Init(_width, 1f, "", Screen.currentResolution.width);
		_heightSizePanel.Init(_height, 1f, "", Screen.currentResolution.height);
		_musicRatePanel.Init(judgeTimeOption.MusicRate);
		_mirrorPanel.Init(judgeTimeOption.Mirror);
		_globalLuaPanel.Init(judgeTimeOption.GlobalLuaName);
		_dspBufferSizePanel.Init(judgeTimeOption.DspBufferSize == 512);
		_hitDistancePanel.Init(judgeTimeOption.JudgeDistance != Constant.JudgeTime.JUDGE_DISTANCE);
		_resultScenePanel.Init(judgeTimeOption.EnableResultScene);
	}

	public void SetActive(bool isActive)
	{
		if (isActive)
		{
			_widthSizePanel.SetValue(Screen.width);
			_heightSizePanel.SetValue(Screen.height);
		}

		gameObject.SetActive(isActive);
	}
}
