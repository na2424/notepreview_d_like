﻿using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorPickerPanel : MonoBehaviour
{
	//---------------------------------
	// インスペクタまたは外部から設定.
	//---------------------------------
    [SerializeField] FlexibleColorPicker _flexibleColorPicker;
    [SerializeField] Button _waveColorCancelButton = default;
	[SerializeField] Button _waveColorOkButton = default;

	//---------------------
	// キャッシュ.
	//---------------------
	GameObject _myObject;

	//---------------------
	// プロパティ.
	//---------------------
	public FlexibleColorPicker FlexibleColorPicker => _flexibleColorPicker;

    public void Init(IChangeColor settingPanel)
    {
		_myObject = gameObject;

		_waveColorOkButton.onClick.AddListener(() =>
		{
			Color color = _flexibleColorPicker.GetColor();
			settingPanel.ChangeColor(color);
			settingPanel.ChangePanelState(false).Forget();
		});

		_waveColorCancelButton.onClick.AddListener(() =>
		{
			_flexibleColorPicker.SetColor(settingPanel.GetColor());
			settingPanel.ChangePanelState(false).Forget();
		});
    }

	public void SetActive(bool isActive)
	{
		_myObject.SetActive(isActive);
	}
}
