﻿using Dialog;
using System;
using UnityEngine;
using UnityEngine.UI;

public class GameSettingsPrefas : ThirtySec.Serializable<GameSettingsPrefas>
{
	public NotesOption NotesOption;
	public DisplayOption DisplayOption;
	public VolumeOption VolumeOption;
	public JudgeTimeOption JudgeTimeOption;
}

public class GameSettingsPanel : MonoBehaviour
{
	[SerializeField] Button _closeButton = default;
	[SerializeField] Toggle _notesToggle = default;
	[SerializeField] Toggle _displayToggle = default;
	[SerializeField] Toggle _volumeToggle = default;
	[SerializeField] Toggle _windowSizeToggle = default;
	[SerializeField] NotesSettingsPanel _notesSettingsPanel;
	[SerializeField] DisplaySettingsPanel _displaySettingsPanel;
	[SerializeField] VolumeSettingsPanel _volumeSettingsPanel;
	[SerializeField] OtherSettingsPanel _windowSizeSettingsPanel;

	public void Init()
	{
		_closeButton.onClick.AddListener(() => 
		{
			try
			{
				GameSettingsPrefas.instance.NotesOption = GameManager.Instance.NotesOption;
				GameSettingsPrefas.instance.DisplayOption = GameManager.Instance.DisplayOption;
				GameSettingsPrefas.instance.VolumeOption = GameManager.Instance.VolumeOption;
				GameSettingsPrefas.instance.JudgeTimeOption = GameManager.Instance.JudgeTimeOption;
				GameSettingsPrefas.Save();
			}
			catch(Exception ex)
			{
				var builder = new DialogParametor.Builder("エラー", $"オプションの保存に失敗しました : {ex.Message}");
				builder.AddDefaultAction("選曲を続ける", () => { });
				DialogManager.Instance.Open(builder.Build());
			}

			SetActive(false);
		});

		_notesToggle.onValueChanged.AddListener(isOn =>
		{
			_notesSettingsPanel.SetActive(isOn);
		});

		_displayToggle.onValueChanged.AddListener(isOn =>
		{
			_displaySettingsPanel.SetActive(isOn);
		});
		
		_volumeToggle.onValueChanged.AddListener(isOn =>
		{
			_volumeSettingsPanel.SetActive(isOn);
		});

		_windowSizeToggle.onValueChanged.AddListener(isOn =>
		{
			_windowSizeSettingsPanel.SetActive(isOn);
		});

		_notesSettingsPanel.Init();
		_displaySettingsPanel.Init();
		_volumeSettingsPanel.Init();
		_windowSizeSettingsPanel.Init();

		_notesSettingsPanel.SetActive(true);
		_displaySettingsPanel.SetActive(false);
		_volumeSettingsPanel.SetActive(false);
		_windowSizeSettingsPanel.SetActive(false);

		gameObject.SetActive(false);
	}

	public void CallUpdate()
	{
		if(_notesSettingsPanel.isActiveAndEnabled)
		{
			_notesSettingsPanel.CallUpdate();
		}
	}

	public void SetActive(bool isActive)
	{
		if(_notesToggle.isOn)
		{
			_notesSettingsPanel.SetActivePreview(isActive);
		}
		
		gameObject.SetActive(isActive);
	}
}
