﻿using Cysharp.Text;
using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public interface IChangeColor
{
	void ChangeColor(Color color);
	Color GetColor();
	UniTask ChangePanelState(bool isShow);
}

public class NotesSettingsPanel : MonoBehaviour
{
	[Serializable]
	public class NotesPanel
	{
		[SerializeField] Button _downButton = default;
		[SerializeField] Button _upButton = default;
		[SerializeField] Slider _slider = default;
		[SerializeField] TextMeshProUGUI _valueText = default;

		public Action<float> OnChangeValue = null;

		int _currentValue = 0;
		float _rate = 1f;
		string _unit;

		public void Init(int value, float delta, string unit, string format = "")
		{
			_downButton.onClick.AddListener(() =>
			{
				_slider.value = Mathf.Clamp(_currentValue - 1, _slider.minValue, _slider.maxValue);
			});

			_upButton.onClick.AddListener(() =>
			{
				_slider.value = Mathf.Clamp(_currentValue + 1, _slider.minValue, _slider.maxValue);
			});

			_slider.onValueChanged.AddListener(value =>
			{
				_currentValue = (int)value;
				_valueText.SetText(ZString.Concat((_currentValue * _rate).ToString(format), _unit));
				OnChangeValue(value);
			});

			_currentValue = value;
			_slider.value = value / _rate;
			_rate = delta;
			_unit = unit;

			_valueText.SetText(ZString.Concat((_currentValue * _rate).ToString(format), _unit));
		}

		public void Reset(float value)
		{
			_slider.value = value;
		}
	}

	[Serializable]
	public class AutoTimingPanel
	{
		[SerializeField] Toggle _toggle = default;

		public Action<bool> OnChangeShowValue = null;

		public void Init(bool isShow)
		{
			SetEvent();

			_toggle.isOn = isShow;
		}

		void SetEvent()
		{
			_toggle.onValueChanged.AddListener(isOn =>
			{
				OnChangeShowValue(isOn);
			});
		}

		public void Reset()
		{
			_toggle.isOn = false;
		}
	}

	[Serializable]
	public class NoteScrollTypePanel
	{
		[SerializeField] Toggle _constantToggle = default;
		[SerializeField] Toggle _decelerateToggle = default;

		public Action<NotesScrollType> OnChangeValue = null;

		public void Init(NotesScrollType notesScrollType)
		{
			_constantToggle.isOn = notesScrollType == NotesScrollType.Constant;
			_decelerateToggle.isOn = notesScrollType == NotesScrollType.Decelerate;

			_constantToggle.onValueChanged.AddListener(isOn =>
			{
				OnChangeValue(isOn ? NotesScrollType.Constant : NotesScrollType.Decelerate);
			});
		}
	}

	[Serializable]
	public class NoteSkinsPanel
	{
		//--------------------------------------
		// インスペクタまたは外部から設定.
		//--------------------------------------
		[SerializeField] Button _downButton = default;
		[SerializeField] Button _upButton = default;
		[SerializeField] RawImage[] _noteImages = default;
		[SerializeField] TextMeshProUGUI _skinNameText = default;
		[SerializeField] NoteTexture[] _defaultNoteTexture;
		[SerializeField] LongTexture[] _defaultLongTexture;
		[SerializeField] NoteTexture _noteTexture;
		[SerializeField] LongTexture _longTexture;

		public Action<string> OnChangeValue = null;

		//------------------
		// キャッシュ.
		//------------------
		List<NoteSkinInfo> _noteSkinInfoList = new List<NoteSkinInfo>();
		int _currentIndex = 0;

		//------------------
		// 定数.
		//------------------
		const int NORMAL = 0;
		const int SLIDE = 1;
		const int FUZZY = 2;

		public void Init(string noteSkinsName)
		{
			// ◀ボタンを押したとき
			_downButton.onClick.AddListener(() =>
			{
				_currentIndex--;

				if (_currentIndex < 0)
				{
					_currentIndex = _noteSkinInfoList.Count - 1;
				}

				_skinNameText.text = _noteSkinInfoList[_currentIndex].skin_name;
				SetNoteRawImage();
				SetTextureScriptableObject();

				// セーブデータに反映
				OnChangeValue?.Invoke(_noteSkinInfoList[_currentIndex].skin_name);
			});

			// ▶ボタンを押したとき
			_upButton.onClick.AddListener(() =>
			{
				_currentIndex++;

				if (_currentIndex >= _noteSkinInfoList.Count)
				{
					_currentIndex = 0;
				}

				_skinNameText.text = _noteSkinInfoList[_currentIndex].skin_name;
				SetNoteRawImage();
				SetTextureScriptableObject();

				// セーブデータに反映
				OnChangeValue?.Invoke(_noteSkinInfoList[_currentIndex].skin_name);
			});

			CancellationTokenSource ctSource = new CancellationTokenSource();
			InitAsync(ctSource.Token, noteSkinsName).Forget();
		}

		async UniTask InitAsync(CancellationToken ct, string noteSkinsName)
		{
			// デフォルトノーツスキンをリストに追加 (インデックス番号0がデフォルトになる)
			_noteSkinInfoList.Add(new NoteSkinInfo()
			{
				skin_name = Constant.Note.DEFAULT_NOTESKINS_NAME,
				NotesTapTexture = _defaultNoteTexture[0].NoteTypeToTexture(NoteType.Normal),
				NotesSlideTexture = _defaultNoteTexture[0].NoteTypeToTexture(NoteType.LongStart),
				NotesFuzzyTexture = _defaultNoteTexture[0].NoteTypeToTexture(NoteType.FuzzyLongStart),
				RelaySlideTexture = _defaultNoteTexture[0].NoteTypeToTexture(NoteType.LongRelay),
				RelayFuzzyTexture = _defaultNoteTexture[0].NoteTypeToTexture(NoteType.FuzzyLongRelay),

				LineSlideTexture = _defaultLongTexture[0].LongTypeToTexture(LongType.Long),
				LineFuzzyTexture = _defaultLongTexture[0].LongTypeToTexture(LongType.FuzzyLong)
			});

			// 色覚対応ノーツスキンをリストに追加 (インデックス番号1)
			_noteSkinInfoList.Add(new NoteSkinInfo()
			{
				skin_name = Constant.Note.COLORB_NOTESKINS_NAME,
				NotesTapTexture = _defaultNoteTexture[1].NoteTypeToTexture(NoteType.Normal),
				NotesSlideTexture = _defaultNoteTexture[1].NoteTypeToTexture(NoteType.LongStart),
				NotesFuzzyTexture = _defaultNoteTexture[1].NoteTypeToTexture(NoteType.FuzzyLongStart),
				RelaySlideTexture = _defaultNoteTexture[1].NoteTypeToTexture(NoteType.LongRelay),
				RelayFuzzyTexture = _defaultNoteTexture[1].NoteTypeToTexture(NoteType.FuzzyLongRelay),

				LineSlideTexture = _defaultLongTexture[1].LongTypeToTexture(LongType.Long),
				LineFuzzyTexture = _defaultLongTexture[1].LongTypeToTexture(LongType.FuzzyLong)
			});

			// カスタムノーツスキンをリストに追加
			_noteSkinInfoList.AddRange(await ExternalDirectory.NoteSkinsCopyFromStreamingToPersistentAsync(ct));

			// セーブデータのノーツスキンの名前から初期値を設定
			_currentIndex = _noteSkinInfoList.FindIndex(t => t.skin_name == noteSkinsName);

			// ない時はデフォルトにする
			if (_currentIndex < 0)
			{
				_currentIndex = 0;
			}

			// 表示名更新
			_skinNameText.SetText(_noteSkinInfoList[_currentIndex].skin_name);

			// RawImageの画像更新
			SetNoteRawImage();

			// 実際に使うノーツスキン画像に反映
			SetTextureScriptableObject();
		}

		/// <summary>
		/// RawImageの画像更新
		/// </summary>
		void SetNoteRawImage()
		{
			_noteImages[NORMAL].texture = _noteSkinInfoList[_currentIndex].NotesTapTexture;
			_noteImages[SLIDE].texture = _noteSkinInfoList[_currentIndex].NotesSlideTexture;
			_noteImages[FUZZY].texture = _noteSkinInfoList[_currentIndex].NotesFuzzyTexture;
		}

		/// <summary>
		/// 実際に使うノーツスキン画像に反映
		/// </summary>
		void SetTextureScriptableObject()
		{
			_noteTexture.SetTextureFromNoteSkinInfo(_noteSkinInfoList[_currentIndex]);
			_longTexture.SetTextureFromNoteSkinInfo(_noteSkinInfoList[_currentIndex]);
		}

		public void Reset()
		{
			_currentIndex = 0;

			_skinNameText.text = _noteSkinInfoList[_currentIndex].skin_name;
			SetNoteRawImage();
			SetTextureScriptableObject();

			// セーブデータに反映
			OnChangeValue?.Invoke(_noteSkinInfoList[_currentIndex].skin_name);
		}
	}


	[Serializable]
	public class TouchSePanel
	{
		//--------------------------------------
		// インスペクタまたは外部から設定.
		//--------------------------------------
		[SerializeField] Button _downButton = default;
		[SerializeField] Button _upButton = default;
		[SerializeField] TextMeshProUGUI _touchSeNameText = default;

		public Action<string> OnChangeValue = null;

		//------------------
		// キャッシュ.
		//------------------
		List<TouchSeInfo> _touchSeInfoList = new List<TouchSeInfo>();
		int _currentIndex = 0;

		//------------------
		// プロパティ.
		//------------------
		public List<TouchSeInfo> TouchSeInfoList => _touchSeInfoList;

		public void Init(string touchSeName)
		{
			// ◀ボタンを押したとき
			_downButton.onClick.AddListener(() =>
			{
				_currentIndex--;

				if (_currentIndex < 0)
				{
					_currentIndex = _touchSeInfoList.Count - 1;
				}

				_touchSeNameText.text = _touchSeInfoList[_currentIndex].TouchSeName;

				// セーブデータに反映
				OnChangeValue?.Invoke(_touchSeInfoList[_currentIndex].TouchSeName);
			});

			// ▶ボタンを押したとき
			_upButton.onClick.AddListener(() =>
			{
				_currentIndex++;

				if (_currentIndex >= _touchSeInfoList.Count)
				{
					_currentIndex = 0;
				}

				_touchSeNameText.text = _touchSeInfoList[_currentIndex].TouchSeName;

				// セーブデータに反映
				OnChangeValue?.Invoke(_touchSeInfoList[_currentIndex].TouchSeName);
			});

			CancellationTokenSource ctSource = new CancellationTokenSource();
			InitTouchSE(touchSeName);
		}

		void InitTouchSE(string touchSeName)
		{
			if (!TouchSeCache.Instance.HasCache)
			{
				// デフォルトタッチSEをリストに追加 (インデックス番号0がデフォルトになる)
				_touchSeInfoList.Add(new TouchSeInfo()
				{
					TouchSeName = Constant.Note.DEFAULT_TOUCH_SE_NAME,
					AcbFile = Constant.Note.DEFAULT_TOUCH_SE_ACB
				});

				// カスタムタッチSEをリストに追加
				_touchSeInfoList.AddRange(ExternalDirectory.LoadTouchSeInfos());

				TouchSeCache.Instance.Cache(_touchSeInfoList);
			}

			_touchSeInfoList = TouchSeCache.Instance.Data;

			if(_touchSeInfoList.Count <= 1)
			{
				_upButton.gameObject.SetActive(false);
				_downButton.gameObject.SetActive(false);
			}

			// セーブデータのノーツスキンの名前から初期値を設定
			_currentIndex = _touchSeInfoList.FindIndex(t => t.TouchSeName == touchSeName);

			// ない時はデフォルトにする
			if (_currentIndex < 0)
			{
				_currentIndex = 0;
			}

			// 表示名更新
			_touchSeNameText.SetText(_touchSeInfoList[_currentIndex].TouchSeName);
		}

		public void Reset()
		{
			_currentIndex = 0;

			_touchSeNameText.text = _touchSeInfoList[_currentIndex].TouchSeName;

			// セーブデータに反映
			OnChangeValue?.Invoke(_touchSeInfoList[_currentIndex].TouchSeName);
		}
	}

	[Serializable]
	public sealed class CustomColorPanel : IChangeColor
	{
		[SerializeField] Toggle _defaultToggle = default;
		[SerializeField] Toggle _customToggle = default;
		[SerializeField] Image _colorImage = default;
		[SerializeField] Button _colorPanelButton;

		[SerializeField] GameObject _mainPanelObject;
		[SerializeField] Transform _colorPanelParent;

		public Action<bool> OnChangeToggle = null;
		public Action<Color> OnChangeColor = null;

		public Color CurrentCustomColor => _colorImage.color; 

		ColorPickerPanel _colorPickerPanel;

		public void Init(Color color, bool isCustom)
		{
			_colorImage.color = color;
			_colorPanelButton.gameObject.SetActive(isCustom);

			_defaultToggle.isOn = !isCustom;
			_customToggle.isOn = isCustom;

			_defaultToggle.onValueChanged.AddListener(isOn => 
			{
				if (isOn)
				{
					_colorPanelButton.gameObject.SetActive(false);
					OnChangeToggle(false);
				}

			});

			_customToggle.onValueChanged.AddListener(isOn => 
			{
                if (isOn)
                {
					_colorPanelButton.gameObject.SetActive(true);
					OnChangeToggle(true);
				}
			});

			_colorPanelButton.onClick.AddListener(() => 
			{
				ChangePanelState(true).Forget();
			});
		}

		public void ChangeColor(Color color)
		{
			_colorImage.color = color;
			OnChangeColor(color);
		}

		public Color GetColor()
		{
			return _colorImage.color;
		}

		public async UniTask ChangePanelState(bool isShowColorPicker)
		{
			_mainPanelObject.SetActive(!isShowColorPicker);

			if (_colorPickerPanel == null)
			{
				await CreateColorPickerPanel();
			}

			_colorPickerPanel.SetActive(isShowColorPicker);
		}

		async UniTask CreateColorPickerPanel()
		{
			var asset = await Resources.LoadAsync<ColorPickerPanel>("ColorPickerPanel") as ColorPickerPanel;
			_colorPickerPanel = Instantiate(asset, _colorPanelParent);
			_colorPickerPanel.Init(this);
			_colorPickerPanel.FlexibleColorPicker.SetColor(_colorImage.color);
		}

		public void Reset(Color color)
		{
			ChangeColor(color);
			_defaultToggle.isOn = true;
			_customToggle.isOn = false;
		}
	}

	//-------------------------------------
	// パネル本体
	//-------------------------------------

	// ノーツプレビュー
	[SerializeField] GameSettingPreview _gameSettingPreview;

	// ノーツ設定
	[SerializeField] NotesPanel _speedPanel;
	[SerializeField] NotesPanel _sizePanel;
	[SerializeField] NotesPanel _timingPanel;
	[SerializeField] NoteSkinsPanel _skinsPanel;
	[SerializeField] TouchSePanel _touchSePanel;
	[SerializeField] NotesPanel _sameTimeBarWidthPanel;

	[Header("ノーツ設定の初期化")]
	[SerializeField] Button _resetNoteSettingButton;

	// レーン設定
	[Header("レーン設定")]
	[SerializeField] NotesPanel _laneAlphaPanel;
	[SerializeField] NotesPanel _lanePosZPanel;
	[SerializeField] CustomColorPanel _laneColorPanel;
	[SerializeField] CustomColorPanel _beamColorPanel;
	[SerializeField] Button _resetLaneSettingButton;

	public void Init()
	{
		_gameSettingPreview.Init();

		var notesOption = GameManager.Instance.NotesOption;

		_speedPanel.OnChangeValue = value =>
		{
			float hiSpeed = value / 10f; ;
			notesOption.HiSpeed = hiSpeed;
			GameParam.Instance.NoteSpeed = hiSpeed;
		};

		_sizePanel.OnChangeValue = value =>
		{
			notesOption.Size = value / 100f;
			_gameSettingPreview.OnChangeNoteSize();
		};

		_timingPanel.OnChangeValue = value =>
		{
			notesOption.Timing = value / 1000f;
		};

		_skinsPanel.OnChangeValue = value =>
		{
			notesOption.NoteSkinsName = value;
		};

		_touchSePanel.OnChangeValue = value =>
		{
			notesOption.TouchSeName = value;
		};

		_sameTimeBarWidthPanel.OnChangeValue = value =>
		{
			notesOption.SameTimeWidthRate = value / 100f;
		};

		_resetNoteSettingButton.onClick.AddListener(() =>
		{
			_speedPanel.Reset(50f);
			_sizePanel.Reset(100f);
			_timingPanel.Reset(0f);
			_skinsPanel.Reset();
			_touchSePanel.Reset();
			_sameTimeBarWidthPanel.Reset(100f);
		});

		_laneAlphaPanel.OnChangeValue = value =>
		{
			notesOption.LaneAlpha = (int)value;
			_gameSettingPreview.OnChangeLaneAlpha();
		};

		_lanePosZPanel.OnChangeValue = value =>
		{
			notesOption.LaneCameraOffsetZ = (int)value;
			_gameSettingPreview.OnChangeLanePosZ();
		};

		_laneColorPanel.OnChangeToggle = value =>
		{
			notesOption.IsCustomSeparator = value;
			_gameSettingPreview.SetSeparatorColor(
				!value,
				value ? _laneColorPanel.CurrentCustomColor : Constant.Note.DefaultSeparateColor
			);
		};

		_laneColorPanel.OnChangeColor = value =>
		{
			notesOption.SeparatorColor = value;
			_gameSettingPreview.SetSeparatorColor(
				!notesOption.IsCustomSeparator,
				value
			);
		};

		_beamColorPanel.OnChangeToggle = value =>
		{
			notesOption.IsCustomBeam = value;
		};

		_beamColorPanel.OnChangeColor = value =>
		{
			notesOption.BeamColor = value;
		};

		_resetLaneSettingButton.onClick.AddListener(() =>
		{
			_laneAlphaPanel.Reset(50);
			_lanePosZPanel.Reset(0);
			_laneColorPanel.Reset(Constant.Note.DefaultSeparateColor);
			_beamColorPanel.Reset(Constant.Note.DefaultBeamColor);
		});

		// decimalに一度キャストしているのは
		// 変数xが6.1fの時に
		// x * 10 = 61
		// (int)(x * 10) = 60
		// になる問題があったため.
		// 直接リテラル6.1fを入れた時は発生しない.

		// ノーツ設定
		_speedPanel.Init((int)((decimal)notesOption.HiSpeed * 10), 0.1f, "", "F1");
		_sizePanel.Init((int)((decimal)notesOption.Size * 100), 1f, "%");
		_timingPanel.Init((int)((decimal)notesOption.Timing * 1000), 1f, "ms");
		_skinsPanel.Init(notesOption.NoteSkinsName);
		_touchSePanel.Init(notesOption.TouchSeName);
		_sameTimeBarWidthPanel.Init((int)((decimal)notesOption.SameTimeWidthRate * 100), 1f, "%");

		// レーン設定
		_laneAlphaPanel.Init(notesOption.LaneAlpha, 1f, "%");
		_lanePosZPanel.Init(notesOption.LaneCameraOffsetZ, 1f, "");
		_laneColorPanel.Init(notesOption.SeparatorColor, notesOption.IsCustomSeparator);
		_beamColorPanel.Init(notesOption.BeamColor, notesOption.IsCustomBeam);
	}

	public void SetActive(bool isActive)
	{
		_gameSettingPreview.SetActive(isActive);
		gameObject.SetActive(isActive);
	}

	public void SetActivePreview(bool isActive)
	{
		_gameSettingPreview.SetActive(isActive);
	}

	public void CallUpdate()
	{
		_gameSettingPreview.CallUpdate();
	}

}
