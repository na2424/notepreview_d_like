﻿using System;
using UnityEngine;

/// <summary>
/// ゲーム設定のノーツのプレビューを操作するクラス.
/// </summary>
public sealed class GameSettingPreview : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] NoteObjectPool _noteObjectPool;
	[SerializeField] BeatBarObjectPool _beatBarObjectPool;
	[SerializeField] NoteTexture _noteTexture;
	[SerializeField] Camera _previewCamera;
	[SerializeField] FilterColorController _filterColorController;
	[SerializeField] LaneController _laneController;
	[SerializeField] JudgePlateController _judgePlateController;

	//------------------
	// キャッシュ.
	//------------------
	double _previewTime = 7d;
	double _time = -1d;
	int _noteIndex = 0;
	int _bar = 0;
	BpmHelper _bpmHelper = new BpmHelper();
	Sequence _previewSequence = new Sequence();
	GameParam _gameParam = new GameParam();
	Vector3 _startCameraPos;

	//------------------
	// 定数.
	//------------------
	const float START_BEAT_POSITION = 3f;
	const float BASE_BPM = 150f;
	const int NOTE_COUNT = 15;
	const float BEAT_DISTANCE = Constant.Note.BEAT_DISTANCE;
	const float LANE_LENGTH = Constant.Note.LANE_LENGTH;
	const float OFFSET_Z = Constant.Note.OFFSET_Z;

	//------------------
	// プロパティ.
	//------------------
	float VisibleBeat => (LANE_LENGTH + OFFSET_Z + GameParam.Instance.OffsetZ) /
		(BEAT_DISTANCE * (Constant.Note.FIXED_BPM / BASE_BPM) * 1f * GameParam.Instance.NoteSpeed);
	//(LANE_LENGTH + OFFSET_Z + GameParam.Instance.OffsetZ) / (BEAT_DISTANCE * 1f * GameParam.Instance.NoteSpeed);

	public void Init()
	{
		_gameParam.Init();
		_gameParam.NoteSpeed = GameManager.Instance.NotesOption.HiSpeed;
		GameManager.Instance.IsCMod = false;
		_startCameraPos = _previewCamera.transform.localPosition;
		SetCameraPos();
		SetFilterColor();
		_laneController.Init();
		_judgePlateController.Init();

		for (int i = 0; i < NOTE_COUNT; i++)
		{
			_previewSequence.BeatPositions.Add(START_BEAT_POSITION + i);
			_previewSequence.Lanes.Add(3);
			_previewSequence.NoteTypes.Add(NoteType.Normal);
			_previewSequence.IsAttacks.Add(false);
		}

		int last = _previewSequence.BeatPositions.Count - 1;

		SongInfo songInfo = new()
		{
			Offset = 0,
			BaseBpm = BASE_BPM
		};

		songInfo.BpmPositions.Add(0f);
		songInfo.Bpms.Add(BASE_BPM);

		GameManager.Instance.SelectSongInfo = songInfo;

		_beatBarObjectPool.Init();
		_noteObjectPool.Init(false);
		_bpmHelper.Init(songInfo);

		_bar = (int)START_BEAT_POSITION;
	}

	public void CallUpdate()
	{
		if (_time > _previewTime)
		{
			_time = -1d;
			_bar = (int)START_BEAT_POSITION;
			_noteIndex = 0;
		}

		_time += Time.deltaTime;
		double beat = _bpmHelper.TimeToBeat(_time);

		if (NeedsCreateBeatBar(beat))
		{
			if (GameManager.Instance.DisplayOption.BeatBar)
			{
				CreateBeatBarFromObjectPool();
			}
			_bar++;
		}

		// 拍線位置更新
		for (int i = 0; i < _beatBarObjectPool.CurrentActiveBar.Count; i++)
		{
			_beatBarObjectPool.CurrentActiveBar[i].CallUpdate(beat, _time, 1f);
		}

		// Note生成
		if (NeedsCreateNote(beat))
		{
			CreateNoteFromObjectPool();
			_noteIndex++;
		}

		// Note位置更新
		for (int i = 0; i < _noteObjectPool.CurrentActiveNote.Count; i++)
		{
			_noteObjectPool.CurrentActiveNote[i].CallUpdate(beat, _time, 1f);
		}
	}

	/// <summary>
	/// 拍線生成する必要があるか
	/// </summary>
	/// <param name="beat">Beat位置</param>
	bool NeedsCreateBeatBar(double beat)
	{
		return
			_noteIndex < _previewSequence.BeatPositions.Count &&
			beat > _bar - VisibleBeat;
	}

	/// <summary>
	/// オブジェクトプールから拍線生成
	/// </summary>
	void CreateBeatBarFromObjectPool()
	{
		var controller = _beatBarObjectPool.Pool.Get();
		double justTime = _bpmHelper.BeatToTime(_bar);

		controller.SetParam(
			beatPosition: _bar,
			justTime: justTime
		);
	}

	/// <summary>
	/// Note生成する必要があるか
	/// </summary>
	/// <param name="beat">Beat位置</param>
	bool NeedsCreateNote(double beat)
	{
		return
			_noteIndex < _previewSequence.BeatPositions.Count &&
			beat > _previewSequence.BeatPositions[_noteIndex] - VisibleBeat;
	}

	/// <summary>
	/// オブジェクトプールからNote生成
	/// </summary>
	void CreateNoteFromObjectPool()
	{
		var noteController = _noteObjectPool.Pool.Get();
		double justTime = _bpmHelper.BeatToTime(_previewSequence.BeatPositions[_noteIndex]);

		noteController.SetParam(
			beatPosition: _previewSequence.BeatPositions[_noteIndex],
			justTime: justTime,
			lane: _previewSequence.Lanes[_noteIndex],
			noteTex: _noteTexture.NoteTypeToTexture(_previewSequence.NoteTypes[_noteIndex]),
			noteType: _previewSequence.NoteTypes[_noteIndex],
			isAttack: _previewSequence.IsAttacks[_noteIndex],
			noteIndex: _noteIndex
		);
	}

	/// <summary>
	/// 表示/非表示切り替え
	/// </summary>
	/// <param name="enabled"></param>
	public void SetActive(bool enabled)
	{
		gameObject.SetActive(enabled);
		_previewCamera.enabled = enabled;
	}

	/// <summary>
	/// ノーツサイズが変更されたときに呼ばれる
	/// </summary>
	public void OnChangeNoteSize()
	{
		_noteObjectPool.ChangeNoteSize();
	}

	public void OnChangeLaneAlpha()
	{
		SetFilterColor();
	}

	void SetFilterColor()
	{
		_filterColorController.SetAlpha(Mathf.Clamp01(GameManager.Instance.NotesOption.LaneAlpha * 0.01f));
	}

	public void SetSeparatorColor(bool isDefault, Color color)
	{
		_laneController.SetColor(color);

		if (isDefault)
		{
			_judgePlateController.SetDefaultColor();
		}
		else
		{
			_judgePlateController.SetColor(color);
		}
	}

	public void OnChangeLanePosZ()
	{
		SetCameraPos();
	}

	void SetCameraPos()
	{
		float cameraOffsetZ = GameManager.Instance.NotesOption.LaneCameraOffsetZ * 0.01f;
		_previewCamera.transform.localPosition = new Vector3(_startCameraPos.x, _startCameraPos.y, _startCameraPos.z + cameraOffsetZ);
	}
}
