﻿//using Cysharp.Text;
//using System;
//using TMPro;
//using UnityEngine;
//using UnityEngine.UI;

//public class JudgeTimeSettingsPanel : MonoBehaviour
//{
//	[Serializable]
//	public class JudgeTimePanel
//	{
//		[SerializeField] Button _downButton = default;
//		[SerializeField] Button _upButton = default;
//		[SerializeField] Slider _timeSilder = default;
//		[SerializeField] TextMeshProUGUI _timeText = default;

//		public Action<int> OnChangeVolume = null;

//		public void Init(float time)
//		{
//			SetEvent();

//			_timeSilder.value = (int)((decimal)time * 1000);
//			_timeText.SetText(ZString.Concat((int)((decimal)time * 1000), "ms"));
//		}

//		void SetEvent()
//		{
//			_downButton.onClick.AddListener(() =>
//			{
//				_timeSilder.value = Mathf.Clamp(_timeSilder.value - 1, _timeSilder.minValue, _timeSilder.maxValue);
//			});

//			_upButton.onClick.AddListener(() =>
//			{
//				_timeSilder.value = Mathf.Clamp(_timeSilder.value + 1, _timeSilder.minValue, _timeSilder.maxValue);
//			});

//			_timeSilder.onValueChanged.AddListener(value =>
//			{
//				_timeText.SetText(ZString.Concat(value, "ms"));
//				OnChangeVolume((int)value);
//			});
//		}

//		public void SetValue(float time)
//		{
//			_timeSilder.value = (int)((decimal)time * 1000);
//		}
//	}

//	[Serializable]
//	public class JudgeDistancePanel
//	{
//		[SerializeField] Button _downButton = default;
//		[SerializeField] Button _upButton = default;
//		[SerializeField] Slider _silder = default;
//		[SerializeField] TextMeshProUGUI _valueText = default;

//		public Action<int> OnChangeVolume = null;

//		public void Init(float revision)
//		{
//			SetEvent();

//			_silder.value = (int)((decimal)revision * 100);
//			_valueText.SetText(ZString.Concat(revision, ""));
//		}

//		void SetEvent()
//		{
//			_downButton.onClick.AddListener(() =>
//			{
//				_silder.value = Mathf.Clamp(_silder.value - 1, _silder.minValue, _silder.maxValue);
//			});

//			_upButton.onClick.AddListener(() =>
//			{
//				_silder.value = Mathf.Clamp(_silder.value + 1, _silder.minValue, _silder.maxValue);
//			});

//			_silder.onValueChanged.AddListener(value =>
//			{
//				_valueText.SetText(ZString.Concat(value/100f, ""));
//				OnChangeVolume((int)value);
//			});
//		}

//		public void SetValue(float revision)
//		{
//			_silder.value = (int)((decimal)revision * 100);
//		}
//	}

//	[Serializable]
//	public class JudgeOtherPanel
//	{
//		[SerializeField] Toggle _yesToggle = default;
//		[SerializeField] Toggle _noToggle = default;
//		[SerializeField] Button _resetButton = default;

//		public Action<bool> OnChangeValue = null;
//		public Action OnClickResetButton = null;


//		public void Init(bool canSave)
//		{
//			_yesToggle.isOn = canSave;
//			_noToggle.isOn = !canSave;

//			_yesToggle.onValueChanged.AddListener(isOn =>
//			{
//				OnChangeValue(isOn);
//			});

//			_resetButton.onClick.AddListener(() =>
//			{
//				_yesToggle.isOn = false;
//				_noToggle.isOn = true;
//				OnClickResetButton(); 
//			});
//		}
//	}

//	// 判定時間調整
//	[SerializeField] JudgeTimePanel _judgeBrilliantPanel;
//	[SerializeField] JudgeTimePanel _judgeGreatPanel;
//	[SerializeField] JudgeTimePanel _judgeFastPanel;
//	[SerializeField] JudgeTimePanel _judgeBadPanel;
//	[SerializeField] JudgeTimePanel _judgeLongRevisionTimePanel;
//	[SerializeField] JudgeDistancePanel _judgeLongRevisionDistancePanel;
//	[SerializeField] JudgeTimePanel _judgeFuzzyStartTimePanel;
//	[SerializeField] JudgeOtherPanel _judgeOtherPanel;
//	[SerializeField] GameObject _customJudgement;

//	// 楽曲再読み込み
//	[SerializeField] Button _reloadSongButton = default;

//	bool _isCustomJudgement = false;

//	public void Init()
//	{
//		var judgeTimeOption = GameManager.Instance.JudgeTimeOption;

//		_judgeBrilliantPanel.OnChangeVolume = value =>
//		{
//			judgeTimeOption.BriliantTime = value / 1000f;
//			CheckCustomJudgement(judgeTimeOption.IsCustom);
//		};

//		_judgeGreatPanel.OnChangeVolume = value =>
//		{
//			judgeTimeOption.GreatTime = value / 1000f;
//			CheckCustomJudgement(judgeTimeOption.IsCustom);
//		};

//		_judgeFastPanel.OnChangeVolume = value =>
//		{
//			judgeTimeOption.FastTime = value / 1000f;
//			CheckCustomJudgement(judgeTimeOption.IsCustom);
//		};

//		_judgeBadPanel.OnChangeVolume = value =>
//		{
//			judgeTimeOption.BadTime = value / 1000f;
//			CheckCustomJudgement(judgeTimeOption.IsCustom);
//		};

//		_judgeLongRevisionTimePanel.OnChangeVolume = value =>
//		{
//			judgeTimeOption.LongRevisionTime = value / 1000f;
//			CheckCustomJudgement(judgeTimeOption.IsCustom);
//		};

//		_judgeLongRevisionDistancePanel.OnChangeVolume = value =>
//		{
//			judgeTimeOption.LongRevisionDistance = value / 100f;
//			CheckCustomJudgement(judgeTimeOption.IsCustom);
//		};

//		_judgeFuzzyStartTimePanel.OnChangeVolume = value =>
//		{
//			judgeTimeOption.FuzzyStartTime = value / 1000f;
//			CheckCustomJudgement(judgeTimeOption.IsCustom);
//		};

//		_judgeOtherPanel.OnChangeValue = value =>
//		{
//			judgeTimeOption.CanSave = value;
//		};

//		_judgeOtherPanel.OnClickResetButton = () =>
//		{
//			_judgeBrilliantPanel.SetValue(Constant.JudgeTime.BRILLIANT_TIME);
//			_judgeGreatPanel.SetValue(Constant.JudgeTime.GREAT_TIME);
//			_judgeFastPanel.SetValue(Constant.JudgeTime.FAST_TIME);
//			_judgeBadPanel.SetValue(Constant.JudgeTime.BAD_TIME);
//			_judgeLongRevisionTimePanel.SetValue(Constant.JudgeTime.LONG_REVISION_TIME);
//			_judgeLongRevisionDistancePanel.SetValue(Constant.JudgeTime.LONG_REVISION_DISTANCE);
//			_judgeFuzzyStartTimePanel.SetValue(Constant.JudgeTime.FUZZY_START_TIME);
//		};

//		_reloadSongButton.onClick.AddListener(() =>
//		{
//			GameSettingsPrefas.instance.NotesOption = GameManager.Instance.NotesOption;
//			GameSettingsPrefas.instance.DisplayOption = GameManager.Instance.DisplayOption;
//			GameSettingsPrefas.instance.VolumeOption = GameManager.Instance.VolumeOption;
//			GameSettingsPrefas.instance.JudgeTimeOption = GameManager.Instance.JudgeTimeOption;
//			GameSettingsPrefas.Save();

//			SongInfoCache.Instance.Clear();
//			GameManager.Instance.SelectDataIndex = 0;
//			GameManager.Instance.ChangeScene(SceneName.SelectMusic);
//		});

//		_judgeBrilliantPanel.Init(judgeTimeOption.BriliantTime);
//		_judgeGreatPanel.Init(judgeTimeOption.GreatTime);
//		_judgeFastPanel.Init(judgeTimeOption.FastTime);
//		_judgeBadPanel.Init(judgeTimeOption.BadTime);
//		_judgeLongRevisionTimePanel.Init(judgeTimeOption.LongRevisionTime);
//		_judgeLongRevisionDistancePanel.Init(judgeTimeOption.LongRevisionDistance);
//		_judgeFuzzyStartTimePanel.Init(judgeTimeOption.FuzzyStartTime);

//		_judgeOtherPanel.Init(judgeTimeOption.CanSave);

//		CheckCustomJudgement(judgeTimeOption.IsCustom);
//	}

//	void CheckCustomJudgement(bool isDefault)
//	{
//		if (isDefault != _isCustomJudgement)
//		{
//			_isCustomJudgement = isDefault;
//			_customJudgement.SetActive(_isCustomJudgement);
//		}
//	}

//	public void SetActive(bool isActive)
//	{
//		gameObject.SetActive(isActive);
//	}

//}
