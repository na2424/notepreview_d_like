﻿using Cysharp.Text;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 減算スコアタイプ
/// </summary>
public enum SubtractedScoreType : int
{
	None = 0,
	Type1,
	Type2
}

/// <summary>
/// ノーツエフェクトタイプ
/// </summary>
public enum NotesEffectType : int
{
	None = 0,
	Type1 = 1,
	Type2 = 2,
	Type3 = 3,
}

public class DisplaySettingsPanel : MonoBehaviour
{
	/// <summary>
	/// 背景の暗さ(ディマー)を設定するクラス
	/// </summary>
	[Serializable]
	public class DimmerPanel
	{
		[SerializeField] Button _downButton = default;
		[SerializeField] Button _upButton = default;
		[SerializeField] Slider _slider = default;
		[SerializeField] TextMeshProUGUI _valueText = default;

		public Action<float> OnChangeValue = null;

		// 現在の値
		float _currentValue = 0f;
		// スライダーの1移動分
		float _deltaValue = 1f;
		// 値の単位文字
		string _unit;

		public void Init(float value, float delta, string unit)
		{
			SetEvent();
			_currentValue = value;
			_slider.value = (int)(value);
			_deltaValue = delta;
			_unit = unit;
		}

		void SetEvent()
		{
			_downButton.onClick.AddListener(() =>
			{
				_slider.value = Mathf.Clamp(_currentValue - _deltaValue, _slider.minValue, _slider.maxValue);
			});

			_upButton.onClick.AddListener(() =>
			{
				_slider.value = Mathf.Clamp(_currentValue + _deltaValue, _slider.minValue, _slider.maxValue);
			});

			_slider.onValueChanged.AddListener(value =>
			{
				_currentValue = value;
				_valueText.SetText(ZString.Concat(value, _unit));
				OnChangeValue(value);
			});
		}

		public void Reset()
		{
			_slider.value = 50f;
		}
	}

	/// <summary>
	/// ノーツエフェクトを設定するパネル
	/// </summary>
	[Serializable]
	public class NotesEffect
	{
		[SerializeField] Toggle[] _noteEffectToggles = default;

		public Action<NotesEffectType> OnChangeTypeValue = null;

		public void Init(NotesEffectType notesEffectType)
		{
			for (int i = 0; i < _noteEffectToggles.Length; i++)
			{
				_noteEffectToggles[i].isOn = notesEffectType == (NotesEffectType)i;
				SetToggleEvent(_noteEffectToggles[i], i);
			}
		}

		void SetToggleEvent(Toggle toggle, int index)
		{
			toggle.onValueChanged.AddListener(isOn =>
			{
				if (isOn)
				{
					OnChangeTypeValue((NotesEffectType)index);
				}
			});
		}

		public void Reset()
		{
			_noteEffectToggles[(int)NotesEffectType.None].isOn = false;
			_noteEffectToggles[(int)NotesEffectType.Type1].isOn = true;
			_noteEffectToggles[(int)NotesEffectType.Type2].isOn = false;
			_noteEffectToggles[(int)NotesEffectType.Type3].isOn = false;
		}
	}

	/// <summary>
	/// 拍線を設定するパネル
	/// </summary>
	[Serializable]
	public class BeatBar
	{
		[SerializeField] Toggle _onToggle = default;
		[SerializeField] Toggle _offToggle = default;

		public Action<bool> OnChangeValue = null;

		public void Init(bool value)
		{
			_onToggle.isOn = value;
			_offToggle.isOn = !value;

			_onToggle.onValueChanged.AddListener(isOn =>
			{
				OnChangeValue(isOn);
			});
		}

		public void Reset()
		{
			_onToggle.isOn = true;
			_offToggle.isOn = false;
		}
	}

	/// <summary>
	/// ロング中継の手前表示を設定するパネル
	/// </summary>
	[Serializable]
	public class RelayFrontDisplay
	{
		[SerializeField] Toggle _onToggle = default;
		[SerializeField] Toggle _offToggle = default;

		public Action<bool> OnChangeValue = null;

		public void Init(bool value)
		{
			_onToggle.isOn = value;
			_offToggle.isOn = !value;

			_onToggle.onValueChanged.AddListener(isOn =>
			{
				OnChangeValue(isOn);
			});
		}

		public void Reset()
		{
			_onToggle.isOn = true;
			_offToggle.isOn = false;
		}
	}

	/// <summary>
	/// 垂直同期を設定するパネル
	/// </summary>
	[Serializable]
	public class VSyncPanel
	{
		[SerializeField] Toggle _offToggle = default;
		[SerializeField] Toggle _onToggle = default;

		public Action<bool> OnChangeValue = null;

		public void Init(bool value)
		{
			_offToggle.isOn = !value;
			_onToggle.isOn = value;

			_onToggle.onValueChanged.AddListener(isOn =>
			{
				OnChangeValue(isOn);
			});
		}

		public void Reset()
		{
			_offToggle.isOn = false;
			_onToggle.isOn = true;
		}
	}

	/// <summary>
	/// FPSを設定するパネル
	/// </summary>
	[Serializable]
	public class FpsPanel
	{
		[SerializeField] TextMeshProUGUI _showText = default;
		[SerializeField] Toggle _showToggle = default;
		[SerializeField] Toggle _60FpsToggle = default;
		[SerializeField] Toggle _120FpsToggle = default;
		[SerializeField] Toggle _noFpsToggle = default;
		[SerializeField] GameObject _toggleParent;

		public Action<bool> OnChangeShowValue = null;
		public Action<int> OnChangeValue = null;

		public void Init(bool isShow, int fps)
		{
			_60FpsToggle.isOn = (fps == 60);
			_120FpsToggle.isOn = (fps == 120);
			_noFpsToggle.isOn = (fps == -1);

			SetEvent();
			_showToggle.isOn = isShow;
		}

		public void SetActiveFpsToggles(bool isActive)
		{
			_toggleParent.SetActive(isActive);
		}

		void SetEvent()
		{
			_showToggle.onValueChanged.AddListener(isOn =>
			{
				_showText.text = isOn ? "表示" : "非表示";
				OnChangeShowValue(isOn);
			});

			_60FpsToggle.onValueChanged.AddListener(isOn =>
			{
				if(!isOn)
				{
					return;
				}

				OnChangeValue(60);
			});

			_120FpsToggle.onValueChanged.AddListener(isOn =>
			{
				if (!isOn)
				{
					return;
				}

				OnChangeValue(120);
			});

			_noFpsToggle.onValueChanged.AddListener(isOn =>
			{
				if (!isOn)
				{
					return;
				}

				OnChangeValue(-1);
			});
		}

		public void Reset()
		{
			_60FpsToggle.isOn = false;
			_120FpsToggle.isOn = true;
			_noFpsToggle.isOn = false;
		}
	}

	[SerializeField] DimmerPanel _dimmerPanel;
	[SerializeField] BeatBar _beatBarPanel;
	[SerializeField] NotesEffect _notesEffectPanel;
	[SerializeField] RelayFrontDisplay _relayFrontDisplay;
	[SerializeField] VSyncPanel _vSyncPanel;
	[SerializeField] FpsPanel _fpsPanel;
	[Header("プレイ表示設定の初期化")]
	[SerializeField] Button _resetDisplaySettingButton;

	public void Init()
	{
		var displayOption = GameManager.Instance.DisplayOption;

		_dimmerPanel.OnChangeValue = value =>
		{
			displayOption.Dimmer = value;
		};

		_notesEffectPanel.OnChangeTypeValue = value =>
		{
			displayOption.NotesEffectType = value;
		};

		_beatBarPanel.OnChangeValue = value =>
		{
			displayOption.BeatBar = value;
		};

		_relayFrontDisplay.OnChangeValue = value =>
		{
			displayOption.RelayFrontDisplay = value;
		};

		_vSyncPanel.OnChangeValue = value =>
		{
			displayOption.VSync = value;
			QualitySettings.vSyncCount = value ? 1 : 0;
			Application.targetFrameRate = value ? -1 : displayOption.TargetFrameRate;
			_fpsPanel.SetActiveFpsToggles(!value);
		};

		_fpsPanel.OnChangeShowValue = value =>
		{
			displayOption.IsShowFps = value;
			FPSCounter.Instance.SetActive(displayOption.IsShowFps);
		};

		_fpsPanel.OnChangeValue = value =>
		{
			displayOption.TargetFrameRate = value;
			Application.targetFrameRate = value;
		};

		_resetDisplaySettingButton.onClick.AddListener(() =>
		{
			_dimmerPanel.Reset();
			_notesEffectPanel.Reset();
			_beatBarPanel.Reset();
			_relayFrontDisplay.Reset();
			_vSyncPanel.Reset();
			_fpsPanel.Reset();
		});

		_dimmerPanel.Init(displayOption.Dimmer, 1f, "");
		_beatBarPanel.Init(displayOption.BeatBar);
		_notesEffectPanel.Init(displayOption.NotesEffectType);
		_relayFrontDisplay.Init(displayOption.RelayFrontDisplay);
		_vSyncPanel.Init(displayOption.VSync);
		_fpsPanel.Init(displayOption.IsShowFps, displayOption.TargetFrameRate);

		_fpsPanel.SetActiveFpsToggles(!displayOption.VSync);
	}

	public void SetActive(bool isActive)
	{
		gameObject.SetActive(isActive);
	}

}
