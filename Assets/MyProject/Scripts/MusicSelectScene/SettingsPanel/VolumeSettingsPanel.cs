﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Cysharp.Text;

[Serializable]
public class VolumePanel
{
	[SerializeField] Button _muteButton = default;
	[SerializeField] Image _muteImage = default;
	[SerializeField] Slider _volumeSilder = default;
	[SerializeField] TextMeshProUGUI _volumeText = default;

	public Action<bool> OnChangeMute = null;
	public Action<int> OnChangeVolume = null;

	bool _isMute = false;
	RefSpriteScriptableObject _muteSprite;

	const int MUTE_ON = 0;
	const int MUTE_OFF = 1;

	public void Init(RefSpriteScriptableObject muteSprite, bool isMute, float volume)
	{
		SetEvent();
		_muteSprite = muteSprite;
		_isMute = isMute;
		ChangeMuteSprite(_isMute);

		_volumeSilder.value = (int)(volume * 100);
		_volumeText.SetText((int)(volume * 100));
	}

	void SetEvent()
	{
		_muteButton.onClick.AddListener(() =>
		{
			_isMute = !_isMute;
			ChangeMuteSprite(_isMute);
			OnChangeMute(_isMute);
		});

		_volumeSilder.onValueChanged.AddListener(value =>
		{
			_volumeText.SetText(value);
			OnChangeVolume((int)value);
		});
	}

	void ChangeMuteSprite(bool isMute)
	{
		_muteImage.sprite = isMute ? _muteSprite.GetSprite(MUTE_ON) : _muteSprite.GetSprite(MUTE_OFF);
	}

	public void Reset(float value)
	{
		// ミュート
		_isMute = false;
		ChangeMuteSprite(_isMute);
		OnChangeMute(_isMute);

		// 音量
		_volumeSilder.value = (int)(value * 100);
	}
}

public class VolumeSettingsPanel : MonoBehaviour
{
	[SerializeField] VolumePanel _musicPanel;
	[SerializeField] VolumePanel _touchSEPanel;
	[SerializeField] VolumePanel _systemSEPanel;
	[Header("音量設定の初期化")]
	[SerializeField] Button _resetTouchSettingButton;
	[SerializeField] RefSpriteScriptableObject _muteSprite;

	public void Init()
	{
		var gameOption = GameManager.Instance.VolumeOption;

		_musicPanel.OnChangeMute = isMute =>
		{
			gameOption.MusicMute = isMute;
			PreviewMusic.Instance.SetMute(isMute);
		};

		_musicPanel.OnChangeVolume = value =>
		{
			gameOption.MusicVolume = value / 100f;
			PreviewMusic.Instance.SetVolume(value / 100f);
		};

		_touchSEPanel.OnChangeMute = isMute =>
		{
			gameOption.TouchSEMute = isMute;
		};

		_touchSEPanel.OnChangeVolume = value =>
		{
			gameOption.TouchSEVolume = value / 100f;
		};

		_systemSEPanel.OnChangeMute = isMute =>
		{
			gameOption.SystemSEMute = isMute;
			SystemSEManager.Instance.SetMute(isMute);
		};

		_systemSEPanel.OnChangeVolume = value =>
		{
			gameOption.SystemSEVolume = value / 100f;
			SystemSEManager.Instance.SetVolume(value / 100f);
		};

		_resetTouchSettingButton.onClick.AddListener(() =>
		{
			_musicPanel.Reset(Constant.PlayerOption.SoundVolume.DEFAULT_MUSIC_VOLUME);
			_touchSEPanel.Reset(Constant.PlayerOption.SoundVolume.DEFAULT_TOUCH_SE_VOLUME);
			_systemSEPanel.Reset(Constant.PlayerOption.SoundVolume.DEFAULT_SYSTEM_SE_VOLUME);
		});

		_musicPanel.Init(_muteSprite, gameOption.MusicMute, gameOption.MusicVolume);
		_touchSEPanel.Init(_muteSprite, gameOption.TouchSEMute, gameOption.TouchSEVolume);
		_systemSEPanel.Init(_muteSprite, gameOption.SystemSEMute, gameOption.SystemSEVolume);
	}

	public void SetActive(bool isActive)
	{
		gameObject.SetActive(isActive);
	}

}
