﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public sealed class PlayModePanelController : MonoBehaviour
{
	[SerializeField] Button _okButton;
	[SerializeField] Toggle _playmodeAutoToggle;
	[SerializeField] Toggle _playmodeRehearsalToggle;
	[SerializeField] Toggle _playmodeInputCheckToggle;

	[SerializeField] TextMeshProUGUI _playmodeText;

	const string AUTO = "最初から";
	const string REHEARSAL = "途中から";
	const string INPUT_CHECK = "入力チェック";

	public void Init()
	{
		gameObject.SetActive(false);
		InitText();

		_okButton.onClick.AddListener(() =>
		{
			gameObject.SetActive(false);
		});

		_playmodeAutoToggle.onValueChanged.AddListener(isOn =>
		{
			if (isOn)
			{
				GameManager.Instance.PlayModeOption.PlayMode = PlayMode.Auto;
				_playmodeText.text = AUTO;
			}
		});

		_playmodeRehearsalToggle.onValueChanged.AddListener(isOn =>
		{
			if (isOn)
			{
				GameManager.Instance.PlayModeOption.PlayMode = PlayMode.Rehearsal;
				_playmodeText.text = REHEARSAL;
			}
		});

		_playmodeInputCheckToggle.onValueChanged.AddListener(isOn =>
		{
			if (isOn)
			{
				GameManager.Instance.PlayModeOption.PlayMode = PlayMode.InputCheck;
				_playmodeText.text = INPUT_CHECK;
			}
		});

		SetIsOn();
	}

	public void Open()
	{
		gameObject.SetActive(true);
		SetIsOn();

		//InitText();
	}

	void SetIsOn()
	{
		var playmode = GameManager.Instance.PlayModeOption.PlayMode;

		switch (playmode)
		{
			case PlayMode.Auto:
				_playmodeAutoToggle.isOn = true;
				break;
			case PlayMode.Rehearsal:
				_playmodeRehearsalToggle.isOn = true;
				break;
			case PlayMode.InputCheck:
				_playmodeInputCheckToggle.isOn = true;
				break;
		}
	}

	void InitText()
	{
		var mode = GameManager.Instance.PlayModeOption.PlayMode;

		switch (mode)
		{
			case PlayMode.Auto:
				_playmodeText.text = AUTO;
				break;
			case PlayMode.Rehearsal:
				_playmodeText.text = REHEARSAL;
				break;
			case PlayMode.InputCheck:
				_playmodeText.text = INPUT_CHECK;
				break;
		}
	}
}
