﻿using Dialog;
using System;
using System.IO;
using UnityEngine;
using XLua;
using DankagLikeLuaAPI;
using System.Collections.Generic;

[System.Serializable]
public sealed class RootHierarchyTransformInjection
{
	public string name;
	public Transform value;
}

[System.Serializable]
public sealed class Injection
{
	public string name;
	public GameObject value;
}

[System.Serializable]
public sealed class MeterialInjection
{
	public string name;
	public Material value;
}

[CSharpCallLua]
public delegate void ActionInt4(int id, int lane, int noteType, int judge, bool isAttack);

[CSharpCallLua]
public delegate void ActionInt3(int id, int lane, int noteType);

[CSharpCallLua]
public delegate void ActionInput(int touchId, float posX, float screenPosX, float screenPosY);

[CSharpCallLua]
public delegate void ActionNoteController(NoteController noteController);

[CSharpCallLua]
public delegate void ActionLongController(LongController longController);

/// <summary>
/// Luaの実行を管理するクラス
/// </summary>
public sealed class LuaManager : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------

	[Header("[Injection] -------------")]
	[SerializeField] Injection[] injections;
	[SerializeField] MeterialInjection[] _meterialInjections;
	[SerializeField] RootHierarchyTransformInjection[] _rootHierarchyTransformInjections;
	[SerializeField] DummyNote _dummyNoteInjection;
	[SerializeField] LaneSprite _laneSpriteInjection;

	[Header("[LuaAPI] -------------")]
	[SerializeField] ScreenMan _screenMan = new ScreenMan();
	[SerializeField] GameState _gameState = new GameState();
	[SerializeField] CameraMan _cameraMan = new CameraMan();
	[SerializeField] PlayerStats _playerStats = new PlayerStats();
	[SerializeField] SongMan _songMan = new SongMan();
	[SerializeField] AssetMan _assetMan = new AssetMan();
	[SerializeField] AppMan _appMan = new AppMan();
	[SerializeField] Util _util = new Util();
	[SerializeField] ActorFactory _actorFactory = new ActorFactory();
	[SerializeField] DanmakuStageMan _danmakuStageMan = new DanmakuStageMan();

	//------------------
	// キャッシュ.
	//------------------

	// LuaEnvのインスタンスを生成
	// これはグローバルなものを一つだけ生成することが推奨
	internal static LuaEnv g_luaEnv = new LuaEnv();
	internal static float g_lastGCTime = 0;

	Action _onError;
	Action _luaStart;
	Action _luaUpdate;
	Action _luaFinish;
	Action _luaOnDestroy;
	ActionInt4 _luaOnHitNote;
	ActionInt3 _luaOnMissedNote;
	ActionNoteController _luaOnSpawnNote;
	ActionLongController _luaOnSpawnLong;
	Action _luaOnPause;
	Action _luaOnResume;
	ActionInput _luaOnInputDown;
	ActionInput _luaOnInputMove;
	ActionInput _luaOnInputUp;

	LuaTable _scriptEnv;

	static string g_folderPath;

	List<Actor> _actorList = new List<Actor>();
	List<ActorVideoCanvas> _actorVideoCanvasList = new List<ActorVideoCanvas>();

	//------------------
	// 定数.
	//------------------

	// 1秒
	internal const float GC_INTERVAL = 1;

	//------------------
	// プロパティ.
	//------------------
	public bool IsLoaded { get; private set; } = false;
	public ScreenMan ScreenMan => _screenMan;
	public GameState GameState => _gameState;
	public PlayerStats PlayerStats => _playerStats;
	public CameraMan CameraMan => _cameraMan;
	public SongMan SongMan => _songMan;
	public AssetMan AssetMan => _assetMan;
	public AppMan AppMan => _appMan;
	public Util Util => _util;
	public ActorFactory ActorFactory => _actorFactory;
	public DanmakuStageMan DanmakuStageMan => _danmakuStageMan;

	public List<Actor> ActorList => _actorList;
	public List<ActorVideoCanvas> ActorVideoCanvasList => _actorVideoCanvasList;
	public static string FolderPath => g_folderPath;

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name="luaFileName">Luaファイル名</param>
	[BlackList, DoNotGen]
	public void Init(string folderPath, string luaFileName, Action pause, Action onComplete)
	{
		// Luaが無い時はメモリ解放して以降何もしない
		if (luaFileName.IsNullOrEmpty())
		{
			_screenMan.HideLuaOverlayCanvas();
			_screenMan = null;
			_gameState = null;
			_cameraMan = null;
			_assetMan = null;
			_appMan = null;
			_util = null;
			_playerStats = null;
			_actorFactory = null;
			_danmakuStageMan = null;
			_actorList = null;
			_actorVideoCanvasList = null;
			injections = null;
			_meterialInjections = null;
			_rootHierarchyTransformInjections = null;

			return;
		}

		// エラー時にポーズを呼ぶため
		_onError = pause;

		// カスタムローダを追加
		g_luaEnv.AddLoader(CustomLoader);

		// メタテーブルの設定
		_scriptEnv = CreateScriptEnv();

		_scriptEnv.Set("self", this);
		//SetInjection(ref _scriptEnv);
		SetGlobalInjection();

		_actorFactory.Init(this, pause);

		try
		{
			// Luaファイルの読み込み
			g_folderPath = folderPath;
			var path = Path.Combine(g_folderPath, luaFileName).Replace("\\", "/");

			//var luaString = File.ReadAllBytes(path);
			var luaString = File.ReadAllText(path, System.Text.Encoding.UTF8);
			g_luaEnv.DoString(luaString, "Lua", _scriptEnv);
		}
		catch (Exception e)
		{
			ShowErrorDialog(e.Message);
			return;
		}

		Action luaOnLoaded = _scriptEnv.Get<Action>("onloaded");
		_scriptEnv.Get("start", out _luaStart);
		_scriptEnv.Get("update", out _luaUpdate);
		_scriptEnv.Get("finish", out _luaFinish);
		_scriptEnv.Get("ondestroy", out _luaOnDestroy);
		_scriptEnv.Get("onHitNote", out _luaOnHitNote);
		_scriptEnv.Get("onMissedNote", out _luaOnMissedNote);
		_scriptEnv.Get("onSpawnNote", out _luaOnSpawnNote);
		_scriptEnv.Get("onSpawnLong", out _luaOnSpawnLong);
		_scriptEnv.Get("onPause", out _luaOnPause);
		_scriptEnv.Get("onResume", out _luaOnResume);
		_scriptEnv.Get("onInputDown", out _luaOnInputDown);
		_scriptEnv.Get("onInputMove", out _luaOnInputMove);
		_scriptEnv.Get("onInputUp", out _luaOnInputUp);

		IsLoaded = true;

		onComplete();

		if (luaOnLoaded is not null)
		{
			try
			{
				luaOnLoaded();
			}
			catch (Exception e)
			{
				ShowErrorDialog(e.Message);
				return;
			}
		}
	}

	private byte[] CustomLoader(ref string filepath)
	{
		string path = Path.Combine(g_folderPath, filepath).Replace("\\", "/");

		if (File.Exists(path))
		{
			return File.ReadAllBytes(path);

			// 以下でもOK（文字列を取得してUTF8でバイト列に変換）
			//var text = File.ReadAllText(filepath);
			//return Encoding.UTF8.GetBytes(text);
		}

		// nullを返すとこのローダでは読み込めなかったとみなされる
		return null;
	}

	/// <summary>
	/// メタテーブルの設定.
	/// 参考：https://inzkyk.xyz/lua_5_4/basic_concepts/metatables_and_metamethods/#gsc.tab=0
	/// </summary>
	public LuaTable CreateScriptEnv()
	{
		var luaenv = LuaManager.g_luaEnv;
		var scriptEnv = luaenv.NewTable();
		LuaTable meta = luaenv.NewTable();
		meta.Set("__index", luaenv.Global);
		scriptEnv.SetMetaTable(meta);
		meta.Dispose();

		return scriptEnv;
	}

	public void SetGlobalInjection()
	{
		foreach (var injection in injections)
		{
			g_luaEnv.Global.Set(injection.name, injection.value);
		}

		foreach (var injection in _meterialInjections)
		{
			g_luaEnv.Global.Set(injection.name, injection.value);
		}

		foreach (var injection in _rootHierarchyTransformInjections)
		{
			g_luaEnv.Global.Set(injection.name, injection.value);
		}

		g_luaEnv.Global.Set("DummyNote", _dummyNoteInjection);
		g_luaEnv.Global.Set("DummyNotePrefab", _dummyNoteInjection);
		g_luaEnv.Global.Set("LaneSprite", _laneSpriteInjection);
		g_luaEnv.Global.Set("LaneSpritePrefab", _laneSpriteInjection);

		// 下記に[LuaCallCSharp]のクラスを追加していく...
		g_luaEnv.Global.Set("SCREENMAN", _screenMan);
		g_luaEnv.Global.Set("GAMESTATE", _gameState);
		g_luaEnv.Global.Set("PLAYERSTATS", _playerStats);
		g_luaEnv.Global.Set("CAMERAMAN", _cameraMan);
		g_luaEnv.Global.Set("SONGMAN", _songMan);
		g_luaEnv.Global.Set("ASSETMAN", _assetMan);
		g_luaEnv.Global.Set("APPMAN", _appMan);
		g_luaEnv.Global.Set("UTIL", _util);
		g_luaEnv.Global.Set("ACTORFACTORY", _actorFactory);
		g_luaEnv.Global.Set("DANMAKUSTAGE", _danmakuStageMan);
		g_luaEnv.Global.Set("DSTAGE", _danmakuStageMan);
	}

	/// <summary>
	/// エラー時にダイアログを表示する
	/// </summary>
	/// <param name="message"></param>
	void ShowErrorDialog(string message)
	{
		Action onNext = () =>
		{
			if (!BgManager.Instance.IsDefaultImage())
			{
				BgManager.Instance.ClearBgChangeImageCache();
				BgManager.Instance.SetDefaultImage();
			}
			GameManager.Instance.ChangeScene(SceneName.SelectMusic);
		};

		var builder = new DialogParametor.Builder("Luaの実行に失敗しました", message);
		builder.AddDefaultAction("選曲画面に戻る", onNext);
		builder.AddCallbackOnAutoClosed(onNext);
		DialogManager.Instance.Open(builder.Build());

		Debug.LogError("Luaの実行に失敗しました:" + message);
		//AnalyticsManager.SendError("LuaManager");
	}

	/// <summary>
	/// Luaスクリプトの実行開始
	/// </summary>
	public void StartLua()
	{
		if (!IsLoaded)
		{
			return;
		}

		if (_luaStart is not null)
		{
			try
			{
				_luaStart();
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}

		int loop = _actorList.Count - 1;

		if (loop >= 0)
		{
			for (int i = loop; i >= 0; i--)
			{
				if (_actorList[i] is not null)
				{
					_actorList[i].LuaStart();
				}
			}
		}
	}

	public void CallUpdate(double musicTime)
	{
		if (!IsLoaded)
		{
			return;
		}

		if (_luaUpdate is not null)
		{
			try
			{
				_luaUpdate();
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}

		int loop = _actorList.Count - 1;

		if (loop >= 0)
		{
			for (int i = loop; i >= 0; i--)
			{
				if (_actorList[i] is not null && _actorList[i].HasLuaUpdate)
				{
					_actorList[i].LuaUpdate();
				}
			}
		}

		loop = _actorVideoCanvasList.Count - 1;

		if (loop >= 0)
		{
			for (int i = loop; i >= 0; i--)
			{
				if (_actorVideoCanvasList[i] is not null)
				{
					_actorVideoCanvasList[i].CallUpdate();
				}
			}
		}

		if (_danmakuStageMan is not null)
		{
			_danmakuStageMan.CallUpdate(musicTime);
		}

		if (Time.time - g_lastGCTime > GC_INTERVAL)
		{
			g_luaEnv.Tick();
			g_lastGCTime = Time.time;
		}
	}

	public void OnFinish()
	{
		if (!IsLoaded)
		{
			return;
		}

		if (_luaFinish is not null)
		{
			try
			{
				_luaFinish();
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}

		int loop = _actorList.Count - 1;

		if (loop >= 0)
		{
			for (int i = loop; i >= 0; i--)
			{
				if (_actorList[i] is not null)
				{
					_actorList[i].LuaFinish();
				}
			}
		}
	}

	public void OnHitNote(int id, int lane, int noteType, int judge, bool isAttack)
	{
		if (!IsLoaded)
		{
			return;
		}

		if (_luaOnHitNote is not null)
		{
			try
			{
				_luaOnHitNote(id, lane, noteType, judge, isAttack);
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}

		int loop = _actorList.Count - 1;

		if (loop >= 0)
		{
			for (int i = loop; i >= 0; i--)
			{
				if (_actorList[i] is not null && _actorList[i].HasLuaOnHitNote)
				{
					_actorList[i].LuaOnHitNote(id, lane, noteType, judge, isAttack);
				}
			}
		}
	}

	public void OnMissedNote(int id, int lane, int noteType)
	{
		if (!IsLoaded)
		{
			return;
		}

		if (_luaOnMissedNote is not null)
		{
			try
			{
				_luaOnMissedNote(id, lane, noteType);
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}

		int loop = _actorList.Count - 1;

		if (loop >= 0)
		{
			for (int i = loop; i >= 0; i--)
			{
				if (_actorList[i] is not null && _actorList[i].HasLuaOnMissedNote)
				{
					_actorList[i].LuaOnMissedNote(id, lane, noteType);
				}
			}
		}
	}

	public void OnSpawnNote(NoteController noteController)
	{
		if (!IsLoaded)
		{
			return;
		}

		if (_luaOnSpawnNote is not null)
		{
			try
			{
				_luaOnSpawnNote(noteController);
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}

		int loop = _actorList.Count - 1;

		if (loop >= 0)
		{
			for (int i = loop; i >= 0; i--)
			{
				if (_actorList[i] is not null && _actorList[i].HasLuaOnCreateNote)
				{
					_actorList[i].LuaOnCreateNote(noteController);
				}
			}
		}
	}

	public void OnSpawnLong(LongController longController)
	{
		if (!IsLoaded)
		{
			return;
		}

		if (_luaOnSpawnLong is not null)
		{
			try
			{
				_luaOnSpawnLong(longController);
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}

		int loop = _actorList.Count - 1;

		if (loop >= 0)
		{
			for (int i = loop; i >= 0; i--)
			{
				if (_actorList[i] is not null && _actorList[i].HasLuaOnCreateLong)
				{
					_actorList[i].LuaOnCreateLong(longController);
				}
			}
		}
	}

	public void OnPause()
	{
		if (!IsLoaded)
		{
			return;
		}

		if (_luaOnPause is not null)
		{
			try
			{
				_luaOnPause();
			}
			catch (Exception e)
			{
				//_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}

		int loop = _actorList.Count - 1;

		if (loop >= 0)
		{
			for (int i = loop; i >= 0; i--)
			{
				if (_actorList[i] is not null && _actorList[i].HasLuaOnPasue)
				{
					_actorList[i].LuaOnPause();
				}
			}
		}

		loop = _actorVideoCanvasList.Count - 1;

		if (loop >= 0)
		{
			for (int i = loop; i >= 0; i--)
			{
				if (_actorVideoCanvasList[i] is not null)
				{
					_actorVideoCanvasList[i].OnPause();
				}
			}
		}
	}

	public void OnResume()
	{
		if (!IsLoaded)
		{
			return;
		}

		if (_luaOnResume is not null)
		{
			try
			{
				_luaOnResume();
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}

		int loop = _actorList.Count - 1;

		if (loop >= 0)
		{
			for (int i = loop; i >= 0; i--)
			{
				if (_actorList[i] is not null && _actorList[i].HasLuaOnResume)
				{
					_actorList[i].LuaOnResume();
				}
			}
		}

		loop = _actorVideoCanvasList.Count - 1;

		if (loop >= 0)
		{
			for (int i = loop; i >= 0; i--)
			{
				if (_actorVideoCanvasList[i] is not null)
				{
					_actorVideoCanvasList[i].OnResume();
				}
			}
		}
	}

	public void OnInputDown(int touchId, float posX, float screenPosX, float screenPosY)
	{
		if (!IsLoaded)
		{
			return;
		}

		if (_luaOnInputDown is not null)
		{
			try
			{
				_luaOnInputDown(touchId, posX, screenPosX, screenPosY);
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}

		int loop = _actorList.Count - 1;

		if (loop >= 0)
		{
			for (int i = loop; i >= 0; i--)
			{
				if (_actorList[i] is not null && _actorList[i].HasLuaOnInputDown)
				{
					_actorList[i].LuaOnInputDown(touchId, posX, screenPosX, screenPosY);
				}
			}
		}
	}

	public void OnInputMove(int touchId, float posX, float screenPosX, float screenPosY)
	{
		if (!IsLoaded)
		{
			return;
		}

		if (_luaOnInputMove is not null)
		{
			try
			{
				_luaOnInputMove(touchId, posX, screenPosX, screenPosY);
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}

		int loop = _actorList.Count - 1;

		if (loop >= 0)
		{
			for (int i = loop; i >= 0; i--)
			{
				if (_actorList[i] is not null && _actorList[i].HasLuaOnInputMove)
				{
					_actorList[i].LuaOnInputMove(touchId, posX, screenPosX, screenPosY);
				}
			}
		}
	}

	public void OnInputUp(int touchId, float posX, float screenPosX, float screenPosY)
	{
		if (!IsLoaded)
		{
			return;
		}

		if (_luaOnInputUp is not null)
		{
			try
			{
				_luaOnInputUp(touchId, posX, screenPosX, screenPosY);
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}

		int loop = _actorList.Count - 1;

		if (loop >= 0)
		{
			for (int i = loop; i >= 0; i--)
			{
				if (_actorList[i] is not null && _actorList[i].HasLuaOnInputUp)
				{
					_actorList[i].LuaOnInputUp(touchId, posX, screenPosX, screenPosY);
				}
			}
		}
	}

	private void OnDestroy()
	{
		if (!IsLoaded)
		{
			return;
		}

		if (_luaOnDestroy is not null)
		{
			_luaOnDestroy();
		}

		if (_danmakuStageMan != null)
		{
			_danmakuStageMan.OnFinalize();
			_danmakuStageMan = null;
			//Resources.UnloadUnusedAssets();
		}

		if (_scriptEnv != null)
		{
			_scriptEnv.Dispose();
			_scriptEnv = null;
		}

		_luaStart = null;
		_luaUpdate = null;
		_luaFinish = null;
		_luaOnDestroy = null;
		_luaOnHitNote = null;
		_luaOnMissedNote = null;
		_luaOnSpawnNote = null;
		_luaOnSpawnLong = null;
		_luaOnPause = null;
		_luaOnResume = null;
		_luaOnInputDown = null;
		_luaOnInputMove = null;
		_luaOnInputUp = null;
		_onError = null;

		injections = null;
		_meterialInjections = null;
	}
}