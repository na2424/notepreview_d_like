﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public sealed class JudgeEffectController : MonoBehaviour
{
	[SerializeField] ParticleSystem[] _ps;

	ObjectPool<JudgeEffectController> _pool;

	GameObject _gameObject;
	bool _isPlay = false;

	public void Init(ObjectPool<JudgeEffectController> pool)
	{
		_pool = pool;
		_gameObject = gameObject;
	}

	public void SetActive(bool isActive)
	{
		_gameObject.SetActive(isActive);
	}

	public void Play()
	{
		_isPlay = true;

		foreach (var p in _ps)
		{
			p.Play();
		}
	}

	public void CallUpdate()
	{
		if (_isPlay && !_ps[0].isPlaying)
		{
			OnFinish();
		}
	}

	public void Show()
	{
	}

	public void Hide()
	{
	}

	void OnFinish()
	{
		_isPlay = false;
		_pool.Release(this);
	}
}
