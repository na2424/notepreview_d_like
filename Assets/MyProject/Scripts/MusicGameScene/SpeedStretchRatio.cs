﻿using UnityEngine;

public sealed class SpeedStretchRatio
{
	//------------------
	// キャッシュ.
	//------------------
	static float _currentValue = 1f;
	float[] _speedPositions;
	float[] _speedStretchRatios;
	float[] _speedDelayBeats;
	int _speedPositionCount = 0;

	//------------------
	// プロパティ.
	//------------------
	public static float CurrentValue => _currentValue;

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name="songInfo">楽曲情報</param>
	public void Init(SongInfo songInfo)
	{
		_currentValue = 1f;
		_speedPositions = songInfo.SpeedPositions.ToArray();
		_speedStretchRatios = songInfo.SpeedStretchRatios.ToArray();
		_speedDelayBeats = songInfo.SpeedDelayBeats.ToArray();
	}

	/// <summary>
	/// 更新処理
	/// 
	/// 加速・減速スピードギミックによる現在のスピードを求める
	/// 結果は_currentValueに格納される
	/// </summary>
	/// <param name="beat">拍数</param>
	public void CallUpdate(double beat)
	{
		if (_speedPositions.Length <= _speedPositionCount)
		{
			return;
		}

		int index = _speedPositionCount;

		if (_speedPositions[index] <= beat && beat < (_speedPositions[index] + _speedDelayBeats[index])) ///FIXME: out of range
		{
			var t = Mathf.Clamp01(((float)beat - _speedPositions[index]) / _speedDelayBeats[index]);

			_currentValue = Mathf.Lerp(index >= 1 ? _speedStretchRatios[index - 1] : 1f, _speedStretchRatios[index], t);
		}
		else if ((_speedPositions[index] + _speedDelayBeats[index]) <= beat)
		{
			_currentValue = _speedStretchRatios[index];
			_speedPositionCount++;
		}

		// 0徐算を避けるため
		if (_currentValue == 0f)
		{
			_currentValue = 0.00001f;
		}
	}

	/// <summary>
	/// LuaからSpeedを変更できるようにする
	/// </summary>
	/// <param name="value"></param>
	public void SetValue(float value)
	{
		// 0徐算になるのを避けるため
		if (value == 0f)
		{
			value = 0.00001f;
		}

		_currentValue = value;
	}

}
