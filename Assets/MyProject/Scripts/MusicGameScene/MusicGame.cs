﻿using Cysharp.Threading.Tasks;
using Dialog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ELEBEAT;
using UnityEngine;

/// <summary>
/// ゲームのメインロジックを担当するクラス
/// </summary>
[Serializable]
public sealed class MusicGame
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] NoteObjectPool _noteObjectPool;
	[SerializeField] LongObjectPool _longObjectPool;
	[SerializeField] JudgeEffectPool _judgeEffectPool;
	[SerializeField] BeatBarObjectPool _beatBarObjectPool;
	[SerializeField] SameTimeBar _sameTimeBar;

	[SerializeField] VisibleHoldController _visibleHoldController;
	[SerializeField] NoteTexture _noteTexture;
	[SerializeField] LongTexture _longTexture;

	public event Action<JudgeType> OnMissedNote
	{
		add { _noteObjectPool.OnMissedNote += value; }
		remove { _noteObjectPool.OnMissedNote -= value; }
	}

	public event Action<int, int, int, int, bool> OnHitNoteLua = null;

	public event Action<int, int, int> OnMissedNoteLua
	{
		add
		{
			_noteObjectPool.OnMissedNoteLua += value;
			_onMissedNoteLua += value;
		}

		remove
		{
			_noteObjectPool.OnMissedNoteLua -= value;
			_onMissedNoteLua -= value;
		}
	}

	public event Action<NoteController> OnSpawnNoteLua = null;

	public event Action<LongController> OnSpawnLongLua = null;

	//------------------
	// キャッシュ.
	//------------------
	Sequence _sequence;
	JudgeManager _judgeManager;
	TimingHistory _timingHistory = new TimingHistory();
	NoFuzzyTimingHistory _noFuzzyTimingHistory = new NoFuzzyTimingHistory();
	BpmHelper _bpmHelper = new BpmHelper();
	ScrollHelper _scrollHelper = new ScrollHelper();
	SpeedStretchRatio _speedStretchRatio = new SpeedStretchRatio();
	BgChangeHelper _bgChangeHelper = new BgChangeHelper();
	Action<int, int, int> _onMissedNoteLua;

	int _bar = 0;
	int _noteIndex = 0;
	int _longIndex = 0;
	bool _enableBeatBar = true;
	bool _enableJudgeEffect = true;
	bool _enableRelayFrontDisplay = true;
	bool _isCMod = false;
	bool _isAuto = false;
	// 判定の横幅
	float _judgeDistance = Constant.JudgeTime.JUDGE_DISTANCE;
	// 開始の拍
	float _startBeatBar;
	// 最後の拍数
	float _lastBeatPosition = 0f;
	// NullTapを鳴らさない時間
	float _nullTapNoSoundTime = 0.2f;

	InputBase _input;
	Dictionary<int, int> _upNoteTouchIdDict = new Dictionary<int, int>(8);
	Dictionary<int, int> _relayLongStartIndex = new Dictionary<int, int>(8);
	HashSet<int> _skipNoteIndex = new HashSet<int>(8);
	HashSet<int> _hitTouchId = new HashSet<int>(8);
	HashSet<int> _releasedLongEndNoteIndex = new HashSet<int>(8);
	HashSet<int> _currentHoldTouchId = new HashSet<int>(8);
	Queue<int> _checkLongNoteIndexQueue = new Queue<int>(8);

	float _longDistanceRevision = 0f;
	List<int> _sortedKeyList = new List<int>(8);
	List<float> _bgChangePositions;
	List<string> _bgChangeImageName;
	int _bgChangeIndex = 0;

	double _beforeTouchSeJustTime = -1f;
	double _beforeTouchSeFuzzyJustTime = -1f;
	LongEndType _longEndType = LongEndType.Normal;

	//------------------
	// 定数.
	//------------------
	const float FIND_NOTE_MARGIN_TIME = 0.2f;
	const float JUDGE_DISTANCE_AUTO = 0.02f;
	const int CREATABLE_NOTES_PER_FRAME = 32;
	const int CREATABLE_BARS_PER_FRAME = 16;

	//------------------
	// プロパティ.
	//------------------
	// 総ノーツ数
	public int TotalNote => _sequence.BeatPositions.Count;
	// 譜面データ
	public Sequence Sequence => _sequence;
	// 判定履歴
	public TimingHistory TimingHistory => _timingHistory;
	public NoFuzzyTimingHistory NoFuzzyTimingHistory => _noFuzzyTimingHistory;
	public BpmHelper BpmHelper => _bpmHelper;
	public ScrollHelper ScrollHelper => _scrollHelper;
	public NoteObjectPool NoteObjectPool => _noteObjectPool;
	// 最後のノーツのある時間
	public double LastNoteTime { get; private set; }
	public int MaxSameTimeLongCount { get; private set; }

	public async UniTask<bool> InitAsync(CancellationToken ct, SongInfo songInfo, InputBase input, JudgeManager judgeManager)
	{
		_beatBarObjectPool.Init();

		var noteEffectType = GameManager.Instance.DisplayOption.NotesEffectType;
		_enableJudgeEffect = noteEffectType != NotesEffectType.None;

		_enableBeatBar = GameManager.Instance.DisplayOption.BeatBar;
		_enableRelayFrontDisplay = GameManager.Instance.DisplayOption.RelayFrontDisplay;
		_isAuto = GameManager.Instance.PlayModeOption.PlayMode != PlayMode.InputCheck;
		_isCMod = GameManager.Instance.IsCMod;
		_bgChangePositions = songInfo.BgChangePositions;
		_bgChangeImageName = songInfo.BgChangeImageName;

		_judgeDistance = _isAuto ? JUDGE_DISTANCE_AUTO : GameManager.Instance.JudgeTimeOption.JudgeDistance;

		_input = input;
		_judgeManager = judgeManager;

		_longDistanceRevision = GameManager.Instance.JudgeTimeOption.LongRevisionDistance;

		try
		{
			_sequence = await new SequenceReader().ReadAsync(ct, songInfo);
		}
		catch (Exception ex)
		{
			var builder = new DialogParametor.Builder("譜面読み込みエラー", ex.Message);
			builder.AddDefaultAction("選曲画面に戻る", () => GameManager.Instance.ChangeScene(SceneName.SelectMusic));
			builder.AddCallbackOnAutoClosed(() => GameManager.Instance.ChangeScene(SceneName.SelectMusic));
			DialogManager.Instance.Open(builder.Build());
			return false;
		}

		if (GameManager.Instance.JudgeTimeOption.Mirror)
		{
			SwapMirrorLane();
		}

		MaxSameTimeLongCount = GetMaxSameTimeLong(_sequence.LongInfo);

		_scrollHelper.Init(songInfo);

		_visibleHoldController.Init(noteEffectType, MaxSameTimeLongCount + 1);
		_judgeEffectPool.Init(noteEffectType);
		_noteObjectPool.Init(_sequence.IsAttacks.Any(n => n));
		_longObjectPool.Init(_bpmHelper, _scrollHelper, _visibleHoldController);
		_bpmHelper.Init(songInfo);
		_speedStretchRatio.Init(songInfo);
		_bgChangeHelper.Init(songInfo);
		_sameTimeBar.Init(_bpmHelper, _scrollHelper, _sequence);

		CalculateStartBeatBar();
		CalculateLastNoteTime();
		_lastBeatPosition = _sequence.BeatPositions.NonAllocLast() + 1;

		_bar = (int)Mathf.Min(_startBeatBar, (int)GameParam.VisibleBeat + 1);

		// 中継ノーツとLongの開始ノーツのIndexを紐づける
		SetRelayLongStartIndex();

		return true;

		//
		// ここからローカル関数
		//

		void CalculateStartBeatBar()
		{
			_startBeatBar = (int)_sequence.BeatPositions.NonAllocFirst();
		}

		void CalculateLastNoteTime()
		{
			var lastNoteBeatPosition = _sequence.BeatPositions.NonAllocLast();
			LastNoteTime = _bpmHelper.BeatToTime(lastNoteBeatPosition);
		}

		void SetRelayLongStartIndex()
		{
			foreach (var longInfo in _sequence.LongInfo)
			{
				int count = longInfo.NoteIndex.Count;
				for (int i = 0; i < count - 1; i++)
				{
					//Debug.Log(longInfo.NoteIndex[i] + "," + longInfo.NoteIndex[0]);
					if (!_relayLongStartIndex.ContainsKey(longInfo.NoteIndex[i]))
					{
						_relayLongStartIndex.Add(longInfo.NoteIndex[i], longInfo.NoteIndex[0]);
					}
				}
			}
		}

		void SwapMirrorLane()
		{
			int laneCount = _sequence.Lanes.Count;
			int laneMaxIndex = Constant.Note.LANE_COUNT - 1;

			for (int i = 0; i < laneCount; i++)
			{
				_sequence.Lanes[i] = laneMaxIndex - _sequence.Lanes[i];
			}

			int longInfoCount = _sequence.LongInfo.Count;


			for (int i = 0; i < longInfoCount; i++)
			{
				int longInfoLaneCount = _sequence.LongInfo[i].Lanes.Count;
				for (int j = 0; j < longInfoLaneCount; j++)
				{
					_sequence.LongInfo[i].Lanes[j] = laneMaxIndex - _sequence.LongInfo[i].Lanes[j];
				}
			}
		}

		/// <summary>
		/// 同時ロングが何本あるか取得する
		/// </summary>
		int GetMaxSameTimeLong(List<LongInfo> longInfo)
		{
			int count = longInfo.Count;
			List<(double beat, bool isStart)> longCounter = new List<(double, bool)>();
			const double EPS = 0.002d;

			for (int i = 0; i < count; i++)
			{
				longCounter.Add((longInfo[i].BeatPositions[0], true));
				int lastIndex = longInfo[i].BeatPositions.Count - 1;
				longCounter.Add((longInfo[i].BeatPositions[lastIndex] + EPS, false));
			}

			longCounter.Sort((a, b) => (int)((a.beat - b.beat) * 1000));

			int total = 0;
			int result = 0;
			int loop = longCounter.Count;

			for (int i = 0; i < loop; i++)
			{
				total += longCounter[i].isStart ? 1 : -1;
				if (result < total)
				{
					result = total;
				}
			}
#if UNITY_EDITOR
			Debug.Log("MaxSameTimeLong: " + result);
#endif
			return result;
		}
	}


	/// <summary>
	/// リハーサルモード時の途中再生の位置を設定する
	/// </summary>
	/// <param name="musicTime">再生位置</param>
	public void SetMidPlay(double musicTime)
	{
		var beat = _bpmHelper.TimeToBeat(musicTime);

		_bar = (int)beat;

		for (int i = 0; i < _sequence.BeatPositions.Count; i++)
		{
			if (beat <= _sequence.BeatPositions[i])
			{
				_noteIndex = i;
				break;
			}
		}

		for (int i = 0; i < _sequence.LongInfo.Count; i++)
		{
			if (beat <= _sequence.LongInfo[i].BeatPositions[0])
			{
				_longIndex = i;
				break;
			}
		}

		_bgChangeHelper.SetMidPlay(beat);
	}

	public void SetStartBarFromStartTime(float startTime)
	{
		int startBeat = (int)_bpmHelper.TimeToBeat(startTime);
		_bar = (int)Mathf.Min(startBeat, (int)GameParam.VisibleBeat + 1);
	}

	public void AddMissedTimingHistory(double musicTime)
	{
		_timingHistory.Add(musicTime, 1000d, JudgeType.Missed);
	}

	public void CallUpdate(double musicTime)
	{
		double beat = _bpmHelper.TimeToBeat(musicTime);
		double visualBeat = _scrollHelper.ApplyScrollCalculated(beat);

		//Debug.Log(beat + " , "+ visualBeat);

		// TouchIdの順番に並び替えたListを作る
		// _sortedKeyListに結果が入る
		SortTouchIdList();

		//-----------------------------------------
		// 判定最優先
		//-----------------------------------------
		// Note判定処理
		foreach (int key in _sortedKeyList)
		{
			var value = _input.TouchData[key];

			// タップ判定
			JudgeTap(value, key);

			// 離し判定
			JudgeUp(value, key);

			// ホールド(押しっぱなし)判定
			JudgeHold(value, key);
		}

		//----------------------------------------
		// ノート処理
		//----------------------------------------

		// 加速・減速ギミックによるスピード倍率を更新
		_speedStretchRatio.CallUpdate(beat);
		var speedStretchRatio = SpeedStretchRatio.CurrentValue;

		// Note生成
		for (int i = 0; i < CREATABLE_NOTES_PER_FRAME; i++)
		{
			if (!NeedsCreateNote(visualBeat, musicTime))
			{
				break;
			}

			if (_skipNoteIndex.Contains(_noteIndex))
			{
				_skipNoteIndex.Remove(_noteIndex);
			}
			else
			{
				CreateNoteFromObjectPool();
			}

			_checkLongNoteIndexQueue.Enqueue(_noteIndex);
			_noteIndex++;
		}

		// Note位置更新
		_noteObjectPool.CallUpdate(visualBeat, musicTime, speedStretchRatio);

		//----------------------------------------
		// ロング処理
		//----------------------------------------

		// Long生成
		for (int i = 0; i < _checkLongNoteIndexQueue.Count; i++)
		{
			int longNoteIndex = _checkLongNoteIndexQueue.Dequeue();

			if (!NeedsCreateLong(longNoteIndex))
			{
				continue;
			}

			CreateLongFromObjectPool();

			_longIndex++;
		}

		// Long位置更新
		_longObjectPool.CallUpdate(beat, visualBeat, musicTime, speedStretchRatio);

		// Long判定処理
		// (MEMO: Long位置更新の後でないと1フレーム分、ロングの手前表示がずれるので注意)
		foreach (int key in _sortedKeyList)
		{
			var touchData = _input.TouchData[key];
			int touchId = _isAuto
				? touchData.AutoIndex
				: key;

			JudgeLong(beat, musicTime, touchId, touchData);
		}

		// ロング終端チェック
		CheckEndLong(beat);

		//---------------------------------------
		// 同時押しライン
		//---------------------------------------

		_sameTimeBar.CallUpdate(visualBeat, musicTime, SpeedStretchRatio.CurrentValue);

		//----------------------------------------
		// 拍線処理
		//----------------------------------------

		// 拍線生成
		for (int i = 0; i < CREATABLE_BARS_PER_FRAME; i++)
		{
			if (!NeedsCreateBeatBar(visualBeat, musicTime))
			{
				break;
			}

			if (_enableBeatBar)
			{
				CreateBeatBarFromObjectPool();
			}
			_bar++;
		}

		// 拍線位置更新
		_beatBarObjectPool.CallUpdate(visualBeat, musicTime, speedStretchRatio);

		//----------------------------------------
		// 判定エフェクト
		//----------------------------------------
		_judgeEffectPool.CallUpdate();

		//----------------------------------------
		// BgChange処理
		//----------------------------------------
		CheckBgChange(beat);
	}

	/// <summary>
	/// タッチIDの順に並び変えたリストを作る
	/// </summary>
	void SortTouchIdList()
	{
		_sortedKeyList.Clear();

		foreach (var touch in _input.TouchData)
		{
			_sortedKeyList.Add(touch.Key);
		}

		// MEMO: GC対策
		// 比較関数が外部環境を参照しない場合、比較関数は内部でキャッシュされて使い回されます
		_sortedKeyList.Sort((a, b) => a - b);
	}

	/// <summary>
	/// 拍線生成する必要があるか
	/// </summary>
	/// <param name="beat">Beat位置</param>
	bool NeedsCreateBeatBar(double visualBeat, double musicTime)
	{
		return
			_bar < _lastBeatPosition &&
			(_isCMod ?
			(musicTime > _bpmHelper.BeatToTime(_bar) - GameParam.VisibleTime) :
			(visualBeat > _scrollHelper.ApplyScroll(_bar) - GameParam.VisibleBeat));
	}

	/// <summary>
	/// オブジェクトプールから拍線生成
	/// </summary>
	void CreateBeatBarFromObjectPool()
	{
		var controller = _beatBarObjectPool.Pool.Get();
		double justTime = _bpmHelper.BeatToTime(_bar);

		controller.SetParam(
			beatPosition: (float)_scrollHelper.ApplyScroll(_bar),
			justTime: justTime
		);
	}

	/// <summary>
	/// Note生成する必要があるか
	/// </summary>
	/// <param name="beat">Beat位置</param>
	bool NeedsCreateNote(double visualBeat, double musicTime)
	{
		bool isFixedVisibleTime = false;
		double visibleTime = GameParam.VisibleTime;

		if (visibleTime < 0.2f)
		{
			isFixedVisibleTime = true;
			visibleTime = 0.78f;
		}

		return
			_noteIndex < _sequence.BeatPositions.Count &&
			((_isCMod || isFixedVisibleTime) ?
			(musicTime > _bpmHelper.BeatToTime(_sequence.BeatPositions[_noteIndex]) - visibleTime) :
			(visualBeat > _scrollHelper.ApplyScroll(_sequence.BeatPositions[_noteIndex]) - GameParam.VisibleBeat));
	}

	/// <summary>
	/// オブジェクトプールからNote生成
	/// </summary>
	void CreateNoteFromObjectPool()
	{
		var noteController = _noteObjectPool.Pool.Get();
		double justTime = _bpmHelper.BeatToTime(_sequence.BeatPositions[_noteIndex]);

		noteController.SetParam(
			beatPosition: (float)_scrollHelper.ApplyScroll(_sequence.BeatPositions[_noteIndex]),
			justTime: justTime,
			lane: _sequence.Lanes[_noteIndex],
			noteTex: _noteTexture.NoteTypeToTexture(_sequence.NoteTypes[_noteIndex]),
			noteType: _sequence.NoteTypes[_noteIndex],
			isAttack: _sequence.IsAttacks[_noteIndex],
			noteIndex: _noteIndex
		);

		// 離し判定を有効にするTouchIdを設定
		if (_sequence.NoteTypes[_noteIndex] == NoteType.LongEnd || _sequence.NoteTypes[_noteIndex] == NoteType.FuzzyLongEnd)
		{
			if (_upNoteTouchIdDict.TryGetValue(_noteIndex, out var upTouchId))
			{
				noteController.UpTouchId = upTouchId;
				_upNoteTouchIdDict.Remove(_noteIndex);
			}
		}

		// 同時押しラインの生成判定に使用する
		_sameTimeBar.OnCreateNote(_noteIndex);

		// Lua用
		OnSpawnNoteLua?.Invoke(noteController);
	}

	/// <summary>
	/// Long生成する必要があるか
	/// </summary>
	/// <param name="beat">Beat位置</param>
	bool NeedsCreateLong(int noteIndex)
	{
		return
			(_sequence.NoteTypes[noteIndex] == NoteType.LongStart ||
			 _sequence.NoteTypes[noteIndex] == NoteType.FuzzyLongStart) &&
			noteIndex < _sequence.BeatPositions.Count;
	}

	/// <summary>
	/// オブジェクトプールからLong生成
	/// </summary>
	void CreateLongFromObjectPool()
	{
		var longController = _longObjectPool.Pool.Get();

		LongInfo longInfo = _sequence.LongInfo[_longIndex];

		longController.SetParam(longInfo, _longTexture.LongTypeToTexture(longInfo.LongType));

		foreach (var note in _noteObjectPool.CurrentActiveNote)
		{
			if (note.LongController == null && note.NoteIndex == longInfo.StartNoteIndex)
			{
				note.SetLongController(longController);
				break;
			}
		}

		// Lua用
		OnSpawnLongLua?.Invoke(longController);
	}

	/// <summary>
	/// タップ判定
	/// </summary>
	/// <param name="musicTime">再生時間</param>
	/// <param name="touch">タッチ情報</param>
	void JudgeTap(TouchData touch, int touchId)
	{
		if (touch.Phase != ScreenTouchPhase.Tap)
		{
			return;
		}

		// 各ノートとの時間差を測り、最も近い時間を求める
		double minTime = FindMinTime(touch.TouchTime, touch.PosX, ScreenTouchPhase.Tap, NoteBitFrag.Tap);

		// 最も近い距離時間の中でX座標の位置が近いノートを選ぶ
		NoteController nearNote = FindNearNote(touch.TouchTime, minTime, touch.PosX, NoteBitFrag.Tap);

		// ファジーをタップ判定しない
		if (
			nearNote is not null &&
			((nearNote.NoteType == NoteType.FuzzyLongStart && !_isAuto) ||
			nearNote.NoteType == NoteType.Fuzzy))
		{
			return;
		}

		// 判定する対象があれば判定処理
		if (nearNote is not null)
		{
			JudgeTiming(nearNote, touch.TouchTime, ScreenTouchPhase.Tap, touchId);
		}
		else
		{
			minTime = FindMinTimeAll(touch.TouchTime);

			if (minTime > _nullTapNoSoundTime)
			{
				TouchSEManager.Instance.Play(TouchSEType.NullTap);
			}
		}
	}

	/// <summary>
	/// 離し判定
	/// </summary>
	/// <param name="musicTime">再生時間</param>
	/// <param name="touch">タッチ情報</param>
	void JudgeUp(TouchData touch, int touchId)
	{
		if (touch.Phase != ScreenTouchPhase.Up)
		{
			return;
		}

		// 各ノートとの時間差を測り、最も近い時間を求める
		double minTime = FindMinTime(touch.TouchTime, touch.PosX, ScreenTouchPhase.Up, NoteBitFrag.Up, touchId);

		// 最も近い距離時間の中でX座標の位置が近いノートを選ぶ
		NoteController nearNote = FindNearNote(touch.TouchTime, minTime, touch.PosX, NoteBitFrag.Up);

		// 判定する対象があれば判定処理
		if (nearNote is not null)
		{
			JudgeTiming(nearNote, touch.TouchTime, ScreenTouchPhase.Up, touchId);
		}

		if (_hitTouchId.Contains(touchId) && !_isAuto)
		{
			_hitTouchId.Remove(touchId);
		}
	}

	/// <summary>
	/// 押しっぱなし判定
	/// </summary>
	/// <param name="musicTime">再生時間</param>
	/// <param name="touch">タッチ情報</param>
	void JudgeHold(TouchData touch, int touchId)
	{
		if (touch.Phase != ScreenTouchPhase.Hold)
		{
			return;
		}

		var holdBitFrag = (_longEndType == LongEndType.Normal) ? NoteBitFrag.Hold : NoteBitFrag.PLHold;

		// 各ノートとの時間差を測り、最も近い時間を求める
		double minTime = FindMinTime(touch.TouchTime, touch.PosX, ScreenTouchPhase.Hold, holdBitFrag);

		// 最も近い距離時間の中でX座標の位置が近いノートを選ぶ
		NoteController nearNote = FindNearNote(touch.TouchTime, minTime, touch.PosX, holdBitFrag);

		// 判定する対象があれば判定処理
		if (nearNote is not null)
		{
			JudgeTiming(nearNote, touch.TouchTime, ScreenTouchPhase.Hold, touchId);
		}
	}

	void OnJudgeStartHold(int touchId, LongController holdLong, bool isAttack = false)
	{
		// ホールド中のタッチIDを保持
		SafeAddCurrentHoldTouchId(touchId);

		int upNoteIndex = holdLong.LastNoteIndex;
		bool hasUpNote = false;

		// upNoteIndexのノートにTouchIdを設定
		foreach (var note in _noteObjectPool.CurrentActiveNote)
		{
			if (note.NoteIndex == upNoteIndex)
			{
				hasUpNote = true;
				// LongのUpを有効にする
				note.UpTouchId = touchId;
				break;
			}
		}

		// 表示外ノートに設定する用のDictにTouchIdを格納
		if (!hasUpNote)
		{
			_upNoteTouchIdDict[upNoteIndex] = touchId;
		}

		holdLong.SetTouchId(touchId);
		_visibleHoldController.SetActive(true, holdLong.TouchId, holdLong.StartNoteIndex, (holdLong.LongInfo.LongType == LongType.Long) ? NoteType.LongStart : NoteType.FuzzyLongStart, isAttack);
		_visibleHoldController.SetPosition(holdLong.CurrentPosX, holdLong.TouchId, holdLong.StartNoteIndex);
	}

	/// <summary>
	/// ロング(ライン)判定
	/// (TODO:処理が多いので整理したい)
	/// ・ロングのホールド時に判定ライン下のステンシルマスク(holdLong.UseStencil)を有効にする.
	/// ・ロングのホールド時にホールドオブジェクト(_holdActiveController)を表示する.
	/// ・ロングのホールド時の指の位置は開始、中継、終了の位置が合っていれば何処でも良く、指さえ離さなければロングは表示されたままにする.
	/// ・ロングを途中で離したときにロングを消す処理 (緑ロングと青ロングで処理が異なる).
	/// ・ロングを途中で離したときに後続のノートを消す処理と判定.
	/// ・TouchIdが未設定のロングの場合、一番近いロングを選ぶ処理.
	/// ・TouchIdが設定済みのロングの場合、近さよりも値が一致するロングを選ぶ処理.
	/// </summary>
	/// <param name="beat">Beat位置</param>
	/// <param name="touchId">タッチID</param>
	/// <param name="touch">TouchData</param>
	void JudgeLong(double beat, double musicTime, int touchId, TouchData touch)
	{
		double touchTime = touch.TouchTime;
		LongController holdLong = null;
		LongController releaseLong = null;

		float minDistance = float.MaxValue;

		for (int i = 0; i < _longObjectPool.CurrentActiveLong.Count; i++)
		{
			LongController @long = _longObjectPool.CurrentActiveLong[i];

			if (@long.TouchId == touchId && beat <= @long.EndBeatPosition)
			{
				if (touch.Phase == ScreenTouchPhase.Up)
				{
					releaseLong = @long;
					break;
				}
			}

			if (@long.InputStartBeatPosition <= beat && beat <= @long.EndBeatPosition)
			{
				if (@long.TouchId == -1)
				{
					@long.SetHold(false);
				}

				if (touch.Phase == ScreenTouchPhase.Hold)
				{
					if (@long.TouchId == -1)
					{
						bool canCheckLong = true;

						if (!_isAuto && @long.LongInfo.LongType == LongType.Long)
						{
							canCheckLong = (@long.StartTime + _judgeManager.ComboHitTime) < musicTime;
						}

						if (canCheckLong)
						{
							if (!_currentHoldTouchId.Contains(touchId))
							{
								// タッチIDが未設定のラインは近いものを選ぶ
								var distance = Math.Abs(touch.PosX - @long.CurrentPosX);

								// MEMO: ロング自体の横幅は補正を行なわない
								if (distance < _judgeDistance)
								{
									if (distance < minDistance)
									{
										minDistance = distance;
										holdLong = @long;
									}
								}
							}
						}
					}
					else if (@long.TouchId == touchId)
					{
						holdLong = @long;
						break;
					}
				}
			}
		}

		//
		// Hold
		//

		if (holdLong is not null && !holdLong.IsReleased)
		{
			OnHoldLong(holdLong, touchId, touch.Phase, beat);
		}

		//
		// Release
		//

		if (releaseLong is not null)
		{
			// ホールド中のタッチIDを取り除く
			SafeRemoveCurrentHoldTouchId(touchId);

			OnReleaseLong(releaseLong, touchId, touchTime, beat);
		}
	}

	void OnHoldLong(LongController holdLong, int touchId, ScreenTouchPhase phase, double beat)
	{
		bool hasHold = false;

		// ホールド判定
		if (phase == ScreenTouchPhase.Hold)
		{
			hasHold = true;

			if (holdLong.TouchId == -1)
			{
				// Attackノーツかどうか調べる
				//int index = GetLongSectionIndex(holdLong, beat);

				//MEMO: 現時点でロング開始のAttackだけホールドのアタック表示に対して有効になっている
				bool isAttack = _sequence.IsAttacks[holdLong.LongInfo.StartNoteIndex];

				OnJudgeStartHold(touchId, holdLong, isAttack);
			}
			else if (holdLong.TouchId == touchId)
			{
				_visibleHoldController.SetPosition(holdLong.CurrentPosX, holdLong.TouchId, holdLong.StartNoteIndex);
			}
		}

		holdLong.SetHold(hasHold);
	}

	void OnReleaseLong(LongController releaseLong, int touchId, double touchTime, double beat)
	{
		// ファジーロングを途中で離したときの処理
		// ・中継間のラインだけ消す(ノートは残す)
		if (releaseLong.LongInfo.LongType == LongType.FuzzyLong)
		{
			int count = releaseLong.LongInfo.BeatPositions.Count;
			for (int i = 0; i < count - 1; i++)
			{
				if (releaseLong.LongInfo.BeatPositions[i] <= beat && beat <= releaseLong.LongInfo.BeatPositions[i + 1])
				{
					releaseLong.SetHold(false);

					bool isLastSection = i == releaseLong.LongInfo.BeatPositions.Count - 2;

					if (isLastSection)
					{
						releaseLong.Release();
					}
					else
					{
						releaseLong.RemovePosition(i);
						releaseLong.SetTouchId(-1);
						_visibleHoldController.SetActive(false, touchId, releaseLong.StartNoteIndex);
					}

					break;
				}
			}
		}

		// 青ロングを離した時に後続のノートをReleaseする
		// ファジーロングは間のラインを消すだけだが、
		// ロングは３通りある
		// ・最後のポジション間のライン → 最後のラインと終端ノートを消す(Missにする)
		// ・最後から1つ手前のポジション間のライン → 最後までのライン2つと最後の中継と終端ノートを消す(※2つMissになる)
		// ・その他のポジション間のライン → 2つ先までのラインを消し、ライン間にある中継ノートを消す(Missにする)
		if (releaseLong.LongInfo.LongType == LongType.Long)
		{
			releaseLong.SetHold(false);

			// ロングに含まれる同時押しラインをReleaseする
			_sameTimeBar.OnReleaseLong(releaseLong);

			int longSectionIndex = GetLongSectionIndex(releaseLong, beat);

			if (longSectionIndex == -1)
			{
				return;
			}

			bool isLastSection = longSectionIndex == releaseLong.LongInfo.BeatPositions.Count - 2;

			// 最後
			if (isLastSection)
			{
				var absDiffTime = Math.Abs(touchTime - releaseLong.EndTime);
				var lastNoteIndex = releaseLong.LastNoteIndex;
				var note = FindNoteFromNoteIndex(lastNoteIndex);

				if (note is not null && !note.IsReleased)
				{
					// ノーツが生成済み & 判定外でアップされた時
					note.Release(true);
				}
				else if (!_releasedLongEndNoteIndex.Contains(lastNoteIndex))
				{
					// ノーツが未生成 & 判定外でアップされた時

					// まだ生成されていない先のノートを生成しないようにする
					_skipNoteIndex.Add(lastNoteIndex);
					_noteObjectPool.AddMissed();

					// Lua用
					_onMissedNoteLua?.Invoke(lastNoteIndex, _sequence.Lanes[lastNoteIndex], (int)NoteType.LongEnd);
				}
				else
				{
					// アップ判定済みの時
					_releasedLongEndNoteIndex.Remove(lastNoteIndex);
				}

				releaseLong.Release();

			}
			else
			{
				NoteController note = FindNoteFromNoteIndex(releaseLong.LongInfo.NoteIndex[longSectionIndex]);

				if (note is not null)
				{
					note.Release(true);
				}
				else
				{
					// まだ生成されていない先のノートを生成しないようにする
					_skipNoteIndex.Add(releaseLong.LongInfo.NoteIndex[longSectionIndex]);
					_noteObjectPool.AddMissed();
				}

				if (longSectionIndex + 1 == releaseLong.LongInfo.NoteIndex.Count - 1)
				{
					// 1つ手前
					var lastNoteIndex = releaseLong.LastNoteIndex;
					note = FindNoteFromNoteIndex(lastNoteIndex);

					if (note is not null)
					{
						// ノーツが生成済み & 判定外でアップされた時
						note.Release(true);
					}
					else if (!_releasedLongEndNoteIndex.Contains(lastNoteIndex))
					{
						// ノーツが未生成 & 判定外でアップされた時

						// まだ生成されていない先のノートを生成しないようにする
						_skipNoteIndex.Add(lastNoteIndex);
						_noteObjectPool.AddMissed();

						// Lua用
						_onMissedNoteLua?.Invoke(lastNoteIndex, _sequence.Lanes[lastNoteIndex], (int)NoteType.LongEnd);
					}
					else
					{
						// アップ判定済みの時
						_releasedLongEndNoteIndex.Remove(lastNoteIndex);
					}

					releaseLong.Release();
				}
				else
				{
					// その他
					releaseLong.RemovePosition(longSectionIndex + 1);
					releaseLong.SetTouchId(-1);
					_visibleHoldController.SetActive(false, touchId, releaseLong.StartNoteIndex);
				}
			}
		}
	}

	/// <summary>
	/// beat位置からロング内のどの区間かのIndexを取得する
	/// ロング外であれば-1を返す
	/// </summary>
	/// <param name="longController"></param>
	/// <param name="beat"></param>
	/// <returns>区間のIndex</returns>
	int GetLongSectionIndex(LongController longController, double beat)
	{
		if (beat < longController.LongInfo.BeatPositions[0])
		{
			// Debug.Log("GetLongSectionIndex : 0 | diff : " + (beat - longController.LongInfo.BeatPositions[0]));
			// ロング到達前に早入りしたホールドを離してしまった場合は0にする
			return 0;
		}

		int loop = longController.LongInfo.BeatPositions.Count - 1;

		for (int i = 0; i < loop; i++)
		{
			if (longController.LongInfo.BeatPositions[i] <= beat && beat <= longController.LongInfo.BeatPositions[i + 1])
			{
				return i;
			}
		}

		return -1;
	}

	/// <summary>
	/// ホールド中のロング終端を過ぎた時の処理
	/// </summary>
	/// <param name="beat">拍数</param>

	void CheckEndLong(double beat)
	{
		var count = _longObjectPool.CurrentActiveLong.Count;
		for (int i = 0; i < count; i++)
		{
			LongController @long = _longObjectPool.CurrentActiveLong[i];

			if (@long.EndBeatPosition < beat && @long.IsHold)
			{
				_visibleHoldController.SetActive(false, @long.TouchId, @long.StartNoteIndex);

				// ホールド中のタッチIDを取り除く
				SafeRemoveCurrentHoldTouchId(@long.TouchId);

				// 過ぎたロングは中継とロング開始を紐づけるDictを除かせる
				if (_relayLongStartIndex.ContainsKey(@long.StartNoteIndex))
				{
					_relayLongStartIndex.Remove(@long.StartNoteIndex);
				}
			}
		}
	}

	/// <summary>
	/// FindのGC削減用
	/// </summary>
	/// <param name="noteIndex"></param>
	/// <returns></returns>
	NoteController FindNoteFromNoteIndex(int noteIndex)
	{
		NoteController note = null;
		int noteCount = _noteObjectPool.CurrentActiveNote.Count;

		for (int i = 0; i < noteCount; i++)
		{
			var noteController = _noteObjectPool.CurrentActiveNote[i];
			if (noteController.NoteIndex == noteIndex)
			{
				note = noteController;
				break;
			}
		}

		return note;
	}

	/// <summary>
	/// 有効なNoteか
	/// (NoteTypeも比較できる)
	/// </summary>
	bool IsValidNote(NoteController note, NoteBitFrag noteBitFrag)
	{
		var noteFlag = (NoteBitFrag)(1 << (int)note.NoteType);

		return
			!note.IsReleased &&
			(noteBitFrag & noteFlag) == noteFlag;
	}

	/// <summary>
	/// 各ノートとの時間差を測り、最も近い時間を求める
	/// 判定の範囲外の時はdouble.MaxValueを返す
	/// </summary>
	double FindMinTime(double touchTime, float posX, ScreenTouchPhase touchPhase, NoteBitFrag noteBitFrag, int touchId = -1)
	{
		double minTime = double.MaxValue;
		foreach (NoteController note in _noteObjectPool.CurrentActiveNote)
		{
			// 明らかに判定時間外のノーツは以降の処理を行なわない
			if (note.JustTime - touchTime > _judgeManager.LongHitTime + FIND_NOTE_MARGIN_TIME)
			{
				continue;
			}

			// 無効なノーツは以降の処理を行なわない
			if (!IsValidNote(note, noteBitFrag))
			{
				continue;
			}

			// アップ判定に対する条件その1
			// ・指の状態がアップの時
			// ・ノーツの種類が青ロングの終端
			// ・ロングを掴んている指(TouchID)でなければ判定しない
			if (touchPhase == ScreenTouchPhase.Up &&
				note.NoteType == NoteType.LongEnd &&
				note.UpTouchId != touchId &&
				!_isAuto)
			{
				continue;
			}

			// アップ判定に対する条件その2
			// ・指の状態がアップの時
			// ・ノーツの種類がファジーまたはファジーロングの終端
			// ・判定済みの指の時は判定しない
			if (touchPhase == ScreenTouchPhase.Up &&
				(note.NoteType == NoteType.Fuzzy || (note.NoteType == NoteType.FuzzyLongEnd && note.UpTouchId != touchId)) &&
				_hitTouchId.Contains(touchId) &&
				!_isAuto)
			{
				continue;
			}

			// ホールド判定に対する条件
			// ・指の状態がホールドの時
			// ・ファジー(ホールド)判定開始時間前の時は判定しない
			if (touchPhase == ScreenTouchPhase.Hold && ((touchTime - note.JustTime) < _judgeManager.HoldStart))
			{
				continue;
			}

			// 横幅補正する必要があれば補正値を加える
			bool isRevision = !_isAuto && (note.NoteType != NoteType.Fuzzy) && (touchPhase == ScreenTouchPhase.Hold || touchPhase == ScreenTouchPhase.Up);
			float revisionValue = isRevision ? _longDistanceRevision : 0f;

			// 判定の横距離
			var distance = Mathf.Abs(note.PosX - posX);

			// 判定の横距離外の時は判定しない
			if (distance > (_judgeDistance + revisionValue))
			{
				continue;
			}

			// ジャストからのズレ時間
			var diffTime = Math.Abs(touchTime - note.JustTime);

			// 判定エリア外の時は判定しない
			if (!_judgeManager.IsHitArea(distance, diffTime, isRevision))
			{
				continue;
			}

			// より近い時間であればminTimeに格納
			if (diffTime <= _judgeManager.HitTime && diffTime <= (minTime + double.Epsilon))
			{
				minTime = diffTime;
			}
		}

		return minTime;
	}

	/// <summary>
	/// 最も近い距離時間の中でX座標の位置が近いノートを選ぶ
	/// 判定の範囲外の時はnullを返す
	/// </summary>
	NoteController FindNearNote(double touchTime, double minTime, float posX, NoteBitFrag noteBitFrag)
	{
		// 最も近い時間が取れてない時
		if (minTime >= double.MaxValue - double.Epsilon)
		{
			return null;
		}

		float minDistance = float.MaxValue;
		NoteController nearNote = null;

		foreach (NoteController note in _noteObjectPool.CurrentActiveNote)
		{
			if (note.JustTime - touchTime > _judgeManager.LongHitTime + FIND_NOTE_MARGIN_TIME)
			{
				continue;
			}

			if (!IsValidNote(note, noteBitFrag))
			{
				continue;
			}

			var diffTime = Math.Abs(touchTime - note.JustTime);
			if (diffTime > minTime + double.Epsilon)
			{
				continue;
			}

			var distance = Mathf.Abs(note.PosX - posX);
			if (distance <= minDistance + float.Epsilon)
			{
				minDistance = distance;
				nearNote = note;
			}
		}

		return nearNote;
	}

	/// <summary>
	/// ノートの判定処理
	/// </summary>
	/// <param name="note"></param>
	/// <param name="touchTime"></param>
	/// <param name="touchPhase"></param>
	/// <param name="touchId"></param>
	void JudgeTiming(NoteController note, double touchTime, ScreenTouchPhase touchPhase, int touchId)
	{
		double diffTime = touchTime - note.JustTime;

		if (touchPhase != ScreenTouchPhase.Hold)
		{
			_noFuzzyTimingHistory.Add(diffTime);
		}

		bool isFuzzy = note.NoteType switch
		{
			NoteType.Fuzzy => true,
			NoteType.FuzzyLongStart => true,
			NoteType.FuzzyLongRelay => true,
			NoteType.FuzzyLongEnd => true,
			_ => false
		};

		bool isRelay =
			note.NoteType == NoteType.LongRelay ||
			note.NoteType == NoteType.FuzzyLongRelay;

		var judge = _judgeManager.JudgeTiming(diffTime, touchPhase, isFuzzy);

		_timingHistory.Add(touchTime, diffTime, judge);

		if (judge != JudgeType.None)
		{
			if (_enableRelayFrontDisplay && isRelay)
			{
				// ロングの中継で画像を中継ノーツに差し替える
				if (_relayLongStartIndex.TryGetValue(note.NoteIndex, out var startNoteIndex))
				{
					//Debug.Log(touchId + "," + startNoteIndex);
					_visibleHoldController.OnHitRelay(touchId, startNoteIndex, note.NoteType, note.IsAttack);
				}
			}

			// ファジーのアップ判定を無効にするTouchIdを設定
			if ((touchPhase == ScreenTouchPhase.Tap || touchPhase == ScreenTouchPhase.Hold) &&
				!_hitTouchId.Contains(touchId) && !_isAuto)
			{
				_hitTouchId.Add(touchId);
			}

			// 未生成のロング終端のアップ対策
			if (note.NoteType == NoteType.LongEnd)
			{
				_releasedLongEndNoteIndex.Add(note.NoteIndex);
			}

			// Luaイベント関数を呼ぶ
			if (OnHitNoteLua is not null)
			{
				OnHitNoteLua(note.NoteIndex, note.Lane, (int)note.NoteType, (int)judge, note.IsAttack);
			}

			note.Release();
		}

		if (judge == JudgeType.Brilliant || judge == JudgeType.Great)
		{
			// 青ロングの開始タップ時にロングに指を割り振る
			if (!_isAuto && note.NoteType == NoteType.LongStart)
			{
				if (note.LongController != null && note.LongController.TouchId == -1)
				{
					OnJudgeStartHold(touchId, note.LongController);
				}
			}
		}

		if (judge == JudgeType.Brilliant)
		{
			if (_enableJudgeEffect)
			{
				_judgeEffectPool.Play(note.PosX);
			}
		}

		// 同時押しラインに含まれるノートの場合は同時押しラインをReleaseする
		_sameTimeBar.OnJudgeTiming(note);

		if (judge != JudgeType.None && judge != JudgeType.Missed)
		{
			if(_longEndType != LongEndType.NonUpAndSound || note.NoteType != NoteType.LongEnd)
			{
				PlayHitSound(judge, note.NoteType, note.JustTime);
			}
		}
	}

	/// <summary>
	/// ロングのホールド中のタッチIDを保持
	/// </summary>
	/// <param name="touchId"></param>
	void SafeAddCurrentHoldTouchId(int touchId)
	{
		if (!_currentHoldTouchId.Contains(touchId))
		{
			_currentHoldTouchId.Add(touchId);
		}
	}

	/// <summary>
	/// ロングのホールド中のタッチIDを除く
	/// </summary>
	/// <param name="touchId"></param>
	void SafeRemoveCurrentHoldTouchId(int touchId)
	{
		if (_currentHoldTouchId.Contains(touchId))
		{
			_currentHoldTouchId.Remove(touchId);
		}
	}

	/// <summary>
	/// ヒット音を鳴らす
	/// </summary>
	/// <param name="type">判定</param>
	void PlayHitSound(JudgeType type, NoteType noteType, double justTime)
	{
		if (type == JudgeType.Brilliant)
		{
			switch (noteType)
			{
				case NoteType.Fuzzy:
				case NoteType.FuzzyLongStart:
				case NoteType.FuzzyLongRelay:
				case NoteType.FuzzyLongEnd:
					if (!Calculate.FastApproximately(justTime, _beforeTouchSeFuzzyJustTime, 0.000001d))
					{
						TouchSEManager.Instance.Play(TouchSEType.FuzzyBrilliantHit);
						_beforeTouchSeFuzzyJustTime = justTime;
					}

					break;

				default:

					if (!Calculate.FastApproximately(justTime, _beforeTouchSeJustTime, 0.000001d))
					{
						TouchSEManager.Instance.Play(TouchSEType.BrilliantHit);
						_beforeTouchSeJustTime = justTime;
					}

					break;
			}
		}
		else if (type == JudgeType.Great)
		{
			TouchSEManager.Instance.Play(TouchSEType.GreatHit);
		}
		else
		{
			TouchSEManager.Instance.Play(TouchSEType.NearHit);
		}
	}

	/// <summary>
	/// ポーズ時に呼ばれる処理
	/// </summary>
	public void OnPause()
	{
		if (_isAuto)
		{
			return;
		}

		// ロングに割り振ったロングを掴んでいる指をリセットする
		for (int i = 0; i < _longObjectPool.CurrentActiveLong.Count; i++)
		{
			_longObjectPool.CurrentActiveLong[i].SetTouchId(-1);
		}
	}

	/// <summary>
	/// 再開時に呼ばれる処理
	/// </summary>
	public void OnResume()
	{
		if (_isAuto)
		{
			return;
		}

		_visibleHoldController.ResetAllActive();
	}

	/// <summary>
	/// BgChangeで背景を切り替えるか確認
	/// </summary>
	/// <param name="beat"></param>
	void CheckBgChange(double beat)
	{
		if (_bgChangeIndex < _bgChangePositions.Count && _bgChangePositions[_bgChangeIndex] <= beat)
		{
			BgManager.Instance.ChangeBgImage(_bgChangeImageName[_bgChangeIndex]);

			_bgChangeIndex++;
		}
	}

	/// <summary>
	/// 各ノートとの時間差を測り、最も近い時間を求める
	/// 判定の範囲外の時はdouble.MaxValueを返す
	/// </summary>
	double FindMinTimeAll(double touchTime)
	{
		double minTime = double.MaxValue;
		foreach (NoteController note in _noteObjectPool.CurrentActiveNote)
		{
			// 明らかに判定時間外のノーツは以降の処理を行なわない
			if (note.JustTime - touchTime > 2f)
			{
				continue;
			}

			// ジャストからのズレ時間
			var diffTime = Math.Abs(touchTime - note.JustTime);

			// より近い時間であればminTimeに格納
			if (diffTime <= (minTime + double.Epsilon))
			{
				minTime = diffTime;
			}
		}

		return minTime;
	}

	//-------------------
	// Lua API用.
	//-------------------

	public void SetSpeed(float speed)
	{
		_speedStretchRatio.SetValue(speed);
	}

	public void SetDistance(float value, bool isSafeAuto)
	{
		if (_isAuto && isSafeAuto)
		{
			return;
		}

		_judgeDistance = value;
		_judgeManager.SetDistance(value);
	}

	public void SetOffsetZ(float offsetZ)
	{
		GameParam.Instance.OffsetZ = offsetZ;
	}

	public void SetVisibleRate(float rate)
	{
		GameParam.Instance.VisibleRate = rate;
	}

	public void SetNotesScrollType(bool isDecelerate)
	{
		GameParam.Instance.NotesScrollType = isDecelerate ? NotesScrollType.Decelerate : NotesScrollType.Constant;
	}

	public void SetEnableJudgeEffect(bool enable)
	{
		_enableJudgeEffect = enable;
	}

	public void SetLongEndType(int type)
	{
		_longEndType = (LongEndType)type;
	}

	public Judge GetCurrentJudge()
	{
		return _judgeManager.Judge;
	}

	public void SetNoCreateSameTimeBarAtNoteIndex(int noteIndex)
	{
		_sameTimeBar.SetNoCreateNoteIndex(noteIndex);
	}

	//------------------------

	public void OnFinalize()
	{
		_noteObjectPool.OnFinalize();
		_longObjectPool.OnFinalize();
		_judgeEffectPool.OnFinalize();
		_beatBarObjectPool.OnFinalize();
		_sameTimeBar.OnFinalize();
		_bpmHelper.OnFinalize();
		_scrollHelper.OnFinalize();

		//_visibleHoldController.OnFinalize();

		_sequence.Clear();
		_sequence = null;
		_bpmHelper = null;
		_speedStretchRatio = null;

		_upNoteTouchIdDict.Clear();
		_skipNoteIndex.Clear();
		_hitTouchId.Clear();
		_releasedLongEndNoteIndex.Clear();
		_currentHoldTouchId.Clear();
		_checkLongNoteIndexQueue.Clear();
		_sortedKeyList.Clear();

		_upNoteTouchIdDict = null;
		_skipNoteIndex = null;
		_hitTouchId = null;
		_releasedLongEndNoteIndex = null;
		_currentHoldTouchId = null;
		_checkLongNoteIndexQueue = null;
		_sortedKeyList = null;
	}
}