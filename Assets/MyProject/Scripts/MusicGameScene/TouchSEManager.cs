﻿using CriWare;
using System.Collections.Generic;
using UnityEngine;

public enum TouchSEType : int
{
	BrilliantHit = 0,
	FuzzyBrilliantHit = 1,
	GreatHit = 2,
	NearHit = 3,
	NullTap = 4,
}

public sealed class TouchSEManager : SingletonMonoBehaviour<TouchSEManager>
{
	[SerializeField] CriAtomSource _criAtomSource;

	bool _isMute = false;
	float _volume = 0.5f;

	protected override void Awake()
	{
		base.Awake();
		_volume = GameManager.Instance.VolumeOption.TouchSEVolume;
		_isMute = GameManager.Instance.VolumeOption.TouchSEMute;

		if (_isMute)
		{
			_volume = 0f;
		}

		_criAtomSource.volume = _volume;
	}

	public void SetVolume(float volume)
	{
		_volume = volume;
	}

	public void Play(TouchSEType seType)
	{
		if (_isMute)
		{
			return;
		}

		_criAtomSource.Play((int)seType);

	}
}
