﻿using DankagLikeLuaAPI;
using System;
using System.Buffers;
using System.Collections.Generic;
using ELEBEAT;
using UnityEngine;
using UnityEngine.Pool;
using XLua;
using Nito.Collections;

/// <summary>
/// ロングのライン情報管理するクラス
/// </summary>
[LuaCallCSharp]
public sealed class LongController : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] MeshRenderer _meshRenderer;
	[SerializeField] MeshFilter _meshFilter;
	[SerializeField, Range(0, 1f)] float _sizeX;
	[SerializeField] Material _enableStencilMaterial;
	[SerializeField] Material _disableStencilMaterial;

	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<LongController> _pool;
	Transform _transform;
	LongInfo _longInfo;
	MaterialPropertyBlock _mpb;
	BpmHelper _bpmHelper;
	ScrollHelper _scrollHelper;
	IVisibleHold _visibleHoldController;
	AnimationCurve _animationCurve;
	Action<LongController> _luaLongControllerCallBack = null;

	bool _isReleased = false;
	int _touchId = -1;
	bool _isHoldState = false;
	float _hiSpeed = 1.0f;
	float _normalizedSpeed = 0f;
	double _longEndHitTime = 1d;
	bool _isCMod;

	// メッシュに関わる変数
	Mesh _mesh = null;
	Vector3[] _vertices = null;
	static List<ushort> g_sharedTriangles = new List<ushort>(8 * 6);

	// Speedギミック無しの位置
	Deque<Vector2> _normalizePositions = new Deque<Vector2>();

	float _beforeSpeedStretchRatio = 1.0f;
	float _cacheSizeX;
	float _widthScaleFromOption = 1f;
	float _widthScale = 1f;
	float _visualOffset = 0f;
	int _currentIndex = 0;
	bool _enableDefaultMove = true;
	double _scrolledStartPosition;

	//------------------
	// 定数.
	//------------------
	const float BEAT_DISTANCE = Constant.Note.BEAT_DISTANCE;
	const float TIME_DISTANCE = Constant.Note.TIME_DISTANCE;
	const float LANE_DISTANCE = Constant.Note.LANE_DISTANCE;
	const float OFFSET_X = Constant.Note.OFFSET_X;
	const float OFFSET_Z = Constant.Note.OFFSET_Z;

	//------------------
	// プロパティ.
	//------------------
	public bool IsReleased => _isReleased;
	public bool IsHold => _isHoldState;
	public GameObject MyGameObject { get; private set; }
	public float CurrentPosX { get; private set; }
	public double StartBeatPosition { get; private set; }
	public double EndBeatPosition { get; private set; }

	//途中で離した時の入力の開始拍変更に対応
	public double InputStartBeatPosition { get; private set; }
	public double StartTime { get; private set; }
	public double EndTime => _longInfo.EndTime;
	public int TouchId => _touchId;

	public LongType LongType => _longInfo.LongType;
	public LongInfo LongInfo => _longInfo;
	public int StartNoteIndex => _longInfo.NoteIndex[0];
	public int LastNoteIndex => _longInfo.NoteIndex[_longInfo.NoteIndex.Count - 1];

	void OnDestroy()
	{
		if (_vertices is not null && _vertices.Length != 0)
		{
			ArrayPool<Vector3>.Shared.Return(_vertices);
			_vertices = null;
		}

		if (_mesh is not null)
		{
			Destroy(_mesh);
			_mesh = null;
		}
	}

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name="pool"></param>
	/// <param name="bpmHelper"></param>
	[BlackList, DoNotGen]
	public void Init(ObjectPool<LongController> pool, BpmHelper bpmHelper, ScrollHelper scrollHelper, IVisibleHold visibleHoldController)
	{
		_transform = transform;
		_pool = pool;
		MyGameObject = gameObject;

		_mpb = new MaterialPropertyBlock();
		_mesh = new Mesh();
		_meshFilter.mesh = _mesh;

		_hiSpeed = GameParam.Instance.NoteSpeed;

		var instance = GameManager.Instance;
		_normalizedSpeed = Constant.Note.FIXED_BPM / instance.SelectSongInfo.BaseBpm;
		_longEndHitTime = instance.JudgeTimeOption.GetLongEndHitTime() * instance.JudgeTimeOption.MusicRate;
		_isCMod = instance.IsCMod;

		_widthScaleFromOption = instance.NotesOption.Size;
		_visualOffset = instance.DisplayOption.VisualOffset;
		_animationCurve = instance.NoteScrollAnimationCurve;
		_bpmHelper = bpmHelper;
		_scrollHelper = scrollHelper;
		_visibleHoldController = visibleHoldController;
	}

	/// <summary>
	/// ラインの生成(Poolの使いまわし)時に呼ばれる.自身の設定を行なう
	/// </summary>
	/// <param name="longInfo"></param>
	/// <param name="onRelease"></param>
	/// <param name="longTex"></param>
	[BlackList, DoNotGen]
	public void SetParam(LongInfo longInfo, Texture longTex)
	{
		SetHold(false);
		_isReleased = false;
		_touchId = -1;
		_longInfo = longInfo;

		_currentIndex = 0;
		_isHoldState = false;

		// マテリアルのInstanceを避ける.
		_meshRenderer.material = _disableStencilMaterial;
		_mpb.SetTexture(Constant.ShaderProperty.MainTexId, longTex);
		_mpb.SetFloat(Constant.ShaderProperty.Alpha, 1f);
		//_mpb.SetFloat(Constant.ShaderProperty.StencilId, STENCIL_OFF);
		_meshRenderer.SetPropertyBlock(_mpb);

		StartBeatPosition = longInfo.BeatPositions.NonAllocFirst();
		EndBeatPosition = longInfo.BeatPositions.NonAllocLast();
		InputStartBeatPosition = StartBeatPosition;
		_scrolledStartPosition = _scrollHelper.ApplyScroll(StartBeatPosition);

		StartTime = _bpmHelper.BeatToTime(StartBeatPosition);
		_luaLongControllerCallBack = null;
		_enableDefaultMove = true;

		_widthScale = Math.Clamp(_widthScaleFromOption, 0f, 1f);
		_cacheSizeX = _sizeX * _widthScale;

		SetMesh();
	}

	/// <summary>
	/// メッシュを生成してMeshFilterに設定する
	/// </summary>
	[BlackList, DoNotGen]
	void SetMesh()
	{
		var beatPositions = _longInfo.BeatPositions;
		var lanes = _longInfo.Lanes;
		int count = beatPositions.Count;

		_normalizePositions.Clear();

		for (int i = 0; i < count; i++)
		{
			if (_isCMod)
			{
				double diffTime = _bpmHelper.BeatToTime(beatPositions[i]) - StartTime;

				_normalizePositions.AddToBack(new Vector2(
					lanes[i] * LANE_DISTANCE - OFFSET_X,
					(float)(diffTime * TIME_DISTANCE * _hiSpeed)
				));
			}
			else
			{
				_normalizePositions.AddToBack(new Vector2(
					lanes[i] * LANE_DISTANCE - OFFSET_X,
					(float)((_scrollHelper.ApplyScroll(beatPositions[i]) - _scrolledStartPosition) * BEAT_DISTANCE * _normalizedSpeed * _hiSpeed)
				));
			}
		}

		CreateMesh();
	}

	/// <summary>
	/// メッシュ生成
	/// (MEMO: メッシュを生成した後に同一フレームでApplySpeedStretchRatioを呼ばないとメッシュが正しい位置にならない。
	/// その為、ApplySpeedStretchRatioを呼ぶのを忘れないようにすること。)
	/// </summary>
	/// <param name="positions"></param>
	[BlackList, DoNotGen]
	void CreateMesh()
	{
		_mesh.Clear();

		int count = _normalizePositions.Count;

		if (_vertices is not null && _vertices.Length != 0)
		{
			ArrayPool<Vector3>.Shared.Return(_vertices);
		}

		_vertices = ArrayPool<Vector3>.Shared.Rent(2 * count);
		int vertexCount = _vertices.Length;
		var uvs = ArrayPool<Vector2>.Shared.Rent(vertexCount);
		//Vector2[] uvs = new Vector2[vertexCount];
		//var triangles = new NativeArray<ushort>((count - 1) * 2 * 3, Allocator.Temp);

		g_sharedTriangles.Clear();
		var triCount = (count - 1) * 2 * 3;

		if (g_sharedTriangles.Capacity < triCount)
		{
			g_sharedTriangles.Capacity = triCount + 24;
		}

		for (int i = 0; i < triCount; i++)
		{
			g_sharedTriangles.Add(0);
		}

		float halfSizeX = _cacheSizeX * 0.5f;

		// vertices
		int vi = 0;
		for (int i = 0; i < count; i++)
		{
			for (int x = 0; x < 2; x++)
			{
				_vertices[vi++] = new Vector3(_normalizePositions[i].x + (_cacheSizeX * x - halfSizeX), _normalizePositions[i].y, 0);
			}
		}

		// triangles
		int ti = 0;
		for (ushort y = 0; y < count - 1; y++)
		{
			g_sharedTriangles[ti] = (ushort)(y * 2);
			g_sharedTriangles[ti + 1] = g_sharedTriangles[ti + 5] = (ushort)((y + 1) * 2);
			g_sharedTriangles[ti + 2] = g_sharedTriangles[ti + 4] = (ushort)(1 + y * 2);
			g_sharedTriangles[ti + 3] = (ushort)(1 + (y + 1) * 2);
			ti += 6;
		}

		// uvs
		int ui = 0;
		for (int i = 0; i < count; i++)
		{
			for (int x = 0; x < 2; x++)
			{
				uvs[ui++] = new Vector2((float)i / count, x);
			}
		}

		_mesh.SetVertices(_vertices);
		_mesh.SetTriangles(g_sharedTriangles, 0);
		_mesh.SetUVs(0, uvs);

		if (uvs is not null && uvs.Length != 0)
		{
			ArrayPool<Vector2>.Shared.Return(uvs);
		}
		//uvs.Dispose();
	}

	/// <summary>
	/// ロングを掴んでいる指(タッチID)を保持する
	/// </summary>
	/// <param name="touchId"></param>
	public void SetTouchId(int touchId)
	{
		_touchId = touchId;
	}

	/// <summary>
	/// ホールド状態のマテリアルにするか設定
	/// </summary>
	/// <param name="isHold"></param>
	public void SetHold(bool isHold)
	{
		if (isHold != _isHoldState)
		{
			_isHoldState = isHold;

			// MEMO: MaterialPropertyBlockはレンダリングステートを変更できない (Blend、ZWrite、Stencilsなど)
			// そのため、キャッシュ済みのマテリアルを差し替える
			_meshRenderer.material = isHold ? _enableStencilMaterial : _disableStencilMaterial;
			//_meshRenderer.SetPropertyBlock(_mpb);

			if (isHold)
			{
				SetJudgeLineVertices(SpeedStretchRatio.CurrentValue);
			}
		}
	}

	void SetJudgeLineVertices(float speedStretchRatio)
	{
		float halfSizeX = _cacheSizeX * 0.5f;
		float posZ = _transform.localPosition.z;
		var normalizePosition = _normalizePositions[0];

		// 手前のメッシュの頂点は判定ラインに沿わせる
		for (int vi = 0; vi < 2; vi++)
		{
			_vertices[vi] = new Vector3(
				// X
				(_isHoldState ? CurrentPosX : normalizePosition.x) + (_cacheSizeX * vi - halfSizeX),
				// Y
				_isHoldState ?
					-posZ :
					GameParam.Instance.NotesScrollType == NotesScrollType.Constant ?
						normalizePosition.y * speedStretchRatio :
						_animationCurve.Evaluate(normalizePosition.y * speedStretchRatio + posZ) - posZ,
				// Z
				0);
		}

		_mesh.SetVertices(_vertices);
	}

	[BlackList, DoNotGen]
	public void CallUpdate(double beat, double visualBeat, double musicTime, float speedStretchRatio)
	{
		if (_isReleased || _longInfo is null)
		{
			return;
		}

		UpdatePosition(beat, visualBeat, musicTime, speedStretchRatio);
		UpdateMash(beat);
		ApplySpeedStretchRatio(speedStretchRatio);

		if (_isHoldState &&
			_currentIndex < _longInfo.BeatPositions.Count - 1 &&
			(_longInfo.BeatPositions[_currentIndex] < beat && beat < _longInfo.BeatPositions[_currentIndex + 1]))
		{
			SetJudgeLineVertices(speedStretchRatio);
		}

		CheckReleaseNote(musicTime);

	}

	void UpdatePosition(double beat, double visualBeat, double musicTime, float speedStretchRatio)
	{
		CurrentPosX = -100f;
		float posX = _transform.position.x;

		// レーンを跨ぐロングノートで現在のX座標を求める
		int loop = _longInfo.BeatPositions.Count - 1;
		for (int i = 0; i < loop; i++)
		{
			if (_longInfo.BeatPositions[i] < beat && beat < _longInfo.BeatPositions[i + 1])
			{
				CurrentPosX = Mathf.Lerp(
					_longInfo.Lanes[i] * LANE_DISTANCE - OFFSET_X + posX,
					_longInfo.Lanes[i + 1] * LANE_DISTANCE - OFFSET_X + posX,
					(float)((beat - _longInfo.BeatPositions[i]) / (_longInfo.BeatPositions[i + 1] - _longInfo.BeatPositions[i]))
				);
				break;
			}
		}

		if (_enableDefaultMove)
		{
			_transform.localPosition = new Vector3(
				0f,
				0f,
				_isCMod ?
				(float)((StartTime - musicTime) * TIME_DISTANCE * _hiSpeed * speedStretchRatio - (_visualOffset + OFFSET_Z + GameParam.Instance.OffsetZ)) :
				(float)((_scrolledStartPosition - visualBeat) * BEAT_DISTANCE * _normalizedSpeed * _hiSpeed * speedStretchRatio - (_visualOffset + OFFSET_Z + GameParam.Instance.OffsetZ))
			);
		}

		if (_luaLongControllerCallBack is not null)
		{
			_luaLongControllerCallBack(this);
		}
	}

	void UpdateMash(double beat)
	{
		bool hasUpdateMesh = false;

		// 中継を超えた時に中継前のメッシュの頂点を削除
		if (_currentIndex < _longInfo.BeatPositions.Count - 1 &&
			_longInfo.BeatPositions[_currentIndex + 1] < beat &&
			InputStartBeatPosition <= _longInfo.BeatPositions[_currentIndex + 1])
		{
			_normalizePositions.RemoveFromFront();

			hasUpdateMesh = true;
			_currentIndex++;
		}

		if (hasUpdateMesh)
		{
			if (_normalizePositions.Count > 0)
			{
				CreateMesh();
			}
		}
	}

	/// <summary>
	/// スピードの加速減速をメッシュに対応させる
	/// </summary>
	/// <param name="speedStretchRatio"></param>
	void ApplySpeedStretchRatio(float speedStretchRatio)
	{
		float halfSizeX = _cacheSizeX * 0.5f;

		int vi = 0;
		int count = _normalizePositions.Count;
		float posZ = _transform.localPosition.z;

		for (int i = 0; i < count; i++)
		{
			var normalizePosition = _normalizePositions[i];
			float posY = GameParam.Instance.NotesScrollType == NotesScrollType.Constant ?
					normalizePosition.y * speedStretchRatio :
					_animationCurve.Evaluate(normalizePosition.y * speedStretchRatio + posZ) - posZ;

			for (int x = 0; x < 2; x++)
			{
				_vertices[vi++] = new Vector3(normalizePosition.x + (_cacheSizeX * x - halfSizeX), posY, 0);
			}
		}

		_mesh.SetVertices(_vertices);
		_mesh.RecalculateBounds();

		_beforeSpeedStretchRatio = speedStretchRatio;
	}

	void CheckReleaseNote(double musicTime)
	{
		if (_longInfo.EndTime + _longEndHitTime < musicTime)
		{
			Release();
		}
	}

	[BlackList, DoNotGen]
	public void Release()
	{
		if (_isReleased)
		{
			return;
		}

		if (_longInfo is not null)
		{
			_visibleHoldController.SetActive(false, _touchId, _longInfo.NoteIndex[0]);
		}

		_isReleased = true;
		_pool.Release(this);
	}

	[BlackList, DoNotGen]
	public void Show()
	{
		_meshRenderer.enabled = true;
	}

	[BlackList, DoNotGen]
	public void Hide()
	{
		_meshRenderer.enabled = false;
	}

	/// <summary>
	/// 途中で離したときにラインの一部のポジションを取り除いたメッシュを再生成する。
	/// InputStartBeatPositionにロングの再スタート拍数を設定する
	/// </summary>
	/// <param name="index"></param>
	[BlackList, DoNotGen]
	public void RemovePosition(int index)
	{
		var range = 1 + index - (_longInfo.BeatPositions.Count - _normalizePositions.Count);

		_normalizePositions.RemoveRange(0, range);

		CreateMesh();
		ApplySpeedStretchRatio(_beforeSpeedStretchRatio);

		InputStartBeatPosition = _longInfo.BeatPositions[index + 1];

		_currentIndex += range;
	}

	//-------------------
	// Lua用.
	//-------------------

	public void SetTexture(Texture longTex)
	{
		// マテリアルのInstanceを避ける.
		_mpb.SetTexture(Constant.ShaderProperty.MainTexId, longTex);
		_meshRenderer.SetPropertyBlock(_mpb);
	}

	/// <summary>
	/// ゲーム側のロング移動を有効にするか設定します
	/// ロングをLua側で動かしてゲーム側の移動処理が必要ない場合はfalseを指定します
	/// デフォルトはtrueに設定されています
	/// - LuaAPI 1.5
	/// </summary>
	/// <param name="enable"></param>
	public void EnableDefaultMove(bool enable = true)
	{
		_enableDefaultMove = enable;
	}

	/// <summary>
	/// 毎フレーム呼ばれるコールバック関数を設定します
	/// </summary>
	/// <param name="longControllerCallBack"></param>
	public void SetDelegate(Action<LongController> longControllerCallBack)
	{
		_luaLongControllerCallBack = longControllerCallBack;
	}

	/// <summary>
	/// 透明度を設定します
	/// </summary>
	/// <param name="alpha">透明度(0～1)</param>
	public void SetAlpha(float alpha)
	{
		_mpb.SetFloat(Constant.ShaderProperty.Alpha, alpha);
		_meshRenderer.SetPropertyBlock(_mpb);
	}

	/// <summary>
	/// ロングの幅比を設定します
	/// </summary>
	/// <param name="scale">幅比</param>
	public void SetWidthScale(float scale)
	{
		_widthScale = Mathf.Clamp(scale, 0f, 1f);
		_cacheSizeX = _sizeX * _widthScale;

		float halfSizeX = _cacheSizeX * 0.5f;

		int vi = 0;
		int count = _normalizePositions.Count;
		for (int i = 0; i < count; i++)
		{
			var normalizePosition = _normalizePositions[i];
			for (int x = 0; x < 2; x++)
			{
				_vertices[vi++] = new Vector3(normalizePosition.x + (_cacheSizeX * x - halfSizeX), _vertices[vi].y, 0);
			}
		}

		_mesh.SetVertices(_vertices);
	}

	/// <summary>
	/// ロング位置を座標で設定します
	/// </summary>
	/// <param name="x">X座標</param>
	/// <param name="y">Y座標</param>
	/// <param name="z">Z座標</param>
	public void SetPosition(float x, float y, float z)
	{
		_transform.localPosition = new Vector3(x, y, z);
	}

	/// <summary>
	///	0から数えて左から指定したレーンの位置になるようにX座標を設定します
	/// - LuaAPI 1.5
	/// </summary>
	/// <param name="lane"></param>
	public void SetLanePosition(float lane)
	{
		var pos = _transform.localPosition;
		_transform.localPosition = new Vector3((lane * 0.5f) - 1.5f, pos.y, pos.z);
	}

	/// <summary>
	/// 判定までの拍数からノーツのZ座標を計算してZ座標を設定します
	/// (CMODを使用している場合、少し計算量が増えます)
	/// - LuaAPI 1.5
	/// </summary>
	/// <param name="beat">拍数</param>
	public void SetBeatPosition(float beat)
	{
		var pos = _transform.localPosition;

		// 位置.
		float posZ = Util.Instantce.CalculateBeatToNotePositionZ(beat);

		// 反映.
		_transform.localPosition = new Vector3(pos.x, pos.y, posZ);
	}

	/// <summary>
	/// 判定までの時間(秒)からノーツのZ座標を計算してZ座標を設定します
	/// (CMODを使用していない場合、少し計算量が増えます)
	/// - LuaAPI 1.5
	/// </summary>
	/// <param name="time">時間(秒)</param>
	public void SetTimePosition(float time)
	{
		var pos = _transform.localPosition;

		// 位置.
		float posZ = Util.Instantce.CalculateTimeToNotePositionZ(time);

		// 反映.
		_transform.localPosition = new Vector3(pos.x, pos.y, posZ);
	}
}
