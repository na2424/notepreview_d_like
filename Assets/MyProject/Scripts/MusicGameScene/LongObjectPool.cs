﻿using System;
using System.Collections;
using System.Collections.Generic;
using ELEBEAT;
using UnityEngine;
using UnityEngine.Pool;

public sealed class LongObjectPool : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] LongController _longControllerPrefab;

	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<LongController> _pool = null;
	List<LongController> _currentActiveLong = new List<LongController>();
	Transform _transform;
	BpmHelper _bpmHelper;
	ScrollHelper _scrollHelper;
	VisibleHoldController _visibleHoldController;
	int _listCount = 0;

	//------------------
	// 定数.
	//------------------
	// 初期のプールサイズ
	const int DEFAULT_CAPACITY = 8;
	// プールサイズを最大どれだけ大きくするか
	const int MAX_SIZE = 42;
	const int SWAP_THRESHOLD = 3;

	//------------------
	// プロパティ.
	//------------------
	public List<LongController> CurrentActiveLong => _currentActiveLong;
	public int ListCount => _listCount;

	public void Init(BpmHelper bpmHelper, ScrollHelper scrollHelper, VisibleHoldController visibleHoldController)
	{
		_transform = transform;
		_bpmHelper = bpmHelper;
		_scrollHelper = scrollHelper;
		_visibleHoldController = visibleHoldController;
	}

	public ObjectPool<LongController> Pool
	{
		get
		{
			if (_pool is null)
			{
				_pool = new ObjectPool<LongController>(OnCreatePoolObject, OnTakeFromPool, OnReturnedToPool, OnDestroyPoolObject, false, DEFAULT_CAPACITY, MAX_SIZE);
			}

			return _pool;
		}
	}

	LongController OnCreatePoolObject()
	{
		var controller = Instantiate(_longControllerPrefab, _transform);
		controller.Init(Pool, _bpmHelper, _scrollHelper, _visibleHoldController);
		return controller;
	}

	void OnTakeFromPool(LongController controller)
	{
		_listCount++;
		_currentActiveLong.Add(controller);
		controller.Show();
	}

	void OnReturnedToPool(LongController controller)
	{
		_listCount--;
		// if (_listCount > SWAP_THRESHOLD)
		// {
		// 	_currentActiveLong.RemoveBeforeSwapLast(controller);
		// }
		// else
		// {
			_currentActiveLong.Remove(controller);
		//}
		controller.Hide();
	}

	void OnDestroyPoolObject(LongController controller)
	{
		Destroy(controller.MyGameObject);
	}

	public void CallUpdate(double beat, double visualBeat, double musicTime, float speedStretchRatio = 1f)
	{
		int loop = _listCount - 1;

		if (loop < 0)
		{
			return;
		}

		for (int i = loop; i >= 0; i--)
		{
			if (i < _listCount)
			{
				CurrentActiveLong[i].CallUpdate(beat, visualBeat, musicTime, speedStretchRatio);
			}
		}
	}

	/// <summary>
	/// ノーツのサイズ変更に対応
	/// LuaAPI ver1.2用
	/// </summary>
	public void ChangeLongSize()
	{
		float size = GameManager.Instance.NotesOption.Size;

		if (size > 1f)
		{
			size = 1f;
		}

		for (int i = 0; i < _currentActiveLong.Count; i++)
		{
			_currentActiveLong[i].SetWidthScale(size);
		}
	}

	public void OnFinalize()
	{
		if (_pool is not null)
		{
			_pool.Clear();
			_pool.Dispose();
			_pool = null;
		}

		_currentActiveLong.Clear();
		_currentActiveLong = null;
	}
}
