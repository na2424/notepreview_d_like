﻿using System.Collections.Generic;

public sealed class BgChangeHelper
{
	int _bgChangeIndex = 0;
	List<float> _bgChangePositions;
	List<string> _bgChangeImageName;

	public void Init(SongInfo songInfo)
	{
		_bgChangePositions = songInfo.BgChangePositions;
		_bgChangeImageName = songInfo.BgChangeImageName;
	}

	public void SetMidPlay(double beat)
	{
		for (int i = 0; i < _bgChangePositions.Count; i++)
		{
			if (beat <= _bgChangePositions[i])
			{
				_bgChangeIndex = i;
				break;
			}
		}
	}

	/// <summary>
	/// BgChangeを行なうか確認し、背景を変更する
	/// </summary>
	/// <param name="beat">拍数</param>
	public void CheckBgChange(double beat)
	{
		if (_bgChangeIndex < _bgChangePositions.Count && _bgChangePositions[_bgChangeIndex] <= beat)
		{
			BgManager.Instance.ChangeBgImage(_bgChangeImageName[_bgChangeIndex]);

			_bgChangeIndex++;
		}
	}
}
