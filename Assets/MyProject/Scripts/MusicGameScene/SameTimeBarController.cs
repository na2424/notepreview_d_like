﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Pool;

/// <summary>
/// 同時押しラインの情報を管理するクラス
/// </summary>
public sealed class SameTimeBarController : MonoBehaviour
{
	[SerializeField] MeshRenderer _meshRenderer;
	[SerializeField] MeshFilter _meshFilter;

	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<SameTimeBarController> _pool;
	Transform _transform;
	Mesh _mesh = null;
	Vector3[] _vertices = null;
	double _beatPosition = -1f;
	bool _isReleased = false;
	double _justTime = -1d;
	int _leftNoteIndex = -1;
	int _rightNoteIndex = -1;
	float _normalizedSpeed = 0f;
	bool _isCMod;
	float _visualOffset = 0f;

	//------------------
	// 定数.
	//------------------
	const float BEAT_DISTANCE = Constant.Note.BEAT_DISTANCE;
	const float TIME_DISTANCE = Constant.Note.TIME_DISTANCE;
	const float LANE_DISTANCE = Constant.Note.LANE_DISTANCE;
	const float OFFSET_Z = Constant.Note.OFFSET_Z;
	const float WIDTH = 0.041f;

	static readonly ushort[] TRIANGLES = { 0, 1, 2, 0, 2, 3 };

	//------------------
	// プロパティ.
	//------------------
	public bool IsReleased => _isReleased;
	public int LeftNoteIndex => _leftNoteIndex;
	public int RightNoteIndex => _rightNoteIndex;
	AnimationCurve _animationCurve => GameManager.Instance.NoteScrollAnimationCurve;

	public void Init(ObjectPool<SameTimeBarController> pool)
	{
		_isCMod = GameManager.Instance.IsCMod;
		_visualOffset = GameManager.Instance.DisplayOption.VisualOffset;
		float width = WIDTH * GameManager.Instance.NotesOption.SameTimeWidthRate;

		_transform = transform;
		_pool = pool;
		_normalizedSpeed = Constant.Note.FIXED_BPM / GameManager.Instance.SelectSongInfo.BaseBpm;

		if (GameManager.Instance.JudgeTimeOption.Mirror)
		{
			_transform.localScale = new Vector3(1, -1, 1);
		}

		_vertices = new Vector3[4]
		{
			new Vector3(-LANE_DISTANCE * 3f, 0f, -width),
			new Vector3(-LANE_DISTANCE * 3f, 0f, width),
			new Vector3(LANE_DISTANCE * 3f, 0f, width),
			new Vector3(LANE_DISTANCE * 3f, 0f, -width),
		};

		_mesh = new Mesh();
		_meshFilter.mesh = _mesh;
		_mesh.SetVertices(_vertices);
		_mesh.SetTriangles(TRIANGLES, 0);
	}

	public void SetParam(double beatPosition, double justTime, int laneLeft, int laneRight, int leftNoteIndex, int rightNoteIndex)
	{
		_isReleased = false;
		_beatPosition = beatPosition;
		_justTime = justTime;
		float width = WIDTH * GameManager.Instance.NotesOption.SameTimeWidthRate;

		_vertices[0] = new Vector3(LANE_DISTANCE * (laneLeft - 3f), 0f, -width);
		_vertices[1] = new Vector3(LANE_DISTANCE * (laneLeft - 3f), 0f, width);
		_vertices[2] = new Vector3(LANE_DISTANCE * (laneRight - 3f), 0f, width);
		_vertices[3] = new Vector3(LANE_DISTANCE * (laneRight - 3f), 0f, -width);

		_mesh.SetVertices(_vertices);

		_leftNoteIndex = leftNoteIndex;
		_rightNoteIndex = rightNoteIndex;
	}

	public void OnUpdate(double beat, double musicTime, float speedStretchRatio)
	{
		if (_isReleased || !_meshRenderer.enabled)
		{
			return;
		}

		if (_isCMod)
		{
			UpdateCModPosition(musicTime, speedStretchRatio);
		}
		else
		{
			UpdatePosition(beat, speedStretchRatio);
		}

		CheckRelease(musicTime);
	}

	void UpdateCModPosition(double musicTime, float speedStretchRatio)
	{
		double diffTime = _justTime - musicTime;
		float posZ = (float)(diffTime * TIME_DISTANCE * GameParam.Instance.NoteSpeed * speedStretchRatio - (_visualOffset + OFFSET_Z + GameParam.Instance.OffsetZ));

		if (GameParam.Instance.NotesScrollType == NotesScrollType.Decelerate)
		{
			posZ = _animationCurve.Evaluate(posZ);
		}

		if (float.IsNaN(posZ))
		{
			posZ = 100f;
		}

		_transform.localPosition = new Vector3(
			0f,
			0f,
			posZ
		);
	}

	void UpdatePosition(double beat, float speedStretchRatio)
	{
		float posZ = (float)((_beatPosition - beat) * BEAT_DISTANCE * _normalizedSpeed * GameParam.Instance.NoteSpeed * speedStretchRatio - (_visualOffset + OFFSET_Z + GameParam.Instance.OffsetZ));

		if (GameParam.Instance.NotesScrollType == NotesScrollType.Decelerate)
		{
			posZ = _animationCurve.Evaluate(posZ);
		}

		if (float.IsNaN(posZ))
		{
			posZ = 100f;
		}

		_transform.localPosition = new Vector3(
			0f,
			0f,
			posZ
		);
	}

	void CheckRelease(double musicTime)
	{
		if (_justTime < musicTime)
		{
			Release();
		}
	}

	public void Release()
	{
		if (_isReleased)
		{
			return;
		}

		_isReleased = true;
		_pool.Release(this);
	}

	public void Show()
	{
		_meshRenderer.enabled = true;
	}

	public void Hide()
	{
		_meshRenderer.enabled = false;
	}

	void OnDestroy()
	{
		if (_mesh is not null)
		{
			Destroy(_mesh);
			_mesh = null;
		}

		_vertices = null;
	}
}
