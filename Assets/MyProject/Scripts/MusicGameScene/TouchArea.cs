﻿using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.EnhancedTouch;
using UnityEngine.InputSystem.HID;
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;

public enum ScreenTouchPhase : int
{
	None = 0,
	Tap,
	Hold,
	Up
}

public enum JudgeAreaType
{
	Screen = 0,
	World
}

public struct TouchData
{
	public ScreenTouchPhase Phase;
	public float PosX;
	public double TouchTime;
	public int AutoIndex;

	public TouchData(ScreenTouchPhase phase, float posX, double touchTime, int autoIndex = 0)
	{
		Phase = phase;
		PosX = posX;
		TouchTime = touchTime;
		AutoIndex = autoIndex;
	}
}

/// <summary>
/// 入力のタッチの場所等を管理するクラス
/// </summary>
public sealed class TouchArea : InputBase
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] MeshRenderer[] _areaMeshRenderers;
	//[SerializeField] LaneCoverController _laneCoverController;

	public event ActionInput OnInputDown = null;
	public event ActionInput OnInputMove = null;
	public event ActionInput OnInputUp = null;

	//------------------
	// キャッシュ.
	//------------------
	Transform _transform;
	JudgeAreaType _judgeAreaType;
	MusicManager _musicManager = null;
	Camera _mainCamera = null;
	Dictionary<int, int> _touchArea = new Dictionary<int, int>();
	Dictionary<int, float> _holdTouchIdHitPointX = new Dictionary<int, float>();

	bool _isInit = false;
	bool _isJudgePointFrame = false;

	float _frameRevision;
	Transform[] _areaObjectTransforms;

	Vector2 _areaObjectScreenPositionLeft;
	Vector2 _areaObjectScreenPositionRight;
	Vector2 _judgeLineCenterPos;
	Vector2 _centerScreenPoint;
	Vector2 _judgePointCache = Vector2.zero;
	float _JudgeLineToCenterSqrMagnitude = 0f;

	MaterialPropertyBlock _mpb;

	//------------------
	// 定数.
	//------------------
	readonly int _mask = 1 << LayerName.JudgeArea;
	float SQR_REVISION = 45;

	//------------------
	// プロパティ.
	//------------------
	public bool Enable { get; set; }
	public bool IsScreenJudgeArea => _judgeAreaType == JudgeAreaType.Screen;


	void OnDisable()
	{
		//_laneCoverController.OnDisableObject();

		if (!_isInit)
		{
			DisableTouch();
			return;
		}

		Touch.onFingerDown -= OnFingerDown;
		Touch.onFingerMove -= OnFingerMove;
		Touch.onFingerUp -= OnFingerUp;

		DisableTouch();
	}

	/// <summary>
	/// タッチを有効にする
	/// </summary>
	void EnableTouch()
	{
		if (!EnhancedTouchSupport.enabled)
		{
			EnhancedTouchSupport.Enable();
			TouchSimulation.Enable();
		}
	}

	/// <summary>
	/// タッチを無効にする
	/// </summary>
	void DisableTouch()
	{
		if (EnhancedTouchSupport.enabled)
		{
			TouchSimulation.Disable();
			EnhancedTouchSupport.Disable();
		}
	}

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name="isEnable">有効か</param>
	/// <param name="musicManager">MusicManager</param>
	public void Init(bool isEnable, MusicManager musicManager)
	{
		InitAreaObjects();
		//_laneCoverController.Init();
		_transform = GetComponent<Transform>();

		if (!isEnable)
		{
			return;
		}

		_frameRevision = GameManager.Instance.NotesOption.FrameRevision;
		_judgeAreaType = GameManager.Instance.NotesOption.JudgeAreaType;

		_musicManager = musicManager;
		_mainCamera = Camera.main;

		// 表示領域の中心点（ビューポート座標）取得
		var centerViewportPoint = _mainCamera.rect.center;
		// スクリーン座標に変換
		_centerScreenPoint = _mainCamera.ViewportToScreenPoint(centerViewportPoint);

		// 判定エリアタイプがScreenの時の垂直に降ろす地面を更新
		UpdateScreenJudgeLine();

		// EnhancedTouchの有効化
		EnableTouch();

		InputSystem.pollingFrequency = GameManager.Instance.NotesOption.PollingRate;

		Touch.onFingerDown += OnFingerDown;
		Touch.onFingerMove += OnFingerMove;
		Touch.onFingerUp += OnFingerUp;

		// ビームの色を設定
		SetBeamColor();

		_isInit = true;
	}

	/// <summary>
	/// ビームの色を設定
	/// </summary>
	void SetBeamColor()
	{
		var customBeamColor = GameManager.Instance.NotesOption.BeamColor;
		var defaultBeamColor = Constant.Note.DefaultBeamColor;

		Color topColor = GameManager.Instance.NotesOption.IsCustomBeam
			? new Color(customBeamColor.r, customBeamColor.g, customBeamColor.b, 0)
			: new Color(defaultBeamColor.r, defaultBeamColor.g, defaultBeamColor.b, 0);

		Color bottomColor = GameManager.Instance.NotesOption.IsCustomBeam
			? new Color(0f, 0f, 0f, customBeamColor.a)
			: new Color(0f, 0f, 0f, defaultBeamColor.a);

		_mpb = new MaterialPropertyBlock();
		_mpb.SetColor(Constant.ShaderProperty.TopColor, topColor);
		_mpb.SetColor(Constant.ShaderProperty.BottomColor, bottomColor);

		foreach(var renderer in _areaMeshRenderers)
		{
			renderer.SetPropertyBlock(_mpb);
		}
	}

	/// <summary>
	/// 判定エリアタイプがスクリーンの時の判定ラインを更新
	/// </summary>
	public void UpdateScreenJudgeLine()
	{
		var halfWidthOffset = _transform.right * 2f;
		_areaObjectScreenPositionLeft = _mainCamera.WorldToScreenPoint(_transform.position - halfWidthOffset);
		_areaObjectScreenPositionRight = _mainCamera.WorldToScreenPoint(_transform.position + halfWidthOffset);
		_judgeLineCenterPos = (_areaObjectScreenPositionLeft + _areaObjectScreenPositionRight) / 2;
		_JudgeLineToCenterSqrMagnitude = (_judgeLineCenterPos - _centerScreenPoint).sqrMagnitude + SQR_REVISION;
	}

	bool CheckMoveJudgePoint()
	{
		if (_isJudgePointFrame)
		{
			return false;
		}

		_isJudgePointFrame = true;

		Vector2 judgePoint = _mainCamera.WorldToScreenPoint(_transform.position);

		if (!judgePoint.Equals(_judgePointCache))
		{
			_judgePointCache = judgePoint;
			return true;
		}

		return false;
	}

	/// <summary>
	/// エリアオブジェクトの初期化
	/// </summary>
	void InitAreaObjects()
	{
		foreach (var area in _areaMeshRenderers)
		{
			area.enabled = false;
		}

		_areaObjectTransforms = new Transform[_areaMeshRenderers.Length];

		for (int i = 0; i < _areaObjectTransforms.Length; i++)
		{
			_areaObjectTransforms[i] = _areaMeshRenderers[i].transform;
		}
	}

	// 点Pから直線ABに下ろした垂線の足の座標を返す
	Vector2 PerpendicularFootPoint(Vector2 a, Vector2 b, Vector2 p)
	{
		Vector2 ab = (b - a).normalized;
		return a + Vector2.Dot(p - a, ab) * ab;
	}

	/// <summary>
	/// 指が画面に触れた時に呼ばれる.
	/// </summary>
	/// <param name="finger"></param>
	void OnFingerDown(Finger finger)
	{
		if (!Enable)
		{
			return;
		}

		if (CheckMoveJudgePoint())
		{
			UpdateScreenJudgeLine();
		}

		var screenTouchPosition = PerpendicularFootPoint(_areaObjectScreenPositionLeft, _areaObjectScreenPositionRight, finger.currentTouch.screenPosition);

		//Debug.Log("A:" + (finger.currentTouch.screenPosition - screenTouchPosition).sqrMagnitude + "  B:" + _JudgeLineToCenterSqrMagnitude);

		// タッチ設定の判定エリアタイプがスクリーンの場合
		if (IsScreenJudgeArea && (finger.currentTouch.screenPosition - screenTouchPosition).sqrMagnitude > _JudgeLineToCenterSqrMagnitude)
		{
			return;
		}

		Vector3 screenPoint = IsScreenJudgeArea ?
			screenTouchPosition :
			finger.currentTouch.screenPosition;

		Ray ray = _mainCamera.ScreenPointToRay(screenPoint);

		if (Physics.Raycast(ray, out var hit, 30f, _mask))
		{
			int touchId = finger.currentTouch.touchId;

			if (_holdTouchIdHitPointX.ContainsKey(touchId))
			{
				Debug.Log("onDown touchId alredy:" + touchId);
				return;
			}

			// MEMO:フレーム誤差補正について
			//
			// 判定する時の「ジャストからのズレ」をこのようにしたいので、
			// ジャストからのズレ =  [タッチした時の曲の再生位置] - ジャストとなる曲の再生位置
			//
			// フレーム誤差補正を次のように対応しています.
			// [タッチした時の曲の再生位置] = 現在の曲の再生位置 - (現在のゲーム内時間 - タッチした時のゲーム内時間) * フレーム誤差補正(0～1)
			var diffTime = Time.realtimeSinceStartupAsDouble - finger.currentTouch.time;
			_touchData[touchId] = new TouchData(ScreenTouchPhase.Tap, hit.point.x, _musicManager.GetMusicTime() - (diffTime * _frameRevision));
			_touchArea[touchId] = -1;

			OnHit(hit.point.x, touchId);
			_holdTouchIdHitPointX.Add(touchId, hit.point.x);

			// Luaイベント関数呼び出し
			if (OnInputDown is not null)
			{
				OnInputDown(touchId, hit.point.x, finger.currentTouch.screenPosition.x, finger.currentTouch.screenPosition.y);
			}
		}
	}

	/// <summary>
	///	画面に触れた指が動いたときに呼ばれる.
	///	(静止していると呼ばれない).
	/// </summary>
	/// <param name="finger"></param>
	void OnFingerMove(Finger finger)
	{
		if (!Enable)
		{
			return;
		}

		if (CheckMoveJudgePoint())
		{
			UpdateScreenJudgeLine();
		}

		var screenTouchPosition = PerpendicularFootPoint(_areaObjectScreenPositionLeft, _areaObjectScreenPositionRight, finger.currentTouch.screenPosition);

		// タッチ設定の判定エリアタイプがスクリーンの場合
		if (IsScreenJudgeArea && (finger.currentTouch.screenPosition - screenTouchPosition).sqrMagnitude > _JudgeLineToCenterSqrMagnitude)
		{
			return;
		}

		Vector3 screenPoint = IsScreenJudgeArea ?
			screenTouchPosition :
			finger.currentTouch.screenPosition;

		Ray ray = _mainCamera.ScreenPointToRay(screenPoint);

		if (Physics.Raycast(ray, out var hit, 30f, _mask))
		{
			int touchId = finger.currentTouch.touchId;

			if (_holdTouchIdHitPointX.ContainsKey(touchId))
			{
				if (_touchData.ContainsKey(touchId))
				{
					// onFingerDownと同一フレームで呼ばれるとtouchIdが同じ場合_touchData[touchId]に上書きされるため対策している.
					return;
				}
				else
				{
					_touchData[touchId] = new TouchData(ScreenTouchPhase.Hold, hit.point.x, _musicManager.GetMusicTime());
					_holdTouchIdHitPointX[touchId] = hit.point.x;

					// Luaイベント関数呼び出し
					if (OnInputMove is not null)
					{
						OnInputMove(touchId, hit.point.x, finger.currentTouch.screenPosition.x, finger.currentTouch.screenPosition.y);
					}
				}
			}
			else
			{
				// 横から流れるようにタップするとonFingerDownが呼ばれないことがあるため対策している.
				var diffTime = Time.realtimeSinceStartupAsDouble - finger.currentTouch.time;
				_touchData[touchId] = new TouchData(ScreenTouchPhase.Tap, hit.point.x, _musicManager.GetMusicTime() - (diffTime * _frameRevision));
				_holdTouchIdHitPointX.Add(finger.currentTouch.touchId, hit.point.x);

				// Luaイベント関数呼び出し
				if (OnInputDown is not null)
				{
					OnInputDown(touchId, hit.point.x, finger.currentTouch.screenPosition.x, finger.currentTouch.screenPosition.y);
				}
			}

			if (!_touchArea.ContainsKey(touchId))
			{
				_touchArea[touchId] = -1;
			}

			OnHit(hit.point.x, touchId);
		}


	}

	/// <summary>
	/// 指が画面から離れた時に呼ばれる.
	/// </summary>
	/// <param name="finger"></param>
	void OnFingerUp(Finger finger)
	{
		if (!Enable)
		{
			return;
		}

		if (CheckMoveJudgePoint())
		{
			UpdateScreenJudgeLine();
		}

		// MEMO: 入れない方が正しい挙動になる
		//if (IsScreenJudgeArea && IsTopHalfScreen(finger.currentTouch.screenPosition.y))
		//{
		//	return;
		//}

		var touchId = finger.currentTouch.touchId;

		float pointX = float.MaxValue;

		if (_holdTouchIdHitPointX.ContainsKey(touchId))
		{
			pointX = _holdTouchIdHitPointX[touchId];
			_holdTouchIdHitPointX.Remove(touchId);
		}
		else
		{
			Vector3 screenPoint = IsScreenJudgeArea ?
				PerpendicularFootPoint(_areaObjectScreenPositionLeft, _areaObjectScreenPositionRight, finger.currentTouch.screenPosition) :
				finger.currentTouch.screenPosition;

			Ray ray = _mainCamera.ScreenPointToRay(screenPoint);

			if (Physics.Raycast(ray, out var hit, 30f, _mask))
			{
				pointX = hit.point.x;
			}
		}

		var diffTime = Time.realtimeSinceStartupAsDouble - finger.currentTouch.time;
		_touchData[touchId] = new TouchData(ScreenTouchPhase.Up, pointX, _musicManager.GetMusicTime() - (diffTime * _frameRevision));

		if (_touchArea.ContainsKey(touchId) && _touchArea[touchId] != -1)
		{
			_areaMeshRenderers[_touchArea[touchId]].enabled = false;
			_touchArea.Remove(touchId);
		}
		else
		{
			_touchArea[touchId] = -1;
		}

		// Luaイベント関数呼び出し
		if (OnInputUp is not null)
		{
			OnInputUp(touchId, pointX, finger.currentTouch.screenPosition.x, finger.currentTouch.screenPosition.y);
		}
	}

	public override void CallUpdate(double musicTime)
	{
		// InputSystemに静止中(Stationary)のイベントがない対策.
		if (_holdTouchIdHitPointX.Count > 0)
		{
			foreach (var touch in _holdTouchIdHitPointX)
			{
				if (!_touchData.ContainsKey(touch.Key))
				{
					_touchData[touch.Key] = new TouchData(ScreenTouchPhase.Hold, touch.Value, musicTime);
				}
			}
		}

		_isJudgePointFrame = false;
	}

	void OnHit(float xPos, int touchId)
	{
		float minDistance = float.MaxValue;
		int nearArea = 0;

		for (int i = 0; i < _areaObjectTransforms.Length; i++)
		{
			var areaXPos = _areaObjectTransforms[i].localPosition.x;
			var distance = Mathf.Abs(xPos - areaXPos);

			if (distance < minDistance)
			{
				minDistance = distance;
				nearArea = i;
			}
		}

		int area = _touchArea[touchId];

		if (area != -1 && area != nearArea)
		{
			_areaMeshRenderers[area].enabled = false;
		}

		_touchArea[touchId] = nearArea;

		_areaMeshRenderers[nearArea].enabled = true;
	}

	public void OnFinishMusic()
	{
		//_laneCoverController.OnFinishMusic();
	}

	public void SetJudgeAreaType(int type)
	{
		_judgeAreaType = (JudgeAreaType)type;
	}

	public override void OnFinalize()
	{
		_touchArea.Clear();
		_holdTouchIdHitPointX.Clear();
		_touchArea = null;
		_holdTouchIdHitPointX = null;

		base.OnFinalize();
	}


	//--------------------------
	// キーボード入力
	//--------------------------
	public void OnDownKey(int id, int key, double time)
	{
		if (!Enable || _musicManager is null)
		{
			return;
		}

		int touchId = id;

		if (_holdTouchIdHitPointX.ContainsKey(touchId))
		{
			Debug.Log("onDown touchId alredy:" + touchId);
			return;
		}

		var diffTime = Time.realtimeSinceStartupAsDouble - time;

		float posX = _areaObjectTransforms[key].localPosition.x;

		_touchData[touchId] = new TouchData(ScreenTouchPhase.Tap, posX, _musicManager.GetMusicTime() - (diffTime * _frameRevision));
		_touchArea[touchId] = -1;

		OnHit(posX, touchId);
		_holdTouchIdHitPointX.Add(touchId, posX);
	}

	public void OnUpKey(int id, int key, double time)
	{
		if (!Enable || _musicManager is null)
		{
			return;
		}

		var touchId = id;

		float pointX = float.MaxValue;

		if (_holdTouchIdHitPointX.ContainsKey(touchId))
		{
			pointX = _holdTouchIdHitPointX[touchId];
			_holdTouchIdHitPointX.Remove(touchId);
		}
		else
		{
			pointX = _areaObjectTransforms[key].localPosition.x;
		}

		var diffTime = Time.realtimeSinceStartupAsDouble - time;
		_touchData[touchId] = new TouchData(ScreenTouchPhase.Up, pointX, _musicManager.GetMusicTime() - (diffTime * _frameRevision));

		if (_touchArea.ContainsKey(touchId) && _touchArea[touchId] != -1)
		{
			_areaMeshRenderers[_touchArea[touchId]].enabled = false;
			_touchArea.Remove(touchId);
		}
		else
		{
			_touchArea[touchId] = -1;
		}
	}
}
