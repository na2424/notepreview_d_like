﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// ライフゲージの値と描画を管理するクラス
/// </summary>
public sealed class LifeManager : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] Slider _lifeSlider = default;
	[SerializeField] Image _sliderImage = default;
	[SerializeField] Image _sliderBgImage = default;
	[SerializeField] Image _lifeBgImage = default;

	public Action OnDead = null;

	//------------------
	// キャッシュ.
	//------------------
	float _life = MAX_LIFE / 2;
	float _maxLifeValue = MAX_LIFE;
	bool _isDead = false;
	bool _isDanger = false;
	int _second = 0;
	List<float> _secondLifeCache = new List<float>();
	LifeHistory _lifeHistory = new LifeHistory();

	//------------------
	// 定数.
	//------------------
	//TODO:外部ファイルにする
	readonly float PerfectLife = 2f;
	readonly float GrateLife = 1f;
	readonly float GoodLife = 0f;
	readonly float BadLife = -10f;
	readonly float MissLife = -30f;

	const float DANGER_LIFE = 100f;
	const float MAX_LIFE = 500f;

	//------------------
	// プロパティ.
	//------------------

	public float LifeRate => _life / _maxLifeValue;
	public bool IsDanger => _isDanger;
	public bool IsDead => _isDead;
	public LifeHistory LifeHistory => _lifeHistory;

	public void Init()
	{
		_lifeSlider.maxValue = MAX_LIFE;
		_life = MAX_LIFE / 2f;
		_lifeSlider.value = _life;
	}

	public void UpdateLife(JudgeType type)
	{
		if (_isDead)
		{
			return;
		}

		_life += JudgeTypeToLifeValue(type);

		if (_life > MAX_LIFE)
		{
			_life = MAX_LIFE;
		}
		if (_life <= float.Epsilon)
		{
			_isDead = true;
			_life = 0f;
			OnDead?.Invoke();
		}

		if (!_isDanger && _life <= DANGER_LIFE)
		{
			_isDanger = true;
			_lifeBgImage.DOColor(Color.red, 0.78f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
			_sliderImage.DOColor(Color.red, 0.78f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
			_sliderBgImage.DOColor(Color.red, 0.78f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
		}
		if (_isDanger && _life > DANGER_LIFE)
		{
			_isDanger = false;
			_lifeBgImage.DOKill();
			_sliderImage.DOKill();
			_sliderBgImage.DOKill();
			_lifeBgImage.color = Color.white;
			_sliderImage.color = Color.white;
			_sliderBgImage.color = Color.white;
		}

		_lifeSlider.value = _life;
	}

	public void UpdateRecord(double musicTime)
	{
		if (musicTime < 0f)
		{
			return;
		}

		if (_second < musicTime)
		{
			if (_secondLifeCache.Count > 0)
			{
				float secondTotal = 0f;
				int count = _secondLifeCache.Count;

				for (int i = 0; i < count; i++)
				{
					secondTotal += _secondLifeCache[i];
				}

				_lifeHistory.Add(_second, secondTotal / count);
				_secondLifeCache.Clear();
			}

			_second++;
		}
		else
		{
			_secondLifeCache.Add(_life);
		}

	}

	float JudgeTypeToLifeValue(JudgeType type) => type switch
	{
		JudgeType.Brilliant => PerfectLife,
		JudgeType.Great => GrateLife,
		JudgeType.Fast => GoodLife,
		JudgeType.Bad => BadLife * (1 + 0.5f * (_life / MAX_LIFE)),
		JudgeType.Missed => MissLife * (1 + 0.5f * (_life / MAX_LIFE)),
		_ => 0f
	};

	public int GetCurrentLife() => (int)_life;
	public int GetMaxLife() => (int)MAX_LIFE;

	public void SetLife(float lifeRate)
	{
		var life = Mathf.Lerp(0, _maxLifeValue, Mathf.Clamp01(lifeRate));
		_life = life;
		_lifeSlider.value = _life;

		if (_life <= float.Epsilon)
		{
			_isDead = true;
			_life = 0f;
			OnDead?.Invoke();
		}
	}

	public void ReceiveDamage(int damage)
	{
		_life -= damage;
		_lifeSlider.value = _life;

		if (_life <= float.Epsilon)
		{
			_isDead = true;
			_life = 0f;
			OnDead?.Invoke();
		}
	}

	public void OnFinalize()
	{
		_lifeBgImage.DOKill();
		_sliderImage.DOKill();
		_sliderBgImage.DOKill();
		_secondLifeCache.Clear();
		_secondLifeCache = null;
		OnDead = null;
	}
}
