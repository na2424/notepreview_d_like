﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using ELEBEAT;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Pool;
using UnityEngine.ResourceManagement.AsyncOperations;

/// <summary>
/// ノートのオブジェクトを使い回せるようにするクラス.
/// </summary>
public sealed class NoteObjectPool : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	public event Action<JudgeType> OnMissedNote = null;
	public event Action<int, int, int> OnMissedNoteLua = null;

	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<NoteController> _pool = null;
	List<NoteController> _currentActiveNote = new List<NoteController>();
	Transform _transform;

	AsyncOperationHandle<GameObject> _opHandle;
	NoteController _noteControllerPrefab;
	int _listCount = 0;
	Vector3 _noteSize;

	//------------------
	// 定数.
	//------------------
	const string NOTE_PREFAB_ADDRESS = "Prefabs/Note";
	const string NOTE_OLD_PREFAB_ADDRESS = "Prefabs/Note_Legacy";
	// 初期のプールサイズ
	const int DEFAULT_CAPACITY = 32;
	// プールサイズを最大どれだけ大きくするか
	const int MAX_SIZE = 1024;
	// ListのRemove時のメモリコピーを防ぐための入れ替え閾値
	const int SWAP_THRESHOLD = 3;

	//------------------
	// プロパティ.
	//------------------
	public List<NoteController> CurrentActiveNote => _currentActiveNote;
	public int ListCount => _listCount;

	public ObjectPool<NoteController> Pool
	{
		get
		{
			if (_pool is null)
			{
				_pool = new ObjectPool<NoteController>(OnCreatePoolObject, OnTakeFromPool, OnReturnedToPool, OnDestroyPoolObject, false, DEFAULT_CAPACITY, MAX_SIZE);
			}

			return _pool;
		}
	}

	public void Init(bool useAttack)
	{
		_transform = transform;
		InitAsync(useAttack).Forget();
	}

	async UniTask InitAsync(bool useAttack)
	{
		_opHandle = Addressables.LoadAssetAsync<GameObject>(useAttack ? NOTE_PREFAB_ADDRESS : NOTE_OLD_PREFAB_ADDRESS);
		await _opHandle;
		_noteControllerPrefab = _opHandle.Result.GetComponent<NoteController>();
		_noteSize = _noteControllerPrefab.transform.localScale * GameManager.Instance.NotesOption.Size;
		int length = DEFAULT_CAPACITY / 2;
		Pool.WormUp(length);
	}

	NoteController OnCreatePoolObject()
	{
		var noteController = Instantiate(_noteControllerPrefab, _transform);
		noteController.Init(Pool, _noteSize);
		return noteController;
	}

	void OnTakeFromPool(NoteController noteController)
	{
		_listCount++;
		_currentActiveNote.Add(noteController);
		noteController.Show();
	}

	void OnReturnedToPool(NoteController noteController)
	{
		_listCount--;
		if (noteController.IsMissed)
		{
			OnMissedNote?.Invoke(JudgeType.Missed);
			OnMissedNoteLua?.Invoke(noteController.NoteIndex, noteController.Lane, (int)noteController.NoteType);
		}

		if (_listCount > SWAP_THRESHOLD)
		{
			_currentActiveNote.RemoveBeforeSwapLast(noteController);
		}
		else
		{
			_currentActiveNote.Remove(noteController);
		}

		noteController.Hide();
	}

	void OnDestroyPoolObject(NoteController noteController)
	{
		Destroy(noteController.MyGameObject);
	}

	public void CallUpdate(double beat, double musicTime, float speedStretchRatio = 1f)
	{
		int loop = _listCount - 1;

		if (loop < 0)
		{
			return;
		}

		for (int i = loop; i >= 0; i--)
		{
			if (i < _listCount)
			{
				CurrentActiveNote[i].CallUpdate(beat, musicTime, speedStretchRatio);
			}
		}
	}

	public void ChangeNoteSize()
	{
		foreach (var note in _currentActiveNote)
		{
			note.transform.localScale = _noteControllerPrefab.transform.localScale * GameManager.Instance.NotesOption.Size;
		}
	}

	public void AddMissed()
	{
		OnMissedNote?.Invoke(JudgeType.Missed);
	}

	public NoteController GetNote(int noteIndex)
	{
		int count = _currentActiveNote.Count;

		for (int i = 0; i < count; i++)
		{
			if (_currentActiveNote[i].NoteIndex == noteIndex)
				return _currentActiveNote[i];
		}

		return null;
	}

	public void OnFinalize()
	{
		if (_pool is not null)
		{
			_pool.Clear();
			_pool.Dispose();
			_pool = null;
		}

		_currentActiveNote.Clear();
		_currentActiveNote = null;
		if (_opHandle.Status == AsyncOperationStatus.Succeeded)
		{
			Addressables.Release(_opHandle);
		}
	}
}
