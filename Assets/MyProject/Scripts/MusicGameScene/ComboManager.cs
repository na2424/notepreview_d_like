﻿using Cysharp.Text;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// コンボの数と描画を管理するクラス
/// </summary>
public sealed class ComboManager : MonoBehaviour
{
	[SerializeField] RectTransform _rectTransform;
	[SerializeField] TextMeshProUGUI _comboText;
	[SerializeField] TextMeshProUGUI _comboLabel;

	int _currentCombo = 0;
	int _maxCombo = 0;
	int _totalNote = 0;

	float _startTime = 0f;
	bool _isChangeSize = false;

	const float SIZE_TIME = 0.05f;
	const float MIN_SIZE = 0.75f;

	public int CurrentCombo => _currentCombo;
	public int MaxCombo => _maxCombo;
	public bool IsFullCombo => _maxCombo == _totalNote;

	public void Init()
	{
		_currentCombo = 0;
		_comboText.alpha = 0f;
		_comboLabel.alpha = 0f;
	}

	public void SetTotalNote(int totalNote)
	{
		_totalNote = totalNote;
	}

	public void UpdateCombo(JudgeType judgeType)
	{
		if(
			judgeType == JudgeType.Brilliant ||
			judgeType == JudgeType.Great
		)
		{
			_currentCombo++;
			_comboText.SetText(_currentCombo);
			_isChangeSize = true;
			_startTime = Time.realtimeSinceStartup;

			if(_currentCombo > 1)
			{
				_comboText.alpha = 1f;
				_comboLabel.alpha = 1f;
			}
			

			if (_maxCombo < _currentCombo)
			{
				_maxCombo = _currentCombo;
			}
		}
		else
		{
			_currentCombo = 0;
			_comboText.alpha = 0f;
			_comboLabel.alpha = 0f;
		}
	}

	public void CallUpdate()
	{
		if (_isChangeSize)
		{
			ChangeSize();
		}
	}

	void SetActive(bool isActive)
	{
		_comboText.alpha = isActive ? 1f : 0f;//.gameObject.SetActive(isActive);
		_comboLabel.alpha = isActive ? 1f : 0f; //gameObject.SetActive(isActive);
	}

	void ChangeSize()
	{
		float diffTime = Time.realtimeSinceStartup - _startTime;
		_rectTransform.localScale = Vector3.one * (Mathf.Clamp01(diffTime / SIZE_TIME) * (1f - MIN_SIZE) + MIN_SIZE);

		if (diffTime > SIZE_TIME)
		{
			_isChangeSize = false;
		}
	}
}
