﻿using System.Collections.Generic;
using ELEBEAT;
using UnityEngine;
using UnityEngine.Pool;

public sealed class BeatBarObjectPool : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] BeatBarController _beatBarControllerPrefab;

	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<BeatBarController> _pool = null;
	List<BeatBarController> _currentActiveBar = new List<BeatBarController>();
	Transform _transform;
	int _listCount = 0;

	//------------------
	// 定数.
	//------------------
	// 初期のプールサイズ
	const int DEFAULT_CAPACITY = 16;
	// プールサイズを最大どれだけ大きくするか
	const int MAX_SIZE = 64;
	const int SWAP_THRESHOLD = 3;

	//------------------
	// プロパティ.
	//------------------
	public List<BeatBarController> CurrentActiveBar => _currentActiveBar;

	public void Init()
	{
		_transform = transform;
	}

	public ObjectPool<BeatBarController> Pool
	{
		get
		{
			if (_pool is null)
			{
				_pool = new ObjectPool<BeatBarController>(OnCreatePoolObject, OnTakeFromPool, OnReturnedToPool, OnDestroyPoolObject, false, DEFAULT_CAPACITY, MAX_SIZE);
			}

			return _pool;
		}
	}

	BeatBarController OnCreatePoolObject()
	{
		var controller = Instantiate(_beatBarControllerPrefab, _transform);
		controller.Init(Pool);
		return controller;
	}

	void OnTakeFromPool(BeatBarController controller)
	{
		_listCount++;
		controller.Show();
		_currentActiveBar.Add(controller);
	}

	void OnReturnedToPool(BeatBarController controller)
	{
		_listCount--;
		if (_listCount > SWAP_THRESHOLD)
		{
			_currentActiveBar.RemoveBeforeSwapLast(controller);
		}
		else
		{
			_currentActiveBar.Remove(controller);
		}
		controller.Hide();
	}

	void OnDestroyPoolObject(BeatBarController controller)
	{
		Destroy(controller.gameObject);
	}

	public void CallUpdate(double beat, double musicTime, float speedStretchRatio)
	{
		int loop = _listCount - 1;

		if (loop < 0)
		{
			return;
		}

		for (int i = loop; i >= 0; --i)
		{
			if (i < _listCount)
			{
				CurrentActiveBar[i].CallUpdate(beat, musicTime, speedStretchRatio);
			}
		}
	}

	public void OnFinalize()
	{
		if (_pool is not null)
		{
			_pool.Clear();
			_pool.Dispose();
			_pool = null;
		}

		_currentActiveBar.Clear();
		_currentActiveBar = null;
	}
}
