﻿using Cysharp.Text;
using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class KeyboardInput : MonoBehaviour
{
	[SerializeField] private InputActionReference[] _actionRef;
	[SerializeField] private TouchArea _touchArea;
	public event Action OnPressHIKey;

	int _inputId = 0;
	int[] _cacheKeyInputId = new int[7];


	private void Awake()
	{
		if (_actionRef == null) return;

		for (int i = 0; i < _actionRef.Length; i++)
		{
			_actionRef[i].action.performed += OnDown;
			_actionRef[i].action.canceled += OnUp;
			_actionRef[i].action.Enable();
		}
	}

	private void OnDestroy()
	{
		if (_actionRef == null) return;

		for (int i = 0; i < _actionRef.Length; i++)
		{
			_actionRef[i].action.performed -= OnDown;
			_actionRef[i].action.canceled -= OnUp;
			_actionRef[i].action.Dispose();
		}
	}

	private void OnDown(InputAction.CallbackContext context)
	{
		for (int i = 0; i < _actionRef.Length; i++)
		{
			if (context.action == _actionRef[i].action)
			{
				//Debug.Log("Down:" + i);

				int key = i - 1;

				if (key < 0)
				{
					OnPressHIKey();
				}
				else
				{
					if (!_touchArea.Enable)
					{
						return;
					}

					_touchArea.OnDownKey(_inputId, key, context.time);
					_cacheKeyInputId[key] = _inputId;
					_inputId++;
				}
			}
		}
	}

	private void OnUp(InputAction.CallbackContext context)
	{
		for (int i = 0; i < _actionRef.Length; i++)
		{
			if (context.action == _actionRef[i].action)
			{
				//Debug.Log("Up:" + i);

				int key = i - 1;

				if (key < 0)
				{

				}
				else
				{
					if (!_touchArea.Enable)
					{
						return;
					}

					_touchArea.OnUpKey(_cacheKeyInputId[key], key, context.time);
				}
			}
		}
	}

}