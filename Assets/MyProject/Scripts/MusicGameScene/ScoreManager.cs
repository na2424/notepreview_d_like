﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Cysharp.Text;

/// <summary>
/// スコアの情報と描画を管理するクラス
/// </summary>
public sealed class ScoreManager : MonoBehaviour, ICallUpdate
{
	[Header("左上のスコアパネル")]
	[SerializeField] TextMeshProUGUI _scoreText = default;
	[SerializeField] Slider _scoreSlider = default;
	[SerializeField] Image _rankImage = default;
	[SerializeField] RefSpriteScriptableObject _rankSpriteScriptable;

	//------------------
	// キャッシュ.
	//------------------
	int _score = 0;
	int _beforeScore = 0;
	int _viewScore = 0;
	int _totalNote = 0;
	int _currentRank = 4;

	//------------------
	// 定数.
	//------------------
	//TODO:外部ファイルにする
	const float PERFECT_RATIO = 1.0f;
	const float GREAT_RATIO = 0.75f;
	const float FAST_RATIO = 0.25f;
	const float BAD_RATIO = 0.25f;
	const float MISS_RATIO = 0f;

	// 理論値
	const int MAX_SCORE = 1000000;
	// 桁
	const int DIGITS = 7;

	readonly int[] RANK_BOUNDARY = new int[]
	{
		Constant.RankBoundary.S_PLUS,
		Constant.RankBoundary.S,
		Constant.RankBoundary.A,
		Constant.RankBoundary.B,
		Constant.RankBoundary.C
	};

	readonly Color CLEAR_COLOR = new Color(1f, 1f, 1f, 0f);

	//------------------
	// プロパティ.
	//------------------
	public int Score => _score;
	public int CurrentRank => _currentRank;

	public void Init()
	{
		_score = 0;
		_beforeScore = 0;
		_viewScore = 0;
		_scoreSlider.value = 0f;
		_currentRank = JudgeRank(0);
		_rankImage.sprite = _rankSpriteScriptable.GetSprite(_currentRank);
	}

	public void SetTotalNote(int totalNote)
	{
		_totalNote = totalNote;
	}

	public void CallUpdate()
	{
		if (_beforeScore < _score)
		{
			UpdateViewScore();
		}
	}

	/// <summary>
	/// 判定があった時に呼ばれる処理
	/// </summary>
	/// <param name="judge">判定</param>
	public void OnAfterJudge(Judge judge)
	{
		CalculateScore(judge);
	}

	/// <summary>
	/// スコアを計算
	/// </summary>
	/// <param name="judge"></param>
	void CalculateScore(Judge judge)
	{
		_score = Mathf.FloorToInt((
			judge.Brilliant * PERFECT_RATIO +
			judge.Great * GREAT_RATIO +
			(judge.Fast + judge.Slow) * FAST_RATIO +
			judge.Bad * BAD_RATIO +
			judge.Missed * MISS_RATIO
		) / _totalNote * MAX_SCORE);
	}

	/// <summary>
	/// スコア表示を更新
	/// </summary>
	void UpdateViewScore()
	{
		float diff = (_score - _beforeScore);
		_viewScore += Convert.ToInt32(diff * (Time.deltaTime * 3f));

		if (_viewScore >= _score)
		{
			_viewScore = _score;
			_beforeScore = _score;
			_scoreSlider.value = _score;

			int rank = JudgeRank(_score);

			if (_currentRank > rank)
			{
				// 重くなったのでコメントアウト
				//if (!_rankUIParticle.activeSelf && (rank == 0 || rank == 1))
				//{
				//	_rankUIParticle.SetActive(true);
				//}
				_rankImage.sprite = _rankSpriteScriptable.GetSprite(rank);
				var scale = _rankImage.transform.localScale;
				_rankImage.color = CLEAR_COLOR;
				_rankImage.DOFade(1f, 0.5f);
				_rankImage.transform.localScale *= 3f;
				_rankImage.transform.DOScale(scale, 0.5f);

				_currentRank = rank;
			}
		}

		_scoreText.NonAllocateSetText(_viewScore, DIGITS);
	}

	/// <summary>
	/// 減算スコア表示を計算
	/// </summary>
	//void UpdateViewSubtractedScore()
	//{
	//	if (_subtractedScoreType == SubtractedScoreType.Type1)
	//	{
	//		_subtractedScoreText.NonAllocateSetText(_subtractedScore, DIGITS);
	//	}

	//	if (_subtractedScoreType == SubtractedScoreType.Type2)
	//	{
	//		if (!_subtractionScoreTextObj.activeSelf && _subtractedRate > 0f)
	//		{
	//			_subtractedScoreText.gameObject.SetActive(true);
	//		}

	//		if (_isComboCut)
	//		{
	//			_subtractedScoreText.text = ZString.Concat("-", (_subtractedRate * 100).ToString("F2"), "%");
	//		}
	//		else
	//		{
	//			_subtractedScoreText.SetText(-_greatCount);
	//		}

	//		_beforeSubtractedRate = _subtractedRate;
	//	}
	//}

	int JudgeRank(int score)
	{
		for (int i = 0; i < RANK_BOUNDARY.Length; i++)
		{
			if (score >= RANK_BOUNDARY[i])
			{
				return i;
			}
		}

		return RANK_BOUNDARY.Length;
	}

}
