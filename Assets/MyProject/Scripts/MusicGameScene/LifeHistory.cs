using System.Collections.Generic;

public sealed class LifeHistory
{
    List<float> _musicTime = new List<float>();
    List<float> _life = new List<float>();

    public IReadOnlyList<float> MusicTime => _musicTime;
    public IReadOnlyList<float> Life => _life;

    public void Add(double musicTime, double life)
    {
        _musicTime.Add((float)musicTime);
        _life.Add((float)life);
    }
}