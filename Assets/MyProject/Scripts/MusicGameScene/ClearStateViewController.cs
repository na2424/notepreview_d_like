﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public enum ClearStateType : int
{
	GameOver = 0,
	Cleared,
	FullCombo,
	AllBrilliant
}

/// <summary>
/// ゲームの進行や状態を表示するクラス
/// </summary>
public sealed class ClearStateViewController : MonoBehaviour
{
	[SerializeField] Image _stateImage = default;
	[SerializeField] RefSpriteScriptableObject _stateSprite = default;
	[SerializeField] GameObject _effect;
	[SerializeField] float _size = 1f;

	public void Init()
	{
		_stateImage.gameObject.SetActive(false);

		if (_effect != null)
		{
			_effect.SetActive(false);
		}
	}

	public void ShowState(ClearStateType type, bool isPlaySound = false, bool useAnimation = false, bool useEffect = false)
	{
		Sprite sprite = _stateSprite.GetSprite((int)type);
		_stateImage.sprite = sprite;
		_stateImage.rectTransform.sizeDelta = new Vector2(sprite.texture.width, sprite.texture.height) * _size;


		if (useAnimation)
		{
			_stateImage.color = Color.clear;
			_stateImage.transform.localScale = Vector3.one * 3f;
			_stateImage.DOColor(Color.white, 0.3f);
			_stateImage.transform.DOScale(Vector3.one, 0.3f).OnComplete(() =>
			{
				if (_effect != null && useEffect)
				{
					_effect.SetActive(true);
				}

				if(isPlaySound)
				{
					PlaySE(type);
				}
				
			});
		}
		else
		{
			if (isPlaySound)
			{
				PlaySE(type);
			}
		}

		_stateImage.gameObject.SetActive(true);
	}

	void PlaySE(ClearStateType type)
	{
		if (type == ClearStateType.Cleared ||
			type == ClearStateType.FullCombo ||
			type == ClearStateType.AllBrilliant)
		{
			SystemSEManager.Instance.Play(SystemSEType.Clear);
		}
		else
		{
			SystemSEManager.Instance.Play(SystemSEType.Failed);
		}
	}

	public void HideState()
	{
		_stateImage.gameObject.SetActive(false);
		_stateImage.sprite = null;
	}
}
