﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;
using XLua;

namespace DankagLikeLuaAPI
{
	[LuaCallCSharp]
	public sealed class EnemyBullet : MonoBehaviour, IEnemyBullet
	{
		//--------------------------------------
		// インスペクタまたは外部から設定.
		//--------------------------------------
		[SerializeField] SpriteRenderer _spriteRenderer;
		[SerializeField] Transform _transform;
		[SerializeField] TrailRenderer _trailRenderer;

		//------------------
		// キャッシュ.
		//------------------
		ObjectPool<EnemyBullet> _pool;
		Action<EnemyBullet> _luaUpdateFunc;
		Action<EnemyBullet> _luaDelayFunc;
		DanmakuPlayer _danmakuPlayer;

		Vector2 _velocity;
		Vector2 _acceleration = Vector2.zero;
		float _angularVelocity = 0f;
		float _angularAcceleration = 0f;

		bool _isReleased;
		float _startLifeTime = 0f;
		float _lifeTime = 0f;
		float _size = 1f;
		float _hitDistance = 0f;
		int _damage = 0;
		Dictionary<string, string> _attribute = new Dictionary<string, string>();
		int _releaseType = 0;
		bool _isPersistence = false;
		float _resumptionTime = 1f;

		float _rotationSpeed = 0f;

		float _delayTime = 0f;
		int _frame = 0;
		float _waitHitTime = 0f;
		double _beforeMusicTime = 0f;

		bool _isOnceVisible = false;

		//------------------
		// 定数.
		//------------------
		int WAIT_RELEASE_FRAME = 2;

		//------------------
		// プロパティ.
		//------------------
		public SpriteRenderer SpriteRenderer => _spriteRenderer;
		public Transform Transform => _transform;
		public Vector2 Velocity => _velocity;
		public Vector2 Acceleration => _acceleration;
		public float AngularVelocity => _angularVelocity;
		public float AngularAcceleration => _angularAcceleration;
		public float Size => _size;
		public int Damage => _damage;
		public bool IsPersistence => _isPersistence;
		public int ReleaseType => _releaseType;
		public bool IsReleased => _isReleased;


		[BlackList, DoNotGen]
		public void Init(ObjectPool<EnemyBullet> pool, DanmakuPlayer danmakuPlayer)
		{
			_pool = pool;
			_danmakuPlayer = danmakuPlayer;
		}

		[BlackList, DoNotGen]
		public void SetParam(float musicTime, BulletInfo bulletInfo)
		{
			_isReleased = false;
			_isOnceVisible = false;

			_releaseType = bulletInfo.ReleaseType;

			_spriteRenderer.sprite = bulletInfo.Sprite;
			_spriteRenderer.sortingOrder = bulletInfo.SortingOrder;

			_transform.localPosition = new Vector3(bulletInfo.StartPosition.x, 0f, bulletInfo.StartPosition.y);
			_transform.localRotation = Quaternion.Euler(90f, 0f, bulletInfo.Rotation);
			_transform.localScale = Vector3.one * bulletInfo.Size;

			_velocity = bulletInfo.Velocity;
			_startLifeTime = musicTime;
			_size = bulletInfo.Size;

			_hitDistance = bulletInfo.HitDistance;
			_damage = bulletInfo.Damage * 5;
			_attribute.Clear();
			//_attribute = bulletInfo.Attribute;

			foreach(var pair in bulletInfo.Attribute)
			{
				_attribute[pair.Key] = pair.Value;
			}

			_isPersistence = bulletInfo.IsPersistence;
			_resumptionTime = bulletInfo.ResumptionTime;

			_acceleration = bulletInfo.Acceleration;
			_angularVelocity = bulletInfo.AngularVelocity;
			_angularAcceleration = bulletInfo.AngularAcceleration;

			_rotationSpeed = bulletInfo.RotationSpeed;

			_luaUpdateFunc = null;
			_luaUpdateFunc = bulletInfo.UpdateFunc;

			_luaDelayFunc = null;
			_luaDelayFunc = bulletInfo.DelayFunc;

			_delayTime = bulletInfo.DelayTime;
			_lifeTime = bulletInfo.LifeTime;

			_trailRenderer.Clear();
			_trailRenderer.enabled = bulletInfo.UseTrail;
			_trailRenderer.startWidth = bulletInfo.TrailWidth;
			_trailRenderer.time = bulletInfo.TrailTime;

			_frame = 0;
		}

		[BlackList, DoNotGen]
		public void CallUpdate(double musicTime)
		{
			if (_isReleased || !gameObject.activeSelf)
			{
				return;
			}

			Move();
			CheckVisible();

			_luaUpdateFunc?.Invoke(this);

			if (_luaDelayFunc != null)
			{
				if (_startLifeTime + _delayTime < musicTime)
				{
					_luaDelayFunc?.Invoke(this);
					_luaDelayFunc = null;
				}
			}

			if (_waitHitTime > 0f)
			{
				float diffTime = (float)(musicTime - _beforeMusicTime);
				_waitHitTime -= diffTime;
				_beforeMusicTime = musicTime;
			}
			else
			{
				CheckHitPlayer(musicTime);
			}
			CheckRelease(musicTime);
			_frame++;
		}

		void Move()
		{
			var deltaTime = Time.deltaTime;

			_velocity += _acceleration * deltaTime;
			_angularVelocity += _angularAcceleration * deltaTime;

			if (Mathf.Abs(_angularVelocity) > float.Epsilon)
			{
				_velocity = Quaternion.Euler(0, 0, _angularVelocity * deltaTime) * _velocity;
			}

			var currentPos = _transform.localPosition;
			var move = _velocity * deltaTime;

			_transform.localPosition = new Vector3(currentPos.x + move.x, 0f, currentPos.z + move.y);
			_transform.Rotate(new Vector3(0f, 0f, _rotationSpeed * deltaTime)); // = Quaternion.Euler(0f, 0f, _rotationSpeed);

		}

		void CheckHitPlayer(double musicTime)
		{
			if (_isReleased)
			{
				return;
			}

			bool isHit = _danmakuPlayer.CheckHit(_transform.position.x, _transform.position.z, _hitDistance, _damage);

			if (isHit)
			{
				if (_isPersistence)
				{
					_waitHitTime = _resumptionTime;
					_beforeMusicTime = musicTime;
				}
				else
				{
					Release();
				}
			}
		}

		void CheckVisible()
		{
			if (_isOnceVisible)
			{
				return;
			}

			if (_spriteRenderer.isVisible)
			{
				_isOnceVisible = true;
			}
		}

		void CheckRelease(double musicTime)
		{
			if (_startLifeTime + _lifeTime < musicTime)
			{
				Release();
			}

			bool isRelease =
				_releaseType == 0 ? _frame > WAIT_RELEASE_FRAME && !_spriteRenderer.isVisible :
				_releaseType == 1 ? _frame > WAIT_RELEASE_FRAME && _isOnceVisible && !_spriteRenderer.isVisible :
				_releaseType == 2 ? _frame > WAIT_RELEASE_FRAME :
				_frame > WAIT_RELEASE_FRAME && !_spriteRenderer.isVisible;

			if (isRelease)
			{
				Release();
			}
		}

		[DoNotGen]
		public void Release()
		{
			if (_isReleased || !gameObject.activeSelf)
			{
				return;
			}

			if (_trailRenderer.enabled)
			{
				_trailRenderer.Clear();
			}

			_isReleased = true;
			_pool.Release(this);
			_frame = 0;
		}

		//----------------
		// Lua用の関数
		//----------------

		/// <summary>
		/// 速度を設定します
		/// </summary>
		/// <param name="velocity"></param>
		public void SetVelocity(Vector2 velocity)
		{
			_velocity = velocity;
		}

		/// <summary>
		/// 加速度を設定します
		/// </summary>
		/// <param name="accelation"></param>
		public void SetAccelation(Vector2 accelation)
		{
			_acceleration = accelation;
		}

		/// <summary>
		/// 角速度を設定します
		/// </summary>
		/// <param name="angularVelocity"></param>
		public void SetAngularVelocity(float angularVelocity)
		{
			_angularVelocity = angularVelocity;
		}

		/// <summary>
		/// 角加速度を設定します
		/// </summary>
		/// <param name="angularAcceleration"></param>
		public void SetAngularAcceleration(float angularAcceleration)
		{
			_angularAcceleration = angularAcceleration;
		}

		/// <summary>
		/// Spriteを設定します
		/// </summary>
		/// <param name="sprite"></param>
		public void SetSprite(Sprite sprite)
		{
			_spriteRenderer.sprite = sprite;
		}

		/// <summary>
		/// サイズを設定します
		/// </summary>
		/// <param name="size"></param>
		public void SetSize(float size)
		{
			_size = size;
			_transform.localScale = Vector3.one * size;
		}

		/// <summary>
		/// 当たり判定の距離を設定します
		/// </summary>
		/// <param name="hitDistance"></param>
		public void SetHitDistance(float hitDistance)
		{
			_hitDistance = hitDistance;
		}

		/// <summary>
		/// HPの最大値を100とした場合のダメージ量を設定します
		/// </summary>
		/// <param name="damage"></param>
		public void SetDamage(int damage)
		{
			_damage = damage * 5;
		}

		/// <summary>
		/// 画像の回転速度を設定します
		/// </summary>
		/// <param name="dgree"></param>
		public void SetRotationSpeed(float rotationSpeed)
		{
			_rotationSpeed = rotationSpeed;
		}

		/// <summary>
		/// 破棄タイプを設定します
		/// 0 → 生存時間(LifeTime)を過ぎるか画面から見えなくなったら破棄 (デフォルト)
		/// 1 → 生存時間(LifeTime)を過ぎるか一度画面に表示されて見えなくなったら破棄
		/// 2 → 生存時間(LifeTime)を過ぎた時だけ破棄
		/// </summary>
		public void SetReleaseType(int releaseType)
		{
			_releaseType = releaseType;
		}

		/// <summary>
		/// カスタム属性を取得します
		/// 無い場合はnull(nil)を返します
		/// </summary>
		/// <param name="key">属性キー</param>
		public string GetAttribute(string key)
		{
			if (_attribute.TryGetValue(key, out var value))
			{
				return value;
			}

			return null;
		}

		/// <summary>
		/// カスタム属性を設定します
		/// </summary>
		/// <param name="key">属性キー</param>
		/// <param name="value">属性値</param>
		public void SetAttribute(string key, string value)
		{
			_attribute[key] = value;
		}
	}
}