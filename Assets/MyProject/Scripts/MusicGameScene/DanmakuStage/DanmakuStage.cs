﻿using Cysharp.Threading.Tasks;
using System.Threading;
using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{
	[LuaCallCSharp]
	public enum CameraMode : int
	{
		Kagura = -1,
		NearAbove = 0,
		FarAbove
	}

	[LuaCallCSharp]
	public enum LimitMovement : int
	{
		JudgeLine = 0,
		Area
	}

	[LuaCallCSharp]
	public class DanmakuStage : MonoBehaviour, IDanmakuStage
	{
		[SerializeField] Canvas _canvas;
		[SerializeField] DanmakuPlayer _danmakuPlayer;
		[SerializeField] DanmakuEnemy _danmakuEnemy;
		[SerializeField] PlayerBulletObjectPool _playerBulletObjectPool;
		[SerializeField] EnemyBulletObjectPool _enemyBulletObjectPool;
		[SerializeField] CutInAnimationView _cutInAnimationView;
		[SerializeField] CameraSettings _nearAboveCameraSettings;
		[SerializeField] CameraSettings _farAboveCameraSettings;
		[SerializeField] RefSpriteScriptableObject _defaultEnemyBulletSprites;

		//------------------
		// キャッシュ.
		//------------------
		bool _isExecute = false;
		Camera _mainCamera;
		Transform _mainCameraTr;
		LifeManager _lifeManager;

		Vector3 _startCameraPosition;
		Quaternion _startCameraRotation;
		float _startCameraFieldOfView;
		float _currentMusicTime = 0f;

		CameraMode _cameraMode = CameraMode.FarAbove;
		MusicGame _musicGame;
		BulletBuilder _bulletBuilder;

		//------------------
		// プロパティ.
		//------------------
		public DanmakuPlayer Player => _danmakuPlayer;
		public DanmakuEnemy Enemy => _danmakuEnemy;
		public CameraMode CameraMode => _cameraMode;

		[BlackList, DoNotGen]
		public void OnFinalize()
		{
			_playerBulletObjectPool.OnFinalize();
			_enemyBulletObjectPool.OnFinalize();
			_danmakuPlayer.OnFinalize();
			_danmakuEnemy.OnFinalize();
		}

		[BlackList, DoNotGen]
		public void Init(LifeManager lifeManager, MusicGame musicgGame)
		{
			_lifeManager = lifeManager;
			_musicGame = musicgGame;

			_mainCamera = Camera.main;
			_mainCameraTr = _mainCamera.transform;

			_canvas.worldCamera = _mainCamera;
			_canvas.enabled = false;
			_danmakuPlayer.Init(_playerBulletObjectPool, this);

			_startCameraPosition = _mainCameraTr.position;
			_startCameraRotation = _mainCameraTr.rotation;
			_startCameraFieldOfView = _mainCamera.fieldOfView;

			_bulletBuilder = new BulletBuilder(_enemyBulletObjectPool, _defaultEnemyBulletSprites, () => _currentMusicTime);
			_playerBulletObjectPool.Init(_danmakuEnemy);
			_enemyBulletObjectPool.Init(_danmakuPlayer);
			_danmakuEnemy.Init();
			_danmakuEnemy.OnSetCharacter += _cutInAnimationView.SetCharacterSprite;
		}

		[BlackList, DoNotGen]
		public void CallUpdate(double musicTime)
		{
			_currentMusicTime = (float)musicTime;

			if (!_isExecute)
			{
				return;
			}

			_playerBulletObjectPool.CallUpdate(musicTime);
			_enemyBulletObjectPool.CallUpdate(musicTime);
			_danmakuPlayer.CallUpdate(musicTime);
			_danmakuEnemy.CallUpdate();
		}

		async UniTask StartCutIn()
		{
			// CancellationTokenSourceを作成
			var cts = new CancellationTokenSource();
			// CancellationTokenを取得
			var token = cts.Token;

			await _cutInAnimationView.StartCutIn(token);

			cts.Cancel();
		}

		//-------------------
		// LuaAPI
		//-------------------

		/// <summary>
		/// 弾幕ステージを開始します
		/// </summary>
		public void Begin()
		{
			if (_isExecute)
			{
				return;
			}

			_isExecute = true;
			_bulletBuilder.SetExecute(true);
			_danmakuPlayer.OnStartDanmakuStage();
			_danmakuEnemy.OnStartDanmakuStage();
			SetCameraMode(CameraMode.FarAbove);

			Player.OnHit += _lifeManager.ReceiveDamage;

			StartCutIn().Forget();
		}

		/// <summary>
		/// ダンマクステージを終了します
		/// </summary>
		public void End()
		{
			if (!_isExecute)
			{
				return;
			}

			_isExecute = false;
			_canvas.enabled = false;
			_bulletBuilder.SetExecute(false);
			_danmakuPlayer.OnEndDanmakuStage();
			_danmakuEnemy.OnEndDanmakuStage();
			_playerBulletObjectPool.ReleaseAll();
			_enemyBulletObjectPool.ReleaseAll();
			SetCameraMode(CameraMode.Kagura);
			SystemSEManager.Instance.Play(SystemSEType.DanmakuStageEnd);

			if (_lifeManager != null && Player != null)
			{
				Player.OnHit -= _lifeManager.ReceiveDamage;
			}
		}

		/// <summary>
		/// 敵弾生成を行なうクラスを取得します
		/// メソッドチェーンの形で設定を記述して、
		/// 最後にBuild関数を呼ぶことで弾が発射されます
		/// </summary>
		/// <returns>BulletBuilder</returns>
		public IBulletBuilder GetBulletBuilder()
		{
			return _bulletBuilder;
		}

		/// <summary>
		/// 弾幕ステージの自機クラスを取得します
		/// </summary>
		/// <returns>DanmakuPlayer</returns>
		public DanmakuPlayer GetPlayer()
		{
			return _danmakuPlayer;
		}

		/// <summary>
		/// 弾幕ステージの敵クラスを取得します
		/// </summary>
		/// <returns>DanmakuPlayer</returns>
		public DanmakuEnemy GetEnemy()
		{
			return _danmakuEnemy;
		}

		/// <summary>
		/// 敵キャラクターのSpriteを設定します
		/// カットイン演出のキャラクター画像も変更されます
		/// SpriteはUTILクラスの関数を使用して生成できます
		/// </summary>
		/// <param name="sprite"></param>
		public void SetEnemySprite(Sprite sprite)
		{
			_danmakuEnemy.SetSprite(sprite);
			_cutInAnimationView.SetCharacterSprite(sprite);
		}

		/// <summary>
		/// プレイヤーの移動方法の種類を設定します
		/// 0 → 指を動かした差分
		/// 1 → 指の位置
		/// </summary>
		/// <param name="moveType"></param>
		public void SetMoveTypeInt(int moveType)
		{
			_danmakuPlayer.SetPlayerMoveTypeInt(moveType);
		}

		/// <summary>
		/// カメラモードを設定します(int版)
		/// 0 → 近い上空
		/// 1 → 遠い上空
		/// デフォルトは1に設定されています
		/// </summary>
		/// <param name="cameraMode"></param>
		public void SetCameraModeInt(int cameraMode)
		{
			_cameraMode = (CameraMode)cameraMode;
			SetCameraMode(CameraMode);
		}

		/// <summary>
		/// カメラモードを設定します
		/// </summary>
		/// <param name="cameraMode"></param>
		public void SetCameraMode(CameraMode cameraMode)
		{
			_cameraMode = cameraMode;

			switch (_cameraMode)
			{
				case CameraMode.Kagura:
					_mainCameraTr.SetPositionAndRotation(
						_startCameraPosition,
						_startCameraRotation
					);
					_mainCamera.fieldOfView = _startCameraFieldOfView;
					_musicGame.SetNotesScrollType(true);
					break;
				case CameraMode.NearAbove:
					_mainCameraTr.SetPositionAndRotation(
						_nearAboveCameraSettings.Position,
						_nearAboveCameraSettings.Rotation
					);
					_mainCamera.fieldOfView = _nearAboveCameraSettings.FieldOfView;
					_musicGame.SetNotesScrollType(false);
					_canvas.enabled = true;
					break;
				case CameraMode.FarAbove:
					_mainCameraTr.SetPositionAndRotation(
						_farAboveCameraSettings.Position,
						_farAboveCameraSettings.Rotation
					);
					_mainCamera.fieldOfView = _farAboveCameraSettings.FieldOfView;
					_musicGame.SetNotesScrollType(false);
					_canvas.enabled = true;
					break;
			}
		}

		/// <summary>
		/// 自機の移動範囲を判定ライン上に制限するか設定します
		/// デフォルトはfalseに設定されています
		/// </summary>
		/// <param name="isLimit"></param>
		public void SetLimitPosition(bool isLimit)
		{
			_danmakuPlayer.SetLimitJudgeLine(isLimit);
		}

		/// <summary>
		/// 指定した位置からプレイヤーへの方向ベクトルを取得します
		/// </summary>
		/// <param name="startPosition"></param>
		/// <returns></returns>
		public Vector2 ToPlayerAiming(Vector2 startPosition)
		{
			return (_danmakuPlayer.Position - startPosition).normalized;
		}

	}

}
