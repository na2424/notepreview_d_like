﻿using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class CutInAnimationView : MonoBehaviour
{
	[SerializeField] GameObject _cutInUI;
	[SerializeField] Animation _animation;
	[SerializeField] Image _characterImage;

	public async UniTask StartCutIn(CancellationToken token)
	{
		_cutInUI.SetActive(true);

		_animation.Play();
		SystemSEManager.Instance.Play(SystemSEType.CutIn);

		while (true)
		{
			await UniTask.Yield();

			if (!_animation.isPlaying)
			{
				break;
			}

			if (token.IsCancellationRequested)
			{
				break;
			}
		}

		_cutInUI.SetActive(false);
	}

	public void SetCharacterSprite(Sprite sprite)
	{
		_characterImage.sprite = sprite;
	}
}