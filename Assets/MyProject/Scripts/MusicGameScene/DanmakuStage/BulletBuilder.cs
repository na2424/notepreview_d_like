﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{
	public sealed class BulletInfo
	{
		public Vector2 StartPosition = new Vector2(0, 5);
		public Vector2 Velocity;
		public float LifeTime = 10f;
		public float HitDistance = 0.15f;
		public float Size = 0.3f;
		public int Damage = 10;
		public Sprite Sprite = null;

		public Vector2 Acceleration = Vector2.zero;
		public float AngularVelocity = 0f;
		public float AngularAcceleration = 0f;
		public float Rotation = 0f;
		public float RotationSpeed = 0f;
		public Action<EnemyBullet> UpdateFunc = null;
		public Action<EnemyBullet> DelayFunc = null;
		public float DelayTime = 0f;
		public bool UseTrail = false;
		public float TrailWidth = 0.07f;
		public float TrailTime = 0.5f;
		public int SortingOrder = 3;
		public Dictionary<string, string> Attribute = new Dictionary<string, string>();
		public int ReleaseType = 0;
		public bool IsPersistence = false;
		public float ResumptionTime = 1f;

		public void Reset()
		{
			StartPosition = new Vector2(0, 3);
			Velocity = new Vector2(0, -1);
			LifeTime = 10f;
			HitDistance = 0.15f;
			Size = 0.3f;
			Damage = 10;
			Sprite = null;

			Acceleration = Vector2.zero;
			AngularVelocity = 0f;
			AngularAcceleration = 0f;
			Rotation = 0f;
			RotationSpeed = 0f;
			UpdateFunc = null;
			DelayFunc = null;
			DelayTime = 0f;
			UseTrail = false;
			TrailWidth = 0.07f;
			TrailTime = 0.5f;
			SortingOrder = 3;
			Attribute.Clear();
			ReleaseType = 0;
			IsPersistence = false;
			ResumptionTime = 1f;
		}
	}

	[LuaCallCSharp]
	public sealed class BulletBuilder : IBulletBuilder
	{
		EnemyBulletObjectPool _bulletObjectPool = null;
		RefSpriteScriptableObject _defaultEnemyBulletSprites = null;
		BulletInfo _bulletInfo = null;
		Func<float> _onBuild = null;
		bool _isExecute = false;

		[BlackList, DoNotGen]
		public BulletBuilder(EnemyBulletObjectPool pool, RefSpriteScriptableObject refSpriteScriptableObject, Func<float> onBuild)
		{
			_bulletObjectPool = pool;
			_defaultEnemyBulletSprites = refSpriteScriptableObject;
			_bulletInfo = new BulletInfo();
			_onBuild = onBuild;
		}

		[BlackList, DoNotGen]
		public void SetExecute(bool isExecute)
		{
			_isExecute = isExecute;
		}

		void ResetParam()
		{
			_bulletInfo.Reset();
		}

		/// <summary>
		/// 基礎パラメータを設定します
		/// With～関数より前に記述する必要があります
		/// </summary>
		/// <param name="startPosition">発射位置</param>
		/// <param name="initialVelocity">初速度</param>
		/// <param name="hitDistance">当たり判定距離</param>
		/// <param name="size">見た目の大きさ</param>
		/// <param name="damage">ダメージ量</param>
		/// <returns></returns>
		public BulletBuilder SetBasicParam(Vector2 startPosition, Vector2 initialVelocity, float hitDistance = 0.15f, float size = 0.3f, int damage = 10)
		{
			ResetParam();

			_bulletInfo.StartPosition = startPosition;
			_bulletInfo.Velocity = initialVelocity;
			_bulletInfo.LifeTime = 10f;
			_bulletInfo.HitDistance = hitDistance;
			_bulletInfo.Size = size;
			_bulletInfo.Damage = damage;
			return this;
		}

		/// <summary>
		/// アプリに組み込まれている弾画像のSpriteを数値で設定(0～7)します
		/// </summary>
		/// <param name="spriteTypeInt">弾画像の種類のint</param>
		/// <returns></returns>
		public BulletBuilder WithBasicSprite(int spriteTypeInt)
		{
			if (spriteTypeInt < 0 || 7 < spriteTypeInt)
			{
				spriteTypeInt = 0;
			}

			_bulletInfo.Sprite = _defaultEnemyBulletSprites.GetSprite(spriteTypeInt);
			return this;
		}

		/// <summary>
		/// 弾のSpriteを設定します
		/// </summary>
		public BulletBuilder WithSprite(Sprite sprite)
		{
			_bulletInfo.Sprite = sprite;
			return this;
		}

		/// <summary>
		/// 加速度を設定します
		/// </summary>
		public BulletBuilder WithAcceleration(Vector2 acceleration)
		{
			_bulletInfo.Acceleration = acceleration;
			return this;
		}

		/// <summary>
		/// 角速度を設定します
		/// </summary>
		public BulletBuilder WithAngularVelocity(float angularVelocity)
		{
			_bulletInfo.AngularVelocity = angularVelocity;
			return this;
		}

		/// <summary>
		/// 角加速度を設定します
		/// </summary>
		public BulletBuilder WithAngularAcceleration(float angularAcceleration)
		{
			_bulletInfo.AngularAcceleration = angularAcceleration;
			return this;
		}

		/// <summary>
		/// 画像の回転角度を設定します
		/// </summary>
		public BulletBuilder WithRotation(float rotation)
		{
			_bulletInfo.Rotation = rotation;
			return this;
		}

		/// <summary>
		/// 画像の回転速度を設定します
		/// </summary>
		public BulletBuilder WithRotationSpeed(float rotationSpeed)
		{
			_bulletInfo.RotationSpeed = rotationSpeed;
			return this;
		}

		/// <summary>
		/// 弾の生存時間(秒)を設定します
		/// 画面外に出るか、生存時間が過ぎると弾が消えます
		/// </summary>
		public BulletBuilder WithLifeTime(float lifeTime)
		{
			_bulletInfo.LifeTime = lifeTime;
			return this;
		}

		/// <summary>
		/// 弾の軌跡を設定します
		/// </summary>
		/// <param name="width"></param>
		/// <param name="time"></param>
		/// <returns></returns>
		public BulletBuilder WithTrail(float width = 0.07f, float time = 0.5f)
		{
			_bulletInfo.UseTrail = true;
			_bulletInfo.TrailWidth = width;
			_bulletInfo.TrailTime = time;
			return this;
		}

		/// <summary>
		/// 毎フレーム呼ばれる関数を設定します
		/// 呼ばれる関数の引数にはEnemyBulletクラスの参照が取得されます
		/// </summary>
		/// <param name="luaFunction">引数を1つ持つLuaの関数</param>
		/// <returns></returns>
		public BulletBuilder WithUpdateFunc(Action<EnemyBullet> luaFunction)
		{
			_bulletInfo.UpdateFunc = luaFunction;
			return this;
		}

		/// <summary>
		/// 指定した秒数後に呼ばれる関数を設定します
		/// 呼ばれる関数の引数にはEnemyBulletクラスの参照が取得されます
		/// </summary>
		/// <param name="luaFunction">引数を1つ持つLuaの関数</param>
		/// <param name="delayTime">弾の発生後からの秒数</param>
		/// <returns></returns>
		public BulletBuilder WithDelayFunc(Action<EnemyBullet> luaFunction, float delayTime)
		{
			_bulletInfo.DelayFunc = luaFunction;
			_bulletInfo.DelayTime = delayTime;
			return this;
		}

		/// <summary>
		/// SortingOrder(重ね順)を設定します
		/// デフォルトは3に設定されています
		/// </summary>
		public BulletBuilder WithSortingOrder(int sortingOrder)
		{
			_bulletInfo.SortingOrder = sortingOrder;
			return this;
		}

		/// <summary>
		/// カスタム属性を設定します
		/// </summary>
		/// <param name="key">属性キー</param>
		/// <param name="value">属性値</param>
		public BulletBuilder WithAttribute(string key, string value)
		{
			_bulletInfo.Attribute[key] = value;
			return this;
		}

		/// <summary>
		/// 破棄タイプを設定します
		/// 0 → 生存時間(LifeTime)を過ぎるか画面から見えなくなったら破棄 (Default)
		/// 1 → 生存時間(LifeTime)を過ぎるか一度画面に表示されて見えなくなったら破棄
		/// 2 → 生存時間(LifeTime)を過ぎた時だけ破棄
		/// </summary>
		/// <param name="releaseType"></param>
		/// <returns></returns>
		public BulletBuilder WithReleaseType(int releaseType)
		{
			_bulletInfo.ReleaseType = releaseType;
			return this;
		}

		/// <summary>
		/// 弾が自機に当たっても消えないようにするか
		/// (isPersistenceがtrueの時、resumptionTimeに設定した秒数の後にヒット判定が再開されます)
		/// </summary>
		/// <param name="isPersistence">持続性の弾にするか</param>
		public BulletBuilder WithPersistence(bool isPersistence, float resumptionTime = 1f)
		{
			_bulletInfo.IsPersistence = isPersistence;
			_bulletInfo.ResumptionTime = resumptionTime;
			return this;
		}

		/// <summary>
		/// 弾設定を取得します
		/// </summary>
		public BulletInfo GetBulletInfo()
		{
			return _bulletInfo;
		}

		/// <summary>
		/// 弾設定をBulletBuilderに反映します
		/// </summary>
		public void SetBulletInfo(BulletInfo bulletInfo)
		{
			_bulletInfo = bulletInfo;
		}

		/// <summary>
		/// 設定を構築して弾を発射します
		/// </summary>
		public void Build()
		{
			if (!_isExecute)
			{
				return;
			}

			if (_bulletInfo.Sprite == null)
			{
				WithBasicSprite(0);
			}

			float startLifeTime = _onBuild();
			var bullet = _bulletObjectPool.Pool.Get();
			bullet.SetParam(startLifeTime, _bulletInfo);
		}
	}
}