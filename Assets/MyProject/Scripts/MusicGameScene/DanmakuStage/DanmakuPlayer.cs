﻿using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;
using XLua;

namespace DankagLikeLuaAPI
{
	/// <summary>
	/// プレイヤーの移動方法の種類
	/// 0 → 指を動かした差分
	/// 1 → 指の位置
	/// </summary>
	[LuaCallCSharp]
	public enum PlayerMoveType : int
	{
		Delta = 0,
		Direct
	}

	[LuaCallCSharp]
	public sealed class DanmakuPlayer : MonoBehaviour, IDanmakuPlayer
	{
		//--------------------------------------
		// インスペクタまたは外部から設定.
		//--------------------------------------
		[SerializeField] Transform _transform;
		[SerializeField] SpriteRenderer _spriteRenderer;
		[SerializeField] CanvasScaler _canvasScaler;
		[SerializeField] PlayerMoveType _playerMoveType;
		[SerializeField] GameObject[] _playerOptions;
		[SerializeField] Sprite _bulletSprite;
		[SerializeField] float _shotIntervalTime = 0.1f;

		public event Action<int> OnHit = null;

		//------------------
		// キャッシュ.
		//------------------
		DanmakuStage _danmakuStage;
		PlayerBulletObjectPool _bulletPool;
		Camera _mainCamera;
		Color _playerColor = Color.white;

		bool _isExecute = false;
		bool _isLimitJudgeLine = false;

		Vector2 _areaObjectScreenPositionLeft;
		Vector2 _areaObjectScreenPositionRight;
		DG.Tweening.Sequence _sequence;

		Vector2 _beforeScreenTouchPosition = Vector2.zero;
		int _minPosX = 0;
		int _maxPosX = 0;
		int _minPosY = 0;
		int _maxPosY = 0;

		bool _isInputDowned = false;
		bool[] _useHoming = new bool[2] { false, false };

		BulletInfo _bulletInfo;
		BulletInfo _homingBulletInfo;

		//--------------
		// 定数.
		//--------------
		const float LANE_EDGE_X = 1.75f;
		readonly int LAYER_MASK = 1 << LayerName.DanmakuStage;
		const int SCREEN_HEIGHT_FIT = 720;
		const float SPEED = 5;
		static readonly Quaternion START_ROT = Quaternion.Euler(90f, 0f, 0f);

		//--------------
		// プロパティ.
		//--------------
		public Vector2 Position => new Vector2(_transform.position.x, _transform.position.z);

		[BlackList, DoNotGen]
		public void Init(PlayerBulletObjectPool pool, DanmakuStage danmakuStage)
		{
			_bulletPool = pool;
			_danmakuStage = danmakuStage;

			_mainCamera = Camera.main;
			var halfWidthOffset = _transform.right * 2f;
			_areaObjectScreenPositionLeft = _mainCamera.WorldToScreenPoint(_transform.position - halfWidthOffset);
			_areaObjectScreenPositionRight = _mainCamera.WorldToScreenPoint(_transform.position + halfWidthOffset);

			gameObject.SetActive(false);
			_sequence = DOTween.Sequence();

			_minPosY = 0;
			_maxPosY = Screen.height;
			_minPosX = Mathf.Max(0, (int)(Screen.width / 2 - SCREEN_HEIGHT_FIT * _maxPosY / _canvasScaler.referenceResolution.y));
			_maxPosX = Mathf.Min(Screen.width, (int)(Screen.width / 2 + SCREEN_HEIGHT_FIT * _maxPosY / _canvasScaler.referenceResolution.y));

			_bulletInfo = new BulletInfo()
			{
				StartPosition = Position,
				Velocity = new Vector2(0, 5),
				HitDistance = 0.35f,
				Size = 0.07f,
				Damage = 10,
				Sprite = _bulletSprite
			};

			var sqrt = Mathf.Sqrt(SPEED);

			_homingBulletInfo = new BulletInfo()
			{
				StartPosition = Position,
				Velocity = new Vector2(sqrt, sqrt),
				HitDistance = 0.35f,
				Size = 0.07f,
				Damage = 10,
				Sprite = _bulletSprite
			};
		}

		[BlackList, DoNotGen]
		public void OnFinalize()
		{
			_sequence.Kill();
			_sequence = null;
			OnHit = null;
		}

		[BlackList, DoNotGen]
		public void OnStartDanmakuStage()
		{
			_isExecute = true;
			ResetPlayer();
			gameObject.SetActive(true);
		}

		[BlackList, DoNotGen]
		public void OnEndDanmakuStage()
		{
			_isExecute = false;
			gameObject.SetActive(false);
		}

		double nextTime = 0f;

		[BlackList, DoNotGen]
		public void CallUpdate(double musicTime)
		{
			if (!_isExecute)
			{
				return;
			}

			switch (_playerMoveType)
			{
				case PlayerMoveType.Delta:
					MoveDelta();
					break;

				case PlayerMoveType.Direct:
					MoveDirect();
					break;
			}

			Shot(musicTime);
		}

		bool GetButtonDown()
		{
#if UNITY_STANDALONE
			// クリック（PC）
			if (Input.GetMouseButtonDown(0))
			{
				return true;
			}
#else
			// タッチ（スマホ）
			if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
			{
				return true;
			}
#endif
			return false;
		}

		bool GetButton()
		{
#if UNITY_STANDALONE
			// クリック（PC）
			if (Input.GetMouseButton(0))
			{
				return true;
			}
#else
			// タッチ（スマホ）
			if (Input.touchCount > 0)
			{
				var phase = Input.GetTouch(0).phase;

				if (phase == TouchPhase.Began ||
					phase == TouchPhase.Stationary ||
					phase == TouchPhase.Moved
				)
				{
					return true;
				}
			}
#endif
			return false;
		}

		bool GetButtonUp()
		{
#if UNITY_STANDALONE
			// クリック（PC）
			if (Input.GetMouseButtonUp(0))
			{
				return true;
			}
#else
			// タッチ（スマホ）
			if (Input.touchCount > 0 &&
				(Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled))
			{
				return true;
			}
#endif
			return false;
		}

		/// <summary>
		/// タッチされている位置
		/// </summary>
		private Vector2 TouchPosition()
		{
#if UNITY_STANDALONE
			return (Vector2)Input.mousePosition;
#else
			return Input.GetTouch(0).position;
#endif
		}

		void MoveDelta()
		{
			if (GetButtonDown())
			{
				_beforeScreenTouchPosition = _isLimitJudgeLine ?
					Calculate.PerpendicularFootPoint(_areaObjectScreenPositionLeft, _areaObjectScreenPositionRight, TouchPosition()) :
					TouchPosition();
			}

			if (GetButton())
			{
				var screenTouchPosition = _isLimitJudgeLine ?
					Calculate.PerpendicularFootPoint(_areaObjectScreenPositionLeft, _areaObjectScreenPositionRight, TouchPosition()) :
					TouchPosition();

				var delta = screenTouchPosition - _beforeScreenTouchPosition;
				_beforeScreenTouchPosition = screenTouchPosition;

				var nextPoint = _mainCamera.WorldToScreenPoint(_transform.localPosition) + (Vector3)delta;

				// 画面枠内に収める
				nextPoint = new Vector3(
					Mathf.Clamp(nextPoint.x, _minPosX, _maxPosX),
					Mathf.Clamp(nextPoint.y, _minPosY, _maxPosY),
					0f
				);

				Ray ray = _mainCamera.ScreenPointToRay(nextPoint);

				if (Physics.Raycast(ray, out var hit, 50f, LAYER_MASK))
				{
					_transform.localPosition = _isLimitJudgeLine
						? new Vector3(Mathf.Clamp(hit.point.x, -LANE_EDGE_X, LANE_EDGE_X), 0f, 0f)
						: new Vector3(hit.point.x, 0f, hit.point.z);
				}
			}

			if (GetButtonUp())
			{
				if (Input.touchCount > 0)
				{
					for (int i = 0; i < Input.touches.Length; i++)
					{
						var phase = Input.touches[i].phase;
						if (phase == TouchPhase.Stationary || phase == TouchPhase.Moved)
						{
							_beforeScreenTouchPosition = _isLimitJudgeLine ?
								Calculate.PerpendicularFootPoint(_areaObjectScreenPositionLeft, _areaObjectScreenPositionRight, Input.touches[i].position) :
								Input.touches[i].position;

							break;
						}
					}
				}
			}
		}

		void MoveDirect()
		{
			if (GetButtonDown())
			{
				_isInputDowned = true;
			}

			if (_isInputDowned && GetButton())
			{
				var screenTouchPosition = _isLimitJudgeLine ?
					Calculate.PerpendicularFootPoint(_areaObjectScreenPositionLeft, _areaObjectScreenPositionRight, TouchPosition()) :
					TouchPosition();

				// 画面枠内に収める
				var nextPoint = new Vector3(
					Mathf.Clamp(screenTouchPosition.x, _minPosX, _maxPosX),
					Mathf.Clamp(screenTouchPosition.y, _minPosY, _maxPosY),
					0f
				);

				Ray ray = _mainCamera.ScreenPointToRay(nextPoint);

				if (Physics.Raycast(ray, out var hit, 50f, LAYER_MASK))
				{
					_transform.localPosition = _isLimitJudgeLine
						? new Vector3(Mathf.Clamp(hit.point.x, -LANE_EDGE_X, LANE_EDGE_X), 0f, 0f)
						: new Vector3(hit.point.x, 0f, hit.point.z);
				}
			}

			if (GetButtonUp())
			{
				if (Input.touchCount > 0)
				{
					for (int i = 0; i < Input.touches.Length; i++)
					{
						var phase = Input.touches[i].phase;
						if (phase == TouchPhase.Stationary || phase == TouchPhase.Moved)
						{
							_isInputDowned = false;
							break;
						}
					}
				}
			}
		}

		void Shot(double musicTime)
		{
			if (Input.GetMouseButton(0))
			{
				if (musicTime > nextTime)
				{
					for (int i = 0; i < 2; i++)
					{
						var bullet = _bulletPool.Pool.Get();
						_bulletInfo.StartPosition = Position + new Vector2(0.2f * i - 0.1f, 0.3f);
						bullet.OnUpdate = null;
						bullet.SetParam(musicTime, _bulletInfo);
					}

					HomingShot(musicTime);

					nextTime = musicTime + _shotIntervalTime;
				}
			}
		}

		void HomingShot(double musicTime)
		{
			for (int i = 0; i < 2; i++)
			{
				if (!_useHoming[i])
				{
					continue;
				}

				var bullet = _bulletPool.Pool.Get();
				_homingBulletInfo.StartPosition = Position + new Vector2(0.4f * i - 0.2f, 0.3f);
				var velocity = new Vector2(i * 2 - 1, 2).normalized * 5f;
				_homingBulletInfo.Velocity = velocity;
				var rot = Vector2.SignedAngle(Vector2.up, velocity);
				_homingBulletInfo.Rotation = rot;
				bullet.SetParam(musicTime, _homingBulletInfo);
				bullet.OnUpdate = null;
				bullet.OnUpdate = Homing;
			}
		}

		void Homing(PlayerBullet bullet)
		{
			if (_danmakuStage.Enemy.IsDead)
			{
				return;
			}

			var pos = bullet.Transform.position;
			if (pos.z > _danmakuStage.Enemy.Position.y)
			{
				return;
			}
			var diff = _danmakuStage.Enemy.Position - new Vector2(pos.x, pos.z);
			var velocity = (bullet.Velocity + diff.normalized * 0.22f).normalized * SPEED;
			bullet.SetVelocity(velocity);
			var v = velocity.normalized;
			var rot = Quaternion.LookRotation(new Vector3(v.x, 0f, v.y), Vector3.up) * START_ROT;
			bullet.SetRotation(rot);
		}

		[BlackList, DoNotGen]
		public bool CheckHit(float bulletPosX, float bulletPosY, float hitDistance, int damage)
		{
			float diffX = Mathf.Abs(_transform.position.x - bulletPosX);
			float diffY = Mathf.Abs(_transform.position.z - bulletPosY);
			bool isHit = diffX * diffX + diffY * diffY < hitDistance * hitDistance;

			if (isHit)
			{
				OnHit?.Invoke(damage);
				SetDamageEffect(damage >= 0);
			}

			return isHit;
		}

		void SetDamageEffect(bool isDamage)
		{
			_sequence.Kill();
			_sequence = null;

			if (isDamage)
			{
				_sequence = DOTween.Sequence();
				_sequence
					.Append(_spriteRenderer.DOColor(Color.red, 0.2f))
					.Append(_spriteRenderer.DOColor(_playerColor, 0.2f));
			}
			else
			{
				_sequence = DOTween.Sequence();
				_sequence
					.Append(_spriteRenderer.DOColor(Color.green, 0.2f))
					.Append(_spriteRenderer.DOColor(_playerColor, 0.2f));
			}
		}

		void ResetPlayer()
		{
			_transform.localPosition = Vector3.zero;
		}

		//---------------------
		// Lua用関数.
		//---------------------

		/// <summary>
		/// 自身のTransformを取得します
		/// </summary>
		/// <returns></returns>
		public Transform GetTransform()
		{
			return _transform;
		}

		/// <summary>
		/// SpriteRendererを取得します
		/// </summary>
		/// <returns></returns>
		public SpriteRenderer GetSpriteRenderer()
		{
			return _spriteRenderer;
		}

		/// <summary>
		/// プレイヤーの移動方法を設定します
		/// </summary>
		/// <param name="playerMoveType"></param>
		public void SetPlayerMoveType(PlayerMoveType playerMoveType)
		{
			_playerMoveType = playerMoveType;
		}

		/// <summary>
		/// プレイヤーの移動方法を設定します(Int版)
		/// 0 → 指を動かした差分
		/// 1 → 指の位置
		/// </summary>
		/// <param name="playerMoveType"></param>
		public void SetPlayerMoveTypeInt(int playerMoveTypeInt)
		{
			_playerMoveType = (PlayerMoveType)playerMoveTypeInt;
		}

		/// <summary>
		/// 移動範囲を判定ライン上に制限するか設定します
		/// デフォルトはfalseに設定されています
		/// </summary>
		/// <param name="isLimit"></param>
		public void SetLimitJudgeLine(bool isLimit)
		{
			_isLimitJudgeLine = isLimit;
		}

		/// <summary>
		/// ホーミング弾(オプション)を使用するか設定します
		/// </summary>
		/// <param name="useLeft">左側</param>
		/// <param name="useRight">右側</param>
		public void UseHomingBullet(bool useLeft, bool useRight)
		{
			_useHoming[0] = useLeft;
			_useHoming[1] = useRight;

			_playerOptions[0].SetActive(useLeft);
			_playerOptions[1].SetActive(useRight);
		}

		/// <summary>
		/// ファイル名から画像を読み込んで設定します
		/// </summary>
		/// <param name="path">path</param>
		public void LoadSprite(string path)
		{
			Texture2D texture2D = TextureLoader.Load(path);
			_spriteRenderer.sprite = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f), 100f);
		}

		/// <summary>
		/// スプライトを設定します
		/// </summary>
		/// <param name="sprite">Sprite</param>
		public void SetSprite(Sprite sprite)
		{
			_spriteRenderer.sprite = sprite;
		}

		/// <summary>
		/// Colorを設定します
		/// </summary>
		/// <param name="color">Color</param>
		public void SetColor(Color color)
		{
			_playerColor = color;
			_spriteRenderer.color = color;
		}

		/// <summary>
		/// Colorを設定します
		/// </summary>
		/// <param name="r">Red</param>
		/// <param name="g">Green</param>
		/// <param name="b">Blue</param>
		/// <param name="a">Alpha</param>
		public void SetColor(float r, float g, float b, float a)
		{
			_playerColor = new Color(r, g, b, a);
			_spriteRenderer.color = _playerColor;
		}

		/// <summary>
		/// 透明度を設定します(0～1)
		/// </summary>
		/// <param name="alpha">透明度</param>
		public void SetAlpha(float alpha)
		{
			var color = _spriteRenderer.color;
			_spriteRenderer.color = new Color(color.r, color.g, color.b, alpha);
		}

		/// <summary>
		/// 画像のサイズを設定します
		/// デフォルトは0.5に設定されています
		/// </summary>
		/// <param name="size"></param>
		public void SetSize(float size)
		{
			_transform.localScale = Vector3.one * size;
		}

		/// <summary>
		/// 敵弾に当たった時に呼び出す関数を設定します
		/// </summary>
		/// <param name="func"></param>
		public void AddListenerOnHit(Action<int> func)
		{
			OnHit += func;
		}
	}
}
