﻿using DankagLikeLuaAPI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;
using XLua;

public class EnemyBulletObjectPool : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] Transform _transform;
	[SerializeField] EnemyBullet _EnemyBulletPrefab;

	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<EnemyBullet> _pool = null;
	List<EnemyBullet> _currentActiveBullets;
	DanmakuPlayer _danmakuPlayer;

	//------------------
	// キャッシュ.
	//------------------
	// 初期のプールサイズ
	public int DefaultCapacity = 128;
	// プールサイズを最大どれだけ大きくするか
	public int MaxSize = 3000;

	//------------------
	// 定数.
	//------------------

	//------------------
	// プロパティ.
	//------------------
	public List<EnemyBullet> CurrentActiveBullets => _currentActiveBullets;

	public ObjectPool<EnemyBullet> Pool
	{
		get
		{
			if (_pool is null)
			{
				_pool = new ObjectPool<EnemyBullet>(
					OnCreatePoolObject,
					OnTakeFromPool,
					OnReturnedToPool,
					OnDestroyPoolObject,
					false,
					DefaultCapacity,
					MaxSize
				);
			}

			return _pool;
		}
	}

	[BlackList, DoNotGen]
	public void Init(DanmakuPlayer danmakuPlayer)
	{
		_danmakuPlayer = danmakuPlayer;
		_currentActiveBullets = new List<EnemyBullet>();
	}

	EnemyBullet OnCreatePoolObject()
	{
		var enemyBullet = Instantiate(_EnemyBulletPrefab, _transform);
		enemyBullet.Init(Pool, _danmakuPlayer);
		enemyBullet.transform.localScale = _EnemyBulletPrefab.transform.localScale;
		_currentActiveBullets.Add(enemyBullet);
		return enemyBullet;
	}

	void OnTakeFromPool(EnemyBullet enemyBullet)
	{
		_currentActiveBullets.Add(enemyBullet);
		enemyBullet.gameObject.SetActive(true);
	}

	void OnReturnedToPool(EnemyBullet enemyBullet)
	{
		_currentActiveBullets.Remove(enemyBullet);
		enemyBullet.gameObject.SetActive(false);
	}

	void OnDestroyPoolObject(EnemyBullet enemyBullet)
	{
		//Debug.Log("Note Destroy");
		_currentActiveBullets.Remove(enemyBullet);
		Destroy(enemyBullet.gameObject);
	}

	public void CallUpdate(double musicTime)
	{
		int count = _currentActiveBullets.Count;
		for (int i = count - 1; i >= 0; i--)
		{
			_currentActiveBullets[i].CallUpdate(musicTime);
		}
	}

	public void ReleaseAll()
	{
		int count = _currentActiveBullets.Count;
		for (int i = count - 1; i >= 0; i--)
		{
			if (!_currentActiveBullets[i].IsReleased)
			{
				_currentActiveBullets[i].Release();
			}
		}
	}

	public void OnFinalize()
	{
		if (_pool is not null)
		{
			ReleaseAll();

			_pool.Clear();
			_pool.Dispose();
			_pool = null;
		}

		_currentActiveBullets.Clear();
		_currentActiveBullets = null;
	}
}
