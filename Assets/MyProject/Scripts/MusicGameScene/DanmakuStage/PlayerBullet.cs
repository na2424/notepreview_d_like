﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;
using XLua;

namespace DankagLikeLuaAPI
{
	[LuaCallCSharp]
	public sealed class PlayerBullet : MonoBehaviour, IPlayerBullet
    {
		//--------------------------------------
		// インスペクタまたは外部から設定.
		//--------------------------------------
		[SerializeField] SpriteRenderer _spriteRenderer;
		[SerializeField] Transform _transform;

		public Action<PlayerBullet> OnUpdate;

		//------------------
		// キャッシュ.
		//------------------

		ObjectPool<PlayerBullet> _pool;
		DanmakuEnemy _danmakuEnemy;

		Vector2 _velocity;
		Vector2 _accelation = Vector2.zero;
		float _angularVelocity = 0f;
		float _angularAcceleration = 0f;

		bool _isReleased;
		double _startLifeTime = 0d;
		double _lifeTime = 0d;
		float _hitDistance = 0f;
		int _damage = 0;

		bool _oneFramed = false;

		//-----------------
		// プロパティ.
		//-----------------
		public SpriteRenderer SpriteRenderer => _spriteRenderer;
		public Transform Transform => _transform;
		public Vector2 Velocity => _velocity;
		public int Damage => _damage;
		public bool IsReleased => _isReleased;

		[BlackList, DoNotGen]
		public void Init(ObjectPool<PlayerBullet> pool, DanmakuEnemy danmakuEnemy)
		{
			_pool = pool;
			_danmakuEnemy = danmakuEnemy;
		}

		[BlackList, DoNotGen]
		public void SetParam(double musicTime, BulletInfo bulletInfo)
		{
			_isReleased = false;

			_spriteRenderer.sprite = bulletInfo.Sprite;

			_transform.localPosition = new Vector3(bulletInfo.StartPosition.x, 0f, bulletInfo.StartPosition.y);
			_transform.localRotation = Quaternion.Euler(90f, 0f, bulletInfo.Rotation);
			_transform.localScale = Vector3.one * bulletInfo.Size;

			_velocity = bulletInfo.Velocity;
			_startLifeTime = musicTime;

			_hitDistance = bulletInfo.HitDistance;
			_damage = bulletInfo.Damage * 5;

			_accelation = bulletInfo.Acceleration;
			_angularVelocity = bulletInfo.AngularVelocity;
			_angularAcceleration = bulletInfo.AngularAcceleration;

			_lifeTime = bulletInfo.LifeTime;

			_oneFramed = false;
		}

		[BlackList, DoNotGen]
		public void CallUpdate(double musicTime)
		{
			if (_isReleased || !gameObject.activeSelf)
			{
				return;
			}

			OnUpdate?.Invoke(this);

			Move();

			CheckHitEnemy();
			CheckRelease(musicTime);
			_oneFramed = true;
		}

		void Move()
		{
			var deltaTime = Time.deltaTime;

			_velocity += _accelation * deltaTime;
			_angularVelocity += _angularAcceleration * deltaTime;

			if (Mathf.Abs(_angularVelocity) > float.Epsilon)
			{
				_velocity = Quaternion.Euler(0, 0, _angularVelocity * deltaTime) * _velocity;
			}

			var currentPos = _transform.localPosition;
			var move = _velocity * deltaTime;

			_transform.localPosition = new Vector3(currentPos.x + move.x, 0f, currentPos.z + move.y);

		}

		void CheckHitEnemy()
		{
			if (_isReleased)
			{
				return;
			}
			bool isHit = _danmakuEnemy.CheckHit(_transform.position.x, _transform.position.z, _hitDistance, _damage);
			if (isHit)
			{
				Release();
			}
		}

		void CheckRelease(double musicTime)
		{
			if (_startLifeTime + _lifeTime < musicTime)
			{
				Release();
			}

			if (_oneFramed && !_spriteRenderer.isVisible)
			{
				Release();
			}
		}

		[DoNotGen]
		public void Release()
		{
			if (_isReleased || !gameObject.activeSelf)
			{
				return;
			}

			_isReleased = true;
			_pool.Release(this);
			_oneFramed = false;
		}

		/// <summary>
		/// 速度を設定します
		/// </summary>
		/// <param name="velocity"></param>
		public void SetVelocity(Vector2 velocity)
		{
			_velocity = velocity;
		}

		/// <summary>
		/// 角度を設定します
		/// </summary>
		/// <param name="rotation"></param>
		public void SetRotation(Quaternion rotation)
		{
			_transform.localRotation = rotation;
		}
	}
}