﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using XLua;
using Cysharp.Text;
using UnityEngine.UI;

namespace DankagLikeLuaAPI
{
	[LuaCallCSharp]
	public sealed class DanmakuEnemy : MonoBehaviour, IDanmakuEnemy
	{
		[SerializeField] Transform _transform;
		[SerializeField] ParticleSystem[] _ps;
		[SerializeField] SpriteRenderer _spriteRenderer;
		[SerializeField] TMP_Text _lifePointText;
		[SerializeField] Slider _lifeSlider;
		[SerializeField] RefSpriteScriptableObject _characterSO;

		bool _isExecute = false;
		bool _isDead = false;
		int _lifePoint = 10000;
		int _maxLifePoint = 10000;
		Color _enemyColor;
		DG.Tweening.Sequence _sequence;

		public Action<Sprite> OnSetCharacter = null;
		public Action<int> OnHit = null;
		public Action OnDead = null;

		//--------------
		// プロパティ.
		//--------------
		public Vector2 Position => new Vector2(_transform.position.x, _transform.position.z);
		public bool IsDead => _isDead;

		[BlackList, DoNotGen]
		public void Init()
		{
			_lifeSlider.maxValue = _maxLifePoint;

			gameObject.SetActive(false);
			_sequence = DOTween.Sequence();
			SetLifeUI(_lifePoint);
		}

		[BlackList, DoNotGen]
		public void CallUpdate()
		{
			if (!_isExecute)
			{
				return;
			}
		}

		[BlackList, DoNotGen]
		public void OnFinalize()
		{
			_sequence.Kill();
			_sequence = null;
			OnSetCharacter = null;
			OnHit = null;
			OnDead = null;
		}

		[BlackList, DoNotGen]
		public void OnStartDanmakuStage()
		{
			_isExecute = true;
			Reset();
			gameObject.SetActive(true);
		}

		[BlackList, DoNotGen]
		public void OnEndDanmakuStage()
		{
			_isExecute = false;
			gameObject.SetActive(false);
		}

		[BlackList, DoNotGen]
		public bool CheckHit(float bulletPosX, float bulletPosY, float hitDistance, int damage)
		{
			if (!_isExecute)
			{
				return false;
			}

			if (_isDead)
			{
				return false;
			}

			float diffX = Mathf.Abs(_transform.position.x - bulletPosX);
			float diffY = Mathf.Abs(_transform.position.z - bulletPosY);
			bool isHit = diffX * diffX + diffY * diffY < hitDistance * hitDistance;

			if (isHit)
			{
				_lifePoint -= damage;
				SetLifeUI(_lifePoint);
				OnHit?.Invoke(damage);

				if (!_ps[0].isPlaying)
				{
					SetDamageEffect(damage >= 0);
				}

				if (_lifePoint <= 0)
				{
					OnDead?.Invoke();
					gameObject.SetActive(false);
					_isDead = true;
				}
			}

			return isHit;
		}

		void SetDamageEffect(bool isDamage)
		{
			foreach (var p in _ps)
			{
				p.Play();
			}
		}

		void SetLifeUI(int lifePont)
		{
			var value = Mathf.Clamp(lifePont, 0, 9999999);
			_lifePointText.SetText(value);
			_lifeSlider.value = value;
		}

		void Reset()
		{
			_isDead = false;
			_lifePoint = _maxLifePoint;
			SetLifeUI(_lifePoint);
		}

		//---------------------
		// Lua用関数.
		//---------------------

		/// <summary>
		/// SpriteRendererを取得します
		/// </summary>
		/// <returns></returns>
		public SpriteRenderer GetSpriteRenderer()
		{
			return _spriteRenderer;
		}

		/// <summary>
		/// Transformを取得します
		/// </summary>
		/// <returns></returns>
		public Transform GetTransform()
		{
			return _transform;
		}

		/// <summary>
		/// 指定した数値(0～60)からキャラクターのSpriteを設定します
		/// </summary>
		/// <param name="charaIndex"></param>
		public void SetCharacter(int charaIndex)
		{
			var sprite = _characterSO.GetSprite(Mathf.Clamp(charaIndex, 0, _characterSO.Length));
			_spriteRenderer.sprite = sprite;
			OnSetCharacter?.Invoke(sprite);
		}

		/// <summary>
		/// ファイル名から画像を読み込んでSpriteを設定します
		/// </summary>
		/// <param name="path">path</param>
		public void LoadSprite(string path)
		{
			Texture2D texture2D = TextureLoader.Load(path);
			_spriteRenderer.sprite = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f), 100f);
		}

		/// <summary>
		/// Spriteを設定します
		/// </summary>
		/// <param name="sprite"></param>
		public void SetSprite(Sprite sprite)
		{
			_spriteRenderer.sprite = sprite;
			OnSetCharacter?.Invoke(sprite);
		}

		/// <summary>
		/// 位置を設定します(Vector2版)
		/// </summary>
		/// <param name="position"></param>
		public void SetPosition(Vector2 position)
		{
			_transform.localPosition = new Vector3(position.x, 0f, position.y);
		}

		/// <summary>
		/// 位置を設定します(数値2つ版)
		/// </summary>
		/// <param name="x"></param>
		/// <param name="z"></param>
		public void SetPosition(float x, float z)
		{
			_transform.localPosition = new Vector3(x, 0f, z);
		}

		/// <summary>
		/// サイズを設定します
		/// </summary>
		/// <param name="size"></param>
		public void SetSize(float size)
		{
			_transform.localScale = new Vector2(size, size);
		}

		/// <summary>
		/// Colorを設定します
		/// </summary>
		/// <param name="color">Color</param>
		public void SetColor(Color color)
		{
			_enemyColor = color;
			_spriteRenderer.color = color;
		}

		/// <summary>
		/// Colorを設定します (数値4つ版)
		/// </summary>
		/// <param name="r">Red</param>
		/// <param name="g">Green</param>
		/// <param name="b">Blue</param>
		/// <param name="a">Alpha</param>
		public void SetColor(float r, float g, float b, float a)
		{
			_enemyColor = new Color(r, g, b, a);
			_spriteRenderer.color = _enemyColor;
		}

		/// <summary>
		/// 透明度を設定します(0～1)
		/// </summary>
		/// <param name="alpha">透明度</param>
		public void SetAlpha(float alpha)
		{
			var color = _spriteRenderer.color;
			_spriteRenderer.color = new Color(color.r, color.g, color.b, alpha);
		}

		/// <summary>
		/// 残りライフポイントを取得します
		/// </summary>
		/// <param name="lifePoint"></param>
		public int GetLifePoint()
		{
			return _lifePoint;
		}

		/// <summary>
		/// 残りライフポイントを設定します
		/// </summary>
		/// <param name="lifePoint"></param>
		public void SetLifePoint(int lifePoint)
		{
			_lifePoint = lifePoint;
			SetLifeUI(lifePoint);
		}

		/// <summary>
		/// 最大ライフポイントを設定します
		/// </summary>
		/// <param name="lifePoint"></param>
		public void SetMaxLifePoint(int maxLifePoint)
		{
			_maxLifePoint = maxLifePoint;
			_lifeSlider.maxValue = maxLifePoint;
		}

		/// <summary>
		/// ライフ数値を表示するか設定します
		/// </summary>
		public void SetActiveLifePoint(bool isActive)
		{
			_lifePointText.gameObject.SetActive(isActive);
		}

		/// <summary>
		/// 倒されたときに呼ばれる関数を設定します
		/// </summary>
		/// <param name="func"></param>
		public void AddListnerOnDead(Action func)
		{
			OnDead += func;
		}

		/// <summary>
		/// 倒されたときに呼ばれる関数を設定します
		/// </summary>
		/// <param name="func"></param>
		public void AddListenerOnDead(Action func)
		{
			OnDead += func;
		}

	}
}

