﻿using DankagLikeLuaAPI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;
using XLua;

public sealed class PlayerBulletObjectPool : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] Transform _transform;
	[SerializeField] PlayerBullet _PlayerBulletPrefab;

	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<PlayerBullet> _pool = null;
	List<PlayerBullet> _currentActiveBullets;
	DanmakuEnemy _danmakuEnemy;
	Vector3 _defaultBulletSize = Vector3.one;

	// 初期のプールサイズ
	public int DefaultCapacity = 64;
	// プールサイズを最大どれだけ大きくするか
	public int MaxSize = 128;

	//------------------
	// 定数.
	//------------------

	//------------------
	// プロパティ.
	//------------------
	public List<PlayerBullet> CurrentActiveBullets => _currentActiveBullets;

	public ObjectPool<PlayerBullet> Pool
	{
		get
		{
			if (_pool is null)
			{
				_pool = new ObjectPool<PlayerBullet>(
					OnCreatePoolObject,
					OnTakeFromPool,
					OnReturnedToPool,
					OnDestroyPoolObject,
					false,
					DefaultCapacity,
					MaxSize
				);
			}

			return _pool;
		}
	}

	[BlackList, DoNotGen]
	public void Init(DanmakuEnemy danmakuEnemy)
	{
		_danmakuEnemy = danmakuEnemy;
		_currentActiveBullets = new List<PlayerBullet>();
		_defaultBulletSize = _PlayerBulletPrefab.transform.localScale;
	}

	PlayerBullet OnCreatePoolObject()
	{
		var bullet = Instantiate(_PlayerBulletPrefab, _transform);
		bullet.Init(Pool, _danmakuEnemy);
		bullet.transform.localScale = _defaultBulletSize;
		_currentActiveBullets.Add(bullet);
		return bullet;
	}

	void OnTakeFromPool(PlayerBullet bullet)
	{
		_currentActiveBullets.Add(bullet);
		bullet.gameObject.SetActive(true);
	}

	void OnReturnedToPool(PlayerBullet bullet)
	{
		_currentActiveBullets.Remove(bullet);
		bullet.gameObject.SetActive(false);
	}

	void OnDestroyPoolObject(PlayerBullet bullet)
	{
		//Debug.Log("Note Destroy");
		_currentActiveBullets.Remove(bullet);
		Destroy(bullet.gameObject);
	}

	public void CallUpdate(double musicTime)
	{
		int count = _currentActiveBullets.Count;
		for (int i = count - 1; i >= 0; i--)
		{
			_currentActiveBullets[i].CallUpdate(musicTime);
		}
	}

	public void ReleaseAll()
	{
		int count = _currentActiveBullets.Count;
		for (int i = count - 1; i >= 0; i--)
		{
			if (!_currentActiveBullets[i].IsReleased)
			{
				_currentActiveBullets[i].Release();
			}
		}
	}

	public void OnFinalize()
	{
		if (_pool is not null)
		{
			ReleaseAll();

			_pool.Clear();
			_pool.Dispose();
			_pool = null;
		}

		_currentActiveBullets.Clear();
		_currentActiveBullets = null;
	}
}
