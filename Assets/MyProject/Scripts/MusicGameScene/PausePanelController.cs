﻿using Cysharp.Text;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// ポーズ画面を管理する
/// </summary>
public sealed class PausePanelController : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------

	[SerializeField] Image _bgImage;
	[SerializeField] ColorDefine _colorDefine;

	[Header("---SelectPanel---")]
	[SerializeField] GameObject _selectPanel;
	[SerializeField] TextMeshProUGUI _selectTitleText;
	[SerializeField] TextMeshProUGUI _selectDescriptionText;
	[SerializeField] Button _retirementButton;
	[SerializeField] Button _retryButton;
	[SerializeField] SizeChangeButton _resumeButton;

	[Header("---ConfirmationPanel---")]
	[SerializeField] GameObject _confirmationPanel;
	[SerializeField] TextMeshProUGUI _titleText;
	[SerializeField] TextMeshProUGUI _descriptionText;
	[SerializeField] TextMeshProUGUI _excuteButtonText;
	[SerializeField] Button _cancelButton;
	[SerializeField] Button _excuteButton;

	[Header("---ResumePanel---")]
	[SerializeField] GameObject _resumePanel;
	[SerializeField] Image _resumeNumberImage;
	[SerializeField] Slider _resumeSlider;
	[SerializeField] AnimationCurve _animationCurve;
	[SerializeField] Sprite[] _resumeNumberSprites;

	public event Action OnExit = null;
	public event Action OnRetire = null;
	public event Action OnFinalize = null;

	//------------------
	// キャッシュ.
	//------------------
	Action _excute = null;

	//------------------
	// 定数.
	//------------------
	static readonly string PAUSE = "ポーズ";
	static readonly string GAME_OVER = "ゲームオーバー";
	static readonly string GAME_OVER_DESCRIPTION = "ライフがゼロになりました";

	static readonly string RETIREMENT = "リタイア";
	static readonly string RETRY = "リトライ";

	static readonly string RETIREMENT_DESCRIPTION = "リタイアしますか？";
	static readonly string RETRY_DESCRIPTION = "リトライしますか？";

	const float RESUME_WAIT_TIME = 3f;

	//------------------
	// プロパティ.
	//------------------
	public bool IsShowPauseWindow { get; private set; } = false;
	public bool IsShowResume { get; private set; } = false;

	public void Init()
	{
		_bgImage.gameObject.SetActive(false);
		_selectPanel.SetActive(false);
		_confirmationPanel.SetActive(false);
		_resumePanel.SetActive(false);

		_resumeSlider.value = 1f;

		// リタイアボタン
		_retirementButton.onClick.AddListener(() =>
		{
			_excute = Retirement;
			SwitchToConfirmationPanel(RETIREMENT, RETIREMENT, RETIREMENT_DESCRIPTION);
		});

		// リトライボタン
		_retryButton.onClick.AddListener(() =>
		{
			_excute = Retry;
			SwitchToConfirmationPanel(RETRY, RETRY, RETRY_DESCRIPTION);
		});

		// 再開ボタン
		_resumeButton.onClick.AddListener(() =>
		{
			Resume();
			_selectPanel.SetActive(false);
			_resumeButton.SetOneSize();
		});

		// キャンセルボタン
		_cancelButton.onClick.AddListener(() =>
		{
			_excute = null;
			SwitchToSelectPanel();
		});

		// 実行ボタン
		_excuteButton.onClick.AddListener(() =>
		{
			_excute?.Invoke();
			_confirmationPanel.SetActive(false);
		});
	}

	public void Show(bool isDead = false)
	{
		IsShowPauseWindow = true;
		_resumeButton.gameObject.SetActive(!isDead);
		_selectTitleText.text = isDead ? GAME_OVER : PAUSE;
		_selectDescriptionText.text = isDead ? GAME_OVER_DESCRIPTION : ZString.Concat(StringReplaceSpriteAsset.ReplaceUra(GameManager.Instance.SelectSongInfo.Title), "\n", GetDifficultyString());

		_bgImage.gameObject.SetActive(true);
		_selectPanel.SetActive(true);
	}

	string GetDifficultyString() => GameManager.Instance.SelectDifficulty switch
	{
		DifficultyType.Easy => ZString.Format("<color=#{0}>Easy</color>", ColorUtility.ToHtmlStringRGBA(_colorDefine.Easy)),
		DifficultyType.Normal => ZString.Format("<color=#{0}>Normal</color>", ColorUtility.ToHtmlStringRGBA(_colorDefine.Normal)),
		DifficultyType.Hard => ZString.Format("<color=#{0}>Hard</color>", ColorUtility.ToHtmlStringRGBA(_colorDefine.Hard)),
		DifficultyType.Extra => ZString.Format("<color=#{0}>Extra</color>", ColorUtility.ToHtmlStringRGBA(_colorDefine.Extra)),
		DifficultyType.Lunatic => ZString.Format("<color=#{0}>Lunatic</color>", ColorUtility.ToHtmlStringRGBA(_colorDefine.Lunatic)),
		_ => ""
	};

	void SwitchToSelectPanel()
	{
		_confirmationPanel.SetActive(false);
		_selectPanel.SetActive(true);
	}

	void SwitchToConfirmationPanel(string excuteButtonText, string titleText, string descriptionText)
	{
		SetConfirmationTexts(excuteButtonText, titleText, descriptionText);
		_selectPanel.SetActive(false);
		_confirmationPanel.SetActive(true);
	}

	void SetConfirmationTexts(string excuteButtonText, string titleText, string descriptionText)
	{
		_excuteButtonText.text = excuteButtonText;
		_titleText.text = titleText;
		_descriptionText.text = descriptionText;
	}

	void Retirement()
	{
		IsShowPauseWindow = false;
		OnRetire?.Invoke();
		ChangeSceneAsync().Forget();
	}

	async UniTask ChangeSceneAsync()
	{
		BgManager.Instance.UseDimmer(false);
		if (!BgManager.Instance.IsDefaultImage())
		{
			BgManager.Instance.ClearBgChangeImageCache();
			BgManager.Instance.SetDefaultImage();
		}
		BgManager.Instance.ClearMaterial();

		OnFinalize?.Invoke();
		await FadeManager.Instance.FadeOutAsync(SceneBuildIndex.SelectMusic);
	}

	void Retry()
	{
		IsShowPauseWindow = false;

		if (!BgManager.Instance.IsDefaultImage())
		{
			BgManager.Instance.ClearBgChangeImageCache();
			BgManager.Instance.SetDefaultImage();
		}
		BgManager.Instance.ClearMaterial();

		OnFinalize?.Invoke();
		GC.Collect();
		GameManager.Instance.ChangeScene(SceneName.Game);
	}

	void Resume()
	{
		IsShowPauseWindow = false;
		GC.Collect();
		StartCoroutine(ResumeRoutine());
	}

	IEnumerator ResumeRoutine()
	{
		IsShowResume = true;

		var resumeNumberTr = _resumeNumberImage.transform;
		var clearColor = new Color(1f, 1f, 1f, 0f);
		var startSize = new Vector3(3f, 3f, 3f);
		_resumeNumberImage.sprite = _resumeNumberSprites[2];
		_resumeNumberImage.color = clearColor;
		resumeNumberTr.localScale = startSize;

		_resumePanel.SetActive(true);

		float remainingTime = RESUME_WAIT_TIME;
		int beforeCeilValue = Mathf.CeilToInt(RESUME_WAIT_TIME);

		SystemSEManager.Instance.Play(SystemSEType.ResumeCount);
		_resumeNumberImage.DOFade(1f, 0.3f);
		resumeNumberTr.DOScale(1f, 0.3f);

		while (0f < remainingTime)
		{
			_resumeSlider.value = _animationCurve.Evaluate(remainingTime - Mathf.FloorToInt(remainingTime));
			int ceilValue = Mathf.CeilToInt(remainingTime);

			if (beforeCeilValue != ceilValue)
			{
				SystemSEManager.Instance.Play(SystemSEType.ResumeCount);
				_resumeNumberImage.sprite = _resumeNumberSprites[ceilValue - 1];
				_resumeNumberImage.color = clearColor;
				resumeNumberTr.localScale = startSize;
				_resumeNumberImage.DOFade(1f, 0.3f);
				resumeNumberTr.DOScale(1f, 0.3f);

				beforeCeilValue = ceilValue;
			}

			remainingTime -= Time.deltaTime;
			yield return null;

		}

		_resumePanel.SetActive(false);
		_bgImage.gameObject.SetActive(false);
		OnExit?.Invoke();

		IsShowResume = false;
	}


}
