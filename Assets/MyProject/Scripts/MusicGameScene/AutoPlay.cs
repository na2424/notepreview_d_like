﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

/// <summary>
/// オートプレイ時のタッチ入力を生成するクラス
/// </summary>
public sealed class AutoPlay : InputBase
{
	//------------------
	// キャッシュ.
	//------------------
	Sequence _sequence;
	BpmHelper _bpmHelper;
	NoteObjectPool _noteObjectPool;
	int _noteIndex = 0;
	Dictionary<int, int> _holdNoteIndexDict = new Dictionary<int, int>();
	bool _useNotePosition = false;
	int _noteCount = 0;
	int _longInfoCount = 0;
	int[] _longInfoNoteCounts;
	// 1フレーム当たりにAutoPlayがロングを叩ける回数
	int _fingerLongPerFrame = 5;

	//------------------
	// 定数.
	//------------------
	static readonly float LANE_DISTANCE = Constant.Note.LANE_DISTANCE;
	static readonly float OFFSET_X = Constant.Note.OFFSET_X;

	public void Init(Sequence sequence, BpmHelper bpmHelper, NoteObjectPool noteObjectPool, int fingerLongPerFrame)
	{
		_sequence = sequence;
		_bpmHelper = bpmHelper;
		_noteObjectPool = noteObjectPool;
		_noteCount = _sequence.BeatPositions.Count;
		_longInfoCount = _sequence.LongInfo.Count;
		_fingerLongPerFrame = fingerLongPerFrame;

		_longInfoNoteCounts = new int[_longInfoCount];

		for (int i = 0; i < _longInfoNoteCounts.Length; i++)
		{
			_longInfoNoteCounts[i] = _sequence.LongInfo[i].BeatPositions.Count;
		}
	}

	public void SetMidPlay(double musicTime)
	{
		var beat = _bpmHelper.TimeToBeat(musicTime);

		for (int i = 0; i < _noteCount; i++)
		{
			if (beat <= _sequence.BeatPositions[i])
			{
				_noteIndex = i;
				break;
			}
		}

		// for (int i = 0; i < _longInfoCount; i++)
		// {
		// 	if(beat <= _sequence.LongInfo[i].BeatPositions[0])
		// 	{
		// 		_checkedLongIndex = i - 1;
		// 	}
		// }
	}

	public override void CallUpdate(double musicTime)
	{
		_touchData.Clear();
		_holdNoteIndexDict.Clear();

		double beat = _bpmHelper.TimeToBeat(musicTime);

		//
		// Note
		//

		while (true)
		{
			if (_noteIndex < _noteCount && _sequence.BeatPositions[_noteIndex] < beat)
			{
				var posX = -10000f;

				// MEMO:
				// Luaで_useNotePositionを有効にした場合、画面上にあるノーツからPosXを取得する
				// 通常は事前に決められたレーンの場所を叩く
				if (_useNotePosition)
				{
					var note = _noteObjectPool.GetNote(_noteIndex);
					posX = ((note != null) ? note.PosX : (_sequence.Lanes[_noteIndex] * LANE_DISTANCE - OFFSET_X));
				}
				else
				{
					posX = _sequence.Lanes[_noteIndex] * LANE_DISTANCE - OFFSET_X;
				}

				var noteType = _sequence.NoteTypes[_noteIndex];

				var phase = GetPhase(noteType);

				int touchNoteIndex = _noteIndex;

				if (phase == ScreenTouchPhase.Hold)
				{
					if (_holdNoteIndexDict.TryGetValue(_noteIndex, out var value))
					{
						touchNoteIndex = value;
					}
				}

				if (!_touchData.ContainsKey(_noteIndex))
				{
					_touchData.Add(_noteIndex, new TouchData(phase, posX, _bpmHelper.BeatToTime(_sequence.BeatPositions[_noteIndex]), touchNoteIndex));
				}
				_noteIndex++;
			}
			else
			{
				break;
			}
		}

		//
		// Long
		//

		int holdLongIndex = -1;

		for (int fingerIndex = 0; fingerIndex < _fingerLongPerFrame; fingerIndex++)
		{
			LongInfo longInfo = null;

			// ホールド中にすべきlongInfoを選ぶ
			for (int i = holdLongIndex + 1; i < _longInfoCount; i++)
			{
				// invalidLongIndexの初期値が-1のため
				if (i == holdLongIndex)
				{
					continue;
				}

				var last = _longInfoNoteCounts[i] - 1;
				var indexInfo = _sequence.LongInfo[i];
				var lastBeat = indexInfo.BeatPositions[last];

				if (indexInfo.BeatPositions[0] < beat && beat < lastBeat)
				{
					holdLongIndex = i;
					longInfo = indexInfo;
					break;
				}
			}

			if (longInfo is not null)
			{
				var loop = _longInfoNoteCounts[holdLongIndex] - 1;
				// レーンを跨ぐロングノートで現在のX座標を求めて_touchDataに入れる
				for (int i = 0; i < loop; i++)
				{
					//OK
					var indexPosition = longInfo.BeatPositions[i];
					var nextPosition = longInfo.BeatPositions[i + 1];

					if (indexPosition < beat && beat < nextPosition)
					{
						float longPosX = Mathf.Lerp(
							longInfo.Lanes[i] * LANE_DISTANCE - OFFSET_X,
							longInfo.Lanes[i + 1] * LANE_DISTANCE - OFFSET_X,
							(float)((beat - indexPosition) / (nextPosition - indexPosition))
						);

						int startNoteIndex = longInfo.NoteIndex[0];

						if (!_touchData.ContainsKey(startNoteIndex))
						{
							_touchData.Add(startNoteIndex, new TouchData(ScreenTouchPhase.Hold, longPosX, _bpmHelper.BeatToTime(indexPosition), startNoteIndex));

							if (0 < i - 1)
							{
								_holdNoteIndexDict.Add(longInfo.NoteIndex[i - 1], startNoteIndex);
							}
						}
						break;
					}
				}
			}
		}
	}

	ScreenTouchPhase GetPhase(NoteType noteType) => noteType switch
	{
		NoteType.Normal => ScreenTouchPhase.Tap,
		NoteType.LongStart => ScreenTouchPhase.Tap,
		NoteType.LongRelay => ScreenTouchPhase.Hold,
		NoteType.LongEnd => ScreenTouchPhase.Up,
		NoteType.Fuzzy => ScreenTouchPhase.Up,
		NoteType.FuzzyLongStart => ScreenTouchPhase.Tap,
		NoteType.FuzzyLongRelay => ScreenTouchPhase.Hold,
		NoteType.FuzzyLongEnd => ScreenTouchPhase.Hold,
		_ => ScreenTouchPhase.Tap
	};

	public void SetAutoType(bool use)
	{
		_useNotePosition = use;
	}

	public override void OnFinalize()
	{
		_sequence = null;
		_holdNoteIndexDict.Clear();
		_holdNoteIndexDict = null;
		base.OnFinalize();
	}
}
