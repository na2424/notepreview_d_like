﻿using System.Collections;
using System.Collections.Generic;
using ELEBEAT;
using UnityEngine;
using UnityEngine.Pool;

public sealed class SameTimeBarObjectPool : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] SameTimeBarController _beatBarControllerPrefab;

	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<SameTimeBarController> _pool = null;
	List<SameTimeBarController> _currentActiveSameTimeBar = new List<SameTimeBarController>();
	Transform _transform;
	int _listCount = 0;

	//------------------
	// 定数.
	//------------------
	// 初期のプールサイズ
	const int DEFAULT_CAPACITY = 16;
	// プールサイズを最大どれだけ大きくするか
	const int MAX_SIZE = 128;
	// ListのRemove時のメモリコピーを防ぐための入れ替え閾値
	const int SWAP_THRESHOLD = 3;

	//------------------
	// プロパティ.
	//------------------
	public List<SameTimeBarController> CurrentActiveSameTimeBar => _currentActiveSameTimeBar;
	public int ListCount => _listCount;

	public void Init()
	{
		_transform = transform;

		SameTimeBarController[] sameTimeBarControllers = new SameTimeBarController[DEFAULT_CAPACITY];

		for (int i = 0; i < DEFAULT_CAPACITY; i++)
		{
			sameTimeBarControllers[i] = Pool.Get();
		}

		for (int i = 0; i < DEFAULT_CAPACITY; i++)
		{
			sameTimeBarControllers[i].Release();
			sameTimeBarControllers[i] = null;
		}
	}

	public ObjectPool<SameTimeBarController> Pool
	{
		get
		{
			if (_pool is null)
			{
				_pool = new ObjectPool<SameTimeBarController>(OnCreatePoolObject, OnTakeFromPool, OnReturnedToPool, OnDestroyPoolObject, false, DEFAULT_CAPACITY, MAX_SIZE);
			}

			return _pool;
		}
	}

	SameTimeBarController OnCreatePoolObject()
	{
		var controller = Instantiate(_beatBarControllerPrefab, _transform);
		controller.Init(Pool);
		return controller;
	}

	void OnTakeFromPool(SameTimeBarController controller)
	{
		_listCount++;
		controller.Show();
		_currentActiveSameTimeBar.Add(controller);
	}

	void OnReturnedToPool(SameTimeBarController controller)
	{
		_listCount--;

		if (_listCount > SWAP_THRESHOLD)
		{
			_currentActiveSameTimeBar.RemoveBeforeSwapLast(controller);
		}
		else
		{
			_currentActiveSameTimeBar.Remove(controller);
		}
		controller.Hide();
	}

	void OnDestroyPoolObject(SameTimeBarController controller)
	{
		Destroy(controller.gameObject);
	}

	public void CallUpdate(double beat, double musicTime, float speedStretchRatioValue)
	{
		int loop = _listCount - 1;

		if (loop < 0)
		{
			return;
		}

		for (int i = loop; i >= 0; i--)
		{
			if (i < _listCount)
			{
				CurrentActiveSameTimeBar[i].OnUpdate(beat, musicTime, speedStretchRatioValue);
			}
		}
	}

	public void OnFinalize()
	{
		if (_pool is not null)
		{
			_pool.Clear();
			_pool.Dispose();
			_pool = null;
		}

		_currentActiveSameTimeBar.Clear();
		_currentActiveSameTimeBar = null;
	}
}
