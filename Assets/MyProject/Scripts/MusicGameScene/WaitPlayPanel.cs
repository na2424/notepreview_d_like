﻿using Cysharp.Text;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using System;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 再生待機画面を管理するクラス
/// </summary>
public sealed class WaitPlayPanel : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[Header("Panels")]
	[SerializeField] CanvasGroup _canvasGroup = default;
	[SerializeField] CanvasGroup _topCanvasGroup = default;
	[SerializeField] CanvasGroup _leftCanvasGroup = default;
	[SerializeField] CanvasGroup _rightCanvasGroup = default;
	[Header("TopPanel")]
	[SerializeField] TextMeshProUGUI _difficultyText = default;
	[SerializeField] Image _difficultyBgImage = default;
	[Header("LeftPanel")]
	[SerializeField] AspectRatioRawImage _jacketRawImage;
	[Header("RightPanel")]
	[SerializeField] Button _playButton = default;

	[Header("リハーサル")]
	[SerializeField] Slider _timeSlider = default;
	[SerializeField] TextMeshProUGUI _timeText = default;
	[SerializeField] Slider _speedSlider = default;
	[SerializeField] TextMeshProUGUI _speedText = default;
	[SerializeField] Slider _musicRateSlider = default;
	[SerializeField] TextMeshProUGUI _musicRateText = default;

	[Header("プレイ")]
	[SerializeField] TextScroll _titleText;
	[SerializeField] TextScroll _artistText;
	[SerializeField] TextScroll[] _descriptionTexts = new TextScroll[0];
	[SerializeField] TextScroll[] _descriptionExtensionTexts = new TextScroll[0];
	[SerializeField] TextScroll _sequenceArtistText = default;
	[SerializeField] TextMeshProUGUI _illustText = default;
	[SerializeField] GameObject _descriptionPanelObject;
	[SerializeField] GameObject _descriptionExtensionPanelObject;
	[SerializeField] GameObject _rehearsalPanelObject;
	[Header("Color")]
	[SerializeField] ColorDefine _colorDefine;

	//------------------
	// キャッシュ.
	//------------------
	float _speedValue = 1f;
	float _musicRate = 1f;
	static float g_timeValue = 0f;

	//------------------
	// 定数.
	//------------------
	const float WAIT_TIME = 0.5f;
	const float TWEEN_TIME = 0.42f;

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name="ct"></param>
	/// <param name="onClickPlayButton"></param>
	/// <param name="info"></param>
	/// <param name="lastNoteTime"></param>
	public void Init(CancellationToken ct, Action<float, float> onClickPlayButton, SongInfo info, double lastNoteTime, string splitCredit)
	{
		_speedValue = GameManager.Instance.NotesOption.HiSpeed;
		_musicRate = GameManager.Instance.JudgeTimeOption.MusicRate;

		_timeSlider.onValueChanged.AddListener(value =>
		{
			g_timeValue = value;
			var minuite = (int)value / 60;
			var second = (int)value % 60;
			var d = (int)((value - Mathf.Floor(value)) * 100);
			_timeText.SetTextFormat("{0:D2}:{1:D2}.{2:D2}", minuite, second, d);
		});

		_speedSlider.onValueChanged.AddListener(value =>
		{
			_speedValue = 1f + value / 10f;
			_speedText.text = _speedValue.ToString("F1");
		});

		_musicRateSlider.onValueChanged.AddListener(value =>
		{
			_musicRate = value * 0.01f;
			_musicRateText.text = _musicRate.ToString("F2");
		});

		_playButton.onClick.AddListener(() =>
		{
			_playButton.interactable = false;
			OnClickPlayButtonAsync(ct, onClickPlayButton).Forget();
		});

		_difficultyText.text = GetDifficultyString();
		_difficultyBgImage.color = GetDifficultyColor();

		_titleText.SetText(ZString.Concat(StringReplaceSpriteAsset.ReplaceUra(info.Title), " ", info.SubTitle));
		_artistText.SetText(ZString.Concat("Artist : ", info.Artist));
		_sequenceArtistText.SetText(ZString.Concat("Score : ", splitCredit.HasValue() ? splitCredit : info.ChartArtist));
		_illustText.text = ZString.Concat("イラスト : ", info.Illust);
		_jacketRawImage.SetTexture(info.JacketTexture);

		if (GameManager.Instance.PlayModeOption.PlayMode == PlayMode.Rehearsal || GameManager.Instance.PlayModeOption.PlayMode == PlayMode.InputCheck)
		{
			_timeSlider.maxValue = (float)lastNoteTime;
			_timeSlider.value = g_timeValue;

			_speedSlider.value = (_speedValue - 1f) * 10f;

			_descriptionPanelObject.SetActive(false);
			_descriptionExtensionPanelObject.SetActive(false);
			_rehearsalPanelObject.SetActive(true);
		}
		else
		{
			int descriptionCount = info.Description.Count;
			bool isExtension = descriptionCount >= 7;

			if (isExtension)
			{
				for (int i = 0; i < descriptionCount; i++)
				{
					if (i > 9)
					{
						break;
					}
					_descriptionExtensionTexts[i].SetText(info.Description[i]);
				}
			}
			else
			{
				for (int i = 0; i < descriptionCount; i++)
				{
					if (i > 5)
					{
						break;
					}
					_descriptionTexts[i].SetText(info.Description[i]);
				}
			}

			_descriptionPanelObject.SetActive(!isExtension);
			_descriptionExtensionPanelObject.SetActive(isExtension);

			_rehearsalPanelObject.SetActive(false);
		}

		_topCanvasGroup.alpha = 0f;
		_leftCanvasGroup.alpha = 0f;
		_rightCanvasGroup.alpha = 0f;
		var _topPanelPosX = _topCanvasGroup.transform.localPosition.x;
		var _leftPanelPosX = _leftCanvasGroup.transform.localPosition.x;
		var _rightPanelPosX = _rightCanvasGroup.transform.localPosition.x;
		_topCanvasGroup.transform.localPosition -= Vector3.right * 100f;
		_leftCanvasGroup.transform.localPosition -= Vector3.right * 100f;
		_rightCanvasGroup.transform.localPosition -= Vector3.right * 100f;

		gameObject.SetActive(true);

		DOTween.Sequence()
		 .Append(_topCanvasGroup.DOFade(1f, TWEEN_TIME))
		 .Join(_topCanvasGroup.transform.DOLocalMoveX(_topPanelPosX, TWEEN_TIME))
		 .Append(_leftCanvasGroup.DOFade(1f, TWEEN_TIME))
		 .Join(_leftCanvasGroup.transform.DOLocalMoveX(_leftPanelPosX, TWEEN_TIME))
		 .Append(_rightCanvasGroup.DOFade(1f, TWEEN_TIME))
		 .Join(_rightCanvasGroup.transform.DOLocalMoveX(_rightPanelPosX, TWEEN_TIME));

		string GetDifficultyString() => GameManager.Instance.SelectDifficulty switch
		{
			DifficultyType.Easy => "Easy",
			DifficultyType.Normal => "Normal",
			DifficultyType.Hard => "Hard",
			DifficultyType.Extra => "Extra",
			DifficultyType.Lunatic => "Lunatic",
			_ => ""
		};

		Color GetDifficultyColor() => GameManager.Instance.SelectDifficulty switch
		{
			DifficultyType.Easy => _colorDefine.Easy,
			DifficultyType.Normal => _colorDefine.Normal,
			DifficultyType.Hard => _colorDefine.Hard,
			DifficultyType.Extra => _colorDefine.Extra,
			DifficultyType.Lunatic => _colorDefine.Lunatic,
			_ => Color.black
		};
	}

	/// <summary>
	/// 最後のノーツの時間を反映
	/// </summary>
	/// <param name="lastNoteTime"></param>
	public void SetLastNoteTime(double lastNoteTime)
	{
		_timeSlider.maxValue = (float)lastNoteTime;
		_timeSlider.value = g_timeValue;
	}

	/// <summary>
	/// 開始ボタンを押した時の処理
	/// </summary>
	/// <param name="ct"></param>
	/// <param name="onClickPlayButton"></param>
	/// <returns></returns>
	async UniTask OnClickPlayButtonAsync(CancellationToken ct, Action<float, float> onClickPlayButton)
	{
		RemoveEvents();

		GameManager.Instance.NotesOption.HiSpeed = _speedValue;
		GameManager.Instance.JudgeTimeOption.MusicRate = _musicRate;

		GameSettingsPrefas.instance.NotesOption = GameManager.Instance.NotesOption;
		GameSettingsPrefas.Save();

		try
		{
			_canvasGroup.DOFade(0f, WAIT_TIME);
			await UniTask.Delay((int)(WAIT_TIME * 1000), false, PlayerLoopTiming.Update, ct);
		}
		catch (OperationCanceledException e)
		{
			Debug.LogWarning(ZString.Concat("[WaitPlayPanel] キャンセルされました: ", e.Message));
		}

		onClickPlayButton(_speedValue, g_timeValue);
		gameObject.SetActive(false);
	}

	void RemoveEvents()
	{
		_timeSlider.onValueChanged.RemoveAllListeners();
		_speedSlider.onValueChanged.RemoveAllListeners();
		_musicRateSlider.onValueChanged.RemoveAllListeners();
	}

	public void OnEndMusic()
	{
		g_timeValue = 0f;
	}
}
