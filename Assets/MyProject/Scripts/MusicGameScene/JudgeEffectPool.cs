﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;
using ELEBEAT;
using Cysharp.Threading.Tasks;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public sealed class JudgeEffectPool : MonoBehaviour
{
	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<JudgeEffectController> _pool = null;
	Transform _transform;
	List<JudgeEffectController> _currentActiveJudgeEffects = new List<JudgeEffectController>();

	AsyncOperationHandle<GameObject> _opHandle = default;
	JudgeEffectController _judgeEffectControllerPrefab;
	int _listCount = 0;

	//------------------
	// 定数.
	//------------------
	// 初期のプールサイズ
	const int DEFAULT_CAPACITY = 32;
	// プールサイズを最大どれだけ大きくするか
	const int MAX_SIZE = 128;
	// ListのRemove時のメモリコピーを防ぐための入れ替え閾値
	const int SWAP_THRESHOLD = 3;

	const string JUDGE_TYPE_3_PREFAB_ADDRESS = "Prefabs/LightEnchant";

	public void Init(NotesEffectType notesEffectType)
	{
		_transform = transform;

		if (notesEffectType != NotesEffectType.None)
		{
			InitAsync(notesEffectType).Forget();
		}
	}

	string GetNoteEffectTypeToAddress(NotesEffectType notesEffectType) => notesEffectType switch
	{
		NotesEffectType.Type1 => AddressableAssetAddress.PREFABS_JUDGE_EFFECT,
		NotesEffectType.Type2 => AddressableAssetAddress.PREFABS_JUDGE_EFFECT_TYPE2,
		NotesEffectType.Type3 => JUDGE_TYPE_3_PREFAB_ADDRESS,
		_ => AddressableAssetAddress.PREFABS_JUDGE_EFFECT
	};

	async UniTask InitAsync(NotesEffectType notesEffectType)
	{
		string address = GetNoteEffectTypeToAddress(notesEffectType);

		_opHandle = Addressables.LoadAssetAsync<GameObject>(address);
		await _opHandle;
		_judgeEffectControllerPrefab = _opHandle.Result.GetComponent<JudgeEffectController>();

		int length = DEFAULT_CAPACITY / 2;

		for (int i = 0; i < length; i++)
		{
			// 初回のカクツキを軽減する為
			Play(100f);
		}
	}

	public ObjectPool<JudgeEffectController> Pool
	{
		get
		{
			if (_pool is null)
			{
				_pool = new ObjectPool<JudgeEffectController>(OnCreatePoolObject, OnTakeFromPool, OnReturnedToPool, OnDestroyPoolObject, false, DEFAULT_CAPACITY, MAX_SIZE);
			}

			return _pool;
		}
	}

	public void Play(float posX)
	{
		var controller = Pool.Get();
		controller.transform.localPosition = new Vector3(posX, 0f, 0f);
		controller.Play();
	}

	JudgeEffectController OnCreatePoolObject()
	{
		_listCount++;
		var controller = Instantiate(_judgeEffectControllerPrefab, _transform);
		controller.Init(Pool);
		_currentActiveJudgeEffects.Add(controller);
		return controller;
	}

	void OnTakeFromPool(JudgeEffectController controller)
	{
		_listCount++;
		_currentActiveJudgeEffects.Add(controller);
		//controller.Show();
		controller.Play();
	}

	void OnReturnedToPool(JudgeEffectController controller)
	{
		_listCount--;
		if (_listCount > SWAP_THRESHOLD)
		{
			_currentActiveJudgeEffects.RemoveBeforeSwapLast(controller);
		}
		else
		{
			_currentActiveJudgeEffects.Remove(controller);
		}
		//controller.Hide();
	}

	void OnDestroyPoolObject(JudgeEffectController controller)
	{
		_listCount--;
		if (_listCount > SWAP_THRESHOLD)
		{
			_currentActiveJudgeEffects.RemoveBeforeSwapLast(controller);
		}
		else
		{
			_currentActiveJudgeEffects.Remove(controller);
		}
		Destroy(controller.gameObject);
	}

	public void CallUpdate()
	{
		int loop = _currentActiveJudgeEffects.Count - 1;

		if (loop < 0)
		{
			return;
		}

		for (int i = loop; i >= 0; --i)
		{
			if (i < _listCount)
			{
				_currentActiveJudgeEffects[i].CallUpdate();
			}
		}
	}

	public void OnFinalize()
	{
		if (_pool is not null)
		{
			_pool.Clear();
			_pool.Dispose();
			_pool = null;
		}

		_currentActiveJudgeEffects.Clear();
		_currentActiveJudgeEffects = null;

		if (!_opHandle.Equals(default) && _opHandle.Status == AsyncOperationStatus.Succeeded)
		{
			Addressables.Release(_opHandle);
		}
	}

	public void Clear()
	{
		int loop = _currentActiveJudgeEffects.Count - 1;
		for (int i = loop; i >= 0; i--)
		{
			_pool.Release(_currentActiveJudgeEffects[i]);
		}
		_pool.Clear();
	}

	public void ChangeEffectType(NotesEffectType notesEffectType)
	{
		if (notesEffectType == NotesEffectType.None)
		{
			return;
		}

		ChangeEffectTypeAsync(notesEffectType).Forget();
	}

	async UniTask ChangeEffectTypeAsync(NotesEffectType notesEffectType)
	{
		Clear();

		string address = GetNoteEffectTypeToAddress(notesEffectType);

		if (!_opHandle.Equals(default) && _opHandle.Status == AsyncOperationStatus.Succeeded)
		{
			Addressables.Release(_opHandle);
		}

		_opHandle = Addressables.LoadAssetAsync<GameObject>(address);
		await _opHandle;
		_judgeEffectControllerPrefab = _opHandle.Result.GetComponent<JudgeEffectController>();
	}
}
