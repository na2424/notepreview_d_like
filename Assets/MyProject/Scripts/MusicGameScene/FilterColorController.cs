﻿using UnityEngine;

public sealed class FilterColorController : MonoBehaviour
{
	[SerializeField] MeshRenderer _meshRenderer;

	public void Init()
	{
		float alpha = Mathf.Clamp01(GameManager.Instance.NotesOption.LaneAlpha * 0.01f);
		var mat = _meshRenderer.material;
		mat.SetColor(Constant.ShaderProperty.TopColor, new Color(0, 0, 0, 0));
		mat.SetColor(Constant.ShaderProperty.BottomColor, new Color(0, 0, 0, alpha));
	}

	public void SetAlpha(float alpha)
	{
		_meshRenderer.material.SetColor(Constant.ShaderProperty.BottomColor, new Color(0, 0, 0, Mathf.Clamp01(alpha)));
	}
}
