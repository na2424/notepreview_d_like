﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Cysharp.Threading.Tasks;
using System.Threading;
using Cysharp.Text;

public class LuaOverlayPanel : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] Canvas _canvas;
	[SerializeField] GameObject _systemMessagePanel;
	[SerializeField] TextMeshProUGUI _logText;

	//------------------
	// キャッシュ.
	//------------------

	CancellationTokenSource _ctSource = null;
	float _logTime = 0f;
	bool _isWait = false;

	//------------------
	// 定数.
	//------------------
	const float WAIT_LOG_TIME = 4.2f;

	//------------------
	// プロパティ.
	//------------------
	public Canvas Canvas => _canvas;

	public void Init()
	{
		_systemMessagePanel.SetActive(false);
	}

	/// <summary>
	/// メッセージテキストを表示する
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="message"></param>
	public void ShowMessage<T>(T message)
	{
		_logTime = 0f;
		_logText.SetText(message);

		if (!_isWait)
		{
			if (_ctSource != null)
			{
				_ctSource.Cancel();
				_ctSource = null;
				return;
			}
			_ctSource = new CancellationTokenSource();
			ShowMessageAsync(_ctSource.Token).Forget();
		}
	}

	async UniTask ShowMessageAsync(CancellationToken ct)
	{
		_isWait = true;
		_systemMessagePanel.SetActive(true);

		while (_logTime < WAIT_LOG_TIME)
		{
			_logTime += Time.deltaTime;

			await UniTask.Yield();

			if (ct.IsCancellationRequested)
			{
				return;
			}
		}

		_logText.text = "";
		_systemMessagePanel.SetActive(false);
		_isWait = false;
	}

	void OnDestroy()
	{
		if (_ctSource != null)
		{
			_ctSource.Cancel();
			_ctSource = null;
		}
	}
}
