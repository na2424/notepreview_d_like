﻿using Cysharp.Text;
using Cysharp.Threading.Tasks;
using Dialog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEngine;
using UnityEngine.Audio;

/// <summary>
/// 楽曲の再生・終了を管理するクラス
/// </summary>
public sealed class MusicManager : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] AudioSource _audioSource;
	[SerializeField] AudioMixerGroup _pitchAudioMixer;
	public event Action OnFinishMusic = null;

	//------------------
	// キャッシュ.
	//------------------
	AudioClip _audioClip;
	double _beforeTime = 0d;
	double _cacheTime = 0d;
	double _lastNoteTime = 0d;
	double _musicEndTime = 0d;
	double _minusPlayTime = -1d;
	double _overPlayTime = 0d;
	float _lastSecondHint = 0.0f;
	float _maxVolume = 0.5f;
	bool _isPlay = false;
	bool _isMinusPlayTime = false;
	bool _isOverPlayTime = false;
	double _frequency = 44100d;

	double _smoothedTimeSamples = 0f;
	double _prevFrameSamples = 0f;
	int _counter = 0;

	//------------------
	// 定数.
	//------------------
	// 曲が終了または最後のノートから何秒待機して終了するかの時間
	const float DELAY_ENDTIME = 2f;
	// 開始してから曲を流し始めるまでの空白時間
	const double START_MARGIN = 1d;
	// 途中再生の時に開始してから指定した時間になるまでの猶予時間
	const double MID_START_MARGIN = 2.2d;
	// ノーツの動きを滑らかにする為にDeltaTime加算で動かしているが、
	// 少しずつ曲の再生時間とずれていくので指定フレーム毎に再生時間と同期を取る。そのフレーム値の定数。
	const int COUNTER_RESET_NUM = 180;

	//------------------
	// プロパティ.
	//------------------
	public bool IsPlay => _isPlay;
	public double MusicEndTime => _musicEndTime;
	public bool HasClip => _audioSource.clip != null;
	public float MusicRate { get; private set; }
	public AudioSource AudioSource => _audioSource;

	/// <summary>
	/// 非同期の初期化処理
	/// </summary>
	/// <param name="ct">キャンセルトークン</param>
	/// <param name="directoryPath">フォルダパス</param>
	/// <param name="fileName">ファイル名</param>
	/// <param name="lastNoteTime">最後のノーツの時間</param>
	/// <param name="time">開始時間</param>
	/// <returns></returns>
	public async UniTask InitAsync(CancellationToken ct, string directoryPath, string fileName, double lastNoteTime, float time = 0f)
	{
		try
		{
			var path = ZString.Concat("file://", Path.Combine(directoryPath, fileName));
			_audioClip = await AudioClipLoader.LoadAsync(ct, path, false);
		}
		catch (Exception ex)
		{
			var builder = new DialogParametor.Builder("楽曲読み込みエラー", ex.Message);
			builder.AddDefaultAction("戻る", () => GameManager.Instance.ChangeScene(SceneName.SelectMusic));
			builder.AddCallbackOnAutoClosed(() => GameManager.Instance.ChangeScene(SceneName.SelectMusic));
			DialogManager.Instance.Open(builder.Build());
			return;
		}

		if(_audioClip == null)
		{
			var builder = new DialogParametor.Builder("楽曲読み込みエラー", "楽曲ファイルが見つかりませんでした");
			builder.AddDefaultAction("戻る", () => GameManager.Instance.ChangeScene(SceneName.SelectMusic));
			builder.AddCallbackOnAutoClosed(() => GameManager.Instance.ChangeScene(SceneName.SelectMusic));
			DialogManager.Instance.Open(builder.Build());
			return;
		}

		_lastNoteTime = lastNoteTime;
		_frequency = _audioClip.frequency;

		SetParam(_audioClip, time);
	}

	/// <summary>
	/// 各パラメータを設定
	/// </summary>
	/// <param name="audioClip">AudioClip</param>
	/// <param name="time">開始時間</param>
	void SetParam(AudioClip audioClip, float time)
	{
		bool isMidStart = time > 0f;

		_minusPlayTime = isMidStart ? time - MID_START_MARGIN : -START_MARGIN;
		_maxVolume = GameManager.Instance.VolumeOption.MusicVolume;
		_audioSource.mute = GameManager.Instance.VolumeOption.MusicMute;
		SetMusicRate(GameManager.Instance.JudgeTimeOption.MusicRate);
		_lastSecondHint = GameManager.Instance.LastSecondHint;
		_musicEndTime = _audioClip.length < _lastNoteTime ? _audioClip.length : _lastNoteTime;

		if (_musicEndTime < _lastSecondHint)
		{
			_musicEndTime = _lastSecondHint;
		}

		_audioSource.volume = isMidStart ? 0f : _maxVolume;
		_audioSource.clip = audioClip;
		_audioSource.loop = false;

		if (isMidStart)
		{
			_counter = COUNTER_RESET_NUM - 5;
			_audioSource.time = (float)_minusPlayTime;
			StartCoroutine(VolumeChangeRoutine(time));
		}

		_isPlay = true;
		_isMinusPlayTime = true;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
	}

	/// <summary>
	/// 楽曲速度の設定
	/// </summary>
	/// <param name="musicRate">速度(0.75～1.5)</param>
	public void SetMusicRate(float musicRate)
	{
		MusicRate = musicRate;
		bool isMusicRate = !Calculate.FastApproximately(musicRate, 1f, 0.0001f);
		_audioSource.outputAudioMixerGroup = isMusicRate ? _pitchAudioMixer : null;

		if (isMusicRate)
		{
			_pitchAudioMixer.audioMixer.SetFloat(Constant.AudioMixer.PITCH, 1f / musicRate);
		}

		_audioSource.pitch = musicRate;
	}

	/// <summary>
	/// 途中再生の場合に呼ばれる.
	/// 少しずつ音量を上げることにより、フェードインさせる
	/// </summary>
	/// <param name="time"></param>
	/// <returns></returns>
	IEnumerator VolumeChangeRoutine(float time)
	{
		var length = (float)MID_START_MARGIN;

		while (_audioSource.time < time)
		{
			float t = Mathf.Clamp01((_audioSource.time - time + length) / length);
			_audioSource.volume = Mathf.Lerp(0f, _maxVolume, t);
			yield return null;
		}

		_audioSource.volume = _maxVolume;
	}

	/// <summary>
	/// 毎フレーム呼ばれる更新処理
	/// </summary>
	public void CallUpdate()
	{
		// Clipが設定されていない時
		if (!HasClip)
		{
			return;
		}

		double deltaTime = Time.deltaTime;

		// 見た目上は再生状態であるが、実際のAudio再生開始の手前位置の時
		if (_isMinusPlayTime && _isPlay)
		{
			_minusPlayTime += deltaTime * _audioSource.pitch;

			// 実際の再生開始時間になったら
			if (_minusPlayTime >= 0)
			{
				// Audio再生
				_audioSource.Play();

				_cacheTime = Time.realtimeSinceStartupAsDouble;
				_isMinusPlayTime = false;
			}

			return;
		}

		// LastSecondHintタグの時間で終了が伸びた分を担当する
		if (_isOverPlayTime && _isPlay)
		{
			_overPlayTime += deltaTime * _audioSource.pitch;

			if (_lastSecondHint < _overPlayTime)
			{
				OnFinishSong();
			}
		}


		if (_isPlay)
		{
			var deltaSamples = _counter == 0
						? (_audioSource.timeSamples - _prevFrameSamples)
						: _audioClip.frequency * _audioSource.pitch * deltaTime;

			_smoothedTimeSamples += deltaSamples;
			_prevFrameSamples = _smoothedTimeSamples;

			_counter = ++_counter % COUNTER_RESET_NUM;
		}

		// 曲が終了または最後のノートから２秒後に譜面を終了させる
		bool isEndMusic = (_audioSource.time >= _audioClip.length - deltaTime);
		bool isOverLastNoteDelayTime = (_audioSource.time >= _lastNoteTime + DELAY_ENDTIME);
		if (isEndMusic || isOverLastNoteDelayTime)
		{
			// LastSecondHintの方が大きい場合はOverPlayTimeに切り替える
			double time = GetMusicTime();
			if (time < _lastSecondHint)
			{
				_overPlayTime = time;
				_isOverPlayTime = true;
				return;
			}

			OnFinishSong();
		}
	}

	void OnFinishSong()
	{
		_isPlay = false;
		OnFinishMusic.Invoke();
		_audioSource.Stop();
		Screen.sleepTimeout = SleepTimeout.SystemSetting;
	}

	/// <summary>
	/// 現在の再生時間を取得
	/// </summary>
	/// <returns>再生時間</returns>
	public double GetMusicTime()
	{
		if (_isMinusPlayTime)
		{
			return _minusPlayTime;
		}

		if (_isOverPlayTime)
		{
			return _overPlayTime;
		}

		if (!_isPlay)
		{
			return _beforeTime;
		}

		double time = _smoothedTimeSamples / _frequency;

		// MEMO: _audioSource.time(timeSamples)が正確な値ではなく、次のフレームで同じ値を返すことがある
		// その為、変わった時の時間を保持(_beforeTime)しておいて、次に変わるまで実時間のキャッシュと現在の実時間の差分を加算する
		if (Calculate.FastApproximately(time, _beforeTime, 0.002d))
		{
			//_audioSource.time(timeSamples)が正確な値ではなく、飛び飛びの値を返すので補正を加える.
			time += Time.realtimeSinceStartupAsDouble - _cacheTime;
		}
		else
		{
			_beforeTime = time;
			_cacheTime = Time.realtimeSinceStartupAsDouble;
		}

		return time;
	}

	/// <summary>
	/// 一時停止
	/// </summary>
	public void Pause()
	{
		_isPlay = false;

		if (!_isMinusPlayTime)
		{
			_audioSource.Pause();
		}

		Screen.sleepTimeout = SleepTimeout.SystemSetting;
	}

	/// <summary>
	/// 再生を再開
	/// </summary>
	public void Play()
	{
		_isPlay = true;

		if (!_isMinusPlayTime)
		{
			_audioSource.Play();
			_cacheTime = Time.realtimeSinceStartupAsDouble;
		}

		Screen.sleepTimeout = SleepTimeout.NeverSleep;
	}

	public void DisbaleAudioMixer()
	{
	}

	void OnDestroy()
	{
		//if (!_audioSource.isPlaying && _audioClip != null)
		//{
		//	Resources.UnloadAsset(_audioClip);
		//}

		_audioClip = null;
		OnFinishMusic = null;
	}

}
