﻿using System;
using TMPro;
using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{
	[Serializable]
	[LuaCallCSharp]
	public sealed class ActorUIText : Actor
	{
		[SerializeField] TextMeshProUGUI _uiText;
		[SerializeField] RectTransform _rectTransform;

		/// <summary>
		/// 自身の持つTextMeshProUGUIを取得します
		/// </summary>
		/// <returns></returns>
		public TextMeshProUGUI GetTextMeshProUGUI()
		{
			return _uiText;
		}

		/// <summary>
		/// 自身の持つRectTransformを取得します
		/// </summary>
		/// <returns></returns>
		public RectTransform GetRectTransform()
		{
			return _rectTransform;
		}

		/// <summary>
		/// 表示するテキストを設定します
		/// </summary>
		/// <param name="text"></param>
		public void SetText(string text)
		{
			_uiText.text = text;
		}

		/// <summary>
		/// テキストの大きさを設定します
		/// デフォルトは36に設定されています
		/// </summary>
		/// <param name="size"></param>
		public void SetSize(int size)
		{
			_uiText.fontSize = size;
		}

		/// <summary>
		/// テキストの色を設定します
		/// </summary>
		/// <param name="color"></param>
		public void SetColor(Color color)
		{
			_uiText.color = color;
		}

		/// <summary>
		/// テキストの色を設定します
		/// </summary>
		/// <param name="red">赤</param>
		/// <param name="green">緑</param>
		/// <param name="blue">青</param>
		/// <param name="alpha">透明度</param>
		public void SetColor(float red, float green, float blue, float alpha)
		{
			_uiText.color = new Color(red, green, blue, alpha);
		}

		/// <summary>
		/// テキストの配置の基準点を設定します
		/// デフォルトのanchorX,Yは0.5に設定されています
		/// </summary>
		/// <param name="x">画面の横軸 (0～1) 左が0、右が1</param>
		/// <param name="anchorY">画面の縦軸 (0～1) 下が0、上が1</param>
		public void SetAnchor(float anchorX, float anchorY)
		{
			anchorX = Mathf.Clamp01(anchorX);
			anchorY = Mathf.Clamp01(anchorY);
			_rectTransform.anchorMin = new Vector2(anchorX, anchorY);
			_rectTransform.anchorMax = new Vector2(anchorX, anchorY);
		}

		/// <summary>
		/// 配置の基準点からのテキストの相対位置を設定します
		/// </summary>
		/// <param name="posX"></param>
		/// <param name="posY"></param>
		public void SetAnchoredPosition(float posX, float posY)
		{
			_rectTransform.anchoredPosition = new Vector2(posX, posY);
		}

		/// <summary>
		/// テキストを太字にするか設定します
		/// </summary>
		/// <param name="isBold">太字にするか</param>
		public void SetBold(bool isBold)
		{
			_uiText.fontStyle = isBold ? FontStyles.Bold : FontStyles.Normal;
		}

		/// <summary>
		/// テキストの外枠を設定します
		/// </summary>
		/// <param name="outlineWidth">外枠の太さ(0～1)</param>
		/// <param name="color">外枠の色</param>
		public void SetOutLine(float outlineWidth, Color color)
		{
			_uiText.material.SetFloat(Constant.ShaderProperty.OutlineWidth, outlineWidth);
			_uiText.material.SetFloat(Constant.ShaderProperty.FaceDilate, outlineWidth);
			_uiText.outlineColor = color;
		}
	}
}