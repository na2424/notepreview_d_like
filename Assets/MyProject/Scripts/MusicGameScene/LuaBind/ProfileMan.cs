﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{
	[Serializable]
	[LuaCallCSharp]
	public sealed class ProfileMan
    {

        public HighScore GetHighScore()
        {
            return new HighScore(0, 0, 0, 0);
        }
    }
}