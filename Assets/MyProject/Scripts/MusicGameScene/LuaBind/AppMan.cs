﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{
	[LuaCallCSharp]
	public enum PlatformType
	{
		Other = 0,
		Windows = 1,
		MacOS = 2,
		Android = 3,
		iOS = 4,
	}

	[Serializable]
	[LuaCallCSharp]
	public sealed class AppMan
	{
		/// <summary>
		/// アプリのバージョンを取得します
		/// </summary>
		/// <returns></returns>
		public string GetAppVersion()
		{
			return Constant.App.VersionName;
		}

		/// <summary>
		/// プラットフォームをで取得します (EnumのPlatformTypeが返ります)
		/// Other	= 0,
		/// Windows = 1,
		/// MacOS	= 2,
		/// Android = 3,
		/// iOS		= 4,
		/// </summary>
		/// <returns></returns>
		public PlatformType GetPlatform()
		{
			return RuntimePlatformToPlatformType(Application.platform);
		}

		/// <summary>
		/// プラットフォームをint型で取得します
		/// Other	= 0,
		/// Windows = 1,
		/// MacOS	= 2,
		/// Android = 3,
		/// iOS		= 4,
		/// </summary>
		/// <returns></returns>
		public int GetPlatformInt()
		{
			return (int)RuntimePlatformToPlatformType(Application.platform);
		}

		/// <summary>
		/// 使用Unityのバージョンを取得します
		/// </summary>
		/// <returns></returns>
		public string GetUnityVersion()
		{
			return Application.unityVersion;
		}

		/// <summary>
		/// アプリフォルダのパスを取得します
		/// </summary>
		/// <returns></returns>
		public string GetPersistentDataPath()
		{
			return Application.persistentDataPath;
		}

		/// <summary>
		/// NotePreviewか
		/// </summary>
		/// <returns></returns>
		public bool IsNotePreview()
		{
			return true;
		}

		PlatformType RuntimePlatformToPlatformType(RuntimePlatform type) => type switch
		{
			RuntimePlatform.WindowsEditor => PlatformType.Windows,
			RuntimePlatform.WindowsPlayer => PlatformType.Windows,
			RuntimePlatform.WindowsServer => PlatformType.Windows,
			RuntimePlatform.OSXEditor => PlatformType.MacOS,
			RuntimePlatform.OSXPlayer => PlatformType.MacOS,
			RuntimePlatform.OSXServer => PlatformType.MacOS,
			RuntimePlatform.Android => PlatformType.Android,
			RuntimePlatform.IPhonePlayer => PlatformType.iOS,
			_ => PlatformType.Other
		};
	}

}
