﻿using UnityEngine;

namespace DankagLikeLuaAPI
{
    public interface IEnemyBullet
    {
        public SpriteRenderer SpriteRenderer { get; }
        public Transform Transform { get; }
        public Vector2 Velocity { get; }
        public Vector2 Acceleration { get; }
        public float AngularVelocity { get; }
        public float AngularAcceleration { get; }
        public float Size { get; }
        public int Damage { get; }
        public bool IsPersistence { get; }
        public int ReleaseType { get; }
        public bool IsReleased { get; }

        /// <summary>
        /// 速度を設定します
        /// </summary>
        /// <param name="velocity">速度</param>
        public void SetVelocity(Vector2 velocity);

        /// <summary>
        /// 加速度を設定します
        /// </summary>
        /// <param name="accelation">加速度</param>
        public void SetAccelation(Vector2 accelation);

        /// <summary>
        /// 角速度を設定します
        /// </summary>
        /// <param name="angularVelocity">角速度</param>
        public void SetAngularVelocity(float angularVelocity);

        /// <summary>
        /// 角加速度を設定します
        /// </summary>
        /// <param name="angularAcceleration">角加速度</param>
        public void SetAngularAcceleration(float angularAcceleration);

        /// <summary>
        /// Spriteを設定します
        /// </summary>
        /// <param name="sprite">スプライト</param>
        public void SetSprite(Sprite sprite);

        /// <summary>
        /// サイズを設定します
        /// </summary>
        /// <param name="size">見た目の大きさ</param>
        public void SetSize(float size);

        /// <summary>
        /// 当たり判定の距離を設定します
        /// </summary>
        /// <param name="hitDistance">当たり判定の距離</param>
        public void SetHitDistance(float hitDistance);

        /// <summary>
        /// HPの最大値を100とした場合のダメージ量を設定します
        /// </summary>
        /// <param name="damage">ダメージ量</param>
        public void SetDamage(int damage);

        /// <summary>
        /// 画像の回転速度を設定します
        /// </summary>
        /// <param name="rotationSpeed">画像の回転速度</param>
        public void SetRotationSpeed(float rotationSpeed);

        /// <summary>
        /// 破棄タイプを設定します
        /// 0 → 生存時間(LifeTime)を過ぎるか画面から見えなくなったら破棄 (デフォルト)
        /// 1 → 生存時間(LifeTime)を過ぎるか一度画面に表示されて見えなくなったら破棄
        /// 2 → 生存時間(LifeTime)を過ぎた時だけ破棄
        /// </summary>
        public void SetReleaseType(int releaseType);

        /// <summary>
        /// カスタム属性を取得します
        /// 無い場合はnull(nil)を返します
        /// </summary>
        /// <param name="key">属性キー</param>
        public string GetAttribute(string key);

        /// <summary>
        /// カスタム属性を設定します
        /// </summary>
        /// <param name="key">属性キー</param>
        /// <param name="value">属性値</param>
        public void SetAttribute(string key, string value);

	}
}