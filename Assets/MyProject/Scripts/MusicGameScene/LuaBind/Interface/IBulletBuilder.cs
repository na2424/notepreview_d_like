﻿using System;
using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{
    /// <summary>
    /// 敵弾生成クラスのInterface
    /// DanmakuStageクラスのGetBulletBuilder関数から取得されます。
    /// 弾の設定をメソッドチェーンで記述して、最後にBuild関数を呼ぶことで弾を発射します。
    /// </summary>
    [LuaCallCSharp]
    public interface IBulletBuilder
    {
        /// <summary>
        /// 基礎パラメータを設定します
        /// With～関数より前に記述する必要があります
        /// </summary>
        /// <param name="startPosition">発射位置</param>
        /// <param name="initialVelocity">初速度</param>
        /// <param name="hitDistance">当たり判定距離</param>
        /// <param name="size">見た目の大きさ</param>
        /// <param name="damage">ダメージ量</param>
        public BulletBuilder SetBasicParam(Vector2 startPosition, Vector2 initialVelocity, float hitDistance = 0.15f, float size = 0.3f, int damage = 10);

        /// <summary>
        /// アプリに組み込まれている弾画像のSpriteを数値で設定(0～7)します
        /// </summary>
        /// <param name="spriteTypeInt">弾画像の種類のint</param>
        public BulletBuilder WithBasicSprite(int spriteTypeInt);

        /// <summary>
        /// 弾のSpriteを設定します
        /// </summary>
        public BulletBuilder WithSprite(Sprite sprite);

        /// <summary>
        /// 加速度を設定します
        /// </summary>
        public BulletBuilder WithAcceleration(Vector2 acceleration);

        /// <summary>
        /// 角速度を設定します
        /// </summary>
        public BulletBuilder WithAngularVelocity(float angularVelocity);

        /// <summary>
        /// 角加速度を設定します
        /// </summary>
        public BulletBuilder WithAngularAcceleration(float angularAcceleration);

        /// <summary>
        /// 画像の回転角度を設定します
        /// </summary>
        public BulletBuilder WithRotation(float rotation);

        /// <summary>
        /// 画像の回転速度を設定します
        /// </summary>
        public BulletBuilder WithRotationSpeed(float rotationSpeed);

        /// <summary>
        /// 弾の生存時間(秒)を設定します
        /// 画面外に出るか、生存時間が過ぎると弾が消えます
        /// </summary>
        public BulletBuilder WithLifeTime(float lifeTime);

        /// <summary>
        /// 弾の軌跡を設定します
        /// </summary>
        public BulletBuilder WithTrail(float width = 0.07f, float time = 0.5f);

        /// <summary>
        /// 毎フレーム呼ばれる関数を設定します
        /// 呼ばれる関数の引数にはEnemyBulletクラスの参照が取得されます
        /// </summary>
        /// <param name="luaFunction">引数を1つ持つLuaの関数</param>
        public BulletBuilder WithUpdateFunc(Action<EnemyBullet> luaFunction);

        /// <summary>
        /// 指定した秒数後に呼ばれる関数を設定します
        /// 呼ばれる関数の引数にはEnemyBulletクラスの参照が取得されます
        /// </summary>
        /// <param name="luaFunction">引数を1つ持つLuaの関数</param>
        /// <param name="delayTime">弾の発生後からの秒数</param>
        public BulletBuilder WithDelayFunc(Action<EnemyBullet> luaFunction, float delayTime);

        /// <summary>
        /// SortingOrder(重ね順)を設定します
        /// </summary>
        /// <param name="sortingOrder">重ね順</param>
        public BulletBuilder WithSortingOrder(int sortingOrder);

		/// <summary>
		/// カスタム属性を設定します
		/// </summary>
		/// <param name="key">属性キー</param>
		/// <param name="value">属性値</param>
		public BulletBuilder WithAttribute(string key, string value);

		/// <summary>
		/// 破棄タイプを設定します
		/// 0 → 生存時間(LifeTime)を過ぎるか画面から見えなくなったら破棄 (Default)
		/// 1 → 生存時間(LifeTime)を過ぎるか一度画面に表示されて見えなくなったら破棄
		/// 2 → 生存時間(LifeTime)を過ぎた時だけ破棄
		/// </summary>
		/// <param name="releaseType">破棄タイプのint</param>
		public BulletBuilder WithReleaseType(int releaseType);

        /// <summary>
        /// 弾が自機に当たっても消えないようにするか
        /// (isPersistenceがtrueの時、resumptionTimeに設定した秒数の後にヒット判定が再開されます)
        /// </summary>
        /// <param name="isPersistence">持続性の弾にするか</param>
        /// <param name="resumptionTime">ヒット判定の再開時間(秒)</param>
        public BulletBuilder WithPersistence(bool isPersistence, float resumptionTime = 1f);

		/// <summary>
		/// 弾設定を取得します
		/// </summary>
		public BulletInfo GetBulletInfo();

		/// <summary>
		/// 弾設定をBulletBuilderに反映します
		/// </summary>
		public void SetBulletInfo(BulletInfo bulletInfo);

		/// <summary>
		/// 設定を構築して弾を発射します
		/// </summary>
		public void Build();

    }
}