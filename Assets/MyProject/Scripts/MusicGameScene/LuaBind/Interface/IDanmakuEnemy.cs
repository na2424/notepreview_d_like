﻿using System;
using UnityEngine;

namespace DankagLikeLuaAPI
{
    public interface IDanmakuEnemy
    {
        /// <summary>
        /// ワールド座標の位置
        /// </summary>
        public Vector2 Position { get; }

		/// <summary>
		/// 倒されたか
		/// </summary>
		public bool IsDead { get; }

		/// <summary>
		/// SpriteRendererを取得します
		/// </summary>
		/// <returns></returns>
		public SpriteRenderer GetSpriteRenderer();

        /// <summary>
        /// Transformを取得します
        /// </summary>
        /// <returns></returns>
        public Transform GetTransform();

        /// <summary>
        /// 指定した数値(0～60)からキャラクターのSpriteを設定します
        /// </summary>
        /// <param name="charaIndex"></param>
        public void SetCharacter(int charaIndex);

        /// <summary>
        /// ファイル名から画像を読み込んでSpriteを設定します
        /// </summary>
        /// <param name="path">path</param>
        public void LoadSprite(string path);

        /// <summary>
        /// Spriteを設定します
        /// </summary>
        /// <param name="sprite"></param>
        public void SetSprite(Sprite sprite);

        /// <summary>
        /// 位置を設定します(Vector2版)
        /// </summary>
        /// <param name="position"></param>
        public void SetPosition(Vector2 position);

        /// <summary>
        /// 位置を設定します(数値2つ版)
        /// </summary>
        /// <param name="x"></param>
        /// <param name="z"></param>
        public void SetPosition(float x, float z);

        /// <summary>
        /// サイズを設定します
        /// </summary>
        /// <param name="size"></param>
        public void SetSize(float size);

        /// <summary>
        /// Colorを設定します
        /// </summary>
        /// <param name="color">Color</param>
        public void SetColor(Color color);

        /// <summary>
        /// Colorを設定します (数値4つ版)
        /// </summary>
        /// <param name="r">Red</param>
        /// <param name="g">Green</param>
        /// <param name="b">Blue</param>
        /// <param name="a">Alpha</param>
        public void SetColor(float r, float g, float b, float a);

        /// <summary>
        /// 透明度を設定します(0～1)
        /// </summary>
        /// <param name="alpha">透明度</param>
        public void SetAlpha(float alpha);

        /// <summary>
        /// 残りライフポイントを取得します
        /// </summary>
        /// <param name="lifePoint"></param>
        public int GetLifePoint();

        /// <summary>
        /// 残りライフポイントを設定します
        /// </summary>
        /// <param name="lifePoint"></param>
        public void SetLifePoint(int lifePoint);

        /// <summary>
        /// 最大ライフポイントを設定します
        /// </summary>
        /// <param name="lifePoint"></param>
        public void SetMaxLifePoint(int maxLifePoint);

        /// <summary>
        /// ライフ数値を表示するか設定します
        /// </summary>
        public void SetActiveLifePoint(bool isActive);

        /// <summary>
        /// 倒されたときに呼ばれる関数を設定します
        /// </summary>
        /// <param name="func"></param>
        public void AddListnerOnDead(Action func);

        /// <summary>
        /// 倒されたときに呼ばれる関数を設定します
        /// </summary>
        /// <param name="func"></param>
        public void AddListenerOnDead(Action func);
    }
}