﻿using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{
    [LuaCallCSharp]
    public interface IDanmakuStage
    {
        DanmakuPlayer Player { get; }
        DanmakuEnemy Enemy { get; }

        /// <summary>
        /// 弾幕ステージを開始します
        /// </summary>
        public void Begin();

        /// <summary>
        /// ダンマクステージを終了します
        /// </summary>
        public void End();

        /// <summary>
        /// 敵弾生成を行なうクラスを取得します
        /// メソッドチェーンの形で設定を記述して、
        /// 最後にBuild関数を呼ぶことで弾が発射されます
        /// </summary>
        public IBulletBuilder GetBulletBuilder();

        /// <summary>
        /// 弾幕ステージの自機クラスを取得します
        /// </summary>
        /// <returns>DanmakuPlayer</returns>
        public DanmakuPlayer GetPlayer();

        /// <summary>
        /// 弾幕ステージの敵クラスを取得します
        /// </summary>
        /// <returns>DanmakuPlayer</returns>
        public DanmakuEnemy GetEnemy();

        /// <summary>
        /// 敵キャラクターのSpriteを設定します
        /// カットイン演出のキャラクター画像も変更されます
        /// SpriteはUTILクラスの関数を使用して生成できます
        /// </summary>
        /// <param name="sprite"></param>
        public void SetEnemySprite(Sprite sprite);

        /// <summary>
        /// プレイヤーの移動方法の種類を設定します
        /// 0 → 指を動かした差分
        /// 1 → 指の位置
        /// </summary>
        public void SetMoveTypeInt(int moveType);

        /// <summary>
        /// カメラモードを設定します(int版)
        /// 0 → 近い上空
        /// 1 → 遠い上空
        /// デフォルトは1に設定されています
        /// </summary>
        public void SetCameraModeInt(int cameraMode);

        /// <summary>
        /// カメラモードを設定します(enum版)
        /// </summary>
        public void SetCameraMode(CameraMode cameraMode);

        /// <summary>
        /// 自機の移動範囲を判定ライン上に制限するか設定します
        /// デフォルトはfalseに設定されています
        /// </summary>
        /// <param name="isLimit">判定ライン上に制限するか</param>
        public void SetLimitPosition(bool isLimit);

        /// <summary>
        /// 指定した位置からプレイヤーへの方向ベクトルを取得します
        /// </summary>
        public Vector2 ToPlayerAiming(Vector2 startPosition);

	}
}