﻿using System;
using UnityEngine;

namespace DankagLikeLuaAPI
{
    public interface IDanmakuPlayer
    {
        /// <summary>
        /// ワールド座標の位置
        /// </summary>
        public Vector2 Position { get; }

        /// <summary>
        /// 自身のTransformを取得します
        /// </summary>
        /// <returns></returns>
        public Transform GetTransform();

        /// <summary>
        /// SpriteRendererを取得します
        /// </summary>
        /// <returns></returns>
        public SpriteRenderer GetSpriteRenderer();

		/// <summary>
		/// プレイヤーの移動方法を設定します
		/// </summary>
		/// <param name="playerMoveType"></param>
		public void SetPlayerMoveType(PlayerMoveType playerMoveType);

        /// <summary>
        /// プレイヤーの移動方法を設定します(Int版)
        /// 0 → 指を動かした差分
        /// 1 → 指の位置
        /// </summary>
        /// <param name="playerMoveType"></param>
        public void SetPlayerMoveTypeInt(int playerMoveTypeInt);

        /// <summary>
        /// 移動範囲を判定ライン上に制限するか設定します
        /// デフォルトはfalseに設定されています
        /// </summary>
        /// <param name="isLimit"></param>
        public void SetLimitJudgeLine(bool isLimit);

        /// <summary>
        /// ファイル名から画像を読み込んで設定します
        /// </summary>
        /// <param name="path">path</param>
        public void LoadSprite(string path);

        /// <summary>
        /// スプライトを設定します
        /// </summary>
        /// <param name="sprite">Sprite</param>
        public void SetSprite(Sprite sprite);

        /// <summary>
        /// Colorを設定します
        /// </summary>
        /// <param name="color">Color</param>
        public void SetColor(Color color);

        /// <summary>
        /// Colorを設定します
        /// </summary>
        /// <param name="r">Red</param>
        /// <param name="g">Green</param>
        /// <param name="b">Blue</param>
        /// <param name="a">Alpha</param>
        public void SetColor(float r, float g, float b, float a);

        /// <summary>
        /// 透明度を設定します(0～1)
        /// </summary>
        /// <param name="alpha">透明度</param>
        public void SetAlpha(float alpha);

        /// <summary>
        /// 画像のサイズを設定します
        /// デフォルトは0.5に設定されています
        /// </summary>
        /// <param name="size"></param>
        public void SetSize(float size);

        /// <summary>
        /// 敵弾に当たった時に呼び出す関数を設定します
        /// (関数の引数にはダメージ量が取得されます)
        /// </summary>
        /// <param name="func">Luaのfunction</param>
        public void AddListenerOnHit(Action<int> func);
    }
}