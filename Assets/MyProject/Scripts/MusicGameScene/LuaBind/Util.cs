﻿using Cysharp.Threading.Tasks;
using DG.Tweening;
using System;
using System.IO;
using System.Threading;
using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{
	[Serializable]
	[LuaCallCSharp]
	public sealed class Util
	{
		[SerializeField] Transform _cameraTransform;
		MusicManager _musicManager;
		MusicGame _musicGame;

		CancellationTokenSource _ctSource;

		float _baseBpm = 120f;
		float _visualOffset = 0f;
		bool _isCmod = false;
		float[] _spectrumData = null;
		static Util _instantce;

		public static Util Instantce => _instantce;

		[BlackList, DoNotGen]
		public void Init(MusicManager musicManager, MusicGame musicGame)
		{
			_instantce = null;
			_instantce = this;
			_musicManager = musicManager;
			_musicGame = musicGame;

			_ctSource = new CancellationTokenSource();
			_baseBpm = GameManager.Instance.SelectSongInfo.BaseBpm;
			_visualOffset = GameManager.Instance.DisplayOption.VisualOffset;
			_isCmod = GameManager.Instance.IsCMod;
		}

		/// <summary>
		/// ファイル名からテクスチャを返します
		/// </summary>
		/// <param name="textureFileName">fileName</param>
		/// <returns>Texture2D</returns>
		public Texture2D LoadTexture(string textureFileName)
		{
			string path = Path.Combine(LuaManager.FolderPath, textureFileName).Replace("\\", "/");
			return TextureLoader.Load(path);
		}

		/// <summary>
		/// テクスチャからSpriteを作成して返します
		/// </summary>
		/// <param name="texture">Texture2D</param>
		/// <returns>Sprite</returns>
		public Sprite CreateSprite(Texture2D texture)
		{
			return Sprite.Create(
				texture,
				new Rect(0f, 0f, texture.width, texture.height),
				new Vector2(0.5f, 0.5f),
				100f
			);
		}

		/// <summary>
		/// 1枚のテクスチャから複数のSpriteを作成して返します
		/// 横の分割数(horizontalCount) * 縦の分割数(verticalCount) 枚のSpriteを
		/// 下段の左から右へ順番に生成します
		/// 
		/// C#から配列を返す場合、xLuaの仕様で開始の添え字が0になるようです
		/// </summary>
		/// <param name="texture"></param>
		/// <param name="horizontalCount"></param>
		/// <param name="verticalCount"></param>
		/// <returns>Sprite[]</returns>
		public Sprite[] CreateMultiSprite(Texture2D texture, int horizontalCount, int verticalCount)
		{
			int count = horizontalCount * verticalCount;
			var sprites = new Sprite[count];
			int width = texture.width / horizontalCount;
			int height = texture.height / verticalCount;

			for (int i = 0; i < count; i++)
			{
				int x = i % horizontalCount;
				int y = (count-i-1) / horizontalCount;

				sprites[i] = Sprite.Create(
					texture,
					new Rect(x * width, y * height, width, height),
					new Vector2(0.5f, 0.5f),
					100f
				);
			}

			return sprites;
		}

		/// <summary>
		/// 引数に渡したShaderのMaterialを生成して返します
		/// </summary>
		/// <param name="shader"></param>
		/// <returns></returns>
		public Material CreateMaterial(Shader shader)
		{
			var material = new Material(shader);
			material.hideFlags = HideFlags.DontSave;
			return material;
		}

		/// <summary>
		/// シェーダープロパティー名からユニークIDを取得します
		///  
		/// すべてのマテリアルプロパティー関数に文字列を渡すよりもユニークIDを使用したほうが効率的です。
		///  例えばもし Material.SetColor を何度も呼び出したり MaterialPropertyBlockを使用する場合は、
		///  必要なプロパティーのユニークIDを一度だけ取得する方が効率的です。
		/// </summary>
		/// <param name="propertyName">シェーダープロパティー名</param>
		/// <returns>プロパティーのユニークID</returns>
		public int ShaderPropertyToID(string propertyName)
		{
			return Shader.PropertyToID(propertyName);
		}

		/// <summary>
		/// 極座標から直交座標のVector2(x, y)を返します
		/// </summary>
		/// <param name="radius"></param>
		/// <param name="declination"></param>
		/// <returns></returns>
		public Vector2 PolarToCartesian(float radius, float declination)
		{
			var rad = Mathf.Deg2Rad * declination;
			var x = Mathf.Cos(rad) * radius;
			var y = Mathf.Sin(rad) * radius;
			return new Vector2(x, y);
		}

		/// <summary>
		/// 直交座標から極座標のVector2(動径, 偏角)を返します
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		public Vector2 CartesianToPolar(float x, float y)
		{
			var r = Mathf.Sqrt(x * x + y * y);
			var theta = Mathf.Atan2(y, x) * Mathf.Rad2Deg;
			return new Vector2(r, theta);
		}

		/// <summary>
		/// 判定までの拍数からノーツのZ座標を計算して返します
		/// (CMODを使用している場合、少し計算量が増えます)
		/// </summary>
		/// <param name="beat"></param>
		/// <returns></returns>
		public float CalculateBeatToNotePositionZ(double beat)
		{
			float posZ = 0f;

			if (_isCmod)
			{
				var currentTime = _musicManager.GetMusicTime();
				var currentBeat = _musicGame.BpmHelper.TimeToBeat(currentTime);
				var targetTime = _musicGame.BpmHelper.BeatToTime(currentBeat + beat);
				var time = targetTime - currentTime;

				posZ = (float)(time * Constant.Note.TIME_DISTANCE * GameParam.Instance.NoteSpeed * SpeedStretchRatio.CurrentValue - (_visualOffset + Constant.Note.OFFSET_Z));
			}
			else
			{
				posZ = (float)(beat * Constant.Note.BEAT_DISTANCE * (Constant.Note.FIXED_BPM / _baseBpm) * GameParam.Instance.NoteSpeed * SpeedStretchRatio.CurrentValue - (_visualOffset + Constant.Note.OFFSET_Z));
			}

			if (GameParam.Instance.NotesScrollType == NotesScrollType.Decelerate)
			{
				posZ = GameManager.Instance.NoteScrollAnimationCurve.Evaluate(posZ);
			}

			if (float.IsNaN(posZ))
			{
				posZ = 100f;
			}

			return posZ;
		}

		/// <summary>
		/// 判定までの時間(秒)からノーツのZ座標を計算して返します
		/// (CMODを使用していない場合、少し計算量が増えます)
		/// </summary>
		/// <param name="time"></param>
		/// <returns></returns>
		public float CalculateTimeToNotePositionZ(double time)
		{
			float posZ = 0f;

			if (_isCmod)
			{
				posZ = (float)(time * Constant.Note.TIME_DISTANCE * GameParam.Instance.NoteSpeed * SpeedStretchRatio.CurrentValue - (_visualOffset + Constant.Note.OFFSET_Z));
			}
			else
			{
				var currentTime = _musicManager.GetMusicTime();
				var currentBeat = _musicGame.BpmHelper.TimeToBeat(currentTime);
				var targetBeat = _musicGame.BpmHelper.TimeToBeat(currentTime + time);
				var beat = targetBeat - currentBeat;

				posZ = (float)(beat * Constant.Note.BEAT_DISTANCE * (Constant.Note.FIXED_BPM / _baseBpm) * GameParam.Instance.NoteSpeed * SpeedStretchRatio.CurrentValue - (_visualOffset + Constant.Note.OFFSET_Z));
			}

			if (GameParam.Instance.NotesScrollType == NotesScrollType.Decelerate)
			{
				posZ = GameManager.Instance.NoteScrollAnimationCurve.Evaluate(posZ);
			}

			if (float.IsNaN(posZ))
			{
				posZ = 100f;
			}

			return posZ;
		}

		/// <summary>
		/// ノーツのZ座標からノーツの向き(Quaternion)を計算して返します
		/// </summary>
		/// <param name="beat"></param>
		/// <returns></returns>
		public Quaternion CalculateNoteRotation(float positionZ)
		{
			// 回転
			float rot = GameManager.Instance.InverseAnimationCurve.Evaluate(positionZ) * 0.78f;

			if (float.IsNaN(rot))
			{
				rot = 0f;
			}

			return Quaternion.Euler(90f - rot, 0f, 0f);
		}

		/// <summary>
		/// スクロールタグを適用した拍数を計算して返します
		/// LuaAPI ver2.0
		/// </summary>
		/// <param name="beat"></param>
		/// <returns></returns>
		public double CalculateScrolledBeat(double beat)
		{
			return _musicGame.ScrollHelper.ApplyScroll(beat);
		}

		/// <summary>
		/// Tweenアニメーション 指定のワールド座標へ移動します
		/// </summary>
		/// <param name="tr">Transform</param>
		/// <param name="pos">移動させたいワールド座標</param>
		/// <param name="duration">移動に掛ける時間</param>
		/// <param name="ease">イージングの種類の文字列</param>
		/// <param name="onComplete">完了後の処理</param>

		public void TweenPosition(Transform tr, Vector3 pos, float duration, string ease = "Linear", Action onComplete = null)
		{
			if (onComplete == null)
				tr.DOMove(pos, duration).SetEase(GetEase(ease));
			else
				tr.DOMove(pos, duration).SetEase(GetEase(ease)).OnComplete(() => onComplete());
		}

		/// <summary>
		/// Tweenアニメーション 指定のローカル座標へ移動します
		/// </summary>
		/// <param name="tr">Transform</param>
		/// <param name="pos">移動させたいローカル座標</param>
		/// <param name="duration">移動に掛ける時間</param>
		/// <param name="ease">イージングの種類の文字列</param>
		/// <param name="onComplete">完了後の処理</param>
		public void TweenLocalPosition(Transform tr, Vector3 pos, float duration, string ease = "Linear", Action onComplete = null)
		{
			if (onComplete == null)
				tr.DOLocalMove(pos, duration).SetEase(GetEase(ease));
			else
				tr.DOLocalMove(pos, duration).SetEase(GetEase(ease)).OnComplete(() => onComplete());
		}

		/// <summary>
		/// Tweenアニメーション 指定のワールド座標の向きに回転させます (Vector3版)
		/// </summary>
		/// <param name="tr">Transform</param>
		/// <param name="rot">回転させたいワールド座標の向き</param>
		/// <param name="duration">回転に掛ける時間</param>
		/// <param name="ease">イージングの種類の文字列</param>
		/// <param name="onComplete">完了後の処理</param>
		public void TweenRotation(Transform tr, Vector3 rot, float duration, string ease = "Linear", Action onComplete = null)
		{
			if (onComplete == null)
				tr.DORotate(rot, duration, RotateMode.FastBeyond360).SetEase(GetEase(ease));
			else
				tr.DORotate(rot, duration, RotateMode.FastBeyond360).SetEase(GetEase(ease)).OnComplete(() => onComplete());
		}

		/// <summary>
		/// Tweenアニメーション 指定のワールド座標の向きに回転させます (Quaternion版)
		/// </summary>
		/// <param name="tr">Transform</param>
		/// <param name="rot">回転させたいワールド座標の向き</param>
		/// <param name="duration">回転に掛ける時間</param>
		/// <param name="ease">イージングの種類の文字列</param>
		/// <param name="onComplete">完了後の処理</param>
		public void TweenRotateQuaternion(Transform tr, Quaternion rot, float duration, string ease = "Linear", Action onComplete = null)
		{
			if (onComplete == null)
				tr.DORotateQuaternion(rot, duration).SetEase(GetEase(ease));
			else
				tr.DORotateQuaternion(rot, duration).SetEase(GetEase(ease)).OnComplete(() => onComplete());
		}

		/// <summary>
		/// Tweenアニメーション 指定のローカル座標の向きに回転させます (Vector3版)
		/// </summary>
		/// <param name="tr">Transform</param>
		/// <param name="rot">回転させたいローカル座標の向き</param>
		/// <param name="duration">回転に掛ける時間</param>
		/// <param name="ease">イージングの種類の文字列</param>
		/// <param name="onComplete">完了後の処理</param>
		public void TweenLocalRotation(Transform tr, Vector3 rot, float duration, string ease = "Linear", Action onComplete = null)
		{
			if (onComplete == null)
				tr.DOLocalRotate(rot, duration, RotateMode.FastBeyond360).SetEase(GetEase(ease));
			else
				tr.DOLocalRotate(rot, duration, RotateMode.FastBeyond360).SetEase(GetEase(ease)).OnComplete(() => onComplete());
		}

		/// <summary>
		/// Tweenアニメーション 指定のローカル座標の向きに回転させます (Quarternion版)
		/// </summary>
		/// <param name="tr">Transform</param>
		/// <param name="rot">回転させたいローカル座標の向き</param>
		/// <param name="duration">回転に掛ける時間</param>
		/// <param name="ease">イージングの種類の文字列</param>
		/// <param name="onComplete">完了後の処理</param>
		public void TweenLocalRotateQuaternion(Transform tr, Quaternion rot, float duration, string ease = "Linear", Action onComplete = null)
		{
			if (onComplete == null)
				tr.DOLocalRotateQuaternion(rot, duration).SetEase(GetEase(ease));
			else
				tr.DOLocalRotateQuaternion(rot, duration).SetEase(GetEase(ease)).OnComplete(() => onComplete());
		}

		/// <summary>
		/// Tweenアニメーション 指定のサイズにします
		/// </summary>
		/// <param name="tr">Transform</param>
		/// <param name="scale">サイズ</param>
		/// <param name="duration">掛ける時間</param>
		/// <param name="ease">イージングの種類の文字列</param>
		/// <param name="onComplete">完了後の処理</param>
		public void TweenScale(Transform tr, Vector3 scale, float duration, string ease = "Linear", Action onComplete = null)
		{
			if (onComplete == null)
				tr.DOScale(scale, duration).SetEase(GetEase(ease));
			else
				tr.DOScale(scale, duration).SetEase(GetEase(ease)).OnComplete(() => onComplete());
		}

		/// <summary>
		/// 実行されているTweenアニメーションを停止します
		/// </summary>
		/// <param name="tr">Tweenアニメーション中のTransform</param>
		public void KillTween(Transform tr, bool complete = false)
		{
			tr.DOKill(complete);
		}

		/// <summary>
		/// 現在再生されているAudioSourceのスペクトラムデータのブロックを取得します
		/// </summary>
		/// <param name="size">サンプルサイズ</param>
		/// <param name="channel">サンプルする元のチャンネル</param>
		/// <param name="fftWindow">サンプリング時に使用する FFTWindow のタイプ</param>
		/// <returns>スペクトラムデータの配列 (要素番号は0から)</returns>
		public float[] GetSpectrumData(int size, int channel, FFTWindow fftWindow)
		{
			if (_spectrumData is null || _spectrumData.Length != size)
			{
				_spectrumData = new float[size];
			}
			_musicManager.AudioSource.GetSpectrumData(_spectrumData, channel, fftWindow);

			return _spectrumData;
		}

		/// <summary>
		/// 指定ミリ秒遅延させてAction(Luaの関数)を実行します
		/// </summary>
		/// <param name="millisecond">ミリ秒</param>
		/// <param name="action">Luaの関数</param>
		public void DelayAction(float millisecond, Action action)
		{
			DelayActionAsync((int)millisecond, action, _ctSource.Token).Forget();
		}

		/// <summary>
		/// 指定フレーム遅延させてAction(Luaの関数)を実行します
		/// </summary>
		/// <param name="frame">フレーム数</param>
		/// <param name="action">Luaの関数</param>
		public void DelayFrameAction(int frame, Action action)
		{
			DelayFrameActionAsync(frame, action, _ctSource.Token).Forget();
		}

		[DoNotGen]
		async UniTask DelayActionAsync(int millisecond, Action action, CancellationToken ct)
		{
			await UniTask.Delay(millisecond, false, PlayerLoopTiming.Update, ct);

			if (ct.IsCancellationRequested)
			{
				return;
			}

			action();
		}

		[DoNotGen]
		async UniTask DelayFrameActionAsync(int frame, Action action, CancellationToken ct)
		{
			await UniTask.DelayFrame(frame, PlayerLoopTiming.Update, ct);

			if (ct.IsCancellationRequested)
			{
				return;
			}

			action();
		}

		[DoNotGen]
		public static Ease GetEase(string ease) => ease switch
		{
			"Linear" => Ease.Linear,
			"InSine" => Ease.InSine,
			"OutSine" => Ease.OutSine,
			"InOutSine" => Ease.InOutSine,
			"InQuad" => Ease.InQuad,
			"OutQuad" => Ease.OutQuad,
			"InOutQuad" => Ease.InOutQuad,
			"InCubic" => Ease.InCubic,
			"OutCubic" => Ease.OutCubic,
			"InOutCubic" => Ease.InOutCubic,
			"InQuart" => Ease.InQuart,
			"OutQuart" => Ease.OutQuart,
			"InOutQuart" => Ease.InOutQuart,
			"InQuint" => Ease.InQuint,
			"OutQuint" => Ease.OutQuint,
			"InOutQuint" => Ease.InOutQuint,
			"InExpo" => Ease.InExpo,
			"OutExpo" => Ease.OutExpo,
			"InOutExpo" => Ease.InOutExpo,
			"InCirc" => Ease.InCirc,
			"OutCirc" => Ease.OutCirc,
			"InOutCirc" => Ease.InOutCirc,
			"InElastic" => Ease.InElastic,
			"OutElastic" => Ease.OutElastic,
			"InOutElastic" => Ease.InOutElastic,
			"InBack" => Ease.InBack,
			"OutBack" => Ease.OutBack,
			"InOutBack" => Ease.InOutBack,
			"InBounce" => Ease.InBounce,
			"OutBounce" => Ease.OutBounce,
			"InOutBounce" => Ease.InOutBounce,
			"Flash" => Ease.Flash,
			"InFlash" => Ease.InFlash,
			"OutFlash" => Ease.OutFlash,
			"InOutFlash" => Ease.InOutFlash,
			_ => Ease.Linear,
		};

		[DoNotGen]
		public void Dispose()
		{
			if (_ctSource != null)
			{
				_ctSource.Cancel();
				_ctSource = null;
			}
		}
	}

}