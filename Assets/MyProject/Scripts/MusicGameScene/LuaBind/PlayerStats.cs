﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{
	[LuaCallCSharp]
	public enum JudgeAreaType : int
	{
		Screen = 0,
		World
	}

	[Serializable]
	[LuaCallCSharp]
	public sealed class PlayerStats
	{
		[SerializeField] NoteTexture _noteTexture;
		[SerializeField] LongTexture _longTexture;

		ScoreManager _scoreManager;
		ComboManager _comboManager;
		LifeManager _lifeManager;
		TouchArea _touchArea;
		
		// プレイヤーのオプション
		NotesOption _notesOption;
		DisplayOption _displayOption;
		VolumeOption _volumeOption;
		JudgeTimeOption _judgeTimeOption;

		/// <summary>
		/// 初期化
		/// </summary>
		/// <param name="scoreManager"></param>
		/// <param name="comboManager"></param>
		/// <param name="lifeManager"></param>
		[BlackList, DoNotGen]
		public void Init(ScoreManager scoreManager, ComboManager comboManager, LifeManager lifeManager, TouchArea touchArea)
		{
			_scoreManager = scoreManager;
			_comboManager = comboManager;
			_lifeManager = lifeManager;
			_touchArea = touchArea;

			_notesOption = GameManager.Instance.NotesOption;
			_displayOption = GameManager.Instance.DisplayOption;
			_volumeOption = GameManager.Instance.VolumeOption;
			_judgeTimeOption = GameManager.Instance.JudgeTimeOption;
		}

		/// <summary>
		/// フルコンボが得られた場合はtrueを返します.
		/// </summary>
		/// <returns></returns>
		public bool IsFullCombo()
		{
			return _comboManager.IsFullCombo;
		}

		/// <summary>
		/// 現在のコンボを取得します.
		/// </summary>
		/// <returns></returns>
		public int GetCurrentCombo()
		{
			return _comboManager.CurrentCombo;
		}

		/// <summary>
		/// 現在のライフのパーセンテージ (0 ～ 1) を取得します.
		/// </summary>
		/// <returns></returns>
		public float GetCurrentLife()
		{
			return _lifeManager.LifeRate;
		}

		/// <summary>
		/// ライフが残り危険か
		/// </summary>
		/// <returns></returns>
		public bool IsDanger()
		{
			return _lifeManager.IsDanger;
		}

		/// <summary>
		/// ライフが無くなったか
		/// </summary>
		/// <returns></returns>
		public bool IsDead()
		{
			return _lifeManager.IsDead;
		}

		/// <summary>
		/// 残りライフを設定する
		/// </summary>
		public void SetLife(float life)
		{
			_lifeManager.SetLife(life);
		}

		/// <summary>
		/// グレードを取得します.
		/// </summary>
		/// <returns></returns>
		public int GetGrade()
		{
			return _scoreManager.CurrentRank;
		}

		/// <summary>
		/// スコアを取得します.
		/// </summary>
		/// <returns></returns>
		public int GetScore()
		{
			return _scoreManager.Score;
		}

		/// <summary>
		/// 最大コンボを取得します.
		/// </summary>
		/// <returns></returns>
		public int MaxCombo()
		{
			return _comboManager.MaxCombo;
		}

		/// <summary>
		/// 判定エリアタイプを設定します。
		/// 0 → Screen
		/// 1 → World
		/// </summary>
		/// <param name="type">判定エリアタイプ</param>
		public void SetJudgeAreaType(int type)
		{
			_touchArea.SetJudgeAreaType(type);
		}

		/// <summary>
		/// 現在の判定ラインの場所を判定エリアタイプがScreenの時にタッチした位置として反映します
		/// カメラの位置や角度を動かした後に呼ぶことで、判定ラインへの垂直ベクトルが更新されます
		/// </summary>
		public void UpdateScreenJudgeLine()
		{
			_touchArea.UpdateScreenJudgeLine();
		}

		/// <summary>
		/// ノーツ設定を取得します
		/// </summary>
		/// <returns>NotesOption</returns>
		public NotesOption GetNotesOptions()
		{
			return _notesOption.DeepCopy();
		}

		/// <summary>
		/// ノーツ設定をJson文字列で取得します
		/// </summary>
		/// <returns>Json文字列</returns>
		public string GetNotesOptionsJson()
		{
			return JsonUtility.ToJson(_notesOption);
		}

		/// <summary>
		/// 表示設定を取得します
		/// </summary>
		/// <returns>DisplayOption</returns>
		public DisplayOption GetDisplayOptions()
		{
			return _displayOption.DeepCopy();
		}

		/// <summary>
		/// 表示設定をJson文字列で取得します
		/// </summary>
		/// <returns>Json文字列</returns>
		public string GetDisplayOptionsJson()
		{
			return JsonUtility.ToJson(_displayOption);
		}

		/// <summary>
		/// 音量設定を取得します
		/// </summary>
		/// <returns>VolumeOption</returns>
		public VolumeOption GetVolumeOptions()
		{
			return _volumeOption.DeepCopy();
		}

		/// <summary>
		/// 音量設定をJson文字列で取得します
		/// </summary>
		/// <returns>Json文字列</returns>
		public string GetVolumeOptionsJson()
		{
			return JsonUtility.ToJson(_volumeOption);
		}

		/// <summary>
		/// その他設定をJson文字列で取得します
		/// </summary>
		/// <returns>JudgeTimeOption</returns>
		public JudgeTimeOption GetOtherOptions()
		{
			return _judgeTimeOption.DeepCopy();
		}

		/// <summary>
		/// その他設定を取得します
		/// </summary>
		/// <returns>JudgeTimeOption</returns>
		public JudgeTimeOption GetJudgeTimeOptions()
		{
			return _judgeTimeOption.DeepCopy();
		}

		/// <summary>
		/// その他設定をJson文字列で取得します
		/// </summary>
		/// <returns>Json文字列</returns>
		public string GetJudgeTimeOptionsJson()
		{
			return JsonUtility.ToJson(_judgeTimeOption);
		}

		/// <summary>
		/// ノーツのスピードオプションの値を取得します
		/// </summary>
		/// <returns></returns>
		public float GetNoteSpeedOption()
		{
			return _notesOption.HiSpeed;
		}

		/// <summary>
		/// ノーツのサイズオプションの値を取得します
		/// </summary>
		/// <returns></returns>
		public float GetNoteSizeOption()
		{
			return _notesOption.Size;
		}

		/// <summary>
		/// ミラーオプションを使用しているか
		/// </summary>
		/// <returns></returns>
		public bool IsMirrorOption()
		{
			return _judgeTimeOption.Mirror;
		}

		/// <summary>
		/// 設定されているノーツのTextureを取得します (NoteType版)
		/// </summary>
		/// <param name="type">NoteType</param>
		/// <returns>ノーツのTexture</returns>
		public Texture GetNoteTexture(NoteType type)
		{
			return _noteTexture.NoteTypeToTexture(type);
		}

		/// <summary>
		/// 設定されているノーツのTextureを取得します (int版)
		/// </summary>
		/// <param name="index">Int</param>
		/// <returns>ノーツのTexture</returns>
		public Texture GetNoteTexture(int index)
		{
			return _noteTexture.NoteTypeToTexture((NoteType)index);
		}

		/// <summary>
		/// 設定されているロングのTextureを取得します (LongType版)
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public Texture GetLongTexture(LongType type)
		{
			return _longTexture.LongTypeToTexture(type);
		}

		/// <summary>
		/// 設定されているロングのTextureを取得します (int版)
		/// </summary>
		/// <param name="index">Int</param>
		/// <returns>ノーツのTexture</returns>
		public Texture GetLongTexture(int index)
		{
			return _longTexture.LongTypeToTexture((LongType)index);
		}
	}
}