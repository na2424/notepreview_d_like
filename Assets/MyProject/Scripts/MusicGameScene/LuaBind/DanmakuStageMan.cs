﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{
	[Serializable]
	[LuaCallCSharp]
	public sealed class DanmakuStageMan : IDanmakuStage
	{
		[SerializeField] Transform _parent;

		LifeManager _lifeManager;
		MusicGame _musicGame;

		DanmakuStage _danmakuStage;

		public IDanmakuStage DanmakuStage
		{
			get
			{
				if (_danmakuStage == null)
				{
					_danmakuStage = GameObject.Instantiate(Resources.Load<DanmakuStage>("DanmakuStage"), _parent);
					_danmakuStage.Init(_lifeManager, _musicGame);
				}

				return _danmakuStage;
			}
		}

		public DanmakuPlayer Player => DanmakuStage.Player;

		public DanmakuEnemy Enemy => DanmakuStage.Enemy;

		[BlackList, DoNotGen]
		public void OnFinalize()
		{
			if (_danmakuStage != null)
			{
				_danmakuStage.OnFinalize();
				_danmakuStage = null;
			}
		}

		[BlackList, DoNotGen]
		public void Init(LifeManager lifeManager, MusicGame musicGame)
		{
			_lifeManager = lifeManager;
			_musicGame = musicGame;
		}

		[BlackList, DoNotGen]
		public void CallUpdate(double musicTime)
		{
			if (_danmakuStage != null)
			{
				_danmakuStage.CallUpdate(musicTime);
			}
		}

		public void Begin()
		{
			DanmakuStage.Begin();
		}

		public void End()
		{
			DanmakuStage.End();
		}

		public IBulletBuilder GetBulletBuilder()
		{
			return DanmakuStage.GetBulletBuilder();
		}

		public DanmakuEnemy GetEnemy()
		{
			return DanmakuStage.GetEnemy();
		}

		public DanmakuPlayer GetPlayer()
		{
			return DanmakuStage.GetPlayer();
		}

		public void SetCameraMode(CameraMode cameraMode)
		{
			DanmakuStage.SetCameraMode(cameraMode);
		}

		public void SetCameraModeInt(int cameraMode)
		{
			DanmakuStage.SetCameraModeInt(cameraMode);
		}

		public void SetEnemySprite(Sprite sprite)
		{
			DanmakuStage.SetEnemySprite(sprite);
		}

		public void SetLimitPosition(bool isLimit)
		{
			DanmakuStage.SetLimitPosition(isLimit);
		}

		public void SetMoveTypeInt(int moveType)
		{
			DanmakuStage.SetMoveTypeInt(moveType);
		}

		public Vector2 ToPlayerAiming(Vector2 startPosition)
		{
			return DanmakuStage.ToPlayerAiming(startPosition);
		}
	}
}