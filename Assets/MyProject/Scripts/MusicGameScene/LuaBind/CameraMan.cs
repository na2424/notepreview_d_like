﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{
	[Serializable]
	[LuaCallCSharp]
	public sealed class CameraMan
	{
		[SerializeField] GameCameraController _gameCameraController;
		[SerializeField] PostEffect _postEffect;

		Transform _cameraTransform;

		[BlackList, DoNotGen]
		public void Init(GameCameraController gameCameraController)
		{
			_cameraTransform = _gameCameraController.transform;
			_postEffect.Init();
		}

		/// <summary>
		/// 引数のオブジェクトをmainCameraの子オブジェクトに設定する
		/// </summary>
		/// <param name="obj">オブジェクトのTransform</param>
		/// <param name="isRotateIdentity">カメラの向きに合わせて回転させるか</param>
		public void SetChild(GameObject obj, bool isRotateIdentity = false)
		{
			var tr = obj.transform;
			tr.SetParent(_cameraTransform);

			if (isRotateIdentity)
			{
				tr.localRotation = Quaternion.identity;
			}
		}

		/// <summary>
		/// 引数のオブジェクトをmainCameraの子オブジェクトに設定する (Transform版です)
		/// </summary>
		/// <param name="tr">オブジェクトのTransform</param>
		/// <param name="isRotateIdentity">カメラの向きに合わせて回転させるか</param>
		public void SetChild(Transform tr, bool isRotateIdentity = false)
		{
			tr.SetParent(_cameraTransform);

			if (isRotateIdentity)
			{
				tr.localRotation = Quaternion.identity;
			}
		}

		/// <summary>
		/// ActorをmainCameraの子オブジェクトに設定する
		/// </summary>
		/// <param name="actor">Actor</param>
		/// <param name="isRotateIdentity">カメラの向きに合わせて回転させるか</param>
		public void SetChild(Actor actor, bool isRotateIdentity = false)
		{
			var actorTransform = actor.transform;
			actorTransform.SetParent(_cameraTransform);

			if (isRotateIdentity)
			{
				actorTransform.localRotation = Quaternion.identity;
			}
		}

		/// <summary>
		/// mainCameraのTransformを取得します
		/// </summary>
		/// <returns></returns>
		public Transform GetTransform()
		{
			return _gameCameraController.Transform;
		}

		/// <summary>
		/// Cameraを取得します
		/// </summary>
		/// <returns></returns>
		public Camera GetCamera()
		{
			return _gameCameraController.GetComponent<Camera>();
		}

		/// <summary>
		/// PostEffectを取得します
		/// </summary>
		/// <returns></returns>
		public PostEffect GetPostEffect()
		{
			return _postEffect;
		}
	}
}
