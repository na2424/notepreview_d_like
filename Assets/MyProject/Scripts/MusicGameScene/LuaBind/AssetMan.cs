﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{

	[Serializable]
	[LuaCallCSharp]
	public sealed class AssetMan
	{
		[SerializeField] AssetBundleManager _assetBundleManager;

		[BlackList, DoNotGen]
		public void Init(Action onError)
		{
			_assetBundleManager.Init(onError);
		}

		/// <summary>
		/// アセットバンドルをロードします。
		/// アセットバンドルのハッシュ値を返します。
		/// </summary>
		/// <param name="name">アセットバンドルのファイル名(譜面フォルダからの相対パス)</param>
		/// <returns>アセットバンドルのハッシュ値</returns>
		public int LoadAssetBundle(string name)
		{
			return _assetBundleManager.LoadAssetBundle(name);
		}

		/// <summary>
		/// GameObjectをアセットバンドルのハッシュ値とアセット名からロードして取得します。
		/// </summary>
		/// <param name="hash">アセットバンドルのハッシュ値</param>
		/// <param name="name">アセット名</param>
		/// <returns></returns>
		public GameObject LoadGameObject(int hash, string name)
		{
			return _assetBundleManager.LoadAsset<GameObject>(hash, name);
		}

		/// <summary>
		/// Shaderをアセットバンドルのハッシュ値とアセット名からロードして取得します。
		/// </summary>
		/// <param name="hash">アセットバンドルのハッシュ値</param>
		/// <param name="name">アセット名</param>
		/// <returns></returns>
		public Shader LoadShader(int hash, string name)
		{
			return _assetBundleManager.LoadAsset<Shader>(hash, name);
		}

		/// <summary>
		/// Materialをアセットバンドルのハッシュ値とアセット名からロードして取得します。
		/// </summary>
		/// <param name="hash">アセットバンドルのハッシュ値</param>
		/// <param name="name">アセット名</param>
		/// <returns></returns>
		public Material LoadMaterial(int hash, string name)
		{
			return _assetBundleManager.LoadAsset<Material>(hash, name);
		}

		/// <summary>
		/// Spriteをアセットバンドルのハッシュ値とアセット名からロードして取得します。
		/// </summary>
		/// <param name="hash">アセットバンドルのハッシュ値</param>
		/// <param name="name">アセット名</param>
		/// <returns></returns>
		public Sprite LoadSprite(int hash, string name)
		{
			return _assetBundleManager.LoadAsset<Sprite>(hash, name);
		}

		/// <summary>
		/// アセットバンドル名とアセット名からGameObjectを生成します。
		/// </summary>
		/// <param name="name"></param>
		/// <param name="assetName"></param>
		public void InstantiateAssetBundle(string name, string assetName)
		{
			_assetBundleManager.InstantiateAssetBundle(name, assetName);
		}

	}

}
