﻿using DG.Tweening;
using System;
using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{
	[Serializable]
	[LuaCallCSharp]
	public sealed class GameState
	{
		[SerializeField] FilterColorController _filterColorController;
		[SerializeField] LaneController _laneController;
		[SerializeField] JudgePlateController _judgePlateController;
		[SerializeField] JudgeEffectPool _judgeEffectPool;
		[SerializeField] SameTimeBarObjectPool _sameTimeBarObjectPool;
		[SerializeField] VisibleHoldController _visibleHoldController;
		[SerializeField] AutoPlay _autoPlay;

		MusicManager _musicManager;
		MusicGame _musicGame;

		[BlackList, DoNotGen]
		public void Init(MusicManager musicManager, MusicGame musicGame)
		{
			_musicManager = musicManager;
			_musicGame = musicGame;
		}

		/// <summary>
		/// PlayModeを数値で取得します.
		/// 0 → 通常
		/// 1 → オート
		/// 2 → リハーサル
		/// 3 → オンライン、入力チェック
		/// </summary>
		/// <returns></returns>
		public int GetPlayMode()
		{
			return (int)GameManager.Instance.PlayModeOption.PlayMode;
		}

		/// <summary>
		/// 音楽のレートを取得します。
		/// </summary>
		/// <returns></returns>
		public float GetMusicRate()
		{
			return _musicManager.MusicRate;
		}


		/// <summary>
		/// 現在の曲の時間を取得します。
		/// </summary>
		/// <returns></returns>
		public double GetSongTime()
		{
			return _musicManager.GetMusicTime();
		}

		/// <summary>
		/// 現在の拍を取得します。
		/// </summary>
		/// <returns></returns>
		public double GetSongBeat()
		{
			var musicTime = _musicManager.GetMusicTime();
			return _musicGame.BpmHelper.TimeToBeat(musicTime);
		}

		/// <summary>
		/// ノーツが表示される拍数を取得します。
		/// </summary>
		/// <returns></returns>
		public float GetVisibleBeat()
		{
			return GameParam.VisibleBeat;
		}

		/// <summary>
		/// ノーツが表示される秒数を取得します。
		/// </summary>
		/// <returns></returns>
		public double GetVisibleTime()
		{
			return GameParam.VisibleTime;
		}

		/// <summary>
		/// 曲の再生時間から拍数を求める
		/// (BPM変化対応)
		/// </summary>
		/// <param name="musicTime">再生時間</param>
		/// <returns>拍数</returns>
		public double TimeToBeat(double musicTime)
		{
			return _musicGame.BpmHelper.TimeToBeat(musicTime);
		}

		/// <summary>
		/// 拍数から時間を求める
		/// (BPM変化対応)
		/// </summary>
		/// <param name="beat">拍数</param>
		/// <returns>時間</returns>
		public double BeatToTime(double beat)
		{
			return _musicGame.BpmHelper.BeatToTime(beat);
		}

		Tweener _speedTweener = null;

		/// <summary>
		/// 現在のSpeedギミックの値を変更します
		/// </summary>
		/// <param name="speed"></param>
		public void SetSpeed(float speed, float duration = 0f, string ease = "Linear")
		{
			if (duration <= float.Epsilon)
			{
				_musicGame.SetSpeed(speed);
			}
			else
			{
				var easeType = Util.GetEase(ease);

				if (_speedTweener != null)
				{
					_speedTweener.Kill();
					_speedTweener = null;
				}

				_speedTweener = DOTween.To(() => SpeedStretchRatio.CurrentValue, (x) => _musicGame.SetSpeed(x), speed, duration).SetEase(easeType);
			}
		}

		/// <summary>
		/// 判定の横幅の値を変更します
		/// </summary>
		/// <param name="distance"></param>
		public void SetDistance(float distance, bool isSafeAuto = true)
		{
			_musicGame.SetDistance(distance, isSafeAuto);
		}

		/// <summary>
		/// ノーツの判定位置の基準となるZ座標を設定します
		/// ノーツの見た目の位置が前後します
		/// デフォルトは0に設定されています
		/// </summary>
		/// <param name="offsetZ"></param>
		public void SetOffsetZ(float offsetZ)
		{
			_musicGame.SetOffsetZ(offsetZ);
		}

		/// <summary>
		/// ノーツが生成されるタイミングを比で変更します
		/// (計算結果が0.2秒より少なくなる場合は補正が掛かります)
		/// API ver1.6
		/// </summary>
		public void SetVisibleRate(float rate)
		{
			_musicGame.SetVisibleRate(rate);
		}

		/// <summary>
		/// ノーツスクロールの種類を等速か減速か設定する
		/// デフォルトは減速の1に設定されています。
		/// 0 → 等速
		/// 1 → 減速
		/// </summary>
		/// <param name="isDecelerate"></param>
		public void SetNotesScrollType(int type)
		{
			_musicGame.SetNotesScrollType(type != 0);
		}

		/// <summary>
		/// レーンの透明度を設定する (0～1)
		/// </summary>
		/// <param name="alpha"></param>
		public void SetLaneAlpha(float alpha)
		{
			_filterColorController.SetAlpha(alpha);
		}

		/// <summary>
		/// 判定エフェクトを変更する
		/// </summary>
		/// <param name="type"></param>
		public void ChangeJudgeEffect(int type)
		{
			var changeType = type switch
			{
				0 => NotesEffectType.None,
				1 => NotesEffectType.Type1,
				2 => NotesEffectType.Type2,
				3 => NotesEffectType.Type3,
				_ => NotesEffectType.None
			};

			_judgeEffectPool.ChangeEffectType(changeType);
			_visibleHoldController.ChangeEffectType(changeType);
			_musicGame.SetEnableJudgeEffect(type != 0);
		}

		/// <summary>
		/// ホールドのテクスチャを変更する
		/// </summary>
		/// <param name="noteType">ノーツの種類(ロング、ファジーロング)</param>
		/// <param name="noteTexture">ノーツのテクスチャ</param>
		public void ChangeHoldTexture(NoteType noteType, Texture noteTexture)
		{
			_visibleHoldController.ChangeHoldTexture(noteType, noteTexture);
		}

		/// <summary>
		/// ホールドのテクスチャを変更する
		/// </summary>
		/// <param name="noteTypeIndex">ノーツの種類のint(ロング、ファジーロング)</param>
		/// <param name="noteTexture">ノーツのテクスチャ</param>
		public void ChangeHoldTexture(int noteTypeIndex, Texture noteTexture)
		{
			_visibleHoldController.ChangeHoldTexture(noteTypeIndex, noteTexture);
		}

		/// <summary>
		/// ホールドのテクスチャを元に戻す
		/// </summary>
		public void ResetHoldTexture()
		{
			_visibleHoldController.ResetHoldTexture();
		}

		/// <summary>
		/// 現在の判定を取得する
		/// </summary>
		/// <returns>Judge</returns>
		public Judge GetCurrentJudge()
		{
			return _musicGame.GetCurrentJudge();
		}

		/// <summary>
		/// Autoが入力する座標を事前に決められたレーンではなく、ノーツの位置から使用するようにするか
		/// (ノーツのX座標をLuaで動かした場合にAutoが対応できるようにします)
		/// </summary>
		/// <param name="useNotePosition">ノーツの位置を使用するか</param>
		public void SetAutoType(int type)
		{
			_autoPlay.SetAutoType(type == 1);
		}

		/// <summary>
		/// 同時押し線を表示するか設定します
		/// </summary>
		/// <param name="isActive">表示するか</param>
		public void SetActiveSameTimeBar(bool isActive)
		{
			_sameTimeBarObjectPool.gameObject.SetActive(isActive);
		}

		/// <summary>
		/// 指定したノーツ番号を持つ同時押し線を削除します
		/// 同時押し線が生成されていない場合は生成しないようになります
		/// API ver2.2
		/// </summary>
		/// <param name="index">ノーツ番号</param>
		public void RemoveSameTimeBar(int index)
		{
			int loop = _sameTimeBarObjectPool.CurrentActiveSameTimeBar.Count - 1;
			bool hasIndex = false;
			for (int i = loop; i >= 0; i--)
			{
				if (_sameTimeBarObjectPool.CurrentActiveSameTimeBar[i].IsReleased)
				{
					continue;
				}

				if (_sameTimeBarObjectPool.CurrentActiveSameTimeBar[i].LeftNoteIndex == index)
				{
					hasIndex = true;
					_sameTimeBarObjectPool.CurrentActiveSameTimeBar[i].Release();
					break;
				}

				if (_sameTimeBarObjectPool.CurrentActiveSameTimeBar[i].RightNoteIndex == index)
				{
					hasIndex = true;
					_sameTimeBarObjectPool.CurrentActiveSameTimeBar[i].Release();
					break;
				}
			}

			if (!hasIndex)
			{
				_musicGame.SetNoCreateSameTimeBarAtNoteIndex(index);
			}
		}

		/// <summary>
		/// ロング終端のタイプを設定します
		/// 0 → 通常 (アップ判定)
		/// 1 → ファジーロングと同様にする(判定音あり)
		/// 2 → ファジーロングと同様にする(判定音なし)
		/// API ver1.7
		/// </summary>
		/// <param name="type"></param>
		public void SetLongEndType(int type)
		{
			_musicGame.SetLongEndType(type);
		}

		/// <summary>
		/// Note構造体の配列を生成して返します
		/// API ver1.8
		/// </summary>
		/// <returns>Note構造体の配列</returns>
		public Note[] GetNotes()
		{
			var sequence = _musicGame.Sequence;
			int count = sequence.BeatPositions.Count;

			var notes = new Note[count];

			for (int i = 0; i < count; i++)
			{
				notes[i] = new Note(
					sequence.BeatPositions[i],
					sequence.Lanes[i],
					sequence.NoteTypes[i],
					sequence.IsAttacks[i]
				);
			}

			return notes;
		}

		/// <summary>
		/// 直近の判定済みノーツのジャストからのズレの時間を取得します
		/// onHitNoteイベント関数内で使用されることを想定しています
		/// API ver1.9
		/// </summary>
		/// <returns></returns>
		public float GetNoteOffset()
		{
			int index = _musicGame.TimingHistory.Timing.Count - 1;
			if (index < 0)
			{
				return 0f;
			}

			return _musicGame.TimingHistory.Timing[index];
		}

		/// <summary>
		/// レーンの区切り線の色を変更します
		/// API ver2.2
		/// </summary>
		/// <param name="color"></param>
		public void SetLaneSeparateColor(Color color)
		{
			_laneController.SetColor(color);
			_judgePlateController.SetColor(color);
		}
	}
}