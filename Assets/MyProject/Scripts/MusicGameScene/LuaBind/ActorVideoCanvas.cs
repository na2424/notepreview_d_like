﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using XLua;

namespace DankagLikeLuaAPI
{
	public enum ScreenMatchMode
	{
		Expand = 0,
		Shrink = 1,
	}

	[Serializable]
	[LuaCallCSharp]
	public sealed class ActorVideoCanvas : Actor
	{
		[SerializeField] VideoPlayer _videoPlayer;
		[SerializeField] Canvas _canvas;
		[SerializeField] CanvasScaler _canvasScaler;
		[SerializeField] RawImage _rawImage;

		MusicManager _musicManager;

		ScreenMatchMode _screenMatchMode = ScreenMatchMode.Expand;
		double _offsetTime = 0d;
		bool _isAutoMusicSync = true;
		float _col = 1f;
		float _alpha = 1f;
		Action _finishCallBack = null;

		[BlackList, DoNotGen]
		public void Init(MusicManager musicManager)
		{
			_videoPlayer.timeReference = VideoTimeReference.ExternalTime;
			_musicManager = musicManager;
			_canvas.worldCamera = Camera.main;
			
			_videoPlayer.loopPointReached += OnLoopPointReached;
			_videoPlayer.prepareCompleted += OnPrepareCompleted;
		}

		[BlackList, DoNotGen]
		public void OnPause()
		{
			if (_isAutoMusicSync)
			{
				_videoPlayer.Pause();
			}
		}

		[BlackList, DoNotGen]
		public void OnResume()
		{
			if (_isAutoMusicSync)
			{
				_videoPlayer.Play();
			}
		}

		[BlackList, DoNotGen]
		public void CallUpdate()
		{
			if (!_isAutoMusicSync)
			{
				return;
			}

			if (!_musicManager.IsPlay)
			{
				return;
			}

			if (_videoPlayer.isPlaying && _videoPlayer.timeReference == VideoTimeReference.ExternalTime)
			{
				double time = _musicManager.GetMusicTime();
				_videoPlayer.externalReferenceTime = time + _offsetTime;
			}
		}

		void OnLoopPointReached(VideoPlayer source)
		{
			_finishCallBack?.Invoke();
		}

		void OnPrepareCompleted(VideoPlayer vp)
		{
			_rawImage.texture = vp.texture;

			switch (_screenMatchMode)
			{
				case ScreenMatchMode.Expand:
					SetInSize(vp.texture);
					break;
				case ScreenMatchMode.Shrink:
					SetOutSize(vp.texture);
					break;
			}
		}

		/// <summary>
		/// VideoPlayerを取得します
		/// </summary>
		/// <returns>VideoPlayer</returns>
		public VideoPlayer GetVideoPlayer()
		{
			return _videoPlayer;
		}

		/// <summary>
		/// 動画が再生されるRawImageを取得します
		/// </summary>
		/// <returns>RawImage</returns>
		public RawImage GetRawImage()
		{
			return _rawImage;
		}

		/// <summary>
		/// 動画のファイルパスを設定します。
		/// </summary>
		/// <param name="filePath">譜面フォルダからの相対パス</param>
		public void SetVideoPath(string filePath)
		{
			string path = Path.Combine(LuaManager.FolderPath, filePath).Replace("\\", "/");
			_videoPlayer.url = path;
			_videoPlayer.Prepare();
		}

		/// <summary>
		/// 動画と楽曲の再生同期を指定した時間(秒)ずらします。
		/// </summary>
		/// <param name="offsetTime"></param>
		public void SetOffsetTime(double offsetTime)
		{
			_offsetTime = offsetTime;
		}

		/// <summary>
		/// 動画の明るさを設定します(0～1)
		/// </summary>
		/// <param name="brightness">brightness</param>
		public void SetBrightness(float brightness)
		{
			_col = Mathf.Clamp01(brightness);
			_rawImage.color = new Color(_col, _col, _col, _alpha);
		}

		/// <summary>
		/// 動画の透明度を設定します(0～1)
		/// 0に近づくほど透明になります。
		/// </summary>
		/// <param name="alpha">透明度</param>
		public void SetAlpha(float alpha)
		{
			_alpha = Mathf.Clamp01(alpha);
			_rawImage.color = new Color(_col, _col, _col, _alpha);
		}

		/// <summary>
		/// 動画が最後まで再生されたときに呼ばれる関数を設定します
		/// </summary>
		/// <param name="function">関数</param>
		public void SetFinishCallBack(Action function)
		{
			_finishCallBack = function;
		}

		/// <summary>
		/// 動画を再生します
		/// </summary>
		public void Play()
		{
			_videoPlayer.Play();
		}

		/// <summary>
		/// 動画を一時停止します
		/// </summary>
		public void Pause()
		{
			_videoPlayer.Pause();
		}

		/// <summary>
		/// 動画を停止します
		/// </summary>
		public void Stop()
		{
			_videoPlayer.Stop();
		}

		/// <summary>
		/// 動画の大きさを画面の内側に合わせるか外側に合わせるか設定します。
		/// 0 → 内側
		/// 1 → 外側
		/// </summary>
		/// <param name="mode"></param>
		public void SetScreenMatchMode(int mode)
		{
			var nextMode = mode == 0 ? ScreenMatchMode.Expand : ScreenMatchMode.Shrink;

			if (nextMode != _screenMatchMode)
			{
				if (_rawImage.texture == null)
				{
					return;
				}

				switch (nextMode)
				{
					case ScreenMatchMode.Expand:
						SetInSize(_rawImage.texture);
						break;
					case ScreenMatchMode.Shrink:
						SetOutSize(_rawImage.texture);
						break;
				}
			}

			_screenMatchMode = nextMode;
		}

		/// <summary>
		/// 自動で楽曲の再生時間と同期させるか設定します。
		/// デフォルトはisAutoがtrueに設定されています。
		/// </summary>
		/// <param name="isAuto"></param>
		public void SetAutoMusicSync(bool isAuto)
		{
			_isAutoMusicSync = isAuto;
		}

		/// <summary>
		/// AutoMusicSyncがfalseの時にLua側でVideoPlayerのexternalReferenceTimeを設定します
		/// </summary>
		/// <param name="time"></param>
		public void SetExternalReferenceTime(double time)
		{
			_videoPlayer.externalReferenceTime = time;
		}

		/// <summary>
		/// 画面サイズとテクスチャのサイズを比較して、
		/// 背景の大きさを設定する(外側に合わせる)
		/// </summary>
		/// <param name="texture"></param>
		void SetOutSize(Texture texture)
		{
			// Canvasの解像度
			_canvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);
			// スクリーンのサイズ比
			float screenRatio = (float)Screen.width / (float)Screen.height;

			float width = texture.width;
			float height = texture.height;
			float ratio = width / height;

			_rawImage.rectTransform.sizeDelta = ratio >= screenRatio ?
				new Vector2(width * Screen.height / (float)height, Screen.height) :
				new Vector2(Screen.width, height * Screen.width / (float)width);
		}

		/// <summary>
		/// 画面サイズとテクスチャのサイズを比較して、
		/// 背景の大きさを設定する(内側に合わせる)
		/// </summary>
		/// <param name="texture"></param>
		void SetInSize(Texture texture)
		{
			// Canvasの解像度
			_canvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);
			// スクリーンのサイズ比
			float screenRatio = (float)Screen.width / (float)Screen.height;

			float width = texture.width;
			float height = texture.height;
			float ratio = width / height;

			_rawImage.rectTransform.sizeDelta = ratio >= screenRatio ?
				new Vector2(Screen.width, height * Screen.width / (float)width) :
				new Vector2(width * Screen.height / (float)height, Screen.height);
		}

		private void OnDestroy()
		{
			_videoPlayer.loopPointReached -= OnLoopPointReached;
			_videoPlayer.prepareCompleted -= OnPrepareCompleted;
		}
	}
}