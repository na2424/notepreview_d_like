﻿using Dialog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{
	[Serializable]
	[LuaCallCSharp]
	public sealed class ActorFactory
	{
		[SerializeField] Actor2D _actor2DPrefab;
		[SerializeField] Actor3D _actor3DPrefab;
		[SerializeField] ActorAudio _actorAudioPrefab;
		[SerializeField] ActorUIText _actorUITextPrefab;
		[SerializeField] ActorVideoCanvas _actorVideoCanvasPrefab;

		[SerializeField] Transform _luaOverrayPanel;
		[SerializeField] MusicManager _musicManager;

		LuaManager _luaManager;
		Action _pause;

		[DoNotGen]
		public void Init(LuaManager luaManager, Action pause)
		{
			_luaManager = luaManager;
			_pause = pause;
		}

		/// <summary>
		/// 指定したLuaスクリプトをActorとして生成します
		/// </summary>
		/// <param name="luaFileName">Luaのファイルパス</param>
		/// <param name="hasInjection">クラスのグローバル変数を使えるようにするか</param>
		/// <returns>生成されたActorを返す</returns>
		public Actor2D Create2D(string luaFileName = null, bool hasInjection = true)
		{
			Actor actor = GameObject.Instantiate(_actor2DPrefab);
			LuaTable scriptEnv = null;

			try
			{
				if (!luaFileName.IsNullOrEmpty())
				{
					scriptEnv = ReadLua(luaFileName);
					scriptEnv.Set("self", actor);
				}
			}
			catch (Exception e)
			{
				Debug.LogError(e.Message);
				return null;
			}

			actor.Init(luaFileName, scriptEnv, _pause, "(Actor2D)");

			_luaManager.ActorList.Add(actor);

			return (Actor2D)actor;
		}

		/// <summary>
		/// 指定したLuaスクリプトをActor3Dとして生成します
		/// </summary>
		/// <param name="luaFileName">Luaのファイルパス</param>
		/// <param name="hasInjection">クラスのグローバル変数を使えるようにするか</param>
		/// <returns>生成されたActorを返す</returns>
		//public Actor3D Create3D(string luaFileName)
		//{
		//	var scriptEnv = ReadLua(luaFileName);

		//	if (scriptEnv == null)
		//	{
		//		return null;
		//	}

		//	Actor actor = GameObject.Instantiate(_actor3DPrefab);
		//	scriptEnv.Set("self", actor);

		//	actor.Init(luaFileName, scriptEnv, _pause);
		//	_luaManager.ActorList.Add(actor);

		//	return (Actor3D)actor;
		//}

		/// <summary>
		/// 指定したLuaスクリプトをActorAudioとして生成します
		/// </summary>
		/// <param name="luaFileName">Luaのファイルパス</param>
		/// <param name="hasInjection">クラスのグローバル変数を使えるようにするか</param>
		/// <returns>生成されたActorを返す</returns>
		public ActorAudio CreateAudio(string luaFileName = null)
		{
			Actor actor = GameObject.Instantiate(_actorAudioPrefab);
			LuaTable scriptEnv = null;

			try
			{
				if (!luaFileName.IsNullOrEmpty())
				{
					scriptEnv = ReadLua(luaFileName);
					scriptEnv.Set("self", actor);
				}
			}
			catch (Exception e)
			{
				Debug.LogError(e.Message);
				return null;
			}

			actor.Init(luaFileName, scriptEnv, _pause, "(ActorAudio)");

			_luaManager.ActorList.Add(actor);

			return (ActorAudio)actor;
		}

		/// <summary>
		/// 指定したLuaスクリプトをActorTextとして生成します
		/// </summary>
		/// <param name="luaFileName">Luaのファイルパス</param>
		/// <param name="hasInjection">クラスのグローバル変数を使えるようにするか</param>
		/// <returns>生成されたActorを返す</returns>
		public ActorUIText CreateUIText(string luaFileName = null)
		{
			Actor actor = GameObject.Instantiate(_actorUITextPrefab, _luaOverrayPanel);
			LuaTable scriptEnv = null;

			try
			{
				if (!luaFileName.IsNullOrEmpty())
				{
					scriptEnv = ReadLua(luaFileName);
					scriptEnv.Set("self", actor);
				}
			}
			catch (Exception e)
			{
				Debug.LogError(e.Message);
				return null;
			}

			actor.Init(luaFileName, scriptEnv, _pause, "(ActorUIText)");

			_luaManager.ActorList.Add(actor);

			return (ActorUIText)actor;
		}

		/// <summary>
		/// 指定したLuaスクリプトをActorVideoCanvasとして生成します
		/// </summary>
		/// <param name="luaFileName">Luaのファイルパス</param>
		/// <returns>生成されたActorを返す</returns>
		public ActorVideoCanvas CreateVideoCanvas(string luaFileName = null)
		{
			Actor actor = GameObject.Instantiate(_actorVideoCanvasPrefab);
			LuaTable scriptEnv = null;

			try
			{
				if (!luaFileName.IsNullOrEmpty())
				{
					scriptEnv = ReadLua(luaFileName);
					scriptEnv.Set("self", actor);
				}
			}
			catch (Exception e)
			{
				Debug.LogError(e.Message);
				return null;
			}

			actor.Init(luaFileName, scriptEnv, _pause, "(ActorVideoCanvas)");
			var actorVideoCanvas = actor as ActorVideoCanvas;
			actorVideoCanvas.Init(_musicManager);

			_luaManager.ActorList.Add(actor);
			_luaManager.ActorVideoCanvasList.Add(actorVideoCanvas);

			return actorVideoCanvas;
		}

		LuaTable ReadLua(string luaFileName)
		{
			string folderPath = LuaManager.FolderPath;
			var path = Path.Combine(folderPath, luaFileName).Replace("\\", "/");
			var scriptEnv = _luaManager.CreateScriptEnv();

			try
			{
				var luaString = File.ReadAllText(path, System.Text.Encoding.UTF8);
				LuaManager.g_luaEnv.DoString(luaString, "Lua", scriptEnv);
			}
			catch (Exception e)
			{
				ShowErrorDialog(e.Message);
				return null;
			}

			return scriptEnv;
		}

		/// <summary>
		/// エラー時にダイアログを表示する
		/// </summary>
		/// <param name="message"></param>
		void ShowErrorDialog(string message)
		{
			Action onNext = () =>
			{
				if (!BgManager.Instance.IsDefaultImage())
				{
					BgManager.Instance.ClearBgChangeImageCache();
					BgManager.Instance.SetDefaultImage();
				}
				BgManager.Instance.ClearMaterial();

				GameManager.Instance.ChangeScene(SceneName.SelectMusic);
			};

			var builder = new DialogParametor.Builder("Luaの実行に失敗しました", message);
			builder.AddDefaultAction("選曲画面に戻る", onNext);
			builder.AddCallbackOnAutoClosed(onNext);
			DialogManager.Instance.Open(builder.Build());

			Debug.LogError("Luaの実行に失敗しました:" + message);
		}
	}
}