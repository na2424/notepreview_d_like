﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{
	[Serializable]
	[LuaCallCSharp]
	public sealed class SongMan
	{
		SongInfo _songInfo;
		Sequence _sequence;
		MusicManager _musicManager;

		[BlackList, DoNotGen]
		public void Init(SongInfo songInfo, Sequence sequence, MusicManager musicManager)
		{
			_songInfo = songInfo;
			_sequence = sequence;
			_musicManager = musicManager;
		}

		/// <summary>
		/// 難易度の種類を取得します。
		/// </summary>
		/// <returns></returns>
		public global::DifficultyType GetDifficulty()
		{
			return GameManager.Instance.SelectDifficulty;
		}

		/// <summary>
		/// 難易度の種類を数値で取得します。
		/// Easy    -> 0
		/// Normal  -> 1
		/// Hard    -> 2
		/// Extra   -> 3
		/// Lunatic -> 4
		/// </summary>
		/// <returns></returns>
		public int GetDifficultyToInt()
		{
			return (int)GameManager.Instance.SelectDifficulty;
		}

		/// <summary>
		/// 数値の難易度を取得します。
		/// </summary>
		/// <returns></returns>
		public int GetMeter()
		{
			return _songInfo.Difficulty[GetDifficultyToInt()];
		}

		//	table GetAllSteps(void )
		//Gets a table of all the Steps.
		//ITG

		/// <summary>
		/// 背景画像のパスを取得します。
		/// </summary>
		/// <returns></returns>
		public string GetBackgroundPath()
		{
			return _songInfo.BgFileName;
		}

		/// <summary>
		/// バナーのパスを取得します。
		/// </summary>
		/// <returns></returns>
		public string GetBannerPath()
		{
			return _songInfo.JacketFileName;
		}

		/// <summary>
		/// アーティスト名を取得します。
		/// </summary>
		/// <returns></returns>
		public string GetArtist()
		{
			return _songInfo.Artist;
		}

		/// <summary>
		/// タイトルを取得します。
		/// </summary>
		/// <returns></returns>
		public string GetTitle()
		{
			return _songInfo.Title;
		}

		/// <summary>
		/// サブタイトルを取得します。
		/// </summary>
		/// <returns></returns>
		public string GetSubtitle()
		{
			return _songInfo.SubTitle;
		}

		/// <summary>
		/// 楽曲詳細(Description)を取得します。
		/// </summary>
		/// <returns></returns>
		public string[] GetDescription()
		{
			return _songInfo.Description.ToArray();
		}

		/// <summary>
		/// イラスト名を取得します。
		/// </summary>
		/// <returns></returns>
		public string GetIllust()
		{
			return _songInfo.Illust;
		}

		/// <summary>
		/// 譜面制作者名を取得します。
		/// </summary>
		/// <returns></returns>
		public string GetChartArtist()
		{
			if (_sequence.Credit.HasValue())
			{
				return _sequence.Credit;
			}

			return _songInfo.ChartArtist;
		}

		/// <summary>
		/// BaseBpmを取得します。
		/// </summary>
		/// <returns></returns>
		public float GetBaseBpm()
		{
			return _songInfo.BaseBpm;
		}

		/// <summary>
		/// Bpmが変化する拍位置を取得します。
		/// </summary>
		/// <returns></returns>
		public float[] GetBpmPositions()
		{
			return _songInfo.BpmPositions.ToArray();
		}

		/// <summary>
		/// Bpmsを取得します。
		/// </summary>
		/// <returns></returns>
		public float[] GetBpms()
		{
			return _songInfo.Bpms.ToArray();
		}

		/// <summary>
		/// Speedが変化する拍位置を取得します。
		/// </summary>
		/// <returns></returns>
		public float[] GetSpeedPositions()
		{
			return _songInfo.SpeedPositions.ToArray();
		}

		/// <summary>
		/// Speedの値を取得します。
		/// </summary>
		/// <returns></returns>
		public float[] GetSpeedStretchRatios()
		{
			return _songInfo.SpeedStretchRatios.ToArray();
		}

		/// <summary>
		/// Speedの変化に掛かる拍数を取得します。
		/// </summary>
		/// <returns></returns>
		public float[] GetSpeedDelayBeats()
		{
			return _songInfo.SpeedDelayBeats.ToArray();
		}

		/// <summary>
		/// Scrollが変化する拍位置を取得します。
		/// </summary>
		/// <returns></returns>
		public float[] GetScrollPositions()
		{
			return _songInfo.ScrollPositions.ToArray();
		}

		/// <summary>
		/// Scrollsの値を取得します。
		/// </summary>
		/// <returns></returns>
		public float[] GetScrolls()
		{
			return _songInfo.Scrolls.ToArray();
		}

		/// <summary>
		/// オフセットを取得します。
		/// </summary>
		/// <returns></returns>
		public float GetOffset()
		{
			return _songInfo.Offset;
		}

		/// <summary>
		/// 曲のディレクトリを取得します。
		/// </summary>
		/// <returns></returns>
		public string GetSongDir()
		{
			return _songInfo.DirectoryPath;
		}

		/// <summary>
		/// 音楽の長さの秒数を取得します。
		/// </summary>
		/// <returns></returns>
		public double MusicLengthSeconds()
		{
			return _musicManager.MusicEndTime;
		}

		public List<float> GetBeatPositions()
		{
			return _sequence.BeatPositions;
		}

		public List<int> GetLanes()
		{
			return _sequence.Lanes;
		}

		public List<NoteType> GetNoteTypes()
		{
			return _sequence.NoteTypes;
		}

	}

}