﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{
	[Serializable]
	[LuaCallCSharp]
	public sealed class Actor2D : Actor
	{
		public SpriteRenderer SpriteRenderer;

		/// <summary>
		/// 画像のファイル名からロードします
		/// </summary>
		/// <param name="imagePath"></param>
		public void LoadImage(string imagePath)
		{
			var path = Path.Combine(LuaManager.FolderPath, imagePath);

			SpriteRenderer.sprite = LoadSprite(path);
		}

		/// <summary>
		/// 自身の持つSpriteRendererを取得します
		/// </summary>
		/// <returns></returns>
		public SpriteRenderer GetSpriteRenderer()
		{
			return SpriteRenderer;
		}

		/// <summary>
		/// 自身のTransformを取得します
		/// </summary>
		/// <returns></returns>
		public Transform GetTransform()
		{
			return transform;
		}

		/// <summary>
		/// 表示順番を変更します(sortingLayerID)
		/// 0 (Background) → レーンの後ろに描画します
		/// 1 (Default) → レーンと同じレイヤーに描画します
		/// 2 (Foreground) → レーンの手前に描画します
		/// </summary>
		/// <param name="order"></param>
		public void SetSortingLayer(int layerId)
		{
			string layername =
				layerId == 0 ? "Background" :
				layerId == 1 ? "Default" :
				layerId == 2 ? "Foreground" :
				"Background";

			SpriteRenderer.sortingLayerID = SortingLayer.NameToID(layername);
		}

		/// <summary>
		/// レイヤー内の表示順番を変更します(sortingOrder)
		/// </summary>
		/// <param name="order"></param>
		public void SetSortingOrder(int order)
		{
			SpriteRenderer.sortingOrder = order;
		}

		/// <summary>
		/// 設定されているSpriteを取得します
		/// </summary>
		/// <param name="sprite">Sprite</param>
		public Sprite GetSprite()
		{
			return SpriteRenderer.sprite;
		}

		/// <summary>
		/// Spriteを設定します
		/// </summary>
		/// <param name="sprite"></param>
		public void SetSprite(Sprite sprite)
		{
			SpriteRenderer.sprite = sprite;
		}

		/// <summary>
		/// 設定されているMaterialを取得します
		/// </summary>
		/// <returns>Material</returns>
		public Material GetMaterial()
		{
			return SpriteRenderer.material;
		}

		/// <summary>
		/// Materialを設定します
		/// </summary>
		/// <param name="material"></param>
		public void SetMaterial(Material material)
		{
			SpriteRenderer.material = material;
		}

		/// <summary>
		/// 自身の持つLuaの関数を呼び出します。
		/// (引数と戻り値なし版)
		/// </summary>
		/// <param name="functionName"></param>
		public void InvokeFunction(string functionName)
		{
			InvokeFunctionBase(functionName);
		}

		/// <summary>
		/// 自身の持つLuaの関数を呼び出します。
		/// (intの引数と戻り値あり版)
		/// </summary>
		/// <param name="functionName">関数名</param>
		/// <param name="value">引数</param>
		/// <returns>戻り値</returns>
		public int InvokeFunction(string functionName, int value)
		{
			return InvokeFunctionBase(functionName, value);
		}

		/// <summary>
		/// Spriteを生成する
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		static Sprite LoadSprite(string path)
		{
			Texture2D texture2D = TextureLoader.Load(path);

			return Sprite.Create(
				texture2D,
				new Rect(0f, 0f, texture2D.width, texture2D.height),
				new Vector2(0.5f, 0.5f), 100f
			);
		}
	}
}