﻿using System;
using XLua;

namespace DankagLikeLuaAPI
{
	[Serializable]
	[LuaCallCSharp]
	public class HighScore
	{
        public int Score;
        public int MaxCombo;
        public int ClearState;
        public int Rank;

        public int GetScore()
        {
            return Score;
        }

        public int GetMaxCombo()
        {
            return MaxCombo;
        }

        public int GetClearState()
        {
            return ClearState;
        }

        public int GetRank()
        {
            return Rank;
        }

        public HighScore(int score, int maxCombo, int clearState, int rank)
        {
            Score = score;
            MaxCombo = maxCombo;
            ClearState = clearState;
            Rank = rank;
        }

	}
}

//----------------
// ClearStateメモ.
//----------------
// GameOver = 0,
// Cleared,
// FullCombo,
// AllBrilliant