﻿using Cysharp.Text;
using Cysharp.Threading.Tasks;
using Dialog;
using System;
using System.Threading;
using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{
	[Serializable]
	[LuaCallCSharp]
	public sealed class ActorAudio : Actor
	{
		public AudioSource AudioSource;

		CancellationTokenSource _ctSource = null;

		/// <summary>
		/// 音声をロードする
		/// </summary>
		/// <param name="filePath">ファイル名 (譜面フォルダからの相対パス)</param>
		/// <param name="onComplete">読み込み完了時のコールバック</param>
		public void LoadAudio(string filePath, Action onComplete = null)
		{
			AudioSource.volume = GameManager.Instance.VolumeOption.SystemSEVolume;

			if (_ctSource != null)
			{
				_ctSource.Cancel();
				_ctSource = null;
			}

			_ctSource = new CancellationTokenSource();

			var path = ZString.Concat("file://", System.IO.Path.Combine(LuaManager.FolderPath, filePath));

			LoadAudioAsync(_ctSource.Token, path, onComplete).Forget();
		}

		/// <summary>
		/// 自身のAudioSourceを取得します
		/// </summary>
		/// <returns></returns>
		public AudioSource GetAudioSource()
		{
			return AudioSource;
		}

		/// <summary>
		/// 自身のTransformを取得します
		/// </summary>
		/// <returns></returns>
		public Transform GetTransform()
		{
			return transform;
		}


		async UniTask LoadAudioAsync(CancellationToken ct, string filePath, Action onComplete)
		{
			try
			{
				AudioSource.clip = await AudioClipLoader.LoadAsync(ct, filePath, false);
			}
			catch (Exception ex)
			{
				Debug.LogError(ex.Message);
				_onError?.Invoke();

				var builder = new DialogParametor.Builder(
					"楽曲読み込みエラー",
					ZString.Concat("楽曲を読み込めませんでした。ファイルの形式、フォルダやファイル名に読み込めない文字がないか確認してください。\n楽曲ファイルパス : ", filePath));

				builder.AddDefaultAction("戻る", () => GameManager.Instance.ChangeScene(SceneName.SelectMusic));
				builder.AddCallbackOnAutoClosed(() => GameManager.Instance.ChangeScene(SceneName.SelectMusic));
				DialogManager.Instance.Open(builder.Build());
				return;
			}
			

			if (ct.IsCancellationRequested)
			{
				return;
			}

			onComplete?.Invoke();
		}

		/// <summary>
		/// 音量を設定する (0～1)
		/// </summary>
		/// <param name="volume"></param>
		public void SetVolume(float volume)
		{
			AudioSource.volume = Mathf.Clamp01(volume);
		}

		/// <summary>
		/// システムSEの音量に設定する
		/// </summary>
		public void SetSystemSEVolume()
		{
			AudioSource.volume = GameManager.Instance.VolumeOption.SystemSEVolume;
		}

		/// <summary>
		/// タッチSEの音量に設定する
		/// </summary>
		public void SetTouchSEVolume()
		{
			AudioSource.volume = GameManager.Instance.VolumeOption.TouchSEVolume;
		}

		/// <summary>
		/// 楽曲の音量に設定する
		/// </summary>
		public void SetMusicVolume()
		{
			AudioSource.volume = GameManager.Instance.VolumeOption.MusicVolume;
		}

		/// <summary>
		/// 音声を再生する
		/// </summary>
		public void Play()
		{
			AudioSource.Play();
		}

		/// <summary>
		/// 音声を停止する
		/// </summary>
		public void Stop()
		{
			AudioSource.Stop();
		}

		/// <summary>
		/// 音声を一時停止する
		/// </summary>
		public void Pause()
		{
			AudioSource.Pause();
		}

		void OnDestroy()
		{
			if (_ctSource != null)
			{
				_ctSource.Cancel();
				_ctSource = null;
			}
		}
	}
}