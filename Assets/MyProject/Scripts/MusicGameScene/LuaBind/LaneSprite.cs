﻿using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{
	[LuaCallCSharp]
	public sealed class LaneSprite : MonoBehaviour
	{
		public SpriteRenderer SpriteRenderer;
		Transform _cacheTransform;

		Transform cacheTransform
		{
			get
			{
				if (_cacheTransform == null)
				{
					_cacheTransform = transform;
				}
				return _cacheTransform;
			}

			set
			{
				_cacheTransform = value;
			}
		}

		/// <summary>
		/// 自身の持つSpriteRendererを取得します
		/// </summary>
		/// <returns></returns>
		public SpriteRenderer GetSpriteRenderer()
		{
			return SpriteRenderer;
		}

		/// <summary>
		/// 自身のTransformを取得します
		/// </summary>
		/// <returns></returns>
		public Transform GetTransform()
		{
			return cacheTransform;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="isActive"></param>
		public void SetActive(bool isActive)
		{
			gameObject.SetActive(isActive);
		}

		/// <summary>
		///	0から数えて左から指定したレーンの位置になるようにLaneSpriteを設定します
		/// </summary>
		/// <param name="lane"></param>
		public void SetLanePosition(float lane)
		{
			cacheTransform.localPosition = new Vector3((lane * 0.5f) - 1.5f, 0f, 3f);
		}

		/// <summary>
		/// 色を設定する
		/// </summary>
		/// <param name="color"></param>
		public void SetColor(Color color)
		{
			SpriteRenderer.color = color;
		}

		/// <summary>
		/// 色を設定する
		/// </summary>
		/// <param name="r">Red</param>
		/// <param name="g">Green</param>
		/// <param name="b">Blue</param>
		/// <param name="a">Alpha</param>
		public void SetColor(float r, float g, float b, float a)
		{
			SpriteRenderer.color = new Color(r, g, b, a);
		}

		/// <summary>
		/// 色のAlpha値を設定する
		/// </summary>
		/// <param name="a"></param>
		public void SetAlpha(float a)
		{
			var col = SpriteRenderer.color;
			SpriteRenderer.color = new Color(col.r, col.g, col.b, a);
		}

		/// <summary>
		/// 表示順番を変更します(sortingLayerID)
		/// 初期設定ではDefaultになっています。
		/// 
		/// 0 (Background) → レーンの後ろに描画します
		/// 1 (Default) → レーンと同じレイヤーに描画します
		/// 2 (Foreground) → レーンの手前に描画します
		/// 
		/// Backgroundの場合にはレーンが黒色半透明なので、色が少し暗くなります。
		/// </summary>
		/// <param name="order"></param>
		public void SetSortingLayer(int layerId)
		{
			string layername =
				layerId == 0 ? "Background" :
				layerId == 1 ? "Default" :
				layerId == 2 ? "Foreground" :
				"Background";

			SpriteRenderer.sortingLayerID = SortingLayer.NameToID(layername);
		}

		/// <summary>
		/// レイヤー内の表示順番を変更します(sortingOrder)
		/// </summary>
		/// <param name="order"></param>
		public void SetSortingOrder(int order)
		{
			SpriteRenderer.sortingOrder = order;
		}
	}
}