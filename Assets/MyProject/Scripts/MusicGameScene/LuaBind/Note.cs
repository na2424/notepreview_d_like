﻿using System;
using XLua;

namespace DankagLikeLuaAPI
{
	[Serializable]
	[LuaCallCSharp]
	public struct Note
	{
		public float Beat;
		public int Lane;
		public NoteType NoteType;
		public bool IsAttack;

		public Note(float beat, int lane, NoteType noteType, bool isAttack)
		{
			Beat = beat;
			Lane = lane;
			NoteType = noteType;
			IsAttack = isAttack;
		}
	}
}