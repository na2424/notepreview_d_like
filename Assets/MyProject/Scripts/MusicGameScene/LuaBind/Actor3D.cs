﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{
	[Serializable]
	[LuaCallCSharp]
	public sealed class Actor3D : Actor
	{
		[SerializeField] MeshRenderer _meshRenderer;
		[SerializeField] MeshFilter _meshFilter;

		/// <summary>
		/// 自身の持つMeshRendererを取得します
		/// </summary>
		/// <returns></returns>
		public MeshRenderer GetMeshRenderer()
		{
			return _meshRenderer;
		}

		/// <summary>
		/// 自身の持つMeshFilterを取得します
		/// </summary>
		/// <returns></returns>
		public MeshFilter GetMeshFilter()
		{
			return _meshFilter;
		}

		///// <summary>
		///// 3Dモデルのファイル名からロードします
		///// </summary>
		///// <param name="imagePath"></param>
		//public void LoadModel(string modelPath)
		//{
		//	var path = Path.Combine(GameManager.Instance.SelectSongInfo.DirectoryPath, modelPath);

		//	//SpriteRenderer.sprite = LoadSprite(path);
		//}

		/// <summary>
		/// 設定されているMeshを取得します
		/// </summary>
		/// <param name="mesh">Mesh</param>
		public Mesh GetMesh()
		{
			return _meshFilter.mesh;
		}

		/// <summary>
		/// Meshを設定します
		/// </summary>
		/// <param name="mesh"></param>
		public void SetMesh(Mesh mesh)
		{
			_meshFilter.mesh = mesh;
		}

		/// <summary>
		/// 設定されているMaterialを取得します
		/// </summary>
		/// <returns>Material</returns>
		public Material GetMaterial()
		{
			return _meshRenderer.material;
		}

		/// <summary>
		/// Materialを設定します
		/// </summary>
		/// <param name="material"></param>
		public void SetMaterial(Material material)
		{
			_meshRenderer.material = material;
		}

		/// <summary>
		/// 自身の持つLuaの関数を呼び出します。
		/// (引数と戻り値なし版)
		/// </summary>
		/// <param name="functionName"></param>
		public void InvokeFunction(string functionName)
		{
			InvokeFunctionBase(functionName);
		}

		/// <summary>
		/// 自身の持つLuaの関数を呼び出します。
		/// (intの引数と戻り値あり版)
		/// </summary>
		/// <param name="functionName">関数名</param>
		/// <param name="value">引数</param>
		/// <returns>戻り値</returns>
		public int InvokeFunction(string functionName, int value)
		{
			return InvokeFunctionBase(functionName, value);
		}
	}
}