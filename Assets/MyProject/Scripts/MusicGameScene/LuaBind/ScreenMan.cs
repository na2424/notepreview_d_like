﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{

	[Serializable]
	[LuaCallCSharp]
	public class ScreenMan
	{
		[SerializeField] LuaOverlayPanel _luaOverlayPanel;

		[SerializeField] Canvas _overlayCanvas;
		[SerializeField] Canvas _luaOverlayCanvas;
		[SerializeField] Canvas _cameraCanvas;

		[BlackList, DoNotGen]
		public void Init()
		{
			_luaOverlayPanel.Init();
		}

		/// <summary>
		/// 画面上部に数秒間表示されるメッセージを表示します。
		/// メッセージは出力ログにも表示されます。
		/// string message : 表示するメッセージ。
		/// </summary>
		/// <param name="message"></param>
		public void SystemMessage(string message)
		{
			_luaOverlayPanel.ShowMessage(message);
			Debug.Log(message);
		}

		/// <summary>
		/// 画面上部に数秒間表示されるメッセージを表示します。数値版です。
		/// メッセージは出力ログにも表示されます。
		/// double message : 表示するメッセージ。
		/// </summary>
		/// <param name="value"></param>
		public void SystemMessage(double value)
		{
			_luaOverlayPanel.ShowMessage(value);
			Debug.Log(value);
		}

		/// <summary>
		/// 画面上部に数秒間表示されるメッセージを表示します。object版です。
		/// メッセージは出力ログにも表示されます。
		/// object message : 表示するメッセージ。
		/// </summary>
		/// <param name="obj"></param>
		public void SystemMessage(object obj)
		{
			_luaOverlayPanel.ShowMessage(obj);
			Debug.Log(obj);
		}

		/// <summary>
		/// 画面の横幅を取得します
		/// </summary>
		/// <returns></returns>
		public int GetScreenWidth()
		{
			return Screen.width;
		}

		/// <summary>
		/// 画面の縦幅を取得します
		/// </summary>
		/// <returns></returns>
		public int GetScreenHeight()
		{
			return Screen.height;
		}

		/// <summary>
		/// 画面のDPIを取得します
		/// </summary>
		/// <returns></returns>
		public float GetDpi()
		{
			return Screen.dpi;
		}

		/// <summary>
		/// Overlay階層のCanvasを取得します
		/// </summary>
		/// <returns></returns>
		public Canvas GetOverlayCanvas()
		{
			return _overlayCanvas;
		}

		/// <summary>
		/// Lua用のOverlay階層のCanvasを取得します
		/// </summary>
		/// <returns></returns>
		public Canvas GetLuaOverlayCanvas()
		{
			return _luaOverlayCanvas;
		}

		/// <summary>
		/// Camera階層のCanvasを取得します
		/// </summary>
		/// <returns></returns>
		public Canvas GetCameraCanvas()
		{
			return _cameraCanvas;
		}

		/// <summary>
		/// 画像をロードします。ChangeBgImageを使用する前に必要です。
		/// </summary>
		/// <param name="bgChangeImageName"></param>
		public void LoadBgChangeImages(List<string> bgChangeImageName)
		{
			BgManager.Instance.LoadBgChangeImages(LuaManager.FolderPath, bgChangeImageName);
		}

		/// <summary>
		/// 画像をロードします。ChangeBgImageを使用する前に必要です。
		/// </summary>
		/// <param name="bgChangeImageName"></param>
		public void LoadBgChangeImages(string[] bgChangeImageName)
		{
			BgManager.Instance.LoadBgChangeImages(LuaManager.FolderPath, bgChangeImageName.ToList());
		}

		/// <summary>
		/// 画像をロードします。ChangeBgImageを使用する前に必要です。
		/// </summary>
		/// <param name="bgChangeImageName"></param>
		public void LoadBgChangeImage(string bgChangeImageName)
		{
			BgManager.Instance.LoadBgChangeImages(LuaManager.FolderPath, new List<string>() { bgChangeImageName });
		}

		/// <summary>
		/// ロードしたファイル名を指定して背景を変更します。
		/// </summary>
		/// <param name="name"></param>
		public void ChangeBgImage(string name)
		{
			BgManager.Instance.ChangeBgImage(name);
		}

		/// <summary>
		/// 元のBACKGROUNDタグの背景に戻します。
		/// </summary>
		public void ResetBgImage()
		{
			BgManager.Instance.SetDefaultImage();
		}

		/// <summary>
		/// 背景にMaterialを設定します。
		/// </summary>
		/// <param name="material"></param>
		public void SetBgMaterial(Material material)
		{
			BgManager.Instance.SetMaterial(material);
		}

		/// <summary>
		/// 背景のMaterialをデフォルトにリセットします。
		/// </summary>
		public void ResetBgMaterial()
		{
			BgManager.Instance.ResetMaterial();
		}

		/// <summary>
		/// 背景ディマーを変更します(0～1)
		/// </summary>
		/// <param name="dimmer">背景ディマー(0～1)</param>
		public void SetBgDimmer(float dimmer)
		{
			BgManager.Instance.SetDimmer(dimmer);
		}

		public void HideLuaOverlayCanvas()
		{
			_luaOverlayCanvas.enabled = false;
		}
	}
}