﻿using Cysharp.Text;
using Dialog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using XLua;

// デリゲート
[CSharpCallLua]
public delegate int FuncDelegate(int a);

public abstract class Actor : MonoBehaviour
{
	string _name;
	LuaTable _scriptEnv;
	Action _luaStart;
	Action _luaUpdate;
	Action _luaFinish;
	Action _luaOnDestroy;
	ActionInt4 _luaOnHitNote;
	ActionInt3 _luaOnMissedNote;
	ActionNoteController _luaOnSpawnNote;
	ActionLongController _luaOnSpawnLong;
	Action _luaOnPause;
	Action _luaOnResume;
	ActionInput _luaOnInputDown;
	ActionInput _luaOnInputMove;
	ActionInput _luaOnInputUp;

	protected Action _onError;

	GameObject _gameObject;

	Dictionary<string, Action> _actionCache;
	Dictionary<string, FuncDelegate> _funcDelegateCache;

	public bool HasLuaUpdate => _luaUpdate != null;
	public bool HasLuaOnHitNote => _luaOnHitNote != null;
	public bool HasLuaOnMissedNote => _luaOnMissedNote != null;
	public bool HasLuaOnCreateNote => _luaOnSpawnNote != null;
	public bool HasLuaOnCreateLong => _luaOnSpawnLong != null;
	public bool HasLuaOnPasue => _luaOnPause != null;
	public bool HasLuaOnResume => _luaOnResume != null;
	public bool HasLuaOnInputDown => _luaOnInputDown != null;
	public bool HasLuaOnInputMove => _luaOnInputMove != null;
	public bool HasLuaOnInputUp => _luaOnInputUp != null;

	[BlackList, DoNotGen]
	public void Init(string name, LuaTable scriptEnv, Action onError, string actorTypeName)
	{
		_gameObject = gameObject;
		_name = scriptEnv == null ? "NoLua" : name;
		gameObject.name = ZString.Concat(Path.GetFileNameWithoutExtension(name), actorTypeName);
		_onError = onError;

		if (scriptEnv == null)
		{
			return;
		}

		_scriptEnv = scriptEnv;

		Action onloaded = scriptEnv.Get<Action>("onloaded");
		scriptEnv.Get("start", out _luaStart);
		scriptEnv.Get("update", out _luaUpdate);
		scriptEnv.Get("finish", out _luaFinish);
		scriptEnv.Get("ondestroy", out _luaOnDestroy);
		scriptEnv.Get("onHitNote", out _luaOnHitNote);
		scriptEnv.Get("onMissedNote", out _luaOnMissedNote);
		scriptEnv.Get("onSpawnNote", out _luaOnSpawnNote);
		scriptEnv.Get("onSpawnLong", out _luaOnSpawnLong);
		scriptEnv.Get("onPause", out _luaOnPause);
		scriptEnv.Get("onResume", out _luaOnResume);
		scriptEnv.Get("onInputDown", out _luaOnInputDown);
		scriptEnv.Get("onInputMove", out _luaOnInputMove);
		scriptEnv.Get("onInputUp", out _luaOnInputUp);

		if (onloaded != null)
		{
			try
			{
				onloaded();
			}
			catch (Exception e)
			{
				_onError();
				ShowErrorDialog(e.Message);
			}
		}
	}

	public void LuaStart()
	{
		if (_luaStart != null)
		{
			try
			{
				_luaStart();
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}
	}

	public void LuaUpdate()
	{
		if (!_gameObject.activeSelf)
		{
			return;
		}


		if (_luaUpdate != null)
		{
			try
			{
				_luaUpdate();
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}
	}

	public void LuaFinish()
	{
		if (!_gameObject.activeSelf)
		{
			return;
		}

		if (_luaFinish != null)
		{
			try
			{
				_luaFinish();
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}
	}

	public void LuaOnHitNote(int id, int lane, int noteType, int judge, bool isAttack)
	{
		if (_luaOnHitNote != null)
		{
			try
			{
				_luaOnHitNote(id, lane, noteType, judge, isAttack);
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}
	}

	public void LuaOnMissedNote(int id, int lane, int noteType)
	{
		if (_luaOnMissedNote != null)
		{
			try
			{
				_luaOnMissedNote(id, lane, noteType);
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}
	}

	public void LuaOnCreateNote(NoteController noteController)
	{
		if (_luaOnSpawnNote != null)
		{
			try
			{
				_luaOnSpawnNote(noteController);
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}
	}

	public void LuaOnCreateLong(LongController longController)
	{
		if (_luaOnSpawnLong != null)
		{
			try
			{
				_luaOnSpawnLong(longController);
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}
	}

	public void LuaOnPause()
	{
		if (_luaOnPause != null)
		{
			try
			{
				_luaOnPause();
			}
			catch (Exception e)
			{
				//_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}
	}

	public void LuaOnResume()
	{
		if (_luaOnResume != null)
		{
			try
			{
				_luaOnResume();
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}
	}

	public void LuaOnInputDown(int touchId, float posX, float screenPosX, float screenPosY)
	{
		if (_luaOnInputDown != null)
		{
			try
			{
				_luaOnInputDown(touchId, posX, screenPosX, screenPosY);
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}
	}

	public void LuaOnInputMove(int touchId, float posX, float screenPosX, float screenPosY)
	{
		if (_luaOnInputMove != null)
		{
			try
			{
				_luaOnInputMove(touchId, posX, screenPosX, screenPosY);
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}
	}

	public void LuaOnInputUp(int touchId, float posX, float screenPosX, float screenPosY)
	{
		if (_luaOnInputUp != null)
		{
			try
			{
				_luaOnInputUp(touchId, posX, screenPosX, screenPosY);
			}
			catch (Exception e)
			{
				_onError?.Invoke();
				ShowErrorDialog(e.Message);
				return;
			}
		}
	}

	protected void InvokeFunctionBase(string functionName)
	{
		if (_actionCache == null)
		{
			_actionCache = new Dictionary<string, Action>(StringComparer.Ordinal);
		}

		if (_actionCache.ContainsKey(functionName))
		{
			_actionCache[functionName]();
			return;
		}

		var function = _scriptEnv.Get<Action>(functionName);
		_actionCache[functionName] = function;
		function.Invoke();
	}

	protected int InvokeFunctionBase(string functionName, int value)
	{
		if (_funcDelegateCache == null)
		{
			_funcDelegateCache = new Dictionary<string, FuncDelegate>(StringComparer.Ordinal);
		}

		if (_funcDelegateCache.ContainsKey(functionName))
		{
			return _funcDelegateCache[functionName](value);
		}

		var function = _scriptEnv.Get<FuncDelegate>(functionName);
		_funcDelegateCache[functionName] = function;
		return function.Invoke(value);
	}

	void OnDestroy()
	{
		if (_luaOnDestroy != null)
		{
			_luaOnDestroy();
		}

		if (_actionCache != null)
		{
			_actionCache.Clear();
			_actionCache = null;
		}

		if (_funcDelegateCache != null)
		{
			_funcDelegateCache.Clear();
			_funcDelegateCache = null;
		}

		_luaOnDestroy = null;
		_luaUpdate = null;
		_luaStart = null;
		_luaOnHitNote = null;
		_luaOnMissedNote = null;
		_luaOnSpawnNote = null;
		_luaOnSpawnLong = null;
		_luaOnPause = null;
		_luaOnResume = null;
		_luaOnInputDown = null;
		_luaOnInputMove = null;
		_luaOnInputUp = null;

		_scriptEnv?.Dispose();
		_scriptEnv = null;

		_onError = null;
	}

	/// <summary>
	/// エラー時にダイアログを表示する
	/// </summary>
	/// <param name="message"></param>
	void ShowErrorDialog(string message)
	{
		Action onNext = () =>
		{
			if (!BgManager.Instance.IsDefaultImage())
			{
				BgManager.Instance.ClearBgChangeImageCache();
				BgManager.Instance.SetDefaultImage();
			}
			BgManager.Instance.ClearMaterial();
			GameManager.Instance.ChangeScene(SceneName.SelectMusic);
		};

		var builder = new DialogParametor.Builder("Luaの実行中にエラーが発生しました", ZString.Concat(_name, ':', message));
		builder.AddDefaultAction("選曲画面に戻る", onNext);
		builder.AddCallbackOnAutoClosed(onNext);
		DialogManager.Instance.Open(builder.Build());
	}
}
