﻿using System;
using UnityEngine;
using XLua;

namespace DankagLikeLuaAPI
{
	[LuaCallCSharp]
	public sealed class DummyNote : MonoBehaviour
	{
		[SerializeField] MeshRenderer _renderer;

		Transform _cacheTransform;

		Transform cacheTransform
		{
			get
			{
				if (_cacheTransform == null)
				{
					_cacheTransform = transform;
				}
				return _cacheTransform;
			}

			set
			{
				_cacheTransform = value;
			}
		}

		MaterialPropertyBlock _cacheMpb;
		MaterialPropertyBlock cacheMpb
		{
			get
			{
				if (_cacheMpb == null)
				{
					_cacheMpb = new MaterialPropertyBlock();
				}
				return _cacheMpb;
			}
		}

		public Material Material => _renderer.material;

		/// <summary>
		/// 自身のTransformを取得します
		/// </summary>
		/// <returns></returns>
		public Transform GetTransform()
		{
			return cacheTransform;
		}

		/// <summary>
		/// 表示/非表示を設定
		/// </summary>
		/// <param name="isActive"></param>
		public void SetActive(bool isActive)
		{
			gameObject.SetActive(isActive);
		}

		/// <summary>
		///	0から数えて左から指定したレーンの位置になるようにDummyNoteのX座標を設定します
		/// </summary>
		/// <param name="lane"></param>
		public void SetLanePosition(float lane)
		{
			var pos = cacheTransform.localPosition;
			cacheTransform.localPosition = new Vector3((lane * 0.5f) - 1.5f, pos.y, pos.z);
		}

		/// <summary>
		/// 判定までの拍数からノーツのZ座標を計算してDummyNoteのZ座標と回転を設定します
		/// (CMODを使用している場合、少し計算量が増えます)
		/// </summary>
		/// <param name="beat">拍数</param>
		public void SetBeatPosition(float beat)
		{
			var pos = cacheTransform.localPosition;

			// 位置.
			float posZ = Util.Instantce.CalculateBeatToNotePositionZ(beat);

			// 回転.
			var rot = Util.Instantce.CalculateNoteRotation(posZ);

			// 反映.
			cacheTransform.SetLocalPositionAndRotation(
				new Vector3(pos.x, pos.y, posZ),
				rot
			);
		}

		/// <summary>
		/// 判定までの時間(秒)からノーツのZ座標を計算してDummyNoteのZ座標と回転を設定します
		/// (CMODを使用していない場合、少し計算量が増えます)
		/// </summary>
		/// <param name="time">時間(秒)</param>
		public void SetTimePosition(float time)
		{
			var pos = cacheTransform.localPosition;

			// 位置.
			float posZ = Util.Instantce.CalculateTimeToNotePositionZ(time);

			// 回転.
			var rot = Util.Instantce.CalculateNoteRotation(posZ);

			// 反映.
			cacheTransform.SetLocalPositionAndRotation(
				new Vector3(pos.x, pos.y, posZ),
				rot
			);
		}

		/// <summary>
		/// ノーツのサイズを設定します
		/// </summary>
		/// <param name="size"></param>
		public void SetSize(float size)
		{
			var s = size * 0.75f;
			cacheTransform.localScale = Vector3.one * s;
		}

		/// <summary>
		/// テクスチャを設定します
		/// </summary>
		/// <param name="noteTex">Texture</param>
		public void SetTexture(Texture noteTex)
		{
			// マテリアルのInstanceを避ける.
			cacheMpb.SetTexture(Constant.ShaderProperty.MainTexId, noteTex);
			_renderer.SetPropertyBlock(cacheMpb);
		}

		/// <summary>
		/// Colorを設定します
		/// </summary>
		/// <param name="color">Color</param>
		public void SetColor(Color color)
		{
			cacheMpb.SetColor(Constant.ShaderProperty.Color, color);
			_renderer.SetPropertyBlock(cacheMpb);
		}

		/// <summary>
		/// Colorを設定します
		/// </summary>
		/// <param name="r">Red</param>
		/// <param name="g">Green</param>
		/// <param name="b">Blue</param>
		/// <param name="a">Alpha</param>
		public void SetColor(float r, float g, float b, float a)
		{
			cacheMpb.SetColor(Constant.ShaderProperty.Color, new Color(r, g, b, a));
			_renderer.SetPropertyBlock(cacheMpb);
		}

		/// <summary>
		/// 透明度を設定します(0～1)
		/// 設定されたColorに対して乗算されます
		/// </summary>
		/// <param name="alpha">透明度</param>
		public void SetAlpha(float alpha)
		{
			cacheMpb.SetFloat(Constant.ShaderProperty.Alpha, Mathf.Clamp01(alpha));
			_renderer.SetPropertyBlock(cacheMpb);
		}
	}
}
