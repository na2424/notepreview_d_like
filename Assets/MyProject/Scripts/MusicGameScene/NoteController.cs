﻿using DankagLikeLuaAPI;
using System;
using UnityEngine;
using UnityEngine.Pool;
using XLua;

[LuaCallCSharp]
public enum NotesScrollType : int
{
	Constant = 0,
	Decelerate = 1,
}

/// <summary>
/// Noteの1つあたりの情報を管理するクラス
/// </summary>
[LuaCallCSharp]
public sealed class NoteController : MonoBehaviour
{
	//---------------------------------
	// インスペクタまたは外部から設定.
	//---------------------------------
	[SerializeField] Transform _transform;
	[SerializeField] MeshRenderer _renderer;
	[SerializeField] MeshRenderer _attackMeshRenderer;

	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<NoteController> _pool;
	MaterialPropertyBlock _mpb;
	Action<NoteController> _luaNoteControllerCallBack;

	bool _isReleased;
	bool _isCMod;
	bool _enableDefaultMove = true;
	double _beatPosition = -1f;
	double _justTime = -1d;
	double _hitTime = 1d;
	float _visualOffset = 0f;
	int _lane = -1;
	bool _isAttack = false;
	int _noteIndex = -1;

	float _normalizedSpeed = 1f;
	float _individualSpeed = 1f;

	//------------------
	// 定数.
	//------------------
	const float BEAT_DISTANCE = Constant.Note.BEAT_DISTANCE;
	const float TIME_DISTANCE = Constant.Note.TIME_DISTANCE;
	const float LANE_DISTANCE = Constant.Note.LANE_DISTANCE;
	const float OFFSET_X = Constant.Note.OFFSET_X;
	const float OFFSET_Z = Constant.Note.OFFSET_Z;

	//------------------
	// プロパティ.
	//------------------
	public bool IsReleased => _isReleased;
	public int NoteIndex => _noteIndex;
	public int Lane => _lane;
	public bool IsAttack => _isAttack;
	public bool IsMissed { get; private set; } = false;
	public GameObject MyGameObject { get; private set; }
	public NoteType NoteType { get; private set; }
	public LongController LongController { get; private set; }
	public float PosX => _transform.position.x;
	public double BeatPosition => _beatPosition;
	public double JustBeat => _beatPosition;
	public double JustTime => _justTime;
	public int UpTouchId { get; set; } = -1;
	AnimationCurve NoteAnimationCurve => GameManager.Instance.NoteScrollAnimationCurve;
	AnimationCurve InverseNoteAnimationCurve => GameManager.Instance.InverseAnimationCurve;

	[BlackList, DoNotGen]
	public void Init(ObjectPool<NoteController> pool, Vector3 noteSize)
	{
		MyGameObject = gameObject;
		_pool = pool;
		_transform.localScale = noteSize;
		_mpb = new MaterialPropertyBlock();
		_normalizedSpeed = Constant.Note.FIXED_BPM / GameManager.Instance.SelectSongInfo.BaseBpm;

		_hitTime = GameManager.Instance.JudgeTimeOption.GetLongEndHitTime() * GameManager.Instance.JudgeTimeOption.MusicRate;
		_isCMod = GameManager.Instance.IsCMod;
		_visualOffset = GameManager.Instance.DisplayOption.VisualOffset;

		// MEMO: ==演算子がオーバーロードされているため、本当のnullを代入する
		if (_attackMeshRenderer == null)
		{
			_attackMeshRenderer = null;
		}
	}

	[BlackList, DoNotGen]
	public void SetParam(float beatPosition, double justTime, int lane, Texture noteTex, NoteType noteType, bool isAttack, int noteIndex)
	{
		_isReleased = false;
		_beatPosition = beatPosition;
		_justTime = justTime;
		_lane = lane;
		_isAttack = isAttack;
		_individualSpeed = 1f;

		if (_attackMeshRenderer is not null)
		{
			_attackMeshRenderer.enabled = _isAttack;
		}

		_noteIndex = noteIndex;
		_enableDefaultMove = true;
		// マテリアルのInstanceを避ける.
		_mpb.SetTexture(Constant.ShaderProperty.MainTexId, noteTex);
		_mpb.SetFloat(Constant.ShaderProperty.Alpha, 1f);
		_renderer.SetPropertyBlock(_mpb);
		IsMissed = false;
		NoteType = noteType;
		LongController = null;
		_luaNoteControllerCallBack = null;
	}

	[BlackList, DoNotGen]
	public void SetLongController(LongController longController)
	{
		if (
			NoteType == NoteType.LongStart ||
			NoteType == NoteType.FuzzyLongStart ||
			NoteType == NoteType.LongRelay ||
			NoteType == NoteType.FuzzyLongRelay
		)
		{
			LongController = longController;
		}
	}

	[BlackList, DoNotGen]
	public void CallUpdate(double beat, double musicTime, float speedStretchRatio = 1f)
	{
		if (_isReleased || !_renderer.enabled)
		{
			return;
		}

		if (_enableDefaultMove)
		{
			if (_isCMod)
			{
				UpdateCModNotePosition(musicTime, speedStretchRatio);
			}
			else
			{
				UpdateNotePosition(beat, speedStretchRatio);
			}
		}

		// Lua用のコールバック
		if (_luaNoteControllerCallBack is not null)
		{
			_luaNoteControllerCallBack(this);
		}

		CheckReleaseNote(musicTime);
	}
	void UpdateCModNotePosition(double musicTime, float speedStretchRatio)
	{
		double diffTime = _justTime - musicTime;

		// 位置
		float posZ = (float)(diffTime * TIME_DISTANCE * _individualSpeed * GameParam.Instance.NoteSpeed * speedStretchRatio - (_visualOffset + OFFSET_Z + GameParam.Instance.OffsetZ));

		if (GameParam.Instance.NotesScrollType == NotesScrollType.Decelerate)
		{
			posZ = NoteAnimationCurve.Evaluate(posZ);
		}

		if (float.IsNaN(posZ))
		{
			posZ = 100f;
		}

		// 回転
		float rot = InverseNoteAnimationCurve.Evaluate(posZ) * 0.78f;

		if (float.IsNaN(rot))
		{
			rot = posZ * 0.78f;
		}

		// 更新
		_transform.SetLocalPositionAndRotation(
			new Vector3(_lane * LANE_DISTANCE - OFFSET_X, 0f, posZ),
			Quaternion.Euler(90f - rot, 0f, 0f)
		);
	}

	void UpdateNotePosition(double beat, float speedStretchRatio)
	{
		double diffBeat = _beatPosition - beat;

		// 位置
		float posZ = (float)(diffBeat * BEAT_DISTANCE * _normalizedSpeed * _individualSpeed * GameParam.Instance.NoteSpeed * speedStretchRatio - (_visualOffset + OFFSET_Z + GameParam.Instance.OffsetZ));

		if (GameParam.Instance.NotesScrollType == NotesScrollType.Decelerate)
		{
			posZ = NoteAnimationCurve.Evaluate(posZ);
		}

		if (float.IsNaN(posZ))
		{
			posZ = 100f;
		}

		// 回転
		float rot = InverseNoteAnimationCurve.Evaluate(posZ) * 0.78f;

		if (float.IsNaN(rot))
		{
			rot = posZ * 0.78f;
		}

		// 更新
		_transform.SetLocalPositionAndRotation(
			new Vector3(_lane * LANE_DISTANCE - OFFSET_X, 0f, posZ),
			Quaternion.Euler(90f - rot, 0f, 0f)
		);
	}

	void CheckReleaseNote(double musicTime)
	{
		if (_justTime + _hitTime < musicTime)
		{
			Release(true);
		}
	}

	[BlackList, DoNotGen]
	public void Release(bool isMissed = false)
	{
		if (_isReleased)
		{
			return;
		}

		IsMissed = isMissed;
		_isReleased = true;
		_pool.Release(this);
	}

	[BlackList, DoNotGen]
	public void Show()
	{
		_renderer.enabled = true;
	}

	[BlackList, DoNotGen]
	public void Hide()
	{
		_renderer.enabled = false;
		if (_attackMeshRenderer is not null)
		{
			_attackMeshRenderer.enabled = false;
		}
	}

	//---------------------
	// LuaAPI
	//---------------------
	/// <summary>
	/// ゲーム側のノーツ移動を有効にするか設定します
	/// ノーツをLua側で動かしてゲーム側の移動処理が必要ない場合はfalseを指定します
	/// デフォルトはtrueに設定されています
	/// - LuaAPI 1.5
	/// </summary>
	/// <param name="enable"></param>
	public void EnableDefaultMove(bool enable = true)
	{
		_enableDefaultMove = enable;
	}

	/// <summary>
	/// 毎フレーム呼ばれるコールバック関数を設定します
	/// </summary>
	/// <param name="noteControllerCallBack"></param>
	public void SetDelegate(Action<NoteController> noteControllerCallBack)
	{
		_luaNoteControllerCallBack = noteControllerCallBack;
	}

	/// <summary>
	/// ノーツ位置を座標で設定します
	/// </summary>
	/// <param name="x">X座標</param>
	/// <param name="y">Y座標</param>
	/// <param name="z">Z座標</param>
	public void SetPosition(float x, float y, float z)
	{
		// 回転.
		var rot = Util.Instantce.CalculateNoteRotation(z);

		// 反映.
		_transform.SetLocalPositionAndRotation(
			new Vector3(x, y, z),
			rot
		);
	}

	/// <summary>
	///	0から数えて左から指定したレーンの位置になるようにDummyNoteのX座標を設定します
	/// - LuaAPI 1.5
	/// </summary>
	/// <param name="lane"></param>
	public void SetLanePosition(float lane)
	{
		var pos = _transform.localPosition;
		_transform.localPosition = new Vector3((lane * 0.5f) - 1.5f, pos.y, pos.z);
	}

	/// <summary>
	/// 判定までの拍数からノーツのZ座標を計算してDummyNoteのZ座標と回転を設定します
	/// (CMODを使用している場合、少し計算量が増えます)
	/// - LuaAPI 1.5
	/// </summary>
	/// <param name="beat">拍数</param>
	public void SetBeatPosition(float beat)
	{
		var pos = _transform.localPosition;

		// 位置.
		float posZ = Util.Instantce.CalculateBeatToNotePositionZ(beat);

		// 回転.
		var rot = Util.Instantce.CalculateNoteRotation(posZ);

		// 反映.
		_transform.SetLocalPositionAndRotation(
			new Vector3(pos.x, pos.y, posZ),
			rot
		);
	}

	/// <summary>
	/// 判定までの時間(秒)からノーツのZ座標を計算してDummyNoteのZ座標と回転を設定します
	/// (CMODを使用していない場合、少し計算量が増えます)
	/// - LuaAPI 1.5
	/// </summary>
	/// <param name="time">時間(秒)</param>
	public void SetTimePosition(float time)
	{
		var pos = _transform.localPosition;

		// 位置.
		float posZ = Util.Instantce.CalculateTimeToNotePositionZ(time);

		// 回転.
		var rot = Util.Instantce.CalculateNoteRotation(posZ);

		// 反映.
		_transform.SetLocalPositionAndRotation(
			new Vector3(pos.x, pos.y, posZ),
			rot
		);
	}

	/// <summary>
	/// ノーツのサイズを設定します
	/// - LuaAPI 1.5
	/// </summary>
	/// <param name="size"></param>
	public void SetSize(float size)
	{
		var s = size * 0.75f;
		_transform.localScale = Vector3.one * s;
	}

	/// <summary>
	/// ノーツのテクスチャを設定します
	/// </summary>
	/// <param name="noteTex"></param>
	public void SetTexture(Texture noteTex)
	{
		// マテリアルのInstanceを避ける.
		_mpb.SetTexture(Constant.ShaderProperty.MainTexId, noteTex);
		_renderer.SetPropertyBlock(_mpb);
	}

	/// <summary>
	/// ノーツの透明度(0～1)を設定します
	/// - LuaAPI 1.5
	/// </summary>
	/// <param name="alpha"></param>
	public void SetAlpha(float alpha)
	{
		_mpb.SetFloat(Constant.ShaderProperty.Alpha, alpha);
		_renderer.SetPropertyBlock(_mpb);
	}

	/// <summary>
	/// アタックノーツを設定します
	/// </summary>
	/// <param name="isAttack"></param>
	public void SetAttack(bool isAttack)
	{
		if (_attackMeshRenderer is not null)
		{
			_attackMeshRenderer.enabled = isAttack;
		}
	}

	/// <summary>
	/// ノーツ単体(個別)のスピードを設定します
	/// </summary>
	public void SetIndividualSpeed(float individualSpeed)
	{
		_individualSpeed = individualSpeed;
	}

}
