﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class JudgePlateController : MonoBehaviour
{
	MeshRenderer _meshRenderer;
	MaterialPropertyBlock _mpb;

	public void Init()
	{
		_meshRenderer = GetComponent<MeshRenderer>();
		_mpb = new MaterialPropertyBlock();

		if (GameManager.Instance.NotesOption.IsCustomSeparator)
		{
			SetColor(GameManager.Instance.NotesOption.SeparatorColor);
		}
		else
		{
			SetDefaultColor();
		}
	}

	public void SetColor(Color color)
	{
		var plateColor = _meshRenderer.material.GetColor(Constant.ShaderProperty.Color);

		Color.RGBToHSV(color, out var h, out var s, out var v);
		Color.RGBToHSV(plateColor, out var ph, out var ps, out var pv);
		var targetColor = Color.HSVToRGB(h, ps, pv);
		targetColor = new Color(targetColor.r, targetColor.g, targetColor.b, Constant.Note.DefaultJudgePlateColor.a);
		_mpb.SetColor(Constant.ShaderProperty.Color, targetColor);
		_meshRenderer.SetPropertyBlock(_mpb);
	}

	public void SetDefaultColor()
	{
		_mpb.SetColor(Constant.ShaderProperty.Color, Constant.Note.DefaultJudgePlateColor);
		_meshRenderer.SetPropertyBlock(_mpb);
	}
}
