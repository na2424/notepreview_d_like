﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class LaneController : MonoBehaviour
{
	[SerializeField] SpriteRenderer[] _separator;
	public void Init()
	{
		if (GameManager.Instance.NotesOption.IsCustomSeparator)
		{
			SetColor(GameManager.Instance.NotesOption.SeparatorColor);
		}
	}

	public void SetColor(Color color)
	{
		Color customColor = color;
		for (int i = 0; i < _separator.Length; i++)
		{
			_separator[i].color = customColor;
		}
	}
}
