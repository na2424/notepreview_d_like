﻿using Cysharp.Text;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Dialog;
using System;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using CriWare;

/// <summary>
/// [ゲーム画面の管理クラス].
/// 下記はシーンの設計思想について記述する.
/// 
/// シーン内の他のスクリプトはStartとUpdateメソッドを持たず代わりにInitとCallUpdateを用意しておく、
/// また、初期化に時間が掛かるようであれば 非同期にして終わるまで待機させる.フレーム落ちは起こさないようにする.
/// シーン内のスクリプトで行われる初期化はこのシーンコントローラーを介して行う.
/// 理由は3つあって1つ目は参照で初期化処理を追えること (シーンのあちこちにStartがあると誰が何をしているのか見つけるのが難しくなる)、
/// 2つ目は初期化の順番やタイミングを相対的に選べること.
/// 3つ目は今回未使用であるが、UniRxを使用する時のSubscribeと相性がいい.(ReactiveProperty等の各イベントやUpdateAsObservableの登録がしやすい)
/// 
/// ■ Updateの代わりにCallUpdateを用意する理由.
/// UnityのUpdateはマネージドコード(C# IL)と非マネージドコード(ネイティブC++)のやり取りにコストがあるため、
/// 一つのUpdateから他のUpdateとなる処理を呼ぶほうが効率が良いことが知られている.
/// CallUpdateを呼ぶタイミングを相対的に選べるため依存関係のある処理に対応しやすくなる.
/// </summary>
public sealed class MusicGameSceneController : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] Button _pauseButton;
	[SerializeField] GameCameraController _gameCameraController;
	[SerializeField] PlayModeController _playStateController;
	[SerializeField] WaitPlayPanel _waitPlayPanel;
	[SerializeField] PausePanelController _pausePanelController;
	[SerializeField] ClearStateViewController _stateViewController;
	[SerializeField] MusicTimePanelController _musicTimePanelController;
	[SerializeField] FilterColorController _filterColorController;
	[SerializeField] LaneController _laneController;
	[SerializeField] JudgePlateController _judgePlateController;
	[SerializeField] KeyboardInput _keyboardInput;

	[SerializeField] TouchArea _touchArea;
	[SerializeField] AutoPlay _autoPlay;

	[SerializeField] MusicManager _musicManager;
	[SerializeField] JudgeManager _judgeManager;
	[SerializeField] LifeManager _lifeManager;
	[SerializeField] ComboManager _comboManager;
	[SerializeField] ScoreManager _scoreManager;
	[SerializeField] LuaManager _luaManager;
	[SerializeField] GameObject _runTimeInspectorAndHierarchy;
	[SerializeField] ShaderVariantCollection _shaderVariantCollection;

	[SerializeField] MusicGame _musicGame = new MusicGame();

	//------------------
	// キャッシュ.
	//------------------
	CancellationTokenSource _ctSource = null;
	InputBase _input = null;
	GameParam _gameParam = new GameParam();

	bool _isAuto = false;
	bool _isWaitRunTimeIH = false;
	bool _isLoadedRunTimeIH = false;

	void Start()
	{

#if UNITY_EDITOR
		// エディタでゲーム画面から開始した場合、選曲画面に移動する
		if (GameManager.Instance.SelectSongInfo.DirectoryPath.IsNullOrEmpty())
		{
			GameManager.Instance.ChangeScene(SceneName.SelectMusic);
			return;
		}
#endif
		_runTimeInspectorAndHierarchy.SetActive(false);

		FadeManager.Instance.SetBlack();
		SetEvent();

		if (!_shaderVariantCollection.isWarmedUp)
		{
			_shaderVariantCollection.WarmUp();
		}

		_isAuto = GameManager.Instance.PlayModeOption.PlayMode != PlayMode.InputCheck;
		
		_gameParam.Init();
		_gameParam.NotesScrollType = NotesScrollType.Decelerate;

		_ctSource = new CancellationTokenSource();
		InitAsync(_ctSource.Token).Forget();
	}

	/// <summary>
	/// イベント処理を設定
	/// </summary>
	void SetEvent()
	{
		_musicManager.OnFinishMusic += OnFinishMusic;

		_lifeManager.OnDead += OnDead;

		_judgeManager.OnJudge += _lifeManager.UpdateLife;
		_judgeManager.OnJudge += _comboManager.UpdateCombo;
		_judgeManager.OnAfterJudge += _scoreManager.OnAfterJudge;

		_musicGame.OnMissedNote += _judgeManager.AddMissedJudge;
		_musicGame.OnMissedNote += _lifeManager.UpdateLife;
		_musicGame.OnMissedNote += _comboManager.UpdateCombo;
		_musicGame.OnMissedNote += OnMissedNote;

		_pausePanelController.OnExit += OnPauseExit;
		_pausePanelController.OnRetire += _waitPlayPanel.OnEndMusic;
		_pausePanelController.OnFinalize += OnFinalize;

		_keyboardInput.OnPressHIKey += OnPressInspectorAndHierarchyKey;

		_pauseButton.onClick.AddListener(() =>
		{
			Pause();
		});
	}

	void OnPressInspectorAndHierarchyKey()
	{
		if (!_isLoadedRunTimeIH && _musicManager.IsPlay)
		{
			Pause();
		}

		if (!_isWaitRunTimeIH)
		{
			WaitActiveRunTimeIHAsync(_ctSource.Token).Forget();
		}
	}

	void Pause()
	{
		_musicManager.Pause();
		_musicGame.OnPause();
		_pausePanelController.Show();

		if (_luaManager.IsLoaded)
		{
			_luaManager.OnPause();
		}
	}

	void OnPauseExit()
	{
		_musicGame.OnResume();
		_input.ClearInputData();
		_musicManager.Play();

		if (_luaManager.IsLoaded)
		{
			_luaManager.OnResume();
		}
	}

	void OnMissedNote(JudgeType _)
	{
		_musicGame.AddMissedTimingHistory(_musicManager.GetMusicTime());
	}

	async UniTask InitAsync(CancellationToken ct)
	{
		FadeManager.Instance.SetBlack();
		BgManager.Instance.UseDimmer(true);

		_gameCameraController.Init();
		_filterColorController.Init();
		_laneController.Init();
		_judgePlateController.Init();
		_playStateController.Init();
		_stateViewController.Init();
		_lifeManager.Init();
		_judgeManager.Init();
		_pausePanelController.Init();
		_comboManager.Init();
		_scoreManager.Init();

		// タッチSEの設定
		LoadTouchSE();

		_input = _isAuto ? _autoPlay : _touchArea;

		var songInfo = CopySongInfo(GameManager.Instance.SelectSongInfo);

		BgManager.Instance.LoadBgChangeImages(songInfo.DirectoryPath, songInfo.BgChangeImageName);

		bool isSuccess = await _musicGame.InitAsync(ct, songInfo, _input, _judgeManager);

		if (isSuccess)
		{
			BindLua(songInfo);

			_waitPlayPanel.Init(ct, OnPlay, songInfo, _musicGame.LastNoteTime, _musicGame.Sequence.Credit);

			_comboManager.SetTotalNote(_musicGame.TotalNote);
			_scoreManager.SetTotalNote(_musicGame.TotalNote);

			if (_isAuto)
			{
				_autoPlay.Init(_musicGame.Sequence, _musicGame.BpmHelper, _musicGame.NoteObjectPool, _musicGame.MaxSameTimeLongCount);
			}

			_touchArea.Init(!_isAuto, _musicManager);
		}

		FadeManager.Instance.FadeInAsync().Forget();
	}

	/// <summary>
	/// タッチSEを読み込む
	/// </summary>
	void LoadTouchSE()
	{
		CriAtom.instance.UnloadTouchSE();

		// カスタムSEの設定
		int seIndex = TouchSeCache.Instance.Data.FindIndex(t => t.TouchSeName == GameManager.Instance.NotesOption.TouchSeName);
		var touchSeInfo = seIndex == -1 ? TouchSeCache.Instance.Data[0] : TouchSeCache.Instance.Data[seIndex];

		CriAtomPlugin.UseCustomTouchSE = touchSeInfo.TouchSeName != Constant.Note.DEFAULT_TOUCH_SE_NAME;

		string seName = touchSeInfo.TouchSeName;
		string acbFile = touchSeInfo.AcbFile;

		try
		{
			CriAtom.instance.LoadTouchSE(seName, acbFile);
		}
		catch
		{
			var builder = new DialogParametor.Builder("タッチSE読み込みエラー", $"{seName}のファイルを確認してください");
			builder.AddDefaultAction("選曲画面に戻る", () => GameManager.Instance.ChangeScene(SceneName.SelectMusic));
			builder.AddCallbackOnAutoClosed(() => GameManager.Instance.ChangeScene(SceneName.SelectMusic));
			DialogManager.Instance.Open(builder.Build());
		}
	}

	/// <summary>
	/// Luaとの連携クラスを初期化
	/// </summary>
	/// <param name="songInfo"></param>
	void BindLua(SongInfo songInfo)
	{
		string folderPath;
		string luaFileName;

		if (songInfo.LuaScript.HasValue())
		{
			folderPath = songInfo.DirectoryPath;
			luaFileName = songInfo.LuaScript;
		}
		else
		{
			int luaIndex = GlobalLuaCache.Instance.Data.FindIndex(t => t.GlobalLuaName == GameManager.Instance.JudgeTimeOption.GlobalLuaName);
			var globalLuaInfo = luaIndex == -1 ? GlobalLuaCache.Instance.Data[0] : GlobalLuaCache.Instance.Data[luaIndex];

			folderPath = ZString.Concat(ExternalDirectory.GlobalLuaPath, "/", globalLuaInfo.GlobalLuaName);
			luaFileName = globalLuaInfo.FileName;
		}

		_luaManager.Init(folderPath, luaFileName, Pause, onComplete: () =>
		{
			_luaManager.ScreenMan.Init();
			_luaManager.GameState.Init(_musicManager, _musicGame);
			_luaManager.PlayerStats.Init(_scoreManager, _comboManager, _lifeManager, _touchArea);
			_luaManager.CameraMan.Init(_gameCameraController);
			_luaManager.SongMan.Init(songInfo, _musicGame.Sequence, _musicManager);
			_luaManager.AssetMan.Init(Pause);
			_luaManager.Util.Init(_musicManager, _musicGame);
			_luaManager.DanmakuStageMan.Init(_lifeManager, _musicGame);

			_musicGame.OnHitNoteLua += _luaManager.OnHitNote;
			_musicGame.OnMissedNoteLua += _luaManager.OnMissedNote;
			_musicGame.OnSpawnNoteLua += _luaManager.OnSpawnNote;
			_musicGame.OnSpawnLongLua += _luaManager.OnSpawnLong;

			if (!_isAuto)
			{
				_touchArea.OnInputDown += _luaManager.OnInputDown;
				_touchArea.OnInputMove += _luaManager.OnInputMove;
				_touchArea.OnInputUp += _luaManager.OnInputUp;
			}
		});
	}

	/// <summary>
	/// 開始ボタンが押された時に呼ばれる処理
	/// </summary>
	/// <param name="speed">ノーツスピード</param>
	/// <param name="startTime">開始時間</param>
	void OnPlay(float speed, float startTime)
	{
		_gameParam.NoteSpeed = speed / GameManager.Instance.JudgeTimeOption.MusicRate;
		_musicGame.SetStartBarFromStartTime(startTime);

		GC.Collect();

		_musicGame.SetStartBarFromStartTime(startTime);

		OnPlayAsync(startTime).Forget();
	}

	async UniTask OnPlayAsync(float startTime)
	{
		var info = GameManager.Instance.SelectSongInfo;

		if (GameManager.Instance.PlayModeOption.PlayMode == PlayMode.Rehearsal)
		{
			_autoPlay.SetMidPlay(startTime);
			_musicGame.SetMidPlay(startTime);
		}
		else if(GameManager.Instance.PlayModeOption.PlayMode == PlayMode.InputCheck)
		{
			_musicGame.SetMidPlay(startTime);
		}
		else
		{
			startTime = 0f;
		}

		await _musicManager.InitAsync(_ctSource.Token, info.DirectoryPath, info.MusicFileName, _musicGame.LastNoteTime, startTime);

		_musicTimePanelController.Init(_musicManager.MusicEndTime);

		if (_luaManager.IsLoaded)
		{
			_luaManager.StartLua();
		}
	}

	void Update()
	{
		_musicManager.CallUpdate();

		_touchArea.Enable = _musicManager.IsPlay;

		if (!_musicManager.IsPlay)
		{
			return;
		}

		double musicTime = _musicManager.GetMusicTime();

		_input.CallUpdate(musicTime);
		_musicGame.CallUpdate(musicTime);
		_input.ClearInputData();

		_judgeManager.CallUpdate();
		_scoreManager.CallUpdate();
		_comboManager.CallUpdate();
		_lifeManager.UpdateRecord(musicTime);
		_musicTimePanelController.CallUpdate(musicTime);

		if (_luaManager.IsLoaded)
		{
			_luaManager.CallUpdate(musicTime);
		}
	}

	async UniTask WaitActiveRunTimeIHAsync(CancellationToken ct)
	{
		_isWaitRunTimeIH = true;

		_runTimeInspectorAndHierarchy.SetActive(!_runTimeInspectorAndHierarchy.activeSelf);

		await UniTask.Yield(ct);

		_isWaitRunTimeIH = false;
		_isLoadedRunTimeIH = true;
	}

	void OnDead()
	{
		//if (GameManager.Instance.PlayModeOption.PlayMode == PlayMode.Normal)
		//{
		//	_musicManager.Pause();
		//	_pausePanelController.Show(true);
		//}
	}

	void OnFinishMusic()
	{
		_pauseButton.interactable = false;

		var resultData = CreateResultData();
		GameManager.Instance.ResultData = resultData;
		ShowClearState(resultData);

		_waitPlayPanel.OnEndMusic();

		if (!BgManager.Instance.IsDefaultImage())
		{
			BgManager.Instance.ClearBgChangeImageCache();
			BgManager.Instance.SetDefaultImage();
		}
		BgManager.Instance.ClearMaterial();

		BgManager.Instance.UseDimmer(false);

		WaitAsync(_ctSource.Token).Forget();
	}

	void ShowClearState(ResultData resultData)
	{
		if (resultData.IsAllBrilliant)
		{
			//Debug.Log("AllBrilliant");
			_stateViewController.ShowState(ClearStateType.AllBrilliant, true, true, true);
		}
		else if (resultData.IsFullCombo)
		{
			//Debug.Log("FullCombo");
			_stateViewController.ShowState(ClearStateType.FullCombo, true, true, true);
		}
		else if (!resultData.IsDead)
		{
			//Debug.Log("StageClear");
			_stateViewController.ShowState(ClearStateType.Cleared, true, true);
		}
		else
		{
			//Debug.Log("Dead");
			_stateViewController.ShowState(ClearStateType.GameOver, true, true);
		}
	}

	async UniTask WaitAsync(CancellationToken ct)
	{
		try
		{
			await UniTask.Delay(2000, DelayType.DeltaTime, PlayerLoopTiming.Update, ct);
		}
		catch (OperationCanceledException e)
		{
			Debug.LogWarning("キャンセルされました:" + e.Message);
			return;
		}

		int nextSceneIndex = GameManager.Instance.JudgeTimeOption.EnableResultScene
			? SceneBuildIndex.Result
			: SceneBuildIndex.SelectMusic;

		await FadeManager.Instance.FadeOutAsync(nextSceneIndex);
	}

	ResultData CreateResultData() => new()
	{
		Score = _scoreManager.Score,
		Judge = _judgeManager.Judge,
		IsDead = _lifeManager.IsDead,
		IsFullCombo = _comboManager.IsFullCombo,
		IsAllBrilliant = _judgeManager.IsAllBrilliant,
		MaxCombo = _comboManager.MaxCombo,
		TimingHistory = _musicGame.TimingHistory,
		NoFuzzyTimingHistory = _musicGame.NoFuzzyTimingHistory,
		LifeHistory = _lifeManager.LifeHistory
	};

	SongInfo CopySongInfo(SongInfo songInfo)
	{
		return new SongInfo()
		{
			Version = songInfo.Version,
			Title = songInfo.Title,
			SubTitle = songInfo.SubTitle,
			Artist = songInfo.Artist,
			ChartArtist = songInfo.ChartArtist,
			Illust = songInfo.Illust,
			Description = songInfo.Description,
			Difficulty = songInfo.Difficulty,
			Offset = songInfo.Offset,
			BaseBpm = songInfo.BaseBpm,
			BpmPositions = songInfo.BpmPositions.DeepCopy(),
			Bpms = songInfo.Bpms.DeepCopy(),
			SpeedPositions = songInfo.SpeedPositions.DeepCopy(),
			SpeedStretchRatios = songInfo.SpeedStretchRatios.DeepCopy(),
			SpeedDelayBeats = songInfo.SpeedDelayBeats.DeepCopy(),
			ScrollPositions = songInfo.ScrollPositions.DeepCopy(),
			Scrolls = songInfo.Scrolls.DeepCopy(),
			BgChangePositions = songInfo.BgChangePositions,
			BgChangeImageName = songInfo.BgChangeImageName,
			IsCMod = songInfo.IsCMod,
			LuaScript = songInfo.LuaScript,
			SampleStart = songInfo.SampleStart,
			SampleLength = songInfo.SampleLength,
			DirectoryPath = songInfo.DirectoryPath,
			MusicFileName = songInfo.MusicFileName,
			JacketFileName = songInfo.JacketFileName,
			BgFileName = songInfo.BgFileName,
			JacketTexture = songInfo.JacketTexture
		};
	}

	void OnFinalize()
	{
		_input.OnFinalize();
		_lifeManager.OnFinalize();
		_musicGame.OnFinalize();
	}

	void OnDestroy()
	{
		_ctSource?.Cancel();
		_ctSource = null;

		if (_luaManager.IsLoaded)
		{
			_musicGame.OnHitNoteLua -= _luaManager.OnHitNote;
			_musicGame.OnMissedNoteLua -= _luaManager.OnMissedNote;
		}

		_pausePanelController.OnExit -= _musicManager.Play;
		_pausePanelController.OnRetire -= _waitPlayPanel.OnEndMusic;

		_keyboardInput.OnPressHIKey -= OnPressInspectorAndHierarchyKey;

		if (_luaManager.IsLoaded)
		{
			_musicGame.OnHitNoteLua -= _luaManager.OnHitNote;
			_musicGame.OnMissedNoteLua -= _luaManager.OnMissedNote;
			_musicGame.OnSpawnNoteLua -= _luaManager.OnSpawnNote;
			_musicGame.OnSpawnLongLua -= _luaManager.OnSpawnLong;

			if (!_isAuto)
			{
				_touchArea.OnInputDown -= _luaManager.OnInputDown;
				_touchArea.OnInputMove -= _luaManager.OnInputMove;
				_touchArea.OnInputUp -= _luaManager.OnInputUp;
			}
		}

		_musicManager.OnFinishMusic -= OnFinishMusic;

		_lifeManager.OnDead -= OnDead;

		_judgeManager.OnJudge -= _lifeManager.UpdateLife;
		_judgeManager.OnJudge -= _comboManager.UpdateCombo;
		_judgeManager.OnAfterJudge -= _scoreManager.OnAfterJudge;

		_musicGame.OnMissedNote -= _judgeManager.AddMissedJudge;
		_musicGame.OnMissedNote -= _lifeManager.UpdateLife;
		_musicGame.OnMissedNote -= _comboManager.UpdateCombo;
		_musicGame.OnMissedNote -= OnMissedNote;

		_pausePanelController.OnExit -= OnPauseExit;
		_pausePanelController.OnRetire -= _waitPlayPanel.OnEndMusic;
		_pausePanelController.OnFinalize -= OnFinalize;


		DOTween.Clear();
	}
}
