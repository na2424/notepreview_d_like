﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public enum PlayMode
{
	Normal = 0,
	Auto,
	Rehearsal,
	InputCheck
}

[Serializable]
public class PlayModePair
{
	public PlayMode PlayMode;
	public Sprite Sprite;
}

[RequireComponent(typeof(Image))]
public sealed class PlayModeController : MonoBehaviour
{
	[SerializeField] Image _playModeImage;
	[SerializeField] List<PlayModePair> _playStates;

	public void Init()
	{
		PlayMode playState = GameManager.Instance.PlayModeOption.PlayMode;

		if(playState == PlayMode.InputCheck)
		{
			_playModeImage.gameObject.SetActive(false);
		}
		else
		{
			_playModeImage.sprite = _playStates.Find(p => p.PlayMode == playState).Sprite;
			_playModeImage.gameObject.SetActive(true);
		}
	}
}
