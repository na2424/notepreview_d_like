﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class GameCameraController : MonoBehaviour
{
	[SerializeField] Camera _camera;
	[SerializeField] CameraSettings _horizontalCameraSettings;
	[SerializeField] CameraSettings _verticalCameraSettings;

	Transform _transform;

	const float THRESHOLD_ASPECT_RATIO = 1.5f;

	public Transform Transform => _transform;
	public Camera Camera => _camera;

	public void Init()
	{
		float aspectRatio = (float)Screen.width / (float)Screen.height;

		var cameraSetting = aspectRatio >= THRESHOLD_ASPECT_RATIO ?
			_horizontalCameraSettings :
			_verticalCameraSettings;

		_transform = transform;
		_transform.localPosition = cameraSetting.Position;
		_transform.localRotation = cameraSetting.Rotation;
		_camera.fieldOfView = cameraSetting.FieldOfView;

		_camera.enabled = true;
	}
}
