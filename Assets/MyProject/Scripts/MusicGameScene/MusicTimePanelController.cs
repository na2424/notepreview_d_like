﻿using UnityEngine;
using UnityEngine.UI;

public sealed class MusicTimePanelController : MonoBehaviour
{
	[SerializeField] Slider _musicTimeSlider;
	[SerializeField] Mask _mask;

	double _musicEndTime = double.MaxValue;
	bool _isMask = false;

	const float USE_MASK_THRESHOLD = 0.042f;

	public void Init(double musicEndTime)
	{
		_musicTimeSlider.value = 1f;
		_musicEndTime = musicEndTime;
		_mask.enabled = false;
	}

	public void CallUpdate(double musicTime)
	{
		var remain = Mathf.Clamp01(1f - (float)(musicTime / _musicEndTime));

		if (!_isMask && remain < USE_MASK_THRESHOLD)
		{
			_isMask = true;
			_mask.enabled = true;
		}

		_musicTimeSlider.SetValueWithoutNotify(remain);
	}
}
