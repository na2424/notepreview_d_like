﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 判定の描画を担当するクラス
/// </summary>
public sealed class JudgeUIController : MonoBehaviour
{
	[Header("判定")]
	[SerializeField] Image _image;
	[SerializeField] Sprite[] _judgeSprite = new Sprite[5];

	[Header("EARLY / LATE 表示")]
	[SerializeField] Image _earlyLateImage;
	[SerializeField] Sprite[] _earlyLateSprite = new Sprite[2];

	RectTransform _earlyLateTransform;

	float _judgeStartTime = 0f;
	float _earlyRateStartTime = 0f;
	bool _isChangeSize = false;
	bool _isEarlyRateChangeSize = false;

	bool _isShowEarlyLate = false;

	//readonly Vector3 EARLY_POSITION = new Vector3(-150f, -50f, 0f);
	//readonly Vector3 LATE_POSITION = new Vector3(150f, -50f, 0f);
	//readonly Vector3 EARLY_ROTATION = new Vector3(0f, 0f, 15f);
	//readonly Vector3 LATE_ROTATION = new Vector3(0f, 0f, -15f);

	const float DURATION = 0.5f;
	const float SIZE_TIME = 0.05f;
	const float MIN_SIZE = 0.7f;
	const float EARLY_LATE_MIN_SIZE = 0.8f;

	public void Init()
	{
		_image.sprite = null;
		_image.enabled = false;

		_isShowEarlyLate = false;
		_earlyLateImage.sprite = null;
		_earlyLateImage.enabled = false;
		_earlyLateTransform = _earlyLateImage.rectTransform;
	}

	public void CallUpdate()
	{
		if (_image.enabled && Time.realtimeSinceStartup - _judgeStartTime > DURATION)
		{
			_image.enabled = false;
		}

		if (_isChangeSize)
		{
			ChangeSize();
		}

		if (_isShowEarlyLate)
		{
			if (_earlyLateImage.enabled && Time.realtimeSinceStartup - _earlyRateStartTime > DURATION)
			{
				_earlyLateImage.enabled = false;
			}

			if (_isEarlyRateChangeSize)
			{
				ChangeSizeEarlyRate();
			}
		}
	}

	public void ShowJudge(JudgeType type)
	{
		_image.sprite = _judgeSprite[(int)type];
		_image.enabled = true;
		_isChangeSize = true;
		_judgeStartTime = Time.realtimeSinceStartup;
	}

	public void SetEarlyLate(EarlyLateDisplay disp)
	{
		if (disp == EarlyLateDisplay.None)
		{
			//_earlyLateImage.enabled = false;
			return;
		}

		_earlyRateStartTime = Time.realtimeSinceStartup;
		_isEarlyRateChangeSize = true;
		//_earlyLateTransform.localPosition = disp == EarlyLateDisplay.Early ? EARLY_POSITION : LATE_POSITION;
		//_earlyLateTransform.localRotation = Quaternion.Euler(disp == EarlyLateDisplay.Early ? EARLY_ROTATION : LATE_ROTATION);
		_earlyLateImage.sprite = disp == EarlyLateDisplay.Early ? _earlyLateSprite[0] : _earlyLateSprite[1];
		_earlyLateImage.enabled = true;
	}

	void ChangeSize()
	{
		float diffTime = Time.realtimeSinceStartup - _judgeStartTime;

		_image.rectTransform.localScale = Vector3.one * (Mathf.Clamp01(diffTime / SIZE_TIME) * (1f - MIN_SIZE) + MIN_SIZE);

		if (diffTime > SIZE_TIME)
		{
			_isChangeSize = false;
		}
	}

	void ChangeSizeEarlyRate()
	{
		float diffTime = Time.realtimeSinceStartup - _judgeStartTime;

		_earlyLateImage.rectTransform.localScale = Vector3.one * (Mathf.Clamp01(diffTime / SIZE_TIME) * (1f - EARLY_LATE_MIN_SIZE) + EARLY_LATE_MIN_SIZE);

		if (diffTime > SIZE_TIME)
		{
			_isEarlyRateChangeSize = false;
		}
	}
}
