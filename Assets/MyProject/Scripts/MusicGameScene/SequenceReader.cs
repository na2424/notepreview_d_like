﻿using Cysharp.Text;
using Cysharp.Threading.Tasks;
using Dialog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEngine.Pool;

public sealed class SequenceReader
{
	const int MAX_LANE = 7;
	const float TIME_SIGNATURE = 4f;
	const string ERROR_SPLIT_BPMS_TEXT = "SplitBPMSの記述に不備があります。";
	const string ERROR_SPLIT_SPEEDS_TEXT = "SplitSPEEDSの記述に不備があります。";
	const string ERROR_SPLIT_SCROLLS_TEXT = "SplitSCROLLSの記述に不備があります。";
	const string ERROR_SPLIT_CREDIT_TEXT = "SplitCREDITの記述に不備があります。";
	const string ERROR_LONG_INFO_TEXT = "{0}小節目のロングノーツの記述に不備があります。";
	const string ERROR_ATTACK_TEXT = "{0}小節目のアタックノーツの記述に不備があります。";
	const string ERROR_NO_NOTES = "ノーツが1つも読まれていません。\n譜面ファイルの仕様を確認してください。";

	const char VERSION_2_NONE = '0';
	const char VERSION_2_TAP = '1';
	const char VERSION_2_FUZZY = '2';

	const int VERSION_2_LONG_MAX = 10;

	readonly char[] VERSION_2_LONG_CHAR_START_RELAY =
	{
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'
	};

	readonly char[] VERSION_2_LONG_CHAR_END =
	{
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'
	};

	readonly char[] VERSION_2_FUZZY_LONG_CHAR_START_RELAY =
	{
		'Z', 'Y', 'X', 'W', 'V', 'U', 'T', 'S', 'R', 'Q'
	};

	readonly char[] VERSION_2_FUZZY_LONG_CHAR_END =
	{
		'z', 'y', 'x', 'w', 'v', 'u', 't', 's', 'r', 'q'
	};

	readonly string SPLIT_CREDIT_TAG = "SplitCREDIT:";
	readonly string SPLIT_CREDIT_SHORT_TAG = "CREDIT:";

	/// <summary>
	/// 譜面の非同期読み込み
	/// </summary>
	/// <param name="ct">キャンセルトークン</param>
	/// <param name="directoryPath">ディレクトリパス</param>
	/// <returns>譜面情報のUniTask</returns>
	public async UniTask<Sequence> ReadAsync(CancellationToken ct, SongInfo songInfo)
	{
		var text = await ReadFileText(ct, songInfo.DirectoryPath);

		if (songInfo.Version == 2)
		{
			return LoadSequenceV2(text, songInfo);
		}
		else
		{
			return LoadSequenceV1(text, songInfo);
		}
	}

	/// <summary>
	/// ディレクトリ内の.dlファイルからテキストを取得.
	/// テスト用に.txtも読み込めるようにしています
	/// </summary>
	/// <param name="ct">キャンセルトークン</param>
	/// <param name="directoryPath">ディレクトリパス</param>
	/// <returns>テキストのUniTask</returns>
	async UniTask<string> ReadFileText(CancellationToken ct, string directoryPath)
	{
		string[] dlFiles = null;
		var extensions = new string[] { "*.dl", "*.txt" };

		// .dlまたは.txtのファイルを検索
		foreach (string ext in extensions)
		{
			dlFiles = Directory.GetFiles(directoryPath, ext, System.IO.SearchOption.TopDirectoryOnly);

			if (dlFiles.Length > 0)
			{
				break;
			}
		}

		var path = Path.Combine(directoryPath, dlFiles[0]);
		string result = string.Empty;

		try
		{
			result = await File.ReadAllTextAsync(path, Encoding.UTF8, ct);
		}
		catch (OperationCanceledException e)
		{
			Debug.LogWarning("キャンセルされました: " + e.Message);
			return string.Empty;
		}

		return result;
	}

	/// <summary>
	/// テキストからシーケンス(譜面)データを読み込む.
	/// </summary>
	/// <param name="text">テキスト</param>
	/// <returns>譜面情報</returns>
	Sequence LoadSequenceV2(string text, SongInfo songInfo)
	{
		Sequence sequence = null;

		using (StringReader rs = new StringReader(text))
		{
			using (CollectionPool<List<string>, string>.Get(out var barText))
			{
				Queue<bool> isAttacks = new Queue<bool>();
				int noteReadState = 0;
				int bar = 0;
				int diffIndex = 0;

				// ロングの接続フラグ
				Span<bool> isLongConnects = stackalloc bool[VERSION_2_LONG_CHAR_START_RELAY.Length];
				Span<bool> isFuzzyLongConnects = stackalloc bool[VERSION_2_FUZZY_LONG_CHAR_START_RELAY.Length];

				var selectDifficulty = GameManager.Instance.SelectDifficulty;
				string difficulty = ZString.Concat(EnumUtility.DifficultyToString(selectDifficulty), ":");

				// ストリームの末端まで繰り返す
				while (rs.Peek() > -1)
				{
					// 一行読み込む
					var lineText = rs.ReadLine().Trim();

					if (lineText.Length == 0)
					{
						continue;
					}

					if (noteReadState == 0 && lineText.CustomStartsWith("#NOTES:"))
					{
						noteReadState = 1;
						continue;
					}

					if (noteReadState == 1 && lineText.CustomStartsWith("dankaglike:"))
					{
						noteReadState = 2;
						continue;
					}

					if (noteReadState == 2)
					{
						bool isCheck = false;

						if (lineText.CustomStartsWith(difficulty))
						{
							noteReadState = 3;
							sequence = new Sequence();
							diffIndex = (int)selectDifficulty;
							isCheck = true;
						}

						if (isCheck)
						{
							while (true)
							{
								bool isHitKeyWord = false;
								lineText = rs.ReadLine().Trim();

								// SplitBPMS
								if (lineText.CustomStartsWith("SplitBPMS"))
								{
									isHitKeyWord = true;

									while (!lineText.Contains("]") && rs.Peek() > -1)
									{
										lineText += rs.ReadLine().Trim();
									}

									var bpmData = lineText.GetBetweenChars('[', ']').Split(new char[] { '=', ',' }, StringSplitOptions.RemoveEmptyEntries);

									if (bpmData.Length >= 2)
									{
										songInfo.BpmPositions.Clear();
										songInfo.Bpms.Clear();
									}

									if (bpmData.Length % 2 != 0)
									{
										throw new Exception(ERROR_SPLIT_BPMS_TEXT);
									}

									for (int i = 0; i < bpmData.Length; i++)
									{
										if (i % 2 == 0)
										{
											if (float.TryParse(bpmData[i], NumberStyles.Float, CultureInfo.InvariantCulture, out var bpmPosition))
											{
												songInfo.BpmPositions.Add(bpmPosition);
											}
											else
											{
												throw CreateErrorWithValue(ERROR_SPLIT_BPMS_TEXT, bpmData[i]);
											}
										}
										else
										{
											if (float.TryParse(bpmData[i], NumberStyles.Float, CultureInfo.InvariantCulture, out var bpm))
											{
												songInfo.Bpms.Add(bpm);
											}
											else
											{
												throw CreateErrorWithValue(ERROR_SPLIT_BPMS_TEXT, bpmData[i]);
											}
										}
									}
								}

								// SplitSPEEDS
								if (lineText.CustomStartsWith("SplitSPEEDS"))
								{
									isHitKeyWord = true;

									songInfo.SpeedPositions.Clear();
									songInfo.SpeedStretchRatios.Clear();
									songInfo.SpeedDelayBeats.Clear();

									while (!lineText.Contains("]") && rs.Peek() > -1)
									{
										lineText += rs.ReadLine().Trim();
									}

									var speedData = lineText.GetBetweenChars('[', ']').Split(new char[] { '=', ',' }, StringSplitOptions.RemoveEmptyEntries);

									if (speedData.Length % 3 != 0)
									{
										throw new Exception(ERROR_SPLIT_SPEEDS_TEXT);
									}

									for (int i = 0; i < speedData.Length; i++)
									{
										if (i % 3 == 0)
										{
											if (float.TryParse(speedData[i], NumberStyles.Float, CultureInfo.InvariantCulture, out var speedPosition))
											{
												songInfo.SpeedPositions.Add(speedPosition);
											}
											else
											{
												throw CreateErrorWithValue(ERROR_SPLIT_SPEEDS_TEXT, speedData[i]);
											}
										}
										else if (i % 3 == 1)
										{
											if (float.TryParse(speedData[i], NumberStyles.Float, CultureInfo.InvariantCulture, out var speedStretchRatio))
											{
												songInfo.SpeedStretchRatios.Add(speedStretchRatio);
											}
											else
											{
												throw CreateErrorWithValue(ERROR_SPLIT_SPEEDS_TEXT, speedData[i]);
											}
										}
										else if (i % 3 == 2)
										{
											if (float.TryParse(speedData[i], NumberStyles.Float, CultureInfo.InvariantCulture, out var speedDelayBeat))
											{
												songInfo.SpeedDelayBeats.Add(speedDelayBeat);
											}
											else
											{
												throw CreateErrorWithValue(ERROR_SPLIT_SPEEDS_TEXT, speedData[i]);
											}
										}
									}
								}

								// SplitSCROLLS
								if (lineText.CustomStartsWith("SplitSCROLLS"))
								{
									isHitKeyWord = true;

									songInfo.ScrollPositions.Clear();
									songInfo.Scrolls.Clear();

									while (!lineText.Contains("]") && rs.Peek() > -1)
									{
										lineText += rs.ReadLine().Trim();
									}

									var scrollData = lineText.GetBetweenChars('[', ']').Split(new char[] { '=', ',' }, StringSplitOptions.RemoveEmptyEntries);

									if (scrollData.Length % 2 != 0)
									{
										throw new Exception(ERROR_SPLIT_SCROLLS_TEXT);
									}

									for (int i = 0; i < scrollData.Length; i++)
									{
										if (i % 2 == 0)
										{
											if (float.TryParse(scrollData[i], NumberStyles.Float, CultureInfo.InvariantCulture, out var scrollPosition))
											{
												songInfo.ScrollPositions.Add(scrollPosition);
											}
											else
											{
												throw CreateErrorWithValue(ERROR_SPLIT_SCROLLS_TEXT, scrollData[i]);
											}
										}
										else if (i % 2 == 1)
										{
											if (float.TryParse(scrollData[i], NumberStyles.Float, CultureInfo.InvariantCulture, out var scroll))
											{
												songInfo.Scrolls.Add(scroll);
											}
											else
											{
												throw CreateErrorWithValue(ERROR_SPLIT_SCROLLS_TEXT, scrollData[i]);
											}
										}
									}
								}

								// SplitCREDIT
								if (lineText.CustomStartsWith(SPLIT_CREDIT_TAG) || lineText.CustomStartsWith(SPLIT_CREDIT_SHORT_TAG))
								{
									isHitKeyWord = true;
									int start = lineText.CustomStartsWith("Split") ? SPLIT_CREDIT_TAG.Length : SPLIT_CREDIT_SHORT_TAG.Length;
									string credit = lineText.Substring(start, lineText.Length - start).Trim();
									sequence.Credit = credit;
								}

								if (lineText.Length == 0)
								{
									isHitKeyWord = true;
								}

								if (!isHitKeyWord)
								{
									break;
								}
							}
						}
					}

					if (noteReadState == 3)
					{
						if (lineText.CustomStartsWith(",") || lineText.CustomStartsWith(";"))
						{
							isAttacks.Clear();
							int count = barText.Count;
							for (var i = 0; i < count; i++)
							{
								// アタックノーツ情報がある時
								bool hasAttack = MAX_LANE < barText[i].Length && barText[i][MAX_LANE] == '-';

								if (hasAttack)
								{
									for (int j = MAX_LANE; j < barText[i].Length; j++)
									{
										if (barText[i][j] == '1')
										{
											isAttacks.Enqueue(true);
										}

										if (barText[i][j] == '0')
										{
											isAttacks.Enqueue(false);
										}
									}
								}


								for (int j = 0; j < barText[i].Length; j++)
								{
									if (j >= MAX_LANE)
									{
										break;
									}

									char c = barText[i][j];

									// タップノート
									if (c == '1')
									{
										sequence.BeatPositions.Add((bar * TIME_SIGNATURE) + (i * TIME_SIGNATURE / count));
										sequence.Lanes.Add(j);
										sequence.NoteTypes.Add(NoteType.Normal);
										sequence.Connects.Add(0);
										sequence.IsAttacks.Add(IsAttack());
										continue;
									}

									// 単ファジーノート
									if (c == '2')
									{
										sequence.BeatPositions.Add((bar * TIME_SIGNATURE) + (i * TIME_SIGNATURE / count));
										sequence.Lanes.Add(j);
										sequence.NoteTypes.Add(NoteType.Fuzzy);
										sequence.Connects.Add(0);
										sequence.IsAttacks.Add(IsAttack());
										continue;
									}

									//--------------------------------------------
									// ロング[青]
									//
									// 大文字で開始と中継、小文字で終端を表し、
									// 各アルファベットで接続を認識する
									//
									// (A|B|C|D|E|F|G|H|I|J) → 開始
									// (A|B|C|D|E|F|G|H|I|J) → 中継
									// (a|b|c|d|e|f|g|h|i|j) → 終端
									//--------------------------------------------

									bool hasLong = false;

									for (byte n = 0; n < VERSION_2_LONG_CHAR_START_RELAY.Length; n++)
									{
										if (c == VERSION_2_LONG_CHAR_START_RELAY[n])
										{
											hasLong = true;
											sequence.BeatPositions.Add((bar * TIME_SIGNATURE) + (i * TIME_SIGNATURE / count));
											sequence.Lanes.Add(j);
											sequence.Connects.Add(n);
											sequence.IsAttacks.Add(IsAttack());

											if (!isLongConnects[n])
											{
												// ロング開始
												isLongConnects[n] = true;
												sequence.NoteTypes.Add(NoteType.LongStart);
											}
											else
											{
												// ロング中継
												sequence.NoteTypes.Add(NoteType.LongRelay);
											}

											break;
										}
									}

									if (hasLong)
									{
										continue;
									}

									for (byte n = 0; n < VERSION_2_LONG_CHAR_END.Length; n++)
									{
										if (c == VERSION_2_LONG_CHAR_END[n])
										{
											// ロング終端
											hasLong = true;
											isLongConnects[n] = false;

											sequence.BeatPositions.Add((bar * TIME_SIGNATURE) + (i * TIME_SIGNATURE / count));
											sequence.Lanes.Add(j);
											sequence.NoteTypes.Add(NoteType.LongEnd);
											sequence.Connects.Add(n);
											sequence.IsAttacks.Add(IsAttack());

											break;
										}
									}

									if (hasLong)
									{
										continue;
									}

									//--------------------------------------------
									// ファジーロング[緑]
									//
									// 大文字で開始と中継、小文字で終端を表し、
									// 各アルファベットで接続を認識する
									//
									// (Z|Y|X|W|V|U|T|S|R|Q) → 開始
									// (Z|Y|X|W|V|U|T|S|R|Q) → 中継
									// (z|y|x|w|v|u|t|s|r|q) → 終端
									//--------------------------------------------
									bool hasFuzzyLong = false;

									for (byte n = 0; n < VERSION_2_FUZZY_LONG_CHAR_START_RELAY.Length; n++)
									{
										if (c == VERSION_2_FUZZY_LONG_CHAR_START_RELAY[n])
										{
											hasFuzzyLong = true;

											sequence.BeatPositions.Add((bar * TIME_SIGNATURE) + (i * TIME_SIGNATURE / count));
											sequence.Lanes.Add(j);
											sequence.Connects.Add(n);
											sequence.IsAttacks.Add(IsAttack());

											if (!isFuzzyLongConnects[n])
											{
												// ファジーロング開始
												isFuzzyLongConnects[n] = true;
												sequence.NoteTypes.Add(NoteType.FuzzyLongStart);
											}
											else
											{
												// ファジーロング中継
												sequence.NoteTypes.Add(NoteType.FuzzyLongRelay);
											}

											break;
										}
									}

									if (hasFuzzyLong)
									{
										continue;
									}

									for (byte n = 0; n < VERSION_2_FUZZY_LONG_CHAR_END.Length; n++)
									{
										if (c == VERSION_2_FUZZY_LONG_CHAR_END[n])
										{
											// ファジーロング終端
											hasFuzzyLong = true;
											isFuzzyLongConnects[n] = false;

											sequence.BeatPositions.Add((bar * TIME_SIGNATURE) + (i * TIME_SIGNATURE / count));
											sequence.Lanes.Add(j);
											sequence.NoteTypes.Add(NoteType.FuzzyLongEnd);
											sequence.Connects.Add(n);
											sequence.IsAttacks.Add(IsAttack());

											break;
										}
									}

									if (hasFuzzyLong)
									{
										continue;
									}
								}

								if (hasAttack)
								{
									isAttacks.Clear();
								}

								bool IsAttack()
								{
									return hasAttack
										? isAttacks.Count > 0
											? isAttacks.Dequeue()
											: false
										: false;
								}
							}

							if (sequence.IsAttacks.Count != sequence.Lanes.Count)
							{
								throw new Exception(ZString.Format(ERROR_ATTACK_TEXT, bar));
							}

							barText.Clear();
							bar++;
						}
						else
						{
							barText.Add(lineText);
						}

						if (lineText.CustomStartsWith(";"))
						{
							break;
						}
					}
				}
			}
		}

		sequence.LongInfo = CreateLongInfos(sequence, songInfo);

		CheckExistNote(sequence);
		CheckCorrectLongInfo(sequence.LongInfo);

		return sequence;
	}

	/// <summary>
	/// テキストからシーケンス(譜面)データを読み込む.
	/// </summary>
	/// <param name="text">テキスト</param>
	/// <returns>譜面情報</returns>
	Sequence LoadSequenceV1(string text, SongInfo songInfo)
	{
		Sequence sequence = new Sequence();
		int noteReadState = 0;
		int bar = 0;

		Queue<bool> isAttacks = new Queue<bool>();

		string difficulty = ZString.Concat(EnumUtility.DifficultyToString(GameManager.Instance.SelectDifficulty), ":");

		using (StringReader rs = new StringReader(text))
		{
			using (CollectionPool<List<string>, string>.Get(out var barText))
			{
				// ストリームの末端まで繰り返す
				while (rs.Peek() > -1)
				{
					// 一行読み込む
					var lineText = rs.ReadLine().Trim();

					if (noteReadState == 0 && lineText.CustomStartsWith("#NOTES:"))
					{
						noteReadState = 1;
						continue;
					}

					if (noteReadState == 1 && lineText.CustomStartsWith("dankaglike:"))
					{
						noteReadState = 2;
						continue;
					}

					if (noteReadState == 2 && lineText.CustomStartsWith(difficulty))
					{
						noteReadState = 3;

						// SplitBPMS
						lineText = rs.ReadLine().Trim();

						if (lineText.CustomStartsWith("SplitBPMS"))
						{
							var bpmData = lineText.GetBetweenChars('[', ']').Split(new char[] { '=', ',' }, StringSplitOptions.RemoveEmptyEntries);

							if (bpmData.Length >= 2)
							{
								songInfo.BpmPositions.Clear();
								songInfo.Bpms.Clear();
							}

							if (bpmData.Length % 2 != 0)
							{
								throw new Exception(ERROR_SPLIT_BPMS_TEXT);
							}

							for (int i = 0; i < bpmData.Length; i++)
							{
								if (i % 2 == 0)
								{
									if (float.TryParse(bpmData[i], NumberStyles.Float, CultureInfo.InvariantCulture, out var bpmPosition))
									{
										songInfo.BpmPositions.Add(bpmPosition);
									}
									else
									{
										throw CreateErrorWithValue(ERROR_SPLIT_BPMS_TEXT, bpmData[i]);
									}
								}
								else
								{
									if (float.TryParse(bpmData[i], NumberStyles.Float, CultureInfo.InvariantCulture, out var bpm))
									{
										songInfo.Bpms.Add(bpm);
									}
									else
									{
										throw CreateErrorWithValue(ERROR_SPLIT_BPMS_TEXT, bpmData[i]);
									}
								}
							}
						}

						// SplitSPEEDS
						lineText = rs.ReadLine().Trim();

						if (lineText.CustomStartsWith("SplitSPEEDS"))
						{
							songInfo.SpeedPositions.Clear();
							songInfo.SpeedStretchRatios.Clear();
							songInfo.SpeedDelayBeats.Clear();

							var speedData = lineText.GetBetweenChars('[', ']').Split(new char[] { '=', ',' }, StringSplitOptions.RemoveEmptyEntries);

							if (speedData.Length % 3 != 0)
							{
								throw new Exception(ERROR_SPLIT_SPEEDS_TEXT);
							}

							for (int i = 0; i < speedData.Length; i++)
							{
								if (i % 3 == 0)
								{
									if (float.TryParse(speedData[i], NumberStyles.Float, CultureInfo.InvariantCulture, out var speedPosition))
									{
										songInfo.SpeedPositions.Add(speedPosition);
									}
									else
									{
										throw CreateErrorWithValue(ERROR_SPLIT_SPEEDS_TEXT, speedData[i]);
									}
								}
								else if (i % 3 == 1)
								{
									if (float.TryParse(speedData[i], NumberStyles.Float, CultureInfo.InvariantCulture, out var speedStretchRatio))
									{
										songInfo.SpeedStretchRatios.Add(speedStretchRatio);
									}
									else
									{
										throw CreateErrorWithValue(ERROR_SPLIT_SPEEDS_TEXT, speedData[i]);
									}
								}
								else if (i % 3 == 2)
								{
									if (float.TryParse(speedData[i], NumberStyles.Float, CultureInfo.InvariantCulture, out var speedDelayBeat))
									{
										songInfo.SpeedDelayBeats.Add(speedDelayBeat);
									}
									else
									{
										throw CreateErrorWithValue(ERROR_SPLIT_SPEEDS_TEXT, speedData[i]);
									}
								}
							}
						}
						continue;
					}

					if (noteReadState == 3)
					{
						if (lineText.CustomStartsWith(",") || lineText.CustomStartsWith(";"))
						{
							isAttacks.Clear();
							var count = barText.Count;
							for (var i = 0; i < count; i++)
							{
								// アタックノーツ情報がある時
								bool hasAttack = MAX_LANE < barText[i].Length && barText[i][MAX_LANE] == '-';

								if (hasAttack)
								{
									for (int j = MAX_LANE; j < barText[i].Length; j++)
									{
										if (barText[i][j] == '1')
										{
											isAttacks.Enqueue(true);
										}

										if (barText[i][j] == '0')
										{
											isAttacks.Enqueue(false);
										}
									}
								}

								for (int j = 0; j < barText[i].Length; j++)
								{
									if (j >= MAX_LANE)
									{
										break;
									}

									char c = barText[i][j];

									// タップノート
									if (c == '1')
									{
										sequence.BeatPositions.Add((bar * TIME_SIGNATURE) + (i * TIME_SIGNATURE / count));
										sequence.Lanes.Add(j);
										sequence.NoteTypes.Add(NoteType.Normal);
										sequence.Connects.Add(0);
										sequence.IsAttacks.Add(IsAttack());
										continue;
									}

									// 単ファジーノート
									if (c == 'F' || c == 'M')
									{
										sequence.BeatPositions.Add((bar * TIME_SIGNATURE) + (i * TIME_SIGNATURE / count));
										sequence.Lanes.Add(j);
										sequence.NoteTypes.Add(NoteType.Fuzzy);
										sequence.Connects.Add(0);
										sequence.IsAttacks.Add(IsAttack());
										continue;
									}

									//--------------------------------------------
									// ロング[青]
									//
									// 大文字と小文字で別々の繋ぎとして認識する
									//
									// (A|a) → 開始
									// (B|b) → 中継
									// (C|c) → 終端
									//--------------------------------------------
									if (c == 'A' || c == 'a')
									{
										sequence.BeatPositions.Add((bar * TIME_SIGNATURE) + (i * TIME_SIGNATURE / count));
										sequence.Lanes.Add(j);
										sequence.NoteTypes.Add(NoteType.LongStart);
										sequence.Connects.Add((byte)(c == 'a' ? 1 : 0));
										sequence.IsAttacks.Add(IsAttack());
										continue;
									}

									if (c == 'B' || c == 'b')
									{
										sequence.BeatPositions.Add((bar * TIME_SIGNATURE) + (i * TIME_SIGNATURE / count));
										sequence.Lanes.Add(j);
										sequence.NoteTypes.Add(NoteType.LongRelay);
										sequence.Connects.Add((byte)(c == 'b' ? 1 : 0));
										sequence.IsAttacks.Add(IsAttack());
										continue;
									}

									if (c == 'C' || c == 'c')
									{
										sequence.BeatPositions.Add((bar * TIME_SIGNATURE) + (i * TIME_SIGNATURE / count));
										sequence.Lanes.Add(j);
										sequence.NoteTypes.Add(NoteType.LongEnd);
										sequence.Connects.Add((byte)(c == 'c' ? 1 : 0));
										sequence.IsAttacks.Add(IsAttack());
										continue;
									}

									//--------------------------------------------
									// ファジーロング[緑]
									//
									// 大文字と小文字で別々の繋ぎとして認識する
									//
									// (X|x) → 開始
									// (Y|y) → 中継
									// (Z|z) → 終端
									//--------------------------------------------
									if (c == 'X' || c == 'x')
									{
										sequence.BeatPositions.Add((bar * TIME_SIGNATURE) + (i * TIME_SIGNATURE / count));
										sequence.Lanes.Add(j);
										sequence.NoteTypes.Add(NoteType.FuzzyLongStart);
										sequence.Connects.Add((byte)(c == 'x' ? 1 : 0));
										sequence.IsAttacks.Add(IsAttack());
										continue;
									}

									if (c == 'Y' || c == 'y')
									{
										sequence.BeatPositions.Add((bar * TIME_SIGNATURE) + (i * TIME_SIGNATURE / count));
										sequence.Lanes.Add(j);
										sequence.NoteTypes.Add(NoteType.FuzzyLongRelay);
										sequence.Connects.Add((byte)(c == 'y' ? 1 : 0));
										sequence.IsAttacks.Add(IsAttack());
										continue;
									}

									if (c == 'Z' || c == 'z')
									{
										sequence.BeatPositions.Add((bar * TIME_SIGNATURE) + (i * TIME_SIGNATURE / count));
										sequence.Lanes.Add(j);
										sequence.NoteTypes.Add(NoteType.FuzzyLongEnd);
										sequence.Connects.Add((byte)(c == 'z' ? 1 : 0));
										sequence.IsAttacks.Add(IsAttack());
										continue;
									}
								}

								if (hasAttack)
								{
									isAttacks.Clear();
								}

								bool IsAttack()
								{
									return hasAttack
										? isAttacks.Count > 0
											? isAttacks.Dequeue()
											: false
										: false;
								}
							}

							if (sequence.IsAttacks.Count != sequence.Lanes.Count)
							{
								throw new Exception(ZString.Format(ERROR_ATTACK_TEXT, bar));
							}

							barText.Clear();
							bar++;
						}
						else
						{
							barText.Add(lineText);
						}

						if (lineText.CustomStartsWith(";"))
						{
							break;
						}
					}
				}
			}
		}

		sequence.LongInfo = CreateLongInfos(sequence, songInfo);

		CheckExistNote(sequence);
		CheckCorrectLongInfo(sequence.LongInfo);

		return sequence;
	}

	/// <summary>
	/// 値付きのエラーを返す
	/// </summary>
	Exception CreateErrorWithValue(string message, string value)
	{
		return new Exception(ZString.Concat(message, '"', value, '"'));
	}

	/// <summary>
	/// LongInfoのリストを生成
	/// </summary>
	/// <param name="sequence"></param>
	/// <param name="songInfo"></param>
	/// <returns></returns>
	List<LongInfo> CreateLongInfos(Sequence sequence, SongInfo songInfo)
	{
		var bpmHelper = new BpmHelper();
		bpmHelper.Init(songInfo);

		var longInfos = new List<LongInfo>();

		for (int index = 0; index < sequence.BeatPositions.Count; index++)
		{
			if ((sequence.NoteTypes[index] != NoteType.LongStart &&
				sequence.NoteTypes[index] != NoteType.FuzzyLongStart) ||
				index >= sequence.BeatPositions.Count)
			{
				continue;
			}

			LongInfo longInfo = new LongInfo()
			{
				NoteIndex = new List<int>(),
				BeatPositions = new List<double>(),
				Lanes = new List<int>(),
				LongType = sequence.NoteTypes[index] == NoteType.LongStart ? LongType.Long : LongType.FuzzyLong
			};

			// ロングの開始ノートを取得
			longInfo.BeatPositions.Add(sequence.BeatPositions[index]);
			longInfo.Lanes.Add(sequence.Lanes[index]);
			longInfo.StartNoteIndex = index;

			// ロングの中継と終了のノートを取得
			for (int i = index + 1; i < sequence.BeatPositions.Count; i++)
			{
				LongType longType = LongType.Long;

				if (sequence.NoteTypes[i] == NoteType.LongEnd ||
					sequence.NoteTypes[i] == NoteType.LongRelay)
				{
					longType = LongType.Long;
				}

				if (sequence.NoteTypes[i] == NoteType.FuzzyLongEnd ||
					sequence.NoteTypes[i] == NoteType.FuzzyLongRelay)
				{
					longType = LongType.FuzzyLong;
				}

				if (
					// LongTypeとConnectsで別々の接続として認識する
					(longType != longInfo.LongType || sequence.Connects[i] != sequence.Connects[index]) ||
					sequence.BeatPositions[i] == sequence.BeatPositions[index])
				{
					continue;
				}

				if (sequence.NoteTypes[i] == NoteType.LongEnd || sequence.NoteTypes[i] == NoteType.LongRelay ||
					sequence.NoteTypes[i] == NoteType.FuzzyLongEnd || sequence.NoteTypes[i] == NoteType.FuzzyLongRelay)
				{
					longInfo.NoteIndex.Add(i);
					longInfo.BeatPositions.Add(sequence.BeatPositions[i]);
					longInfo.Lanes.Add(sequence.Lanes[i]);
				}

				if (sequence.NoteTypes[i] == NoteType.LongEnd || sequence.NoteTypes[i] == NoteType.FuzzyLongEnd)
				{
					longInfo.EndTime = bpmHelper.BeatToTime(sequence.BeatPositions[i]);
					break;
				}
			}

			longInfos.Add(longInfo);
		}

		return longInfos;
	}

	void CheckExistNote(Sequence sequence)
	{
		if (sequence == null ||
			sequence.BeatPositions == null ||
			sequence.BeatPositions.Count == 0)
		{
			throw new Exception(ERROR_NO_NOTES);
		}
	}

	void CheckCorrectLongInfo(List<LongInfo> longInfos)
	{
		int count = longInfos.Count;
		for (int i = 0; i < count; i++)
		{
			if (longInfos[i].BeatPositions.Count <= 1)
			{
				if (longInfos[i].BeatPositions.Count == 1)
				{
					int bar = (int)(longInfos[i].BeatPositions[0] / TIME_SIGNATURE);
					throw new Exception(ZString.Format(ERROR_LONG_INFO_TEXT, bar));
				}
				throw new Exception(ZString.Format(ERROR_LONG_INFO_TEXT, "?"));
			}
		}
	}
}
