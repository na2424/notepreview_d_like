﻿using System.Collections.Generic;
using UnityEngine;

public interface IVisibleHold
{
	void SetActive(bool isActive, int touchId, int startNoteIndex, NoteType noteType = NoteType.None, bool isAttack = false);
}

/// <summary>
/// ホールド中の判定ライン上のホールド位置を表示するクラス
/// </summary>
public sealed class VisibleHoldController : MonoBehaviour, IVisibleHold
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] GameObject _visibleHoldPrefab;
	[SerializeField] NoteTexture _noteTexture;

	//------------------
	// キャッシュ.
	//------------------
	int _index = 0;
	MaterialPropertyBlock[] _mpbs;
	Transform[] _visibleHoldTransforms;
	ParticleSystem[] _visibleHoldParticles;
	MeshRenderer[] _meshRenderers;
	MeshRenderer[] _attackMeshRenderers;
	bool[] _isRelay;
	Dictionary<long, int> _hashToIndex;
	HashSet<int> _activeIndex;
	Vector3 _xOne = Vector3.right;
	bool _useEffect = false;

	Texture _luaLongStartTexture = null;
	Texture _luaLongRelayTexture = null;
	Texture _luaFuzzyLongStartTexture = null;
	Texture _luaFuzzyLongRelayTexture = null;

	//------------------
	// プロパティ.
	//------------------
	bool HasLongStartTexture => _luaLongStartTexture != null;
	bool HasLongRelayTexture => _luaLongRelayTexture != null;
	bool HasFuzzyLongStartTexture => _luaFuzzyLongStartTexture != null;
	bool HasFuzzyLongRelayTexture => _luaFuzzyLongRelayTexture != null;

	/// <summary>
	/// 初期化処理
	/// </summary>
	public void Init(NotesEffectType notesEffectType, int length)
	{
		_visibleHoldTransforms = new Transform[length];
		_visibleHoldParticles = new ParticleSystem[length * 2];

		for (int i = 0; i < length; i++)
		{
			var visibleHoldObj = Instantiate(_visibleHoldPrefab, transform);
			_visibleHoldTransforms[i] = visibleHoldObj.transform;
			_visibleHoldParticles[i * 2] = _visibleHoldTransforms[i].GetChild(0).GetComponent<ParticleSystem>();
			_visibleHoldParticles[i * 2 + 1] = _visibleHoldTransforms[i].GetChild(0).GetChild(0).GetComponent<ParticleSystem>();
		}

		_mpbs = new MaterialPropertyBlock[length];
		_meshRenderers = new MeshRenderer[length];
		_attackMeshRenderers = new MeshRenderer[length];
		_isRelay = new bool[length];
		_hashToIndex = new Dictionary<long, int>(length);
		_activeIndex = new HashSet<int>(length);

		float size = GameManager.Instance.NotesOption.Size;

		for (int i = 0; i < _visibleHoldTransforms.Length; i++)
		{
			_visibleHoldTransforms[i].localScale *= size;
			_mpbs[i] = new MaterialPropertyBlock();
			_meshRenderers[i] = _visibleHoldTransforms[i].GetComponent<MeshRenderer>();
			_attackMeshRenderers[i] = _visibleHoldTransforms[i].GetChild(1).GetComponent<MeshRenderer>();
		}

		_useEffect = (int)notesEffectType >= (int)NotesEffectType.Type2;

		for (int i = 0; i < _visibleHoldParticles.Length; i += 2)
		{
			_visibleHoldParticles[i].transform.localScale *= size;
		}
	}

	/// <summary>
	/// 全てのアクティブをリセット(非表示)にする
	/// </summary>
	public void ResetAllActive()
	{
		for (int i = 0; i < _meshRenderers.Length; i++)
		{
			_meshRenderers[i].enabled = false;
		}

		// パーティクル
		if (_useEffect)
		{
			for (int i = 0; i < _visibleHoldParticles.Length; i++)
			{
				if (_visibleHoldParticles[i].isPlaying)
				{
					_visibleHoldParticles[i].Stop();
					_visibleHoldParticles[i].Clear();
				}
			}
		}

		_hashToIndex.Clear();
		_activeIndex.Clear();
	}

	/// <summary>
	/// 表示/非表示切り替え
	/// </summary>
	/// <param name="isActive">アクティブか</param>
	/// <param name="touchId">タッチID</param>
	/// <param name="startNoteIndex">ロング開始ノーツのIndex</param>
	/// <param name="noteType">ノートの種類</param>
	/// <param name="isAttack">アタックか</param>
	public void SetActive(bool isActive, int touchId, int startNoteIndex, NoteType noteType = NoteType.None, bool isAttack = false)
	{
		long hash = GetHash(touchId, startNoteIndex);

		if (isActive)
		{
			NextIndex();

			_hashToIndex.Add(hash, _index);

			int index = _index;

			//Debug.Log("SetActive:" + touchId + "," + startNoteIndex + "," + noteType);

			// マテリアルのインスタンスを避ける
			_mpbs[index].SetTexture(Constant.ShaderProperty.MainTexId, HasChangeHoldTexture(noteType) ? NoteTypeToTexture(noteType) : _noteTexture.NoteTypeToTexture(noteType));
			_meshRenderers[index].SetPropertyBlock(_mpbs[index]);
			_meshRenderers[index].enabled = true;

			if (_attackMeshRenderers[index].enabled != isAttack)
			{
				_attackMeshRenderers[index].enabled = isAttack;
			}

			_isRelay[index] = false;

			_activeIndex.Add(index);

			// パーティクル
			if (_useEffect)
			{
				_visibleHoldParticles[index * 2].Play();
				_visibleHoldParticles[index * 2 + 1].Play();
			}
		}
		else
		{
			if (_hashToIndex.ContainsKey(hash))
			{
				int index = _hashToIndex[hash];

				_meshRenderers[index].enabled = false;
				if (_attackMeshRenderers[index].enabled != false)
				{
					_attackMeshRenderers[index].enabled = false;
				}
				_isRelay[index] = false;
				_hashToIndex.Remove(hash);
				_activeIndex.Remove(index);

				// パーティクル
				if (_useEffect)
				{
					//Debug.Log(index + ":" + _visibleHoldParticles[index * 2].isPlaying);
					_visibleHoldParticles[index * 2].Stop();
					_visibleHoldParticles[index * 2 + 1].Stop();
				}

			}
		}
	}

	/// <summary>
	/// ロング中継の判定があった時に呼ばれる
	/// </summary>
	/// <param name="touchId">タッチID</param>
	/// <param name="startNoteIndex">ロングの開始ノーツのIndex</param>
	/// <param name="noteType">ノーツの種類</param>
	public void OnHitRelay(int touchId, int startNoteIndex, NoteType noteType, bool isAttack)
	{
		long hash = GetHash(touchId, startNoteIndex);

		if (_hashToIndex.ContainsKey(hash))
		{
			int index = _hashToIndex[hash];

			//Debug.Log("OnHitRelay:" + touchId + "," +startNoteIndex + "," + noteType);

			if (_isRelay[index])
			{
				return;
			}

			_isRelay[index] = true;

			_mpbs[index].SetTexture(Constant.ShaderProperty.MainTexId, HasChangeHoldTexture(noteType) ? NoteTypeToTexture(noteType) : _noteTexture.NoteTypeToTexture(noteType));
			_meshRenderers[index].SetPropertyBlock(_mpbs[index]);

			if (_attackMeshRenderers[index].enabled != isAttack)
			{
				_attackMeshRenderers[index].enabled = isAttack;
			}

		}
	}

	/// <summary>
	/// ホールド位置の更新
	/// </summary>
	/// <param name="posX">X座標</param>
	/// <param name="touchId">タッチID</param>
	public void SetPosition(float posX, int touchId, int startNoteIndex)
	{
		long hash = GetHash(touchId, startNoteIndex);

		if (_hashToIndex.ContainsKey(hash))
		{
			_visibleHoldTransforms[_hashToIndex[hash]].localPosition = _xOne * posX;
		}
	}

	/// <summary>
	/// 次のIndex番号に更新
	/// </summary>
	void NextIndex(int loop = 0)
	{
		_index++;

		if (_index >= _visibleHoldTransforms.Length)
		{
			_index = 0;
		}

		// まだロング稼働中のIndexだった場合は次のIndexへスキップする
		if (_activeIndex.Contains(_index))
		{
			loop++;

			if (loop < _visibleHoldTransforms.Length)
			{
				NextIndex(loop);
			}
		}
	}

	/// <summary>
	/// ハッシュ値を取得
	/// </summary>
	/// <param name="touchId">タッチID</param>
	/// <param name="startNoteIndex">ロングの開始ノーツのIndex</param>
	/// <returns></returns>
	long GetHash(int touchId, int startNoteIndex)
	{
		long hash = 0;
		hash ^= touchId + 0x9e3779b9 + (hash << 6) + (hash >> 2);
		hash ^= startNoteIndex + 0x9e3779b9 + (hash << 6) + (hash >> 2);
		return hash;
	}

	/// <summary>
	/// ノーツエフェクトの種類を変更
	/// </summary>
	/// <param name="notesEffectType"></param>
	public void ChangeEffectType(NotesEffectType notesEffectType)
	{
		_useEffect = notesEffectType >= NotesEffectType.Type2;

		if (!_useEffect)
		{
			for (int i = 0; i < _visibleHoldParticles.Length; i++)
			{
				if (_visibleHoldParticles[i].isPlaying)
				{
					_visibleHoldParticles[i].Stop();
				}
			}
		}
	}

	/// <summary>
	/// ホールド中のテクスチャを変更
	/// </summary>
	/// <param name="noteType"></param>
	/// <param name="noteTexture"></param>
	public void ChangeHoldTexture(NoteType noteType, Texture noteTexture)
	{
		switch (noteType)
		{
			case NoteType.LongStart:
				_luaLongStartTexture = noteTexture;
				break;

			case NoteType.LongRelay:
				_luaLongRelayTexture = noteTexture;
				break;

			case NoteType.LongEnd:
				_luaLongStartTexture = noteTexture;
				break;

			case NoteType.FuzzyLongStart:
				_luaFuzzyLongStartTexture = noteTexture;
				break;

			case NoteType.FuzzyLongRelay:
				_luaFuzzyLongRelayTexture = noteTexture;
				break;

			case NoteType.FuzzyLongEnd:
				_luaFuzzyLongStartTexture = noteTexture;
				break;
		}
	}

	/// <summary>
	/// ホールド中のテクスチャを変更
	/// </summary>
	/// <param name="noteTypeInt"></param>
	/// <param name="noteTexture"></param>
	public void ChangeHoldTexture(int noteTypeInt, Texture noteTexture)
	{
		NoteType noteType = (NoteType)noteTypeInt;
		ChangeHoldTexture(noteType, noteTexture);
	}

	/// <summary>
	/// ホールド中のテクスチャを元に戻す
	/// </summary>
	public void ResetHoldTexture()
	{
		_luaLongStartTexture = null;
		_luaLongRelayTexture = null;
		_luaFuzzyLongStartTexture = null;
		_luaFuzzyLongRelayTexture = null;
	}

	bool HasChangeHoldTexture(NoteType noteType) => noteType switch
	{
		NoteType.Normal => false,
		NoteType.LongStart => HasLongStartTexture,
		NoteType.LongRelay => HasLongRelayTexture,
		NoteType.LongEnd => HasLongStartTexture,
		NoteType.Fuzzy => false,
		NoteType.FuzzyLongStart => HasFuzzyLongStartTexture,
		NoteType.FuzzyLongRelay => HasFuzzyLongRelayTexture,
		NoteType.FuzzyLongEnd => HasFuzzyLongStartTexture,
		_ => false
	};

	public Texture NoteTypeToTexture(NoteType type) => type switch
	{
		NoteType.Normal => null,
		NoteType.LongStart => _luaLongStartTexture,
		NoteType.LongRelay => _luaLongRelayTexture,
		NoteType.LongEnd => _luaLongStartTexture,
		NoteType.Fuzzy => null,
		NoteType.FuzzyLongStart => _luaFuzzyLongStartTexture,
		NoteType.FuzzyLongRelay => _luaFuzzyLongRelayTexture,
		NoteType.FuzzyLongEnd => _luaFuzzyLongStartTexture,
		_ => null
	};

	void OnDestroy()
	{
		if (_mpbs != null)
		{
			for (int i = 0; i < _mpbs.Length; i++)
			{
				_mpbs[i] = null;
			}

			_mpbs = null;
		}

		if (_hashToIndex != null)
		{
			_hashToIndex.Clear();
			_hashToIndex = null;
		}

		if (_activeIndex != null)
		{
			_activeIndex.Clear();
			_activeIndex = null;
		}

	}
}
