﻿using System;
using UnityEngine;
using XLua;

public enum JudgeType : int
{
	Brilliant = 0,
	Great,
	Fast,
	Slow,
	Bad,
	Missed,
	None,
}

public enum EarlyLateDisplay
{
	None,
	Early,
	Late
}

[Serializable]
public struct EarlyLate
{
	public int Early;
	public int Late;
}

[LuaCallCSharp]
[Serializable]
public struct Judge
{
	public int Brilliant;
	public int Great;
	public int Fast;
	public int Slow;
	public int Bad;
	public int Missed;
}

/// <summary>
/// 判定の時間とカウントを管理するクラス
/// </summary>
public sealed class JudgeManager : MonoBehaviour, ICallUpdate
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] JudgeUIController _judgeUIController;

	public event Action<JudgeType> OnJudge = null;
	public event Action<Judge> OnAfterJudge = null;

	//------------------
	// キャッシュ.
	//------------------
	Judge _judge;
	double _brilliantTime;
	double _greatTime;
	double _fastTime;
	double _badTime;
	double _hitTime;
	double _longHitTime;
	double _comboHitTime;
	double _upRevisionTime;
	float _longDistanceRevision;
	double _holdStartTime;
	bool _isFuzzyJudgeMitigation;
	float _judgeDistance;

	bool _isShowEarlyLateDisplay = false;
	float _earlyLateThreshold = 0.050f;
	double _musicRate = 1d;
	//bool _enableVibration = false;

	//------------------
	// 定数.
	//------------------

	//------------------
	// プロパティ.
	//------------------
	public Judge Judge => _judge;
	public bool IsAllBrilliant =>
		_judge.Great == 0 &&
		_judge.Fast == 0 &&
		_judge.Slow == 0 &&
		_judge.Bad == 0 &&
		_judge.Missed == 0;

	// 判定時間で一番長い時間を取得する
	public double HitTime => _hitTime;
	// ロング補正時間を加えた判定時間で一番長い時間を取得する
	public double LongHitTime => _longHitTime;

	// コンボが繋がる判定で一番長い時間を取得する
	public double ComboHitTime => _comboHitTime;

	// 押しっぱなし判定の判定開始はジャストタイミングより少し手前にする
	public double HoldStart => (_holdStartTime < -_brilliantTime) ? -_brilliantTime : _holdStartTime;

	public void Init()
	{
		_judgeUIController.Init();
		_judge = new Judge();

		_musicRate = GameManager.Instance.JudgeTimeOption.MusicRate;
		_brilliantTime = GameManager.Instance.JudgeTimeOption.BrilliantTime * _musicRate;
		_greatTime = GameManager.Instance.JudgeTimeOption.GreatTime * _musicRate;
		_fastTime = GameManager.Instance.JudgeTimeOption.FastTime * _musicRate;
		_badTime = GameManager.Instance.JudgeTimeOption.BadTime * _musicRate;
		_hitTime = GameManager.Instance.JudgeTimeOption.GetHitTime() * _musicRate;
		_longHitTime = GameManager.Instance.JudgeTimeOption.GetLongEndHitTime() * _musicRate;
		_comboHitTime = GameManager.Instance.JudgeTimeOption.GetComboHitTime() * _musicRate;
		_judgeDistance = GameManager.Instance.JudgeTimeOption.JudgeDistance;
		_upRevisionTime = GameManager.Instance.JudgeTimeOption.LongRevisionTime * _musicRate;
		_longDistanceRevision = GameManager.Instance.JudgeTimeOption.LongRevisionDistance;

		_holdStartTime = GameManager.Instance.JudgeTimeOption.FuzzyStartTime * _musicRate;
		_isShowEarlyLateDisplay = false;
		_earlyLateThreshold = 1;
		_isFuzzyJudgeMitigation = GameManager.Instance.JudgeTimeOption.FuzzyJudgeMitigation;
		//_enableVibration = GameManager.Instance.JudgeTimeOption.Vibration;
	}

	public void CallUpdate()
	{
		_judgeUIController.CallUpdate();
	}

	/// <summary>
	/// 判定エリア内か
	/// </summary>
	/// <param name="diffPosX">横の差分</param>
	/// <param name="diffTime">時間の差分</param>
	/// <param name="isRevision">ロング補正するか</param>
	/// <returns></returns>
	public bool IsHitArea(float diffPosX, double diffTime, bool isRevision)
	{
		double revision = isRevision ? _upRevisionTime : 0d;

		if (diffPosX < (_judgeDistance + _longDistanceRevision) && diffTime < (_greatTime + revision))
		{
			return true;
		}

		float area = Mathf.Clamp01((float)((diffTime - (_greatTime + revision)) / (_badTime - (_greatTime + revision))));

		return diffPosX < Mathf.Lerp((_judgeDistance + _longDistanceRevision), 0f, area);
	}

	/// <summary>
	/// 時間差分から各判定に分ける
	/// </summary>
	/// <param name="diffTime">時間差分</param>
	/// <param name="touchPhase">タッチ状態(タップ、ホールド、アップ)</param>
	/// <returns></returns>
	public JudgeType JudgeTiming(double diffTime, ScreenTouchPhase touchPhase, bool isFuzzy)
	{
		JudgeType judgeType = JudgeType.None;
		double revision = (touchPhase == ScreenTouchPhase.Up) ? _upRevisionTime : 0d;
		double absDiffTime = Math.Abs(diffTime);

		if (_isFuzzyJudgeMitigation && isFuzzy)
		{
			if (absDiffTime < _brilliantTime + revision)
			{
				judgeType = JudgeType.Brilliant;
				OnJudgeEvent(JudgeType.Brilliant);
			}
			else if (absDiffTime < _greatTime + revision)
			{
				judgeType = JudgeType.Brilliant;
				OnJudgeEvent(JudgeType.Brilliant);

			}
			else if (-_fastTime < diffTime && diffTime < 0)
			{
				judgeType = JudgeType.Great;
				OnJudgeEvent(JudgeType.Great);
			}
			else if (0 < diffTime && diffTime < _fastTime)
			{
				judgeType = JudgeType.Great;
				OnJudgeEvent(JudgeType.Great);
			}
			else if (absDiffTime < _badTime)
			{
				judgeType = JudgeType.Great;
				OnJudgeEvent(JudgeType.Great);
			}
		}
		else
		{
			if (absDiffTime < _brilliantTime + revision)
			{
				judgeType = JudgeType.Brilliant;
				OnJudgeEvent(JudgeType.Brilliant);
			}
			else if (absDiffTime < _greatTime + revision)
			{
				judgeType = JudgeType.Great;
				OnJudgeEvent(JudgeType.Great);
			}
			else if (-_fastTime < diffTime && diffTime < 0)
			{
				judgeType = JudgeType.Fast;
				OnJudgeEvent(JudgeType.Fast);
			}
			else if (0 < diffTime && diffTime < _fastTime)
			{
				judgeType = JudgeType.Slow;
				OnJudgeEvent(JudgeType.Slow);
			}
			else if (absDiffTime < _badTime)
			{
				judgeType = JudgeType.Bad;
				OnJudgeEvent(JudgeType.Bad);
			}
		}

		// Fast・Slow表示機能が有効の時に表示する
		if (_isShowEarlyLateDisplay && (judgeType == JudgeType.Brilliant || judgeType == JudgeType.Great))
		{
			EarlyLateDisplay fastSlowDisplay = EarlyLateDisplay.None;

			if (diffTime < -_earlyLateThreshold)
			{
				fastSlowDisplay = EarlyLateDisplay.Early;
			}

			if (_earlyLateThreshold < diffTime)
			{
				fastSlowDisplay = EarlyLateDisplay.Late;
			}

			_judgeUIController.SetEarlyLate(fastSlowDisplay);
		}

		return judgeType;
	}

	/// <summary>
	/// Missedの判定結果を追加
	/// </summary>
	/// <param name="type"></param>
	public void AddMissedJudge(JudgeType _ = JudgeType.Missed)
	{
		OnJudgeEvent(JudgeType.Missed);
		_judgeUIController.ShowJudge(JudgeType.Missed);
	}

	/// <summary>
	/// 判定時の処理.
	/// </summary>
	/// <param name="type">判定</param>
	void OnJudgeEvent(JudgeType type)
	{
		if (type != JudgeType.Missed)
		{
			OnJudge?.Invoke(type);
			_judgeUIController.ShowJudge(type);

			//if (_enableVibration)
			//{
			//	VibrationMng.ShortVibration();
			//}
		}

		AddJudge(type);

		OnAfterJudge?.Invoke(_judge);
	}

	/// <summary>
	/// 判定の記録に追加
	/// </summary>
	/// <param name="type">判定</param>
	void AddJudge(JudgeType type)
	{
		if (type == JudgeType.Brilliant)
		{
			_judge.Brilliant++;
			return;
		}
		if (type == JudgeType.Great)
		{
			_judge.Great++;
			return;
		}

		if (type == JudgeType.Fast) _judge.Fast++;
		if (type == JudgeType.Slow) _judge.Slow++;
		if (type == JudgeType.Bad) _judge.Bad++;
		if (type == JudgeType.Missed) _judge.Missed++;
	}

	public void SetDistance(float value)
	{
		_judgeDistance = value;
	}
}
