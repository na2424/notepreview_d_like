﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 同時押しラインを管理するクラス
/// </summary>
[System.Serializable]
public sealed class SameTimeBar
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] SameTimeBarObjectPool _objectPool;

	//------------------
	// キャッシュ.
	//------------------
	BpmHelper _bpmHelper;
	ScrollHelper _scrollHelper;
	Sequence _sequence;

	bool _isBeforeRelay = false;
	double _beforeNoteBeat = -1d;
	int _beforeNoteIndex = 0;
	Queue<double> _sameTimeBeatPosition = new Queue<double>();
	Queue<int>[] _sameTimeNoteLanes = new Queue<int>[2];
	Queue<int>[] _sameTimeNoteIndex = new Queue<int>[2];
	HashSet<int> _noCreateIndex = new HashSet<int>();

	//------------------
	// 定数.
	//------------------
	const int LEFT = 0;
	const int RIGHT = 1;

	public void Init(BpmHelper bpmHelper, ScrollHelper scrollHelper, Sequence sequence)
	{
		for (int i = 0; i < _sameTimeNoteLanes.Length; i++)
		{
			_sameTimeNoteLanes[i] = new Queue<int>();
			_sameTimeNoteIndex[i] = new Queue<int>();
		}

		_bpmHelper = bpmHelper;
		_scrollHelper = scrollHelper;
		_sequence = sequence;
		_objectPool.Init();
	}

	public void CallUpdate(double visualBeat, double musicTime, float speedStretchRatioValue)
	{
		// 同時押しライン生成
		int count = _sameTimeBeatPosition.Count;
		for (int i = count - 1; i >= 0; i--)
		{
			double beatPosition = _sameTimeBeatPosition.Dequeue();
			double scrolledPosition = _scrollHelper.ApplyScroll(beatPosition);

			int leftLane = _sameTimeNoteLanes[LEFT].Dequeue();
			int rightLane = _sameTimeNoteLanes[RIGHT].Dequeue();
			int leftIndex = _sameTimeNoteIndex[LEFT].Dequeue();
			int rightIndex = _sameTimeNoteIndex[RIGHT].Dequeue();

			bool isNoCreate = false;

			if (_noCreateIndex.Contains(leftIndex))
			{
				isNoCreate = true;
				_noCreateIndex.Remove(leftIndex);
			}

			if (_noCreateIndex.Contains(rightIndex))
			{
				isNoCreate = true;
				_noCreateIndex.Remove(rightIndex);
			}

			if (isNoCreate)
			{
				continue;
			}

			CreateSameTimeBarFromObjectPool(beatPosition, scrolledPosition, leftLane, rightLane, leftIndex, rightIndex);
		}

		// 拍線位置更新
		_objectPool.CallUpdate(visualBeat, musicTime, speedStretchRatioValue);
	}

	/// <summary>
	/// オブジェクトプールから同時押しライン生成
	/// </summary>
	void CreateSameTimeBarFromObjectPool(double beatPosition, double scrolledPosition, int leftLane, int rightLane, int leftIndex, int rightIndex)
	{
		var controller = _objectPool.Pool.Get();
		double justTime = _bpmHelper.BeatToTime(beatPosition);

		controller.SetParam(
			// MEMO: Scroll対応のため、beatPositionからscrolledPositionに変更している
			beatPosition: scrolledPosition,
			justTime: justTime,
			laneLeft: leftLane,
			laneRight: rightLane,
			leftNoteIndex: leftIndex,
			rightNoteIndex: rightIndex
		);
	}

	/// <summary>
	/// Note生成時のイベント
	/// </summary>
	/// <param name="noteIndex"></param>
	public void OnCreateNote(int noteIndex)
	{
		// 同時押しか判定する (ロングの接続同士は同時押しラインを生成しない)
		if (
			Calculate.FastApproximately(_sequence.BeatPositions[noteIndex], _beforeNoteBeat, 0.00001) &&
			!(_isBeforeRelay && (_sequence.NoteTypes[noteIndex] == NoteType.FuzzyLongRelay || _sequence.NoteTypes[noteIndex] == NoteType.LongRelay))
		)
		{
			// Left
			_sameTimeNoteLanes[LEFT].Enqueue(_sequence.Lanes[_beforeNoteIndex]);
			_sameTimeNoteIndex[LEFT].Enqueue(_beforeNoteIndex);
			// Right
			_sameTimeNoteLanes[RIGHT].Enqueue(_sequence.Lanes[noteIndex]);
			_sameTimeNoteIndex[RIGHT].Enqueue(noteIndex);
			// BeatPosition
			_sameTimeBeatPosition.Enqueue(_beforeNoteBeat);
		}
		else
		{
			_beforeNoteBeat = _sequence.BeatPositions[noteIndex];
			_beforeNoteIndex = noteIndex;
			_isBeforeRelay = _sequence.NoteTypes[noteIndex] == NoteType.FuzzyLongRelay || _sequence.NoteTypes[noteIndex] == NoteType.LongRelay;
		}
	}

	/// <summary>
	/// ロングのRelease時のイベント
	/// </summary>
	/// <param name="releaseLong"></param>
	public void OnReleaseLong(LongController releaseLong)
	{
		int loop = _objectPool.ListCount - 1;

		if (loop < 0)
		{
			return;
		}

		for (int i = loop; i >= 0; --i)
		{
			SameTimeBarController sameTimeBarController = _objectPool.CurrentActiveSameTimeBar[i];

			if (sameTimeBarController is null || !sameTimeBarController.gameObject.activeSelf)
			{
				continue;
			}

			foreach (var noteIndex in releaseLong.LongInfo.NoteIndex)
			{
				if (sameTimeBarController.LeftNoteIndex == noteIndex)
				{
					sameTimeBarController.Release();
				}

				if (sameTimeBarController.RightNoteIndex == noteIndex)
				{
					sameTimeBarController.Release();
				}
			}
		}
	}

	/// <summary>
	/// 判定時のイベント
	/// </summary>
	/// <param name="note"></param>
	public void OnJudgeTiming(NoteController note)
	{
		// 同時押しラインに含まれるノートの場合は同時押しラインをReleaseする
		int loop = _objectPool.ListCount - 1;

		if (loop < 0)
		{
			return;
		}

		for (int i = loop; i >= 0; --i)
		{
			if (i >= _objectPool.ListCount)
			{
				continue;
			}

			var sameTimeBar = _objectPool.CurrentActiveSameTimeBar[i];

			if (sameTimeBar.LeftNoteIndex == note.NoteIndex)
			{
				sameTimeBar.Release();
			}

			if (sameTimeBar.RightNoteIndex == note.NoteIndex)
			{
				sameTimeBar.Release();
			}
		}
	}

	public void SetNoCreateNoteIndex(int noteIndex)
	{
		if (!_noCreateIndex.Contains(noteIndex))
		{
			_noCreateIndex.Add(noteIndex);
		}
	}

	public void OnFinalize()
	{
		_sameTimeBeatPosition.Clear();
		_sameTimeBeatPosition = null;

		for (int i = 0; i < _sameTimeNoteLanes.Length; i++)
		{
			_sameTimeNoteLanes[i].Clear();
			_sameTimeNoteLanes[i] = null;
		}
		_sameTimeNoteLanes = null;

		for (int i = 0; i < _sameTimeNoteIndex.Length; i++)
		{
			_sameTimeNoteIndex[i].Clear();
			_sameTimeNoteIndex[i] = null;
		}
		_sameTimeNoteIndex = null;

		_objectPool.OnFinalize();
	}
}
