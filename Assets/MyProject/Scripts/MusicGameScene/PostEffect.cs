﻿using UnityEngine;
using System.Collections;
using XLua;
using System.Collections.Generic;

[LuaCallCSharp]
public class PostEffect : MonoBehaviour
{
	public Material _effectMaterial;

	public List<Material> _effectMaterials = new List<Material>();

	[DoNotGen]
	public void Init()
	{
		enabled = false;
	}

	void OnRenderImage(RenderTexture src, RenderTexture dest)
	{
		if (_effectMaterial != null)
		{
			Graphics.Blit(src, dest, _effectMaterial);
		}

		if (_effectMaterials != null && _effectMaterials.Count > 0)
		{
			foreach (var mat in _effectMaterials)
			{
				Graphics.Blit(src, dest, mat);
			}
		}
	}

	/// <summary>
	/// カメラにポストエフェクト(カメラエフェクト)を設定します
	/// </summary>
	/// <param name="material">ポストエフェクトShaderの付いたMaterial</param>
	public void SetMaterial(Material material)
	{
		_effectMaterial = material;
	}

	/// <summary>
	/// ポストエフェクトを追加します
	/// ポストエフェクトを複数掛けたいときに使用します
	/// </summary>
	/// <param name="material"></param>
	public void AddMaterial(Material material)
	{
		_effectMaterials.Add(material);
	}

	/// <summary>
	/// AddMaterialで追加したポストエフェクトを削除します
	/// </summary>
	/// <param name="material"></param>
	public void RemoveMaterial(Material material)
	{
		_effectMaterials.Remove(material);
	}

	public void SetFloat(string name, float value)
	{
		_effectMaterial.SetFloat(name, value);
	}

	public void SetFloat(int id, float value)
	{
		_effectMaterial.SetFloat(id, value);
	}

	public void SetInt(string name, int value)
	{
		_effectMaterial.SetInt(name, value);
	}

	public void SetInt(int id, int value)
	{
		_effectMaterial.SetFloat(id, value);
	}

	public void SetTexture(string name, Texture value)
	{
		_effectMaterial.SetTexture(name, value);
	}

	public void SetTexture(int id, Texture value)
	{
		_effectMaterial.SetTexture(id, value);
	}

	public void SetColor(string name, Color value)
	{
		_effectMaterial.SetColor(name, value);
	}

	public void SetColor(int id, Color value)
	{
		_effectMaterial.SetColor(id, value);
	}

	public void SetEnable(bool enable)
	{
		enabled = enable;
	}
}