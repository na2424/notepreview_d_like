﻿using UnityEngine;
using UnityEngine.Pool;

public sealed class BeatBarController : MonoBehaviour
{
	[SerializeField] MeshRenderer _meshRenderer;

	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<BeatBarController> _pool;
	Transform _transform;
	double _beatPosition = -1f;
	bool _isReleased = false;
	double _justTime = -1d;
	float _normalizedSpeed = 0f;
	double _hitTime = 1d;
	bool _isCMod = false;
	float _visualOffset = 0f;

	AnimationCurve _animationCurve;

	//------------------
	// 定数.
	//------------------
	const float BEAT_DISTANCE = Constant.Note.BEAT_DISTANCE;
	const float TIME_DISTANCE = Constant.Note.TIME_DISTANCE;
	const float OFFSET_Z = Constant.Note.OFFSET_Z;

	public void Init(ObjectPool<BeatBarController> pool)
	{
		_transform = transform;
		_pool = pool;
		_normalizedSpeed = Constant.Note.FIXED_BPM / GameManager.Instance.SelectSongInfo.BaseBpm;
		_hitTime = GameManager.Instance.JudgeTimeOption.GetHitTime();
		_isCMod = GameManager.Instance.IsCMod;
		_visualOffset = GameManager.Instance.DisplayOption.VisualOffset;
		_animationCurve = GameManager.Instance.NoteScrollAnimationCurve;
	}

	public void SetParam(float beatPosition, double justTime)
	{
		_isReleased = false;
		_beatPosition = beatPosition;
		_justTime = justTime;
	}

	public void CallUpdate(double beat, double musicTime, float speedStretchRatio)
	{
		if (_isReleased || !_meshRenderer.enabled)
		{
			return;
		}

		if (_isCMod)
		{
			UpdateCmodPosition(musicTime, speedStretchRatio);
		}
		else
		{
			UpdatePosition(beat, speedStretchRatio);
		}

		CheckRelease(musicTime);
	}

	void UpdateCmodPosition(double musicTime, float speedStretchRatio)
	{
		double diffTime = _justTime - musicTime;
		float posZ = (float)(diffTime * TIME_DISTANCE * GameParam.Instance.NoteSpeed * speedStretchRatio - (_visualOffset + OFFSET_Z + GameParam.Instance.OffsetZ));

		if (GameParam.Instance.NotesScrollType == NotesScrollType.Decelerate)
		{
			posZ = _animationCurve.Evaluate(posZ);
		}

		if (float.IsNaN(posZ))
		{
			posZ = 100f;
		}

		_transform.SetLocalPositionAndRotation(new Vector3(
			0f,
			0f,
			posZ
		), Quaternion.Euler(
			90f - (_transform.localPosition.z * 0.75f),
			0f,
			0f
		));
	}

	void UpdatePosition(double beat, float speedStretchRatio)
	{
		double diffPosition = _beatPosition - beat;
		float posZ = (float)(diffPosition * BEAT_DISTANCE * _normalizedSpeed * GameParam.Instance.NoteSpeed * speedStretchRatio - (_visualOffset + OFFSET_Z + GameParam.Instance.OffsetZ));

		if (GameParam.Instance.NotesScrollType == NotesScrollType.Decelerate)
		{
			posZ = _animationCurve.Evaluate(posZ);
		}

		if (float.IsNaN(posZ))
		{
			posZ = 100f;
		}

		_transform.SetLocalPositionAndRotation(new Vector3(
			0f,
			0f,
			posZ
		), Quaternion.Euler(
			90f - (_transform.localPosition.z * 0.75f),
			0f,
			0f
		));
	}

	void CheckRelease(double musicTime)
	{
		if (_justTime + _hitTime < musicTime)
		{
			Release();
		}
	}

	public void Release()
	{
		if (_isReleased)
		{
			return;
		}

		_isReleased = true;
		_pool.Release(this);
	}

	public void Show()
	{
		_meshRenderer.enabled = true;
	}

	public void Hide()
	{
		_meshRenderer.enabled = false;
	}

}
