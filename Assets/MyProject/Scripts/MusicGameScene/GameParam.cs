﻿
public sealed class GameParam
{
	public float NoteSpeed = 1f;
	public float NoteSize = 1f;
	public NotesScrollType NotesScrollType = NotesScrollType.Decelerate;
	public float OffsetZ = 0f;
	public float VisibleRate = 1f;

	//------------------
	// プロパティ.
	//------------------
	public static GameParam Instance { get; private set; }

	public static float VisibleBeat => ((Constant.Note.LANE_LENGTH + Constant.Note.OFFSET_Z + Instance.OffsetZ + GameManager.Instance.DisplayOption.VisualOffset) /
		(Constant.Note.BEAT_DISTANCE * (Constant.Note.FIXED_BPM / GameManager.Instance.SelectSongInfo.BaseBpm) * SpeedStretchRatio.CurrentValue * Instance.NoteSpeed)) * Instance.VisibleRate;
	public static double VisibleTime => ((Constant.Note.LANE_LENGTH + Constant.Note.OFFSET_Z + Instance.OffsetZ + GameManager.Instance.DisplayOption.VisualOffset) /
		(Constant.Note.TIME_DISTANCE * SpeedStretchRatio.CurrentValue * Instance.NoteSpeed)) * Instance.VisibleRate;

	/// <summary>
	/// 初期化
	/// </summary>
	public void Init()
	{
		Instance = this;

		NoteSpeed = 1f;
		NoteSize = 1f;
		NotesScrollType = NotesScrollType.Decelerate;
		OffsetZ = 0f;
		VisibleRate = 1f;
	}
}