﻿using System.Collections.Generic;

public sealed class ScrollHelper
{
	List<float> _scrollPositions;
	List<float> _scrolls;

	int _scrollPositionsCount;
	double _cacheBeat = -100d;
	double _cacheScrolledBeat = -100d;

	int _scrollIndex = 1;
	double _calculated = 0;

	public void Init(SongInfo songInfo)
	{
		_scrollPositions = new List<float>(songInfo.ScrollPositions)
		{
			float.MaxValue
		};
		_scrollPositionsCount = _scrollPositions.Count;

		_scrolls = songInfo.Scrolls;
	}

	public double ApplyScrollCalculated(double beat)
	{
		if (beat == _cacheBeat)
		{
			return _cacheScrolledBeat;
		}

		if (_scrollPositionsCount <= 1)
		{
			return beat;
		}

		bool isOverFirstScroll = beat > _scrollPositions[0];

		double scrolledBeat =
			_scrollIndex > 1 ? _calculated
			: isOverFirstScroll ? _scrollPositions[0]
			: beat;

		if (isOverFirstScroll)
		{
			for (int i = _scrollIndex; i < _scrollPositionsCount; i++)
			{
				if (beat - _scrollPositions[i] < 0)
				{
					scrolledBeat += (beat - _scrollPositions[i - 1]) * (double)_scrolls[i - 1];
					break;
				}
				else
				{
					scrolledBeat += (_scrollPositions[i] - _scrollPositions[i - 1]) * (double)_scrolls[i - 1];
					_calculated = scrolledBeat;
					_scrollIndex = i + 1;
				}
			}
		}

		_cacheBeat = beat;
		_cacheScrolledBeat = scrolledBeat;

		return scrolledBeat;
	}

	public double ApplyScroll(double beat)
	{
		if (beat == _cacheBeat)
		{
			return _cacheScrolledBeat;
		}

		if (_scrollPositionsCount <= 1)
		{
			return beat;
		}

		bool isOverFirstScroll = beat > _scrollPositions[0];

		double scrolledBeat = isOverFirstScroll
			? _scrollPositions[0]
			: beat;

		if (isOverFirstScroll)
		{
			for (int i = 1; i < _scrollPositionsCount; i++)
			{
				if (beat - _scrollPositions[i] < 0)
				{
					scrolledBeat += (beat - _scrollPositions[i - 1]) * (double)_scrolls[i - 1];
					break;
				}
				else
				{
					scrolledBeat += (_scrollPositions[i] - _scrollPositions[i - 1]) * (double)_scrolls[i - 1];
				}
			}
		}

		_cacheBeat = beat;
		_cacheScrolledBeat = scrolledBeat;

		return scrolledBeat;
	}

	public void OnFinalize()
	{
		_scrollPositions = null;
		_scrolls = null;
	}
}
