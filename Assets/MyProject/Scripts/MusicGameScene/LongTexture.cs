﻿using UnityEngine;

[CreateAssetMenu(menuName = "MyScriptable/LongTexture")]
public sealed class LongTexture : ScriptableObject
{
	[SerializeField] Texture _longeTex;
	[SerializeField] Texture _fuzzyLongTex;

	/// <summary>
	/// LongTypeから指定のTextureを返す.
	/// </summary>
	/// <param name="type">LongType</param>
	/// <returns>Texture</returns>
	public Texture LongTypeToTexture(LongType type) => type switch
	{
		LongType.Long => _longeTex,
		LongType.FuzzyLong => _fuzzyLongTex,
		_ => _longeTex
	};

	public void SetTextureFromNoteSkinInfo(NoteSkinInfo info)
	{
		_longeTex = info.LineSlideTexture;
		_fuzzyLongTex = info.LineFuzzyTexture;
	}
}