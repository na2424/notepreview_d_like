﻿public enum LongEndType
{
	Normal = 0,
	NonUp = 1,
	NonUpAndSound = 2,
}