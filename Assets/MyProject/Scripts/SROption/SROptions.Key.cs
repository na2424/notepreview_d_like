﻿using SRDebugger;
using SRDebugger.Services.Implementation;
using SRDebugger.UI.Other;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public partial class SROptions
{
	//// プロパティ
	//private float _opacity = 0.9f;

	//[Sort(3)]
	//[NumberRange(0, 1)]
	//[Increment(0.1)]
	//[Category("Background Opacity")]
	//[DisplayName("Opacity")]
	//public float Opacity
	//{
	//	get => _opacity;
	//	set
	//	{
	//		_opacity = value;
	//		DebugPanelBackgroundBehaviour.Instance.SetTransparent(_opacity);
	//	}
	//}

	// プロパティ
	private bool _enableNumberKey = true;

	[Sort(2)]
	[Category("Close/Open Shortcut Key  (F1-F4 keys are always available)")]
	[DisplayName("Enable NumberKey")]
	public bool EnableNumberKey
	{
		get => _enableNumberKey;
		set
		{
			_enableNumberKey = value;
			KeyboardShortcutListenerService.UseNumberKey = value;
		}
	}

	[Sort(1)]
	[Category("Open WebURL")] // Options will be grouped by category
	[DisplayName("LuaAPI")]
	public void LuaAPI()
	{
		Application.OpenURL("https://dankag-like.web.app/luaapi/");
	}
}