﻿using System.Collections.Generic;
using XLua;

[LuaCallCSharp]
public enum LongType : int
{
	Long = 0,
	FuzzyLong = 1
}

[LuaCallCSharp]
public class LongInfo
{
	public int StartNoteIndex;
	public List<int> NoteIndex;
	public List<int> Lanes;
	public List<double> BeatPositions;
	public LongType LongType;
	public double EndTime;
}