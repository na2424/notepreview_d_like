﻿using System.Collections.Generic;
using XLua;

[LuaCallCSharp]
public enum NoteType : int
{
	None = 0,
	Normal = 1,
	LongStart = 2,
	LongRelay = 3,
	LongEnd = 4,
	Fuzzy = 5,
	FuzzyLongStart = 6,
	FuzzyLongRelay = 7,
	FuzzyLongEnd = 8,
}

[System.Flags]
public enum NoteBitFrag : int
{
	None = 0,
	Normal = 1 << 1,
	LongStart = 1 << 2,
	LongRelay = 1 << 3,
	LongEnd = 1 << 4,
	Fuzzy = 1 << 5,
	FuzzyLongStart = 1 << 6,
	FuzzyLongRelay = 1 << 7,
	FuzzyLongEnd = 1 << 8,

	Tap = Normal | LongStart | FuzzyLongStart | Fuzzy,
	Hold = LongRelay | Fuzzy | FuzzyLongStart | FuzzyLongRelay | FuzzyLongEnd,
	Up = LongEnd | Fuzzy | FuzzyLongEnd,

	PLHold = LongRelay | LongEnd | Fuzzy | FuzzyLongStart | FuzzyLongRelay | FuzzyLongEnd,
	PLUp = LongEnd | Fuzzy | FuzzyLongEnd,
}


public class Sequence
{
	public List<float> BeatPositions;
	public List<int> Lanes;
	public List<NoteType> NoteTypes;
	public List<byte> Connects;
	public List<bool> IsAttacks;
	public List<LongInfo> LongInfo;
	public string Credit;


	public bool HasChart =>
		BeatPositions != null && Lanes != null &&
		BeatPositions.Count > 0 && Lanes.Count > 0;

	public Sequence()
	{
		BeatPositions = new List<float>();
		Lanes = new List<int>();
		NoteTypes = new List<NoteType>();
		Connects = new List<byte>();
		IsAttacks = new List<bool>();
		LongInfo = new List<LongInfo>();
		Credit = null;
	}

	public void Clear()
	{
		BeatPositions.Clear();
		Lanes.Clear();
		NoteTypes.Clear();
		Connects.Clear();
		IsAttacks.Clear();
		LongInfo.Clear();

		BeatPositions = null;
		Lanes = null;
		NoteTypes = null;
		Connects = null;
		IsAttacks = null;
		LongInfo = null;
	}
}
