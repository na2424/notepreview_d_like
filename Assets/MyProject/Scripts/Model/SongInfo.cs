﻿using Cysharp.Text;
using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 楽曲データ
/// (譜面情報を含まない)
/// </summary>
[Serializable]
public class SongInfo
{
	public byte Version;
	public string Title;
	public string SubTitle;
	public string Artist;
	public string ChartArtist;
	public string Illust;
	public List<string> Description = new List<string>();
	public List<int> Difficulty = new List<int>();
	public float Offset;
	public float BaseBpm;
	public List<float> BpmPositions = new List<float>();
	public List<float> Bpms = new List<float>();
	public List<float> SpeedPositions = new List<float>();
	public List<float> SpeedStretchRatios = new List<float>();
	public List<float> SpeedDelayBeats = new List<float>();
	public List<float> ScrollPositions = new List<float>();
	public List<float> Scrolls = new List<float>();
	public List<float> BgChangePositions = new List<float>();
	public List<string> BgChangeImageName = new List<string>();
	public bool IsCMod;
	public float SampleStart;
	public float SampleLength;
	public string DirectoryPath;
	public float LastSecondHint;
	public string MusicFileName;
	public string JacketFileName;
	public string BgFileName;
	public string LuaScript;
	public long LastWriteTime;
	public short GroupFolderIndex;
	public Texture2D JacketTexture;


	public SongInfo CopySongInfo()
	{
		SongInfo songInfo = this;

		return new SongInfo()
		{
			Version = songInfo.Version,
			Title = songInfo.Title,
			SubTitle = songInfo.SubTitle,
			Artist = songInfo.Artist,
			ChartArtist = songInfo.ChartArtist,
			Illust = songInfo.Illust,
			Description = songInfo.Description,
			Difficulty = songInfo.Difficulty,
			Offset = songInfo.Offset,
			BaseBpm = songInfo.BaseBpm,
			BpmPositions = songInfo.BpmPositions.DeepCopy(),
			Bpms = songInfo.Bpms.DeepCopy(),
			SpeedPositions = songInfo.SpeedPositions.DeepCopy(),
			SpeedStretchRatios = songInfo.SpeedStretchRatios.DeepCopy(),
			SpeedDelayBeats = songInfo.SpeedDelayBeats.DeepCopy(),
			ScrollPositions = songInfo.ScrollPositions.DeepCopy(),
			Scrolls = songInfo.Scrolls.DeepCopy(),
			BgChangePositions = songInfo.BgChangePositions.DeepCopy(),
			BgChangeImageName = songInfo.BgChangeImageName.DeepCopy(),
			IsCMod = songInfo.IsCMod,
			LuaScript = songInfo.LuaScript,
			SampleStart = songInfo.SampleStart,
			SampleLength = songInfo.SampleLength,
			LastSecondHint = songInfo.LastSecondHint,
			MusicFileName = songInfo.MusicFileName,
			JacketFileName = songInfo.JacketFileName,
			BgFileName = songInfo.BgFileName,
			LastWriteTime = songInfo.LastWriteTime,
			GroupFolderIndex = songInfo.GroupFolderIndex,
			JacketTexture = songInfo.JacketTexture
		};
	}

}