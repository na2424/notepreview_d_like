﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ViewBase : MonoBehaviour, ICallUpdate
{
    public abstract void Init();
    public abstract void CallUpdate();
}
