﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LifetimeScope : MonoBehaviour
{
	protected PresenterBase _presenter;
	protected virtual void Start()
	{
		_presenter.Start();
	}

	protected virtual void Update()
	{
		_presenter.Tick();
	}

	protected virtual void OnDestroy()
	{
		_presenter.Dispose();
		_presenter = null;
	}
}
