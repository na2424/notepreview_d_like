﻿
using System;

/// <summary>
/// MVPパターンの(P)resenterに当たり、View(描画UI)とModel(データ)を繋ぐ役目となる.
/// ライフサイクルコールバックを持つ.
///
/// [ライフサイクル タイミング]
/// 
/// ・Start (開始時)
/// LifetimeScopeからStart時に呼ばれます
/// ・Tick (更新時)
/// LifetimeScopeからUpdate毎に呼ばれます
/// ・Dispose (破棄時)
/// LifetimeScopeからOnDestroy時に呼ばれます
/// 
/// </summary>
/// <typeparam name="V">ViewBaseを継承したPresenterに紐づけるView</typeparam>
/// <typeparam name="M">ModelBaseを継承したPresenterに紐づけるModel</typeparam>
public abstract class PresenterBase<V, M> : PresenterBase
		where V : ViewBase
		where M : new() 
{
	readonly protected V _view;
	readonly protected M _model;

	protected PresenterBase(V view)
	{
		// (V)iew
		_view = (V)view;
		// (M)odel
		_model = new M();
	}
}

public abstract class PresenterBase : IStartable, ITickable, IDisposable
{
	public abstract void Start();
	public abstract void Tick();
	public abstract void Dispose();
} 