﻿using UnityEngine;

public static class AnimationCurveExtensions
{
	/// <summary>
	/// AnimationCurveの逆変換AnimationCurveを生成する
	/// </summary>
	/// <param name="curve">AnimationCurve</param>
	/// <returns>逆変換AnimationCurve</returns>
	public static AnimationCurve CreateInverseCurve(this AnimationCurve curve)
	{
		var resultCurve = new AnimationCurve();

		var keys = curve.keys;
		int keyCount = keys.Length;
		var postWrapmode = curve.postWrapMode;
		resultCurve.postWrapMode = curve.preWrapMode;
		resultCurve.preWrapMode = postWrapmode;

		for (int i = 0; i < keyCount; i++)
		{
			Keyframe K = keys[i];
			var temp = K.time;
			K.time = K.value;
			K.value = temp;
			K.inTangent = 1f / K.inTangent;
			K.outTangent = 1f / K.outTangent;
			keys[i] = K;
		}
		resultCurve.keys = keys;

		return resultCurve;
	}

}
