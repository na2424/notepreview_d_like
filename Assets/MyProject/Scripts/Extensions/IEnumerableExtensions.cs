﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using Random = UnityEngine.Random;

namespace UBL
{
	public static class IEnumerableExtensions
	{
		/// <summary>
		/// Randoms the value.
		/// </summary>
		/// <returns>The value.</returns>
		/// <param name="target">Target.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T RandomValue<T>(this IEnumerable<T> target) where T : class
		{
			if (target == null)
			{
				return null;
			}

			int len = target.Count();
			int index = Random.Range(0, len);
			return target.Where((x, i) => i == index).FirstOrDefault();
		}

		/// <summary>
		/// Determines if is null or empty the specified target.
		/// </summary>
		/// <returns><c>true</c> if is null or empty the specified target; otherwise, <c>false</c>.</returns>
		/// <param name="target">Target.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static bool IsNullOrEmpty<T>(this IEnumerable<T> target)
		{
			return target == null ||
				   target is ICollection<T> ? ICollectionExtentions.IsNullOrEmpty((ICollection<T>) target) :
				   !target.Any();
		}

		/// <summary>
		/// Is Not Null Of Empty
		/// </summary>
		/// <param name="target"></param>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public static bool IsNotNullOrEmpty<T>(this IEnumerable<T> target)
		{
			return target.IsNullOrEmpty() == false;
		}

		/// <summary>
		/// This method checks Count property for ICollection<TSource> type instead of creating enumerator.
		/// (このメソッドは、列挙子を作成する代わりに、ICollection <TSource>タイプのCountプロパティをチェックします。)
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <param name="source"></param>
		/// <returns></returns>
		public static bool CustomAny<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null)
			{
				throw new ArgumentNullException(nameof(source));
			}

			if (source is TSource[] array)
			{
				return array.Length != 0;
			}

			if (source is ICollection<TSource> collection)
			{
				return collection.Count != 0;
			}

			if (source is ICollection baseCollection)
			{
				return baseCollection.Count != 0;
			}

			using (var enumerator = source.GetEnumerator())
			{
				if (enumerator.MoveNext())
				{
					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// シーケンスのダンプメソッド
		/// </summary>
		public static string Dump<TSource>(this IEnumerable<TSource> source)
		{
			return "[" + string.Join(", ", source.Select(e => e.ToString()).ToArray()) + "]";
		}

		public static void ForeachWithIndex<T>(this IEnumerable<T> self, Action<T, int> action)
		{
			int counter = 0;

			foreach (var item in self)
			{
				action(item, counter++);
			}
		}

		public static void ForeachWithIndex<T>(this IEnumerable<T> self, Action<(T, int)> action)
		{
			int counter = 0;

			foreach (var item in self)
			{
				action((item, counter++));
			}
		}

		// 指定サイズでのチャンクに分割する拡張メソッド
		public static IEnumerable<IEnumerable<T>> Chunks<T>(this IEnumerable<T> list, int size)
		{
			while (list.CustomAny())
			{
				yield return list.Take(size);
				list = list.Skip(size);
			}
		}
	}
}