﻿using UnityEngine.Pool;

public static class ObjectPoolExtension
{
    public static void WormUp<T>(this ObjectPool<T> pool, int count) where T : class
	{
        T[] targets = new T[count];

		for (int i = 0; i < count; i++)
		{
			targets[i] = pool.Get();
		}

		for (int i = 0; i < count; i++)
		{
            pool.Release(targets[i]);
			targets[i] = null;
		}
	}
}
