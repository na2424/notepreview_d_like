﻿using Cysharp.Text;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StringExtensions
{
	/// <summary>
	/// nullまたは空文字か
	/// </summary>
	/// <param name="target"></param>
	/// <returns></returns>
	public static bool IsNullOrEmpty(this string target)
	{
		return string.IsNullOrEmpty(target);
	}

	/// <summary>
	/// 文字があるか
	/// </summary>
	/// <param name="target"></param>
	/// <returns></returns>
	public static bool HasValue(this string target)
	{
		return target.IsNullOrEmpty() == false;
	}

	/// <summary>
	/// 2つの文字列の間の文字列を返すメソッド
	/// </summary>
	/// <param name="orgStr">原文</param>
	/// <param name="c1">1つ目の文字</param>
	/// <param name="c2">2つ目の文字</param>
	/// <returns></returns>
	public static string GetBetweenChars(this string orgStr, char c1, char c2)
	{
		string s;

		//例外処理
		try
		{
			int str1Num = orgStr.IndexOf(c1); //str1が原文のどの位置にあるか
			int str2Num = orgStr.IndexOf(c2); //str2がsのどの位置にあるか
			s = orgStr.Substring(str1Num + 1, str2Num - str1Num - 1); //返す文字列
		}
		catch
		{
			throw new Exception(ZString.Concat("この文字列が読み込めませんでした\r\n", orgStr, "\r\n"));
		}

		return s;
	}

	/// <summary>
	/// 特定の文字列で始まるか
	/// String.StartsWithの速度改善をしている
	/// </summary>
	public static bool CustomStartsWith(this string a, string b)
	{
		if (a.IsNullOrEmpty())
		{
			return false;
		}

		int aLen = a.Length;
		int bLen = b.Length;
		int ap = 0;
		int bp = 0;

		while (ap < aLen && bp < bLen && a[ap] == b[bp])
		{
			ap++;
			bp++;
		}

		return (bp == bLen && aLen >= bLen) || (ap == aLen && bLen >= aLen);
	}

	/// <summary>
	/// 特定の文字列で終わるか
	/// String.EndsWithの速度改善をしている
	/// </summary>
	public static bool CustomEndsWith(this string a, string b)
	{
		int ap = a.Length - 1;
		int bp = b.Length - 1;

		while (ap >= 0 && bp >= 0 && a[ap] == b[bp])
		{
			ap--;
			bp--;
		}

		return (bp < 0 && a.Length >= b.Length) || (ap < 0 && b.Length >= a.Length);
	}

	public static string GetFolderNameFromPath(this string path)
	{
		string directoryPath = path.Replace("\\", "/");
		int index = directoryPath.LastIndexOf("/") + 1;
		return directoryPath.Substring(index, directoryPath.Length - index);
	}
}
